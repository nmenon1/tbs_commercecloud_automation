/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_master;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.github.mkolisnyk.cucumber.reporting.types.beans.DetailedReportingDataBean;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;

import cucumber.api.Scenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import net.bytebuddy.implementation.bytecode.Throw;


/*********
 *Function:tbs_SeleniumDriver
 *Purpose: To handle driver
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class DriverFactory {

	private static DriverFactory tbs_SeleniumDriver;
	private static RemoteWebDriver driver;
	private static WebDriverWait waitDriver;
	private static AndroidDriver<MobileElement> Mobdriver;
	public final static int TIMEOUT = 100;
	public final static int PAGE_LOAD_TIMEOUT = 120;
	public static ExtentReports extent;
	public static ExtentTest Testlogger;
	private HashMap<String, Object> storageMap = new HashMap<String, Object>();

	public DriverFactory()  {

		String browserType= ProjectProperties.browserName;
		//tbs_ConfigFileUtils.readConfigFile(, "test.properties");
		String serverHost=ProjectProperties.serverHost;
		System.out.println("Server "+serverHost);
		if (browserType.equalsIgnoreCase("chrome")) {
			ChromeOptions chromeOptions = new ChromeOptions();
			//chromeOptions.setCapability("marionette", true);
			chromeOptions.addArguments("start-maximized");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
			try {
				driver = new RemoteWebDriver(new URL("http://"+serverHost+":4444/wd/hub"),capabilities);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.manage().window().maximize();
			waitDriver = new WebDriverWait(driver, TIMEOUT);
			driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			String window=driver.getWindowHandle();
		}


		else if (browserType.equalsIgnoreCase("ff")) {

			FirefoxOptions opt = new FirefoxOptions();
			opt.setCapability("marionatte", false);
			try {
				driver = new RemoteWebDriver(new URL("http://"+ProjectProperties.serverHost+":4444/wd/hub"),opt);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.manage().window().maximize();
			waitDriver = new WebDriverWait(driver, TIMEOUT);
			driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			String window=driver.getWindowHandle();
			Testlogger.log(LogStatus.PASS, "Driver initialization is passed for browser:"+browserType);
		}


		else if (browserType.equalsIgnoreCase("edge")) {

			EdgeOptions opt = new EdgeOptions();
			opt.setPageLoadStrategy("eager");
			try {
				driver = new RemoteWebDriver(new URL("http://"+ProjectProperties.serverHost+":4444/wd/hub"),opt);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			driver.manage().window().maximize();
			waitDriver = new WebDriverWait(driver, TIMEOUT);
			driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
			String window=driver.getWindowHandle();
			Testlogger.log(LogStatus.PASS, "Driver initialization is passed for browser:"+browserType);
		}


		else if (browserType.equalsIgnoreCase("mobile-android")) {	

			DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("deviceName", "2138149a28057ece"); 
			caps.setCapability(CapabilityType.PLATFORM_NAME, "Android");
			caps.setCapability(CapabilityType.VERSION, "8.0");
			caps.setCapability("browserName", "chrome");
			try {
				driver = new RemoteWebDriver(new URL("http://"+ProjectProperties.serverHost+":4444/wd/hub"),caps);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Testlogger.log(LogStatus.PASS, "Driver initialization is passed for browser:"+browserType);
		}

	}


	public static void openPage(String url) {
		try {
			driver.get(url);
			Testlogger.log(LogStatus.PASS, "Load the URL :"+url);
		}
		catch (Exception e) {
			Testlogger.log(LogStatus.FAIL, "The URL isn't loaded:"+url);
		}
	}

	public static WebDriver getCurrentDriver() {
		return driver;
	}

	public static void setUpDriver(Scenario scenario) throws MalformedURLException {
		if (tbs_SeleniumDriver == null) {
			tbs_SeleniumDriver = new DriverFactory();
			String featureName = scenario.getId().split(";")[0];
			SeleniumUtils.storeKeyValue(KeyStoreConstants.featureName, featureName);
			String scenarioName = scenario.getName();
			SeleniumUtils.storeKeyValue(KeyStoreConstants.scenarioName, scenarioName);
			String detailReportFolder=ProjectProperties.cucumberDetailedReportPath+featureName+"\\"+scenarioName;
			System.out.println("Path : "+detailReportFolder);
			//tbs_ConfigFileUtils.readConfigFile("tbs.detailedReport", "test.properties")+featureName;
			File fileDir = new File(detailReportFolder);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}
			else {
				String[]entries = fileDir.list();
				for(String s: entries){
				    File currentFile = new File(fileDir.getPath(),s);
				    currentFile.delete();
				}
				fileDir.delete();
				fileDir.mkdirs();
			}
			String testMarket=System.getProperty("marketToBeExecuted");
			extent = new ExtentReports (detailReportFolder+"\\"+scenarioName+".html", true);
			extent.addSystemInfo("Automation Framework Author", "mibin.b@infosys.com");
			extent.addSystemInfo("Market", testMarket);
			extent.loadConfig(new File(".\\src\\test\\resources\\extent-config.xml"));
			Testlogger = extent.startTest(scenarioName);

		}
	}
	

	/**
	 * @author Edwin_John
	 * Thread local for getting thread safe driver object
	 */
	public static final ThreadLocal<DriverFactory> driverThread = new ThreadLocal<DriverFactory>() {
		@Override
		protected DriverFactory initialValue(){
			return new DriverFactory();
		}
	};

	/**
	 * @author Edwin_John
	 * Return the current threadlocal instance
	 * @return
	 */
	public static DriverFactory getThread() {
		return driverThread.get();
	}

	/**
	 * Get current webdriver instance for the thread
	 * 
	 * @return
	 */
	public static WebDriver getDriverInstance() {
		return getThread().getCurrentDriver();
	}

	public HashMap<String, Object> getStoredKeys() {
		return storageMap;
	}

	public static void tearDown() {
		extent.endTest(Testlogger);
		extent.flush();
		String browser = ProjectProperties.browserName;
		if (driver != null) {
			driver.close();
			if (browser.equalsIgnoreCase("chrome")) {
				driver.quit();
			}
			extent.close();
		}
		tbs_SeleniumDriver = null;

	}

	public static void pageRefresh()
	{

		driver.navigate().refresh();

	}
	
}
