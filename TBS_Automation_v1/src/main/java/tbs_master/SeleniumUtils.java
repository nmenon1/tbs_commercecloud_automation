/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_master;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.FileSystems;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.tools.ant.attribute.IfBlankAttribute;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;
import com.tbs.rcom.pageLocators.LoginPageLocators;

import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;


/*********
 *Function:tbs_SeleniumHelper
 *Purpose: Repository of selenium functions
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class SeleniumUtils {

    static HashMap<String, Object> hashMap = new HashMap<String, Object>();

    /**
     * To check if there is no such element in the webpage
     *
     * @param elementDetails
     * @return
     * @author Edwin_John
     */
    public static Boolean isElementNotPresent(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        Boolean isPresent = driver.findElements(ElementFormatter.getByObject(elementDetails)).size() == 0;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return isPresent;
    }

    public static boolean isElementPresent(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        Boolean isPresent = driver.findElements(ElementFormatter.getByObject(elementDetails)).size() > 0;
        return isPresent;

    }

    public static float round(float num) {
        return BigDecimal.valueOf(num).setScale(2, BigDecimal.ROUND_HALF_UP).floatValue();

    }

    public static SeleniumUtils elementHighlight(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        WebElement element = SeleniumUtils.findElement(driver, elementDetails);
        for (int i = 0; i < 3; i++) {
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    element, "color: red; border: 3px solid red;");
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    element, "");
        }

        return null;
    }

    public SeleniumUtils elementXpathHighlight(String xpath) {
        for (int i = 0; i < 3; i++) {
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    xpath, "color: red; border: 3px solid red;");
            js.executeScript(
                    "arguments[0].setAttribute('style', arguments[1]);",
                    xpath, "");
        }
        return this;

    }

    public static SeleniumUtils jsdoubleclick(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            new Actions(driver).moveToElement(element).doubleClick().perform();
        } catch (Exception e) {
            SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "The Element " + elementDetails.getelementName() + "wasn't clickable");
            throw new RuntimeException("The Element " + elementDetails.getelementName() + " with xpath as "
                    + elementDetails.getelementValue() + "wasn't clickable", e);
        }
        return null;
    }

    public static SeleniumUtils click(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();

        try {
            WebElement element = findElement(driver, elementDetails);
            waitForElementToBeClickable(elementDetails);
            //SeleniumUtils.scrollToElement(elementDetails);
            Actions action = new Actions(driver);
            action.moveToElement(element);
            action.click(element);
            action.build().perform();
            //element.click();
        } catch (Exception e) {
            SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "The Element " + elementDetails.getelementName() + "wasn't clickable");
            throw new RuntimeException("The Element " + elementDetails.getelementName() + " with xpath as "
                    + elementDetails.getelementValue() + "wasn't clickable", e);

        }
        return null;
    }

    //test func to click
    public static SeleniumUtils xpathClick(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();

        try {
            WebElement element = findElement(driver, elementDetails);
            waitForElementToBeClickable(elementDetails);
            //SeleniumUtils.scrollToElement(elementDetails);
            element.click();
        } catch (Exception e) {
            SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "The Element " + elementDetails.getelementName() + "wasn't clickable");
            throw new RuntimeException("The Element " + elementDetails.getelementName() + " with xpath as "
                    + elementDetails.getelementValue() + "wasn't clickable", e);

        }
        return null;
    }

    public SeleniumUtils ClickXpath(String xpath) {
        try {
            WebElement elem = DriverFactory.getCurrentDriver().findElement(By.xpath(xpath));
            final ElementFormatter elementDetails = new ElementFormatter("Element", xpath, ElementFormatter.XPATH);
            waitForElementToBeClickable(elementDetails);
            WaitForElementToBeVisible(elementDetails);
            elem.click();
        } catch (Exception e) {
            throw new RuntimeException("Not able to click in the xpath:" + xpath, e);
        }
        return null;
    }

    public static void hoverOverElement(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The Element isn't visible in the page", element.isDisplayed());
            Actions action = new Actions(DriverFactory.getCurrentDriver());
            action.moveToElement(element).build().perform();
        } catch (Exception e) {
            throw new RuntimeException("Error occured while hovering on " + elementDetails.getelementName().toString(), e);
        }
    }

    public SeleniumUtils HoverOnElementXpath(String xpath) {

        try {
            WebElement elem = DriverFactory.getCurrentDriver().findElement(By.xpath(xpath));
            //WaitForElementToBeClickable(elem);
            Actions action = new Actions(DriverFactory.getCurrentDriver());
            action.moveToElement(elem).build().perform();
        } catch (Exception e) {
            throw new RuntimeException("Error occured while hovering on element with xpath:" + xpath, e);
        }
        return this;
    }

    public String getDefaultDropdownSelectedValue(WebElement elem) {

        String value = "";
        try {
            Assert.assertTrue(elem.isDisplayed());
            Select select = new Select(elem);
            value = select.getFirstSelectedOption().getText();
        } catch (Exception e) {
        }
        return value;
    }

    public static SeleniumUtils Select(ElementFormatter elementDetails, String selectOption) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            Select select = new Select(element);
            List<WebElement> test = select.getOptions();
            int optionIndex = 0;
            for (int x = 0; x <= test.size() - 1; x++) {
                if (test.get(x).getText().equalsIgnoreCase(selectOption)) {
                    optionIndex = x;
                    break;
                }
            }
            select.selectByIndex(optionIndex);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Not able to select the option " + selectOption, e);
        }
        return null;
    }

    /**
     * Select by Visible text from dropdown
     *
     * @param elementdetails
     * @param visibleText
     * @return
     */
    public static SeleniumUtils selectFromDropdownByText(ElementFormatter elementdetails, String visibleText) {
        WebElement element = DriverFactory.getCurrentDriver().findElement(By.xpath(elementdetails.getelementValue()));
        Select select = new Select(element);
        select.selectByVisibleText(visibleText);
        return null;
    }

    /**
     * Select by Visible text from dropdown
     *
     * @param elementdetails
     * @param visibleText
     * @return
     */
    public static SeleniumUtils selectFromDropdownByIndex(ElementFormatter elementdetails, int index) {
        WebElement element = DriverFactory.getCurrentDriver().findElement(By.xpath(elementdetails.getelementValue()));
        Select select = new Select(element);
        select.selectByIndex(index);
        return null;
    }

    public static SeleniumUtils type(ElementFormatter elementDetails, String keyValue) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            WaitForElementToBeVisible(elementDetails);
            if (keyValue.length() == 0) {
                element.clear();
            } else {

                element.click();
                element.clear();
                element.sendKeys(keyValue);
            }
        } catch (Exception e) {
            throw new RuntimeException("Not able to type the value " + keyValue + " in the textbox for the element " + elementDetails.getelementName(), e);
        }
        return null;
    }

    public SeleniumUtils SendKeys(ElementFormatter elementDetails, Keys keyValue) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            WaitForElementToBeVisible(elementDetails);
            element.sendKeys(keyValue);
        } catch (Exception e) {
            throw new RuntimeException("Not able to perform the keyboard action " + keyValue.toString(), e);
        }
        return this;
    }

    public static String GetText(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        String textValue = "";
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            textValue = element.getText();
        } catch (Exception e) {
            throw new RuntimeException("Not able to get the text from " + elementDetails.getelementName().toString(), e);
        }
        return textValue;
    }

    public static String GetTextofXpath(String xpath) {

        String textValue = "";
        try {
            WebElement elem = DriverFactory.getCurrentDriver().findElement(By.xpath(xpath));
            Assert.assertTrue(elem.isDisplayed());
            textValue = elem.getText();
        } catch (Exception e) {
            throw new RuntimeException("Not able to get the text", e);
        }
        return textValue;
    }

    public static SeleniumUtils waitForElementToBeClickable(ElementFormatter elementDetails) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 20);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementDetails.getelementValue())));
        } catch (Exception e) {
            throw new RuntimeException(elementDetails.getelementName().toString() + " not clickable in the page with xpath-" + elementDetails.getelementValue() + ".", e);
        }
        return null;
    }

    public SeleniumUtils WaitForElementInvisiblity(WebElement elem) {
        try {
            WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 20);
            wait.until(ExpectedConditions.invisibilityOf(elem));
        } catch (Exception e) {
            throw new RuntimeException(elem.toString() + " still visible in the page.", e);
        }
        return this;
    }

    public static SeleniumUtils WaitForElementToBeVisible(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = driver.findElement(By.xpath(elementDetails.getelementValue()));
            WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), 20);
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            throw new RuntimeException(elementDetails.getelementName().toString() + " not visible in the page.", e);
        }
        return null;
    }

    public static SeleniumUtils WaitForElementXpathToBeVisible(String xpath) {
        try {
            WebElement elem = DriverFactory.getCurrentDriver().findElement(By.xpath(xpath));
            WebDriverWait wait = new WebDriverWait(DriverFactory.getCurrentDriver(), (int) 100);
            wait.until(ExpectedConditions.visibilityOf(elem));
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage() + "ppqqqqqqqqqqqrss");
            throw new RuntimeException(xpath + " not visible in the page.", e);
        }
        return null;
    }

    public static boolean IsElementVisible(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        Boolean isPresent = driver.findElements(ElementFormatter.getByObject(elementDetails)).size() > 0;
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return isPresent;

    }

    public static boolean IsElementClickable(ElementFormatter elementDetails) {
        Boolean isClickable = null;
        try {
            WebDriver driver = DriverFactory.getCurrentDriver();
            WebDriverWait wait = new WebDriverWait(driver, 5);
            WebElement element = wait.until(ExpectedConditions.elementToBeClickable(ElementFormatter.getByObject(elementDetails)));
            isClickable = true;
        } catch (Exception e) {
            isClickable = false;
            //driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        }
        return isClickable;
    }

    public static String GetAttribute(WebElement elem, String attribute) {
        String attrValue = "";
        try {
            Assert.assertTrue(elem.isDisplayed());
            attrValue = elem.getAttribute(attribute);
        } catch (Exception e) {
            throw new RuntimeException("Not able to get the attribute value from " + elem.toString(), e);
        }
        return attrValue;
    }

    public static String getAttributeValue(ElementFormatter elementDetails, String attribute) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        SeleniumUtils.scrollToElement(elementDetails);
        String atrributeValue = "";
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            atrributeValue = element.getAttribute(attribute);
        } catch (Exception e) {
            throw new RuntimeException("Not able to get the atrribute value from " + elementDetails.getelementName().toString(), e);
        }
        return atrributeValue;
    }

    public SeleniumUtils ExecuteJavascript(String script, WebElement elem) {
        try {
            JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
            js.executeScript(script, elem);
        } catch (Exception e) {
            throw new RuntimeException("Not able to execute script  " + script + " on element " + elem.toString(), e);
        }
        return this;
    }

    public Integer GetElementCount(WebElement elem, String xpath) {

        Integer countOfElements = 0;
        try {
            List<WebElement> elements = DriverFactory.getCurrentDriver().findElements(By.xpath(xpath));
            countOfElements = elements.size();
        } catch (Exception e) {
            throw new RuntimeException("Not able to find element " + elem.toString(), e);
        }
        return countOfElements;
    }


    public static void ScrollIntoView(ElementFormatter elementDetails, boolean viewOnTop) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement elem = SeleniumUtils.findElement(driver, elementDetails);
            Assert.assertTrue(elem.isDisplayed());
            ((JavascriptExecutor) DriverFactory.getCurrentDriver()).executeScript("arguments[0].scrollIntoView(" + viewOnTop + ");", elem);
        } catch (Exception e) {
            throw new RuntimeException("Not able to find element " + elementDetails.getelementName().toString(), e);
        }
    }

    public static void smoothScrollToElement(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        WebElement element = driver.findElement(ElementFormatter.getByObject(elementDetails));
        // ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

        String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                + "var elementTop = arguments[0].getBoundingClientRect().top;"
                + "window.scrollBy(0, elementTop-(viewPortHeight/3));";

        ((JavascriptExecutor) driver).executeScript(scrollElementIntoMiddle, element);
    }

    /**
     * Scroll to an Element in view
     *
     * @param elementDetails
     * @author Edwin_John
     */
    public static void scrollToElement(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        WebElement element = driver.findElement(ElementFormatter.getByObject(elementDetails));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public SeleniumUtils Select(WebElement elem, int index) {

        try {
            Assert.assertTrue(elem.isDisplayed());
            Select select = new Select(elem);
            select.selectByIndex(index);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Not able to select the option " + index + " in " + elem.toString(), e);
        }
        return this;
    }

    public SeleniumUtils Keypress(ElementFormatter elementDetails, String keyValue) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            WebElement element = findElement(driver, elementDetails);
            Assert.assertTrue("The " + elementDetails.getelementName() + " isn't displayed", element.isDisplayed());
            if (keyValue.equalsIgnoreCase("ENTER")) {
                element.sendKeys(Keys.ENTER);
            } else if (keyValue.equalsIgnoreCase("TAB")) {
                element.sendKeys(Keys.TAB);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Not able to enter the option " + keyValue);
        }
        return this;
    }

    public static String GetAbsolutePath(String Filepath) {
        String absolutePath = "";
        try {
            File f = new File(Filepath);
            absolutePath = f.getAbsolutePath();
        } catch (Exception e) {
            throw new RuntimeException("Not able to get the absolute path value of:" + Filepath, e);
        }
        return absolutePath;
    }

    public static String getScreenhot(String screenshotName) {
        String featureName = SeleniumUtils.getStoredValue(KeyStoreConstants.featureName).toString();
        String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        String scenarioName = SeleniumUtils.getStoredValue(KeyStoreConstants.scenarioName).toString();
        TakesScreenshot ts = (TakesScreenshot) DriverFactory.getCurrentDriver();
        File source = ts.getScreenshotAs(OutputType.FILE);
        String absPath = ProjectProperties.cucumberDetailedReportPath + featureName + "\\" + scenarioName;
        String filePath = FileSystems.getDefault().getPath(absPath).normalize().toAbsolutePath().toString();
        // String filePath = ProjectProperties.screenshotPath+"\\"+scenarioName;
		/*File f = new File(filePath);
        if(f.exists()) {
        	String[] subFolders = f.list();
        	for(String s: subFolders){
        	    File currentFile = new File(f.getPath(),s);
        	    currentFile.delete();
        	}
        	f.delete();
        	f.mkdir();
        }*/
        String destination = //System.getProperty("user.dir")+"\\"+filePath+"\\"+screenshotName+".png";
                filePath + "\\" + screenshotName + ".png";
        File finalDestination = new File(destination);
        try {
            FileUtils.copyFile(source, finalDestination);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return destination;
    }

    public static boolean CheckRating(String xpath) {
        System.out.println("-----------Check FilterBy Rating------------");
        List<Float> values = new ArrayList<>();
        List<WebElement> elementName = DriverFactory.getCurrentDriver().findElements(By.xpath(xpath));
        System.out.println(elementName.size());
        for (WebElement elem : elementName) {
            System.out.println(elem.getAttribute("ng-reflect-rating"));
            values.add(Float.valueOf(elem.getAttribute("ng-reflect-rating")));
        }
        System.out.println(values.size());

        boolean isSorted = true;

        String testMarket = tbs_ConfigFileUtils.readConfigFile(ProjectProperties.marketToBeRun, "test.properties");
        String ratingOption = null;
        for (List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get(testMarket)) {
            ratingOption = TDataDetailsList.get(0).rating.trim();
        }
        if (ratingOption.equalsIgnoreCase("4+")) {
            for (float rating : values) {
                if (rating <= (float) 4) {
                    isSorted = false;
                    System.out.println("fail in 4+");
                    break;
                }
            }
        } else {
            float lower = Float.valueOf(ratingOption.split("-")[0].trim());
            float higher = Float.valueOf(ratingOption.split("-")[1].trim());
            for (float rating : values) {
                if (rating < lower || rating > higher) {
                    isSorted = false;
                    System.out.println("fail in " + ratingOption);
                    break;
                }
            }

        }


        return isSorted;
    }

    public static boolean CheckSorting(String xpath, String option, String condition) {

        try {
            String currency = null;
            String currencyPos = null;
            List<Float> values = new ArrayList<>();
            List<WebElement> elementName = DriverFactory.getCurrentDriver().findElements(By.xpath(xpath));
            System.out.println(elementName.size());
            if (option.equalsIgnoreCase("text")) {
                String testMarket = tbs_ConfigFileUtils.readConfigFile("tbs.market", "test.properties");
                for (List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get(testMarket)) {
                    currency = TDataDetailsList.get(0).currency.trim();
                }
                for (List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get(testMarket)) {
                    currencyPos = TDataDetailsList.get(0).currPosition.trim();
                }

                if (currencyPos.equalsIgnoreCase("LEFT")) {
                    for (WebElement elem : elementName) {
                        String price = elem.getText().replace(currency, "");
                        price = price.replace(",", ".").trim();
                        values.add(Float.valueOf(price.trim()));
                    }
                } else {
                    for (WebElement elem : elementName) {
                        String price = elem.getText().replace(" " + currency, "");
                        price = elem.getText().replace(currency, "");
                        price = price.replace(",", ".").trim();
                        values.add(Float.valueOf(price.trim()));
                    }
                }


            } else {
                for (WebElement elem : elementName) {
                    System.out.println(elem.getAttribute(option));
                    values.add(Float.valueOf(elem.getAttribute(option)));
                }

            }
            System.out.println(values.size());
            Float[] arr = new Float[values.size()];
            arr = values.toArray(arr);
            boolean isSorted = true;
            System.out.println(arr.length);
            System.out.println("condtn:" + condition);
            if (condition.equalsIgnoreCase("ASC")) {
                for (int i = 0; i < arr.length - 1; i++) {
                    if (arr[i] > arr[i + 1]) {
                        isSorted = false;
                        break;
                    }
                }
            } else {
                System.out.println("descc");
                for (int i = 0; i < arr.length - 1; i++) {
                    if (arr[i] < arr[i + 1]) {
                        isSorted = false;
                        break;
                    }
                }
            }
            System.out.println(isSorted);
            return isSorted;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    /**
     * @param key
     * @param value
     * @author Edwin_John
     * Store a key value pair. Key should be string and value can be any object
     */

    public static synchronized void storeKeyValue(String key, Object value) {
        //DriverFactory.getThread().getStoredKeys().put(key, value);
        hashMap.put(key, value);

    }

    /**
     * @param key
     * @return
     * @author Edwin_John
     * Return the value for the specified key.
     */
    public static synchronized Object getStoredValue(String key) {
        //return DriverFactory.getThread().getStoredKeys().get(key);
        Object hashValue = hashMap.get(key);
        return hashValue;
    }

    /**
     * This function is the add on for Selenium findElement method.
     *
     * @param driver
     * @param elementDetails
     * @return
     * @author Edwin_John
     */
    public static WebElement findElement(WebDriver driver, ElementFormatter elementDetails) {
        WebElement element = null;
        try {
            element = driver.findElement(By.xpath(elementDetails.getelementValue()));
        } catch (Exception e) {
            return element;
            //throw new RuntimeException(elementDetails.getelementName()+" not found in the page.");
        }
        return element;
    }

    /**
     * @author arjun.vijay
     * Method to wait for overlay to disapper which blocks clicking element
     */
    public static void waitForOverlayToDisappear(ElementFormatter elem) {
        WebDriver driver = DriverFactory.getCurrentDriver();
		/*WebDriverWait wait = new WebDriverWait(driver,1000);
		wait.until(ExpectedConditions.invisibilityOf(driver.findElement(By.xpath(elem.getelementValue()))));*/
        List<WebElement> list = driver.findElements(By.xpath(elem.getelementValue()));
        int count = list.size();
        while (count > 0) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            list = driver.findElements(By.xpath(elem.getelementValue()));
            count = list.size();
        }
    }


    /**
     * Method to switch to any iframe using the frame name
     *
     * @param frameId
     * @author Edwin_John
     */
    public static void switchToFrame(String frameId) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        driver.switchTo().frame(frameId);
    }


    /**
     * Method to switch to a frame using xpath
     *
     * @param elementDetails
     * @author Edwin_John
     */
    public static void waitForFrameAndswitchToIt(ElementFormatter elementDetails) {
        WebDriver driver = DriverFactory.getCurrentDriver();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        WebElement element = driver.findElement(By.xpath(elementDetails.getelementValue()));
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
        //driver.switchTo().frame(element);
    }

    /**
     * Method to come out of all iframes
     *
     * @author Edwin_John
     */
    public static void switchToDefaultContent() {
        WebDriver driver = DriverFactory.getCurrentDriver();
        driver.switchTo().defaultContent();
    }

    /**
     * Method to switch from iframe to parent window
     *
     * @author arjun.vijay
     */

    public static void switchToParent() {
        WebDriver driver = DriverFactory.getCurrentDriver();
        driver.switchTo().parentFrame();

    }

    /**
     * Convert the Price String after removing symbols and currency to a Double value
     *
     * @param price
     * @return
     * @author Edwin_John
     */
    public static String getPriceAsActualNumber(String price) {
        if ((SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested)).toString().equalsIgnoreCase("denmark")) {
            System.out.println("before price:" + price);
            price = price.substring(0, price.length() - 3);
            System.out.println("After price:" + price);
        }
        price = price.replaceAll("[^.,0-9]", "");
        price = price.replace(",", ".");
        if (price.indexOf('.') == 0)
            price = price.substring(1, price.length());
        return price;
    }

    /**
     * Attach the captured screenshot to report, enter the Screenshot required as input parameter
     *
     * @param screenshotName
     * @author Edwin_John
     */
    public static void attachScreenshotInReport(LogStatus log, String screenshotName) {
        screenshotName = screenshotName.replace("\'", "");
        SeleniumUtils.getScreenhot(screenshotName);
        DriverFactory.Testlogger.log(log, "" + screenshotName + "" + DriverFactory.Testlogger.addScreenCapture("./" + screenshotName + ".png"));
    }

    /**
     * @return
     * @author Arjun Vijay
     * Method to round decimal positions of Double type value
     */
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
