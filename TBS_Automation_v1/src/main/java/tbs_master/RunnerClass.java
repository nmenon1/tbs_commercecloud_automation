package tbs_master;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
plugin = {"json:src/target/cucumber-parallel/json/1.json"},
features = "src/test/resources/tbs_FeatureFiles"
,glue= {"tbs_stepFiles"})
public class RunnerClass {
		
}
