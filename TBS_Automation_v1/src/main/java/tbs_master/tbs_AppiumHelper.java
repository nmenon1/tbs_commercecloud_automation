/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_master;
import java.io.File;
import java.util.List;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;

/*********
 *Function:tbs_SeleniumHelper
 *Purpose: Repository of selenium functions
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class tbs_AppiumHelper {
	
	 public tbs_AppiumHelper mob_click(MobileElement element) {
		 try {
			 element.click();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to click in the "+element.getText(),e);
		}
		return null;
	 }
	 
	 public tbs_AppiumHelper mob_SendKeys(MobileElement element,String keyValue) {
		 try {
				Assert.assertTrue(element.isDisplayed());
				WaitForMobElementToBeVisible(element);
				if(keyValue.length()==0) {
					element.clear();
				}
				else {
					
					element.click();
					element.clear();
					element.sendKeys(keyValue);
				}
			}
			catch(Exception e) {
				throw new RuntimeException("Not able to type the value "+keyValue+" in the textbox",e);
			}
			return this;
	 }
	 public tbs_AppiumHelper WaitForMobElementToBeVisible(MobileElement element) {
		 try {
				WebDriverWait wait= new WebDriverWait(DriverFactory.getCurrentDriver(), (int)100);
				wait.until(ExpectedConditions.visibilityOf(element));
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException(element.getText()+" not visible in the page.",e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_clear(MobileElement element) {
		 try {
			 element.clear();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to clear the field in the "+element.getText(),e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_MoveTo(MobileElement element) {
		 try {
			 Actions action = new Actions(DriverFactory.getCurrentDriver());
			 action.moveToElement(element, 10, 10);
			 action.perform();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to move to the element: "+element.getText(),e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_SingleTap(MobileElement element) {
		 try {
			 TouchActions action = new TouchActions(DriverFactory.getCurrentDriver());
			 action.singleTap(element);
			 action.perform();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to single tap on the element: "+element.getText(),e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_DoubleTap(MobileElement element) {
		 try {
			 TouchActions action = new TouchActions(DriverFactory.getCurrentDriver());
			 action.doubleTap(element);
			 action.perform();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to double tap on the element: "+element.getText(),e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_LogPress(MobileElement element) {
		 try {
			 TouchActions action = new TouchActions(DriverFactory.getCurrentDriver());
			 action.longPress(element);
			 action.perform();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to long press on the element: "+element.getText(),e);
		}
		return null;
	 }
	 public tbs_AppiumHelper mob_Scroll(MobileElement element) {
		 try {
			 TouchActions action = new TouchActions(DriverFactory.getCurrentDriver());
			 action.scroll(element, 10, 100);
			 action.perform();
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage());
			throw new RuntimeException("Not able to scroll on the element: "+element.getText(),e);
		}
		return null;
	 }
	 public static boolean isMobElementPresent(MobileElement element) {
	        try {
	            boolean isPresent = element.isDisplayed();
	            System.out.println(isPresent);
	            return isPresent;
	        } catch (NoSuchElementException e) {
	            return false;
	        }
	        
	    }
	 
	 public tbs_AppiumHelper elementHighlight(MobileElement element) {
			for (int i = 0; i < 3; i++) {
				JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
				js.executeScript(
						"arguments[0].setAttribute('style', arguments[1]);",
						element, "color: red; border: 3px solid red;");
				js.executeScript(
						"arguments[0].setAttribute('style', arguments[1]);",
						element, "");
			}
			return this;
			
		}
	 public String getDefaultDropdownSelectedValue(MobileElement elem) {
			
			String value="";
			try {
				Assert.assertTrue(elem.isDisplayed());
				Select select = new Select(elem);
				value=select.getFirstSelectedOption().getText();
			}
			catch(Exception e) {}
			return value;
		}
	public tbs_AppiumHelper Select(MobileElement elem,String selectOption) {
			
			try {
				Assert.assertTrue(elem.isDisplayed());
				Select select = new Select(elem);
				List<WebElement> test=select.getOptions();
				int optionIndex=0;
				 for (int x=0;x<=test.size()-1;x++) {
			           if(test.get(x).getText().equalsIgnoreCase(selectOption)) {
			        	   optionIndex=x;
			        	   break;
			           }
			       }
				select.selectByIndex(optionIndex);
			}
			catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("Not able to select the option "+selectOption,e);
			}
			return this;
		}
	//
	public static String GetText(MobileElement elem) {
		
		String textValue="";
		try {
			Assert.assertTrue(elem.isDisplayed());
			textValue=elem.getText();
		}
		catch(Exception e) {
			throw new RuntimeException("Not able to get the text from "+elem.toString(),e);
		}
		return textValue;
	}
	
	public static tbs_AppiumHelper WaitForMobElementToBeClickable(MobileElement elem) {
		try {
			WebDriverWait wait= new WebDriverWait(DriverFactory.getCurrentDriver(), (int)100);
			wait.until(ExpectedConditions.elementToBeClickable(elem));
		}
		catch(Exception e) {
			System.out.println(e.getLocalizedMessage()+"abcddddddddddddddd");
			throw new RuntimeException(elem.toString()+" not clickable in the page.",e);
		}
		return null;
	}
	public tbs_AppiumHelper WaitForMobElementInvisiblity(MobileElement elem) {
		try {
			WebDriverWait wait= new WebDriverWait(DriverFactory.getCurrentDriver(), (int)100);
			wait.until(ExpectedConditions.invisibilityOf(elem));
		}
		catch(Exception e) {
			throw new RuntimeException(elem.toString()+" still visible in the page.",e);
		}
		return this;
	}
	
	public boolean IsElementVisible(WebElement elem) {
		try {
			WebDriverWait wait= new WebDriverWait(DriverFactory.getCurrentDriver(), 5);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elem.getText())));
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	public static String GetAttribute(WebElement elem, String attribute) {
		String attrValue="";
		try {
			Assert.assertTrue(elem.isDisplayed());
			attrValue=elem.getAttribute(attribute);
		}
		catch(Exception e) {
			throw new RuntimeException("Not able to get the attribute value from "+elem.toString(),e);
		}
		return attrValue;
	}
}
