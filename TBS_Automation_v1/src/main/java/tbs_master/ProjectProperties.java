package tbs_master;

import java.util.Properties;

import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;

public class ProjectProperties {

	/**
	 * The host/machine on which the scripts should be executed
	 */
	public static String serverHost;
	/**
	 * The port number which is used to connect the Selenium standalone. The default value will be 4444
	 */
	public static String serverPort;
	public static String browserName;
	/**
	 * The URL of the first page you need to hit when browser starts
	 */
	public static String baseURL;
	/**
	 * The maximum value of quantity that can be added for a product in Global store
	 */
	public static String maxQtyValue;
	/**
	 * The market for which the tests should be executed
	 */
	public static String marketToBeRun;
	/**
	 * The path to which the Cucumber detailed reports should be stored
	 */
	public static String cucumberDetailedReportPath;
	/**
	 * The path to which the screenshots will be added
	 */
	public static String screenshotPath;
	/**
	 * The Domain url for invoking the APIs
	 */
	public static String domainURL;
	/**
	 * Stock endpoint for API
	 */
	public static String stockAPIEndPoint;
	/**
	 * Get properties object for test.prop file
	 * 
	 * @return
	 */
	private static Properties getTestProperties() {
		String propFilePath = "./test.properties";
		Properties props = tbs_ConfigFileUtils.getProperties(propFilePath);
		return props;
	}
	
	public static void displayProperties() {
		Properties props = getTestProperties();
		System.out.println("--Property File--");
		for (Object eachKey : props.keySet()) {
			// Do not print the proxy properties in console
			if (!eachKey.toString().toLowerCase().contains("proxy")) {
				System.out.println("\t \t" + eachKey.toString() + ":" + props.getProperty(eachKey.toString()));
			}
		}
		System.out.println("--End of Property File--");
	}
	
	/**
	 * Read values from Properties file
	 * @author Edwin_John
	 */
	public static void readPropertiesFromFile() {
		Properties props = getTestProperties();
		serverHost = props.getProperty("Selenium_Host","localhost");
		serverPort = props.getProperty("Server_Port","4444");
		marketToBeRun = props.getProperty("Market_To_Execute", "United Kingdom");
		browserName = props.getProperty("Browser", "chrome");
		baseURL = props.getProperty("base_URL", "https://www-qa-ccv2.thebodyshop.com/en-gb/");
		//baseURL = props.getProperty("base_URL", "https://www-sit-ccv2.thebodyshop.com/en-gb/");
		cucumberDetailedReportPath = props.getProperty("Detailed_Report_Path" , "\\output\\DetailedReports\\");
		screenshotPath = props.getProperty("Screenshot_Path","\\output\\DetailedReports\\Screenshots");
		stockAPIEndPoint = props.getProperty("StockAPI_EndPt","odata2webservices/InboundOaaStockLevel/StockLevels");
		domainURL = props.getProperty("Domain_URL", "https://processing-qa-ccv2.thebodyshop.com");
	}
	
	static {
		readPropertiesFromFile();
	}
	public static void main(String[] args){
		displayProperties();
	}
}

