package com.tbs.rcom.core.utils;

import java.util.HashMap;

public class KeyStoreConstants {

	public static final String productTitleName="pdpProductName";
	public static final String userName = "userName";
	public static final String emailID = "emailId";
	public static final String quantityAddedToCart = "0";
	public static final String cartItemCount = "Null";
	public static final String plpName = "PLPName";
	public static final String subCateogryName = "subCategoryName";
	public static final String productPrice="productPrice";
	public static final String productPartNumber = "productPartNumber";
	public static final String loyaltyPoints = "loyaltyPoints";
	public static final String searchTag = "searchTag";
	public static final String srpProductId="srpProductId";
	public static final String firstName="firstName";
	public static final String lastName = "lastName";
	public static final String addressLine="addressLine";
	public static final String savedCreditCardNumber="savedCreditCardNumber";
	public static final String savedCardType = "savedCardType";
	public static final String cardHolderName= "cardHolderName";
	public static final String savedCardCvv="savedCardCvv";
	public static final String orderID = "orderId";
	public static final String cardLast4digit="cardLast4digit";
	public static final String savedCardDetails="savedCardDetails";
	public static final String scenarioName = "scenarioName";
	public static final String marketToBeTested = "marketValue";
	public static final String currencySymbol = "currencySymbol";
	public static final String wishListName = "wishListName";
	public static final String cartOrderTotal="0.00";
	public static final String giftWrapPrice="giftWrapPrice";
	public static final String featureName = "featureName";
	public static final String productMatrix = "productMatrix";
	public static final String guestFlowForRegUser = "regUserAsGuestCheckout";
    public static final String signupForLYBC = "false";
    public static final String userType = "userType";
    public static final String orderFlag = "orderFlag";
}

