/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.core.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import tbs_master.tbs_DataValueHandler;
/*********
*Function:tbs_SeleniumDriver
*Purpose: To handle database
*Author:mibin.b
*Copyright : INFOSYS LTD
*********/
public class tbs_DataBaseUtils {
	@SuppressWarnings("finally")
	public static Connection Database_Connection_Establish(){
		Connection dbConnection = null;
		String [] DB_Details=new String[5];{
			DB_Details[0]=tbs_ConfigFileUtils.readConfigFile("QA_DB_Server", "./test.properties");
			DB_Details[1]=tbs_ConfigFileUtils.readConfigFile("QA_DB_Port","./test.properties");
			DB_Details[2]=tbs_ConfigFileUtils.readConfigFile("QA_DB_Service","./test.properties");
			DB_Details[3]=tbs_ConfigFileUtils.readConfigFile("QA_DB_Username","./test.properties");
			DB_Details[4]=tbs_ConfigFileUtils.readConfigFile("QA_DB_Password","./test.properties");}
		try {
			DB_Details[4]=tbs_DataValueHandler.decrypt(DB_Details[4]);
		} catch (Exception e1) {
		} 
		try{
			//com.microsoft.sqlserver.jdbc.SQLServerDriver
			Class.forName("oracle.jdbc.driver.OracleDriver");
//			/conn = DriverManager.getConnection("jdbc:sqlserver://HOSP_SQL1.company.com;user=name;password=abcdefg;database=Test");
			dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@//"+DB_Details[0]+":"+DB_Details[1]+"/"+DB_Details[2],DB_Details[3],DB_Details[4]);
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
		finally{return dbConnection;}
	}

	public static void Database_Connection_Close(Connection dbConnection){
		try{
			dbConnection.close();
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
	}
	
	@SuppressWarnings({ "finally"})
	public static ResultSet Database_Query_Select(Connection dbConnection,String dbSql){
		ResultSet dbResultset=null;
		Statement dbStatement = null;
		try{
			dbStatement = dbConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			dbResultset = dbStatement.executeQuery(dbSql);
		}
		catch(Exception e){
			System.out.println(dbSql+" -- "+e.toString());
			dbStatement.close();
			dbResultset.close();
		}
		finally{return dbResultset;}
	}
	@SuppressWarnings("finally")
	public static int Database_Query_UpadteORInsert(Connection dbConnection,String dbSql){
		int dbResultset=0;
		Statement dbStatement = null;
		try{
			dbStatement = dbConnection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			dbResultset = dbStatement.executeUpdate(dbSql);
		}
		catch(Exception e){
			System.out.println(dbSql+" -- "+e.toString());
			dbStatement.close();
			dbResultset=0;
		}
		finally{
			return dbResultset;}
	}
	public static void Database_Query_Delete(Connection dbConnection,String dbSql){
		Statement dbStatement = null;
		try{
			dbStatement = dbConnection.createStatement();
			dbStatement.execute(dbSql);
		}
		catch(Exception e){
			System.out.println(e.toString());
		}
	}
}
