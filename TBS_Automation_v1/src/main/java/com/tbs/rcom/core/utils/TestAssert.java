package com.tbs.rcom.core.utils;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class TestAssert {
	public static void fail(String message){
		SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, message);
		Assert.fail(message);
	}
	
	public static void validateElementText(WebElement element,String expectedText){
		String actualText=element.getText().trim();
		Assert.assertEquals(actualText, expectedText);
	}
	
	public static void validateMessageSubText(String actualText,String expectedText){
		Assert.assertTrue("expected text "+expectedText+" is not present in the actual text "+actualText,actualText.trim().toLowerCase().contains(expectedText.trim().toLowerCase()));
	} 
	
	/**
	 * Verify that the condition is true. If not fail with the specified error message
	 * @param condition
	 * @param errorMessage
	 */
	public static void verifyTrue(boolean condition, String errorMessage){
		if(condition==false) {
			SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, errorMessage);
		}
		Assert.assertTrue(errorMessage,condition);
	}
	
	/**
	 * Verify that the condition is true. If not fail with the specified error message , If true, print the screenshot with success message
	 * @param condition
	 * @param errorMessage
	 */
	public static void verifyTrueAndPrintMsg(boolean condition, String errorMessage , String successMsg){
		if(condition==false) {
			SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, errorMessage);
		}
		else
			SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, successMsg);
		Assert.assertTrue(errorMessage,condition);
	}
	
	public static void verifyIntegerValue(Integer actualValue,Integer expectedValue){
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	public static void validateText(String actualText,String expectedText){
		Assert.assertTrue("expected text "+expectedText+" is not present in the actual text "+actualText,actualText.trim().equalsIgnoreCase(expectedText.trim()));
	}
	
	public static void validateElementVisible(ElementFormatter elementToValidate, String failureMessage){
		try{
			SeleniumUtils.WaitForElementToBeVisible(elementToValidate);
		}
		catch (Exception e){
		}
	}
}
