package com.tbs.rcom.core.utils;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;
public class YamlUtils {
	public static Object loadYaml(String key, String yamlFilePath)  { 
		try{
			Yaml yaml = new Yaml();
			//System.out.println("Inside load yaml ..............................key--"+key+" file path--"+yamlFilePath);
			@SuppressWarnings("rawtypes")
			
			Map obj =(Map) yaml.load(new FileInputStream(new File(yamlFilePath)));
			//System.out.println("returning yaml objects.............");
			return obj.get(key);
		}
		catch(Exception e){
			e.printStackTrace();
			return e.toString();
		}		
	}
	
	/**
	 * Load the complete objects in the yaml file yamlFilePath as a map
	 * @param yamlFilePath
	 * @return
	 */
	public static Object loadYamlMap(String yamlFilePath)  {
		Map<String,Object> obj=null;
		Yaml yaml=new Yaml();
		try{
			obj =(Map) yaml.load(new FileInputStream(new File(yamlFilePath)));
		}
		catch(Exception e){
			e.printStackTrace();
		}		
		return obj;
	}
}
