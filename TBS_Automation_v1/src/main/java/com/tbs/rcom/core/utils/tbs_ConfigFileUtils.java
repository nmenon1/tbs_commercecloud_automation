/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.core.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
/*********
*Function:tbs_SeleniumDriver
*Purpose: To read / write config file
*Author:mibin.b
*Copyright : INFOSYS LTD
*********/
public class tbs_ConfigFileUtils {
		private final static Logger LOGGER = Logger.getLogger(tbs_ConfigFileUtils.class.getName());
		public static Properties getProperties(String filePath){
			return initialize(filePath);
			
		}
		public static void generateAllureReport()
		  {
		    try
		    {
		      
		      String cmd="";
		      Process p = Runtime.getRuntime().exec(cmd);
		      
		      BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		      BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		      
		      @SuppressWarnings("unused")
			String line = null;
		      while ((line = input.readLine()) != null) {}
		      while ((line = error.readLine()) != null) {}
		    }
		    catch (Exception e)
		    {
		      e.printStackTrace();
		    }
		  }
		public static Properties initialize(String filePath)  {
			
			SortedProperties testdataConfig = new SortedProperties();
			try {
				testdataConfig.load(new FileInputStream(filePath));
			} 
			catch (Exception e) {LOGGER.log(Level.SEVERE,"script exception"+e);}
			return testdataConfig;
		}
		public static String readConfigFile(String key,String filePath) {
			
			String propertyValue=null;
			try {
				Properties p=new Properties();
				p.load(new FileInputStream(filePath));
				propertyValue = p.getProperty(key);
			}
			catch (Exception e) {LOGGER.log(Level.SEVERE,"script exception"+e);
			e.printStackTrace();}
			return propertyValue; 
		}
		public static void writeConfigFile(String filePath,String key,String value){
	
			try {
				Properties testdataConfig =initialize(filePath);
				File file = new File(filePath);
				OutputStream writeContents=new FileOutputStream(file);
				testdataConfig.setProperty(key, value);
				testdataConfig.store(writeContents, "");
				writeContents.close();
			} catch (Exception e){LOGGER.log(Level.SEVERE,"script exception"+e);}
	
		}
	
	}
	
	@SuppressWarnings("serial")
	/*
	 * Sorting the property keys
	 */
	class SortedProperties extends Properties {
	
		@SuppressWarnings({ "rawtypes", "unchecked" })
		public Enumeration keys() {
			Enumeration keysEnum = super.keys();
			Vector<String> keyList = new Vector<String>();
			while(keysEnum.hasMoreElements()){
				keyList.add((String)keysEnum.nextElement());
			}
			Collections.sort(keyList);
			return keyList.elements();
		}
}
