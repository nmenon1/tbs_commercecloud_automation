package com.tbs.rcom.core.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CommonUtils {

	/**
	 * Get the current time stamp value
	 * @author Edwin_John
	 * @return
	 */
	public static String getCurrentTimeStamp(){
		/*String timeStampValue="";
		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
		Calendar cal = Calendar.getInstance();
		String Timestamp = dateFormat.format(cal.getTime());
		String trimedTimeStamp=Timestamp.substring(8, 12);
		timeStampValue=timeStampValue+trimedTimeStamp;*/
		String timeStamp = new SimpleDateFormat("yyMMddHHmmss").format(new Timestamp(System.currentTimeMillis()));
		return timeStamp;
	}
}
