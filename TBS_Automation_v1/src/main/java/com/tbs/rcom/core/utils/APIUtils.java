package com.tbs.rcom.core.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONException;
import org.json.JSONObject;

import com.relevantcodes.extentreports.LogStatus;

import tbs_master.DriverFactory;
import tbs_master.ProjectProperties;
import tbs_master.SeleniumUtils;

public class APIUtils {
	public static void updateStockJSON(String stock,String productId,String warehouseCode) throws IOException {
		String extendStock="0";
		int stockInput = Integer.parseInt(stock);
		int extendedStockValue = Integer.parseInt(extendStock);
		if(stock.equalsIgnoreCase("0"))
			extendStock=stock;
		else {
			extendedStockValue = stockInput-2;
			extendStock=""+extendedStockValue;
		}
		try {
			String domain = ProjectProperties.domainURL;
			if(System.getProperty("baseURLenv").contains("uat"))
			{
				domain=domain.replace("qa", "uat");
			}
			URL url = 
					new URL(""+domain
							+ ""+ProjectProperties.stockAPIEndPoint);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Pre-Persist-Hook", "integrationS4WarehousePrePersistHook");
			con.setRequestProperty("Post-Persist-Hook", "integrationS4StockPostPersistHook");
			con.setRequestProperty("Authorization", "Basic c3VwZXJ1c2VyOjEyMzQ1Ng==");
			con.setDoOutput(true);
			String jsonRequest="{\r\n" +
					" \"@odata.context\": \"$metadata#Stocks/$entity\",\r\n" +
					" \"warehouse\": {\r\n" +
					"\"code\": \""+warehouseCode+"\"\r\n" +
					"},\r\n" +
					" \"available\": "+stock+",\r\n" +
					" \"productCode\": \""+productId+"\",\r\n" +
					" \"sapoaa_roughStockIndicator\": \""+extendStock+"\"\r\n" +
					"}" ;                                                                                                                                                                         
			try(OutputStream os = con.getOutputStream()) {
				byte[] input = jsonRequest.getBytes("utf-8");
				os.write(input, 0, input.length);           
			}
			File file = new File("src/main/java/tbs_TestData/test-data.json");
			if(file.isDirectory()){
				file.delete();
			}
			if(!file.exists()){
				System.out.println("File not found , creating new");
				file.createNewFile();
			}
			try(BufferedReader br = new BufferedReader(
					new InputStreamReader(con.getInputStream(), "utf-8"))) {
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
				System.out.println(response.toString());
				try {
					JSONObject jsonObject = new JSONObject(response.toString());
					Writer writer = null;
					writer=new BufferedWriter(new FileWriter(file));
					writer.write(jsonObject.toString());
					writer.close();
				}catch (JSONException err){

				}
			}

			//DriverFactory.Testlogger.log(LogStatus.PASS, "The Stock was updated to "+stock+" for the "+productId+" in the "+warehouseCode+" Warehouse");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void main(String[] args) {
		try {
			APIUtils.updateStockJSON("35","p003255v","2101");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
