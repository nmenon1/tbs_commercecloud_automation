/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.core.utils;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/*********
*Function:tbs_SeleniumDriver
*Purpose: To handle excel
*Author:mibin.b
*Copyright : INFOSYS LTD
*********/
public class tbs_ExcelUtils {
	@SuppressWarnings("resource")
	public Workbook openExcelWorkBook(String translationExcelPath) {
		Workbook workbook;
		FileInputStream excelFile;
			try {
			   excelFile = new FileInputStream(new File(translationExcelPath));
	        }
			 catch (Exception e) {
				 e.printStackTrace();
				 throw new RuntimeException("The specified excel not found in the path "+translationExcelPath,e);
			}
			try {
				 if(translationExcelPath.toLowerCase().contains(".xlsx")) {
					  workbook = new XSSFWorkbook(excelFile);
				  }
				  else if(translationExcelPath.toLowerCase().contains(".xls")) {
					  workbook = new HSSFWorkbook(excelFile);
				  }
				  else {
					  throw new RuntimeException(translationExcelPath+" File format is not supported . Only .xls / .xlsx will be supported");
				  }
			 }
			 catch(Exception e) {
				  workbook=null;
				  throw new RuntimeException("Exception occured while opening the excel",e);
				  
			 }
			return workbook;
	}
	/**
	 * Description : This method opens the excel work sheet from the workbook.
	 * @param workbook,sheet name
	 * @return excelSheet
	 * @throws RuntimeException
	 * @author GoldlinPP_Berin
	 */
	public Sheet openExcelWorkSheet(Workbook workbook,String sheetName) {
		Sheet excelSheet;
		try {
			excelSheet = workbook.getSheet(sheetName);
		 }
		 catch(Exception e) {
			 sheetName=null;
			 throw new RuntimeException(sheetName+" sheet not found in the excel",e);
		 }
		return excelSheet;
	}
	/**
	 * Description : This method opens the excel work sheet from the workbook.
	 * @param workbook,sheet index
	 * @return excelSheet
	 * @throws RuntimeException
	 * @author GoldlinPP_Berin
	 */
	public Sheet openExcelWorkSheet(Workbook workbook,int sheetIndex) {
		Sheet sheetName;
		try {
			 sheetName = workbook.getSheetAt(sheetIndex);
		 }
		 catch(Exception e) {
			 sheetName=null;
			 throw new RuntimeException("No sheet not found in the excel at index "+sheetIndex,e);
		 }
		return sheetName;
	}
	/**
	 * Description : This method writes the value to the cell of the excel work sheet.
	 * @param workbook,sheetName, rowKey,ColumnName,CellValue
	 * @return void
	 * @throws RuntimeException
	 * @author GoldlinPP_Berin
	 */
	public static void writeValueToExcelCell(String testCaseExcelPath, String excelSheetName,String rowKey,String columnName,String cellValue) {
		try {
			tbs_ExcelUtils excelUtils=new tbs_ExcelUtils();
			Workbook workbook=excelUtils.openExcelWorkBook(testCaseExcelPath);
			Sheet sheetName=excelUtils.openExcelWorkSheet(workbook,excelSheetName);
			Iterator<Row> rowIterator = sheetName.rowIterator();
			int rowNumber=0;
			int columnNumber=0;
			while (rowIterator.hasNext()) {
	            Row row = rowIterator.next();
	          	//finding the column number
	          	if(rowNumber==0) {
	          		Iterator<Cell> columnIterator=row.cellIterator();
		          	while(columnIterator.hasNext()) {
		          		Cell cell=columnIterator.next();
		          		if(cell.getStringCellValue().toString().equalsIgnoreCase(columnName)==true) {
		          			columnNumber=cell.getColumnIndex();
		          			break;
		          		}
		          	}
	          	}
	          	if(row.getCell(0).toString().equalsIgnoreCase(rowKey)==true) {
	          		Cell cell=row.getCell(columnNumber);
	        		if (cell == null)
	        	        cell = row.createCell(columnNumber);
	        		 cell.setCellType(CellType.STRING);
	        		 cell.setCellValue(cellValue);
	        		 break;
	            }
	          	rowNumber++;
			}
		
			FileOutputStream fileOut = new FileOutputStream(testCaseExcelPath);
		    workbook.write(fileOut);
		    fileOut.close();
		    excelUtils.closeExcelWorkBook(workbook);
		}
		 catch(Exception e) {
			 throw new RuntimeException("Exception occured while writing to the excel. sheet path-- "+testCaseExcelPath
					 +" sheet Name-->"+excelSheetName+" Row Key-->"+rowKey+" Column Key-->"+columnName+" Cell value-->"+cellValue,e);
		 }
	}
	/**
	 * Description : This method closes the excel  workbook.
	 * @param workbook
	 * @return void
	 * @author GoldlinPP_Berin
	 */
	public void closeExcelWorkBook(Workbook workbook) {
		try {
      	  workbook.close();
        }
        catch(Exception e) {
      	  e.printStackTrace();
        }
	}
}
