/**
 * @author arjun.vijay
 * Search Results Page Locators
 */

package com.tbs.rcom.pageLocators;



import tbs_master.ElementFormatter;

public class SRPPageLocators {

	public ElementFormatter SRPBreadCrumb= new ElementFormatter("SRP-BreadCrumb", "//cx-breadcrumb//span[contains(text(),'{0}')]", ElementFormatter.XPATH);
	public static ElementFormatter SRPProductsTaggedTxt=new ElementFormatter("SRP-Products-tagged-text", "//h1[contains(text(),'Products tagged')]", ElementFormatter.XPATH);
	public static ElementFormatter SRPResultCountMsg= new ElementFormatter("SRP-Results-count-message", "//div[contains(text(),'Showing')]", ElementFormatter.XPATH);
	public ElementFormatter SRPProdId= new ElementFormatter("ProductId-in-SRP", "//a[contains(@href,'{0}/')]", ElementFormatter.XPATH);

	public static ElementFormatter productLinkByPositionInSRP 
	= new ElementFormatter("Product-Link-In-SRP",
			"(//cx-product-grid-item)[{0}]//a[contains(@class,'product-tile__name')]", ElementFormatter.XPATH) ;
	public static ElementFormatter productPriceInSRP = 
			new ElementFormatter("Product-Price-In-SRP", "(//cx-product-grid-item)[{0}]//div[contains(@class,'product-tile__price') and @aria-label][1]", ElementFormatter.XPATH); 
	public static ElementFormatter productImageByPositionInSRP = new ElementFormatter("Product-Image-By-Positon-SRP", "(//cx-product-grid-item)[{0}]//a[contains(@class,'product-tile__image')]", ElementFormatter.XPATH);
    public static ElementFormatter searchBoxModal = new ElementFormatter("Search-Box-Modal", "//cx-searchbox-modal", ElementFormatter.XPATH);
}
