package com.tbs.rcom.pageLocators;

import gherkin.lexer.El;
import tbs_master.ElementFormatter;

public class OrderConfirmationPageLocators {

	public static final ElementFormatter orderConfirmationMsgWithUserNameEmail = new ElementFormatter("Order-Confirmation-Message-With-User-Email-Text", "//div[contains(@class,'order-confirmation__message')]//p", ElementFormatter.XPATH);
	public static final ElementFormatter orderConfirmationPageBreadCrumb
	= new ElementFormatter("Order-Confirmation-Page-Breadcrumb", "//cx-breadcrumb//span[contains(@aria-label,'Order Confirmation')]", ElementFormatter.XPATH);
	public static final ElementFormatter orderConfirmationPageHeading 
	= new ElementFormatter("Order-Confirmation-Page-Heading-Text", "//div[contains(@class,'order-confirmation')]//h1", ElementFormatter.XPATH);
	public static final ElementFormatter orderConfirmationImage = new ElementFormatter("Order-Confirmation-Page-Image", "//div[@class='order-confirmation']//img[contains(@src,'order-confirmation')]", ElementFormatter.XPATH);
	public static final ElementFormatter orderNumberInOrderConfPage = new ElementFormatter("Order-Number-In-Order-Confirmation-Page", "//div[contains(@class,'order-confirmation__detail')]//div[@class='row'][3]/div[1]", ElementFormatter.XPATH);
	public static final  ElementFormatter manageOrdersBtn = new ElementFormatter("Manage-Orders-Button", "//div[contains(@class,'order-confirmation')]//a[@aria-label='Manage Orders']", ElementFormatter.XPATH);
	public static final ElementFormatter orderDateInOrderConfPage = new ElementFormatter("Order-Date-in-Order-Confirmation-Page", "//div[contains(@class,'order-confirmation__detail')]//div[@class='row'][3]/div[2]", ElementFormatter.XPATH); 
    public static final ElementFormatter orderTotalValue = new ElementFormatter("Order-Total-Value-In-Order-Confirmation-Page", "//div[contains(@class,'order-confirmation__order-total')]//div[contains(@class,'order-confirmation__total')]", ElementFormatter.XPATH);
    public static final ElementFormatter guestUserRegisterBtn = new ElementFormatter("REgister-button-for-guest-user", "//app-order-confirmation-register//button", ElementFormatter.XPATH);
    public static final ElementFormatter registrationFormForGuestUser = new ElementFormatter("Registration-form-for-guestuser", "//div[contains(@id,'register-guest-screen')]", ElementFormatter.XPATH);
    public static final ElementFormatter firstNameTxtFieldRegForm = new ElementFormatter("First-name-text-field-registration-form", "//div[contains(@id,'register-guest-screen')]//input[contains(@name,'profile.firstName')]", ElementFormatter.XPATH);
    public static final ElementFormatter lastNameTxtFieldInRegForm = new ElementFormatter("Last-name-In-Registration-form", "//div[contains(@id,'register-guest-screen')]//input[contains(@name,'profile.lastName')]", ElementFormatter.XPATH);
    public static final ElementFormatter emailIdTxtFieldInRegForm = new ElementFormatter("Email-Id-Text-field-In-Registration-form", "//div[contains(@id,'register-guest-screen')]//input[@name='email']", ElementFormatter.XPATH);
    public static final ElementFormatter confirmEmailIdInRegForm = new ElementFormatter("Confirm-Email-Txt-field-In-Registration-form", "//div[contains(@id,'register-guest-screen')]//input[@name='profile.email']", ElementFormatter.XPATH);
    public static final ElementFormatter pwdTxtFieldInRegForm = new ElementFormatter("Password-text-field-In-registration-form", "//div[contains(@id,'register-guest-screen')]//input[@aria-label='Password']", ElementFormatter.XPATH);
    public static final ElementFormatter confirmPwdTxtFieldInRegForm = new ElementFormatter("Confirm-Password-text-field-Registration-form", "//div[contains(@id,'register-guest-screen')]//input[@aria-label='Confirm password']", ElementFormatter.XPATH);
    public static final ElementFormatter termsAndConditionsCheckbox = new ElementFormatter("T&C-Checbox", "//div[contains(@id,'register-guest-screen')]//input[@name='preferences.terms.TermsOfService.isConsentGranted']", ElementFormatter.XPATH);
    public static final ElementFormatter regFormSubmitBtn = new ElementFormatter("Register-button-In-registration-form", "//div[contains(@id,'register-guest-screen')]//input[@type='submit']", ElementFormatter.XPATH);
    public static final ElementFormatter lybcRegistrationBtn = new ElementFormatter("LYBC-registration-button-In-Order-confirmation-page", "//app-order-confirmation-update-lybc//button", ElementFormatter.XPATH);
    public static final ElementFormatter lybcScreensetInOrderConfPage = new ElementFormatter("LYBC-screenset-In-Order-confirmation-page", "//div[contains(@id,'lybc') and contains(@class,'gigya-screen')]", ElementFormatter.XPATH);
    public static final ElementFormatter birthDateDropdownInLybcForm = new ElementFormatter("Birth-date-in-lybc-form", "//div[contains(@id,'lybc')]//select[contains(@name,'birthDay')]", ElementFormatter.XPATH);
    public static final ElementFormatter birthMonthDropdownInLybcForm = new ElementFormatter("Birth-date-in-lybc-form", "//div[contains(@id,'lybc')]//select[contains(@name,'birthMonth')]", ElementFormatter.XPATH);
    public static final ElementFormatter birthYearDropdownInLybcForm = new ElementFormatter("Birth-year-in-lybc-form", "//div[contains(@id,'lybc')]//select[contains(@name,'birthYear')]", ElementFormatter.XPATH);
    public static final ElementFormatter lybcConsentCheckboxInForm = new ElementFormatter("LYBC-consent-checkbox-in-form", "//div[contains(@id,'lybc')]//input[contains(@name,'LYBC') and @type='checkbox']", ElementFormatter.XPATH);
    public static final ElementFormatter joinLybcBtn = new ElementFormatter("Join-LYBC-button", "//input[@type='submit' and @value='JOIN']", ElementFormatter.XPATH);
    public static final ElementFormatter feedbackFormAtOrderConfirmation = new ElementFormatter("Feedback-Form-At-Order-Confirmation", "//iframe[contains(translate(@title,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxys'),'feedback form')]", ElementFormatter.XPATH); 
    public static final ElementFormatter feedbackCloseButton = new ElementFormatter("Feed-back-form-close-button", "(//a[contains(@ng-click,'closeSurvey')])[1]", ElementFormatter.XPATH);
    
    public static final ElementFormatter googleFeedBackIframe = new ElementFormatter("Google-FeedBack-Iframe", "(//iframe[contains(@src,'google.com/shopping/customerreviews')])[2]", ElementFormatter.XPATH);
    public static final ElementFormatter googleFeedBackCancelBtn = new ElementFormatter("Google-FeedBack-Cancel-Button", "//button/span[text()='NO']", ElementFormatter.XPATH);
}
