/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pageLocators;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import gherkin.lexer.El;
import tbs_master.ElementFormatter;

public class PDPPageLocators {
	public static ElementFormatter emailMeOutOfStockBtn = new ElementFormatter("Email-Me-Out-Stock-Button-in-PDP", "//cx-add-to-cart//app-back-in-stock//button", ElementFormatter.XPATH);
	public static ElementFormatter productImage = new ElementFormatter("Product-Image", "//cx-product-images//cx-media[contains(@format,'zoom')]//img", ElementFormatter.XPATH);
	
	public static ElementFormatter productPriceBlockInPDP = new ElementFormatter("Product-price",
			"//div[contains(@class,'price__block')]//span[contains(@class,'price__element price__element--formatted')]"
			, ElementFormatter.XPATH);
	public static ElementFormatter productTitleHeading = new ElementFormatter("Product-Title-Heading", "//div[contains(@class,'product-intro')]//h1", ElementFormatter.XPATH);
	public static ElementFormatter productPriceMetric_Text = new ElementFormatter("Product-Price-Per-Metric-Value", "//div[contains(@class,'price__block')]//span[contains(@class,'price__element price__element--per-metric')]"
			, ElementFormatter.XPATH);
	
	@FindBy(how=How.XPATH,using="(//span[contains(@class,'price__element price__element--unit')])[1]")
	public static ElementFormatter elm_ProductUnit =
	new ElementFormatter("Product-Unit-Text", "(//span[contains(@class,'price__element price__element--unit')])[1]", ElementFormatter.XPATH);
	
	public static ElementFormatter addToCartBtn = new ElementFormatter("Add-To-Cart-Button", "//cx-add-to-cart//div[contains(@class,'add-to-bag')]/button", ElementFormatter.XPATH);
	public static ElementFormatter qtyAdjusterPlusBtn = new ElementFormatter("Quantity-Adjuster-Plus-Button", "//cx-cart-quantity-selector//button/cx-custom-icon[@name='plus']", ElementFormatter.XPATH); 
	public static ElementFormatter qtyAdjusterMinusBtn = new ElementFormatter("Quantity-Adjuster-Minus-Button", "//cx-cart-quantity-selector//button/cx-custom-icon[@name='minus']", ElementFormatter.XPATH); 
	public static ElementFormatter quantityTextFieldInPDP = new ElementFormatter("Quantity_TextField", "//cx-cart-quantity-selector//input[@id='counterInput']", ElementFormatter.XPATH); 
	public static ElementFormatter elm_ColorSwatch=new ElementFormatter("Color-Swatch-Bttuon", "(//div[contains(@class,'colour-variants__swatch-inner')])[2]", ElementFormatter.XPATH);
	public static ElementFormatter elm_SizeVariant = new ElementFormatter("Size_Variant_Btn", "(//input[contains(@name,'variantOptions')])[2]", ElementFormatter.XPATH);
	public static ElementFormatter itemInSTockTxt = new ElementFormatter("InStock-Text", "//span[@class='info']", ElementFormatter.XPATH); 
	public static ElementFormatter txt_initialColor = new ElementFormatter("Initial-Color-Button", "//div[contains(@class,'colour-variants__preview')]/p[contains(@class,'colour-variants__preview-name')]", ElementFormatter.XPATH); 
	public static ElementFormatter productTitleName = new ElementFormatter("Product-Name-Text", "//h1[contains(@class,'product-intro__name')]", ElementFormatter.XPATH); 
	public static ElementFormatter multiVariantProductID = new ElementFormatter("Product-Id-Text", 
			"(//cx-product-summary//div[contains(@class,'price__element--per-metric')])[{0}]", ElementFormatter.XPATH)  ;
	public static ElementFormatter singleVariantProductID = new ElementFormatter("Single-variant-Product-Id-Text", "//cx-product-summary//div[contains(@class,'per-metric')]", ElementFormatter.XPATH);
	public static ElementFormatter multiVariantLoyaltyPointsTxt = 

			new ElementFormatter("Product-Loyalty-Points-Text", "(//cx-product-summary//button[contains(@aria-label,'reward points')])[{0}]", ElementFormatter.XPATH);
	public static ElementFormatter singleVariantLoyaltyPtsTxt = new ElementFormatter("Single-Variant-Loyalty-Points-txt", "//cx-product-summary//button[contains(@aria-label,'reward points')]", ElementFormatter.XPATH);
	
	public static ElementFormatter productNameInBreadCrumb = 
			new ElementFormatter("Product-Name-In-BreadCrumb", 
					"//cx-breadcrumb//span[contains(text(),'{0}')]",
					ElementFormatter.XPATH);
	public static ElementFormatter pdpProductTitle = new ElementFormatter("PDP-Product-Title", "//cx-product-intro", ElementFormatter.XPATH);
	public static ElementFormatter productTitleHeadingText = new ElementFormatter("Product-Title-Heading-text", "//cx-product-intro//h1[contains(@class,'product-intro__name')]", ElementFormatter.XPATH);
	public static ElementFormatter productSelectionRadioBtnList =
			new ElementFormatter("Product-Selection-Radio-Button-List-In-PDP", "//input[@type='radio' and contains(@class,'price__radio')]", ElementFormatter.XPATH);  
	public static ElementFormatter productRadioBtnRelatedToPrice = new ElementFormatter("Product-Radio-button-Corresponding-To-PRice", "//span[contains(@aria-label,'Product price') and contains(text(),'{0}')]/ancestor::label/preceding-sibling::input[@type='radio']", ElementFormatter.XPATH);
	public static ElementFormatter multiVariantProductPriceTxt = new ElementFormatter("Multi-Variant-Product-Price-Text", "(//cx-product-summary//span[contains(@class,'element price')])[{0}]", ElementFormatter.XPATH);
	public static ElementFormatter singleVariantProductPriceTxt = new ElementFormatter("Single-Variant-Product-Price-Text", "//div[contains(@class,'price-variant')]/following-sibling::div[contains(@class,'price')]//span", ElementFormatter.XPATH);
	public static ElementFormatter priceListforMultiVariantPDP = new ElementFormatter("Price-List-for-Mulit-variant-PDP", "//cx-product-summary//span[contains(@class,'element price')]", ElementFormatter.XPATH);
	public static ElementFormatter addedToCartInterstitialPopup = new ElementFormatter("PDP-AddtoCart-Modal", "//div[@class='modal-content']//cx-added-to-cart-modal", ElementFormatter.XPATH);
	public static ElementFormatter addedToCartMessageInInterstitial = new ElementFormatter("Added-to-cart-message-in-Interstitial", "//div[@class='modal-content']//cx-added-to-cart-modal//h2", ElementFormatter.XPATH);

	public static ElementFormatter itemNameInAddToCartInterstitial = new ElementFormatter("Item-Name-in-add-to-cart-Interstitial", "//cx-added-to-cart-modal//button[contains(@aria-label,'{0}')]", ElementFormatter.XPATH);
	public static ElementFormatter itemPriceInAddtoCartInterstitial = new ElementFormatter("Item-Price-Displayed-In-Add-To-Cart-Interstitial", "//cx-added-to-cart-modal//div[contains(@aria-label,'Price')]", ElementFormatter.XPATH);
	public static ElementFormatter itemQtyInAddToCartInterstitial = new ElementFormatter("Item-Quantity-Displayed-In-Add-To-cart-Intertstitial", "//cx-added-to-cart-modal//div[contains(@class,'quantity')]", ElementFormatter.XPATH);
	public static ElementFormatter viewBasketBtnInAddToCartInterstitial=new ElementFormatter("View-Basket-Button-In-Add-ToCart-Interstitial", "(//cx-added-to-cart-modal//a)[1]", ElementFormatter.XPATH);

	public static ElementFormatter closeInterstitial = new ElementFormatter("AddToBagIntrestitial-Close-Button", "//cx-added-to-cart-modal//button[@aria-label='Close']", ElementFormatter.XPATH);
	public static ElementFormatter continueShoppingBtnInAddToCartInterstitial = new ElementFormatter
			("Continue-Shopping-Button-In-Add-To-cart-Interstitial", "(//cx-added-to-cart-modal//a/following-sibling::button)[1]", ElementFormatter.XPATH);
	public static ElementFormatter elm_CloseOverlay = new ElementFormatter("PDP-Modal-Close-Button", "//cx-icon[contains(@ng-reflect-type,'CLOSE')]", ElementFormatter.XPATH);
	public static ElementFormatter txt_ModalTitle = new ElementFormatter("PDP-Modal-Title", "//h2[contains(@class,'modal-title')]", ElementFormatter.XPATH);
	public static ElementFormatter txt_QtyAddedFromPDP = new ElementFormatter("Quantity-in-PDP-Modal", "//div[contains(text(),'Quantity')]", ElementFormatter.XPATH);
    public static ElementFormatter findInStoreBtn = new ElementFormatter("Find-In-Store-Button-InPDP" , "//div[contains(@aria-label,'Find in store')]", ElementFormatter.XPATH);
	public static ElementFormatter categoryBreadcrumbLnk = new ElementFormatter("Category-BreadcrumbLink","//nav[contains(@class,'breadcrumbs-content')]//a[contains(@ng-reflect-router-link,'/c/')]", ElementFormatter.XPATH);

	public static ElementFormatter wishlistButtonPDP= new ElementFormatter("PDP-wishlist-button", "(//button[contains(@class,'add-to-bag-set__fav btn btn--icon btn--none ng-star-inserted')])[1]", ElementFormatter.XPATH);
    
	public static ElementFormatter productDetailsExpandBtn = new ElementFormatter("Product-Details-Expand-Btn", "//mat-accordion[contains(@class,'product-details')]//mat-expansion-panel[2]", ElementFormatter.XPATH);

}
