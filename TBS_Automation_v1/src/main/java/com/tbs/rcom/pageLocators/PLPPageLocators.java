/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pageLocators;

import tbs_master.ElementFormatter;

public class PLPPageLocators {
	public static final ElementFormatter listOfStarRatingOfProductsInPLP = new ElementFormatter("List-of-Star-rating-of-products-in-PLP", "//cx-product-grid-item//cx-star-rating", ElementFormatter.XPATH);
	public static final ElementFormatter filterByBtn = new ElementFormatter("Filter-By-Button", "//button[@title='Filter products']", ElementFormatter.XPATH);
	public static final ElementFormatter filterModalPopup = new ElementFormatter("Filter-Modal-Popup", "//div[@class='facets-popover']", ElementFormatter.XPATH);
	public static final ElementFormatter filterByOptionCheckbox = new ElementFormatter("Filter-Checkbox-Name", "//app-facet-node//h4[@aria-label='{0}']/parent::div//li[1]//input[@type='checkbox']", ElementFormatter.XPATH);
	public static final ElementFormatter filterOptionTypeTxt = new ElementFormatter("Filter-By-Option-Label", "//app-facet-node//h4[@aria-label='{0}']", ElementFormatter.XPATH);
	public static final ElementFormatter filterOptionLabelTxt = new ElementFormatter("Filter-Option-Selected", "//app-facet-node//h4[@aria-label='Free From']/parent::div//li[1]//input[@type='checkbox']/following-sibling::span", ElementFormatter.XPATH);
	public static final ElementFormatter starRatingDisplayedForProductInPLP = 
			new ElementFormatter("Star-Rating-displayed-For-Product-In-PLP", "//cx-star-rating[contains(@aria-label,'Product rating')]", ElementFormatter.XPATH);
	public static final ElementFormatter filterModalApplyBtn = new ElementFormatter("Apply-Button-Filter-Modal", "//div[@class='facets-popover']//button[@aria-label='Apply']", ElementFormatter.XPATH);
	public static final ElementFormatter filterModalCancelBtn = new ElementFormatter("Filter-Modal-Cancel-Button", "//div[@class='facets-popover']//button[@aria-label='Cancel']", ElementFormatter.XPATH);
	public static final ElementFormatter filterModalCloseButton = new ElementFormatter("Filter-Modal-Close-Button", "//div[@class='facets-popover']//cx-icon[@ng-reflect-type='CLOSE']", ElementFormatter.XPATH);
	public static final ElementFormatter sortDropdown = new ElementFormatter("Sort-Filter-Dropdown", "//cx-sorting//ng-select[@role='listbox']", ElementFormatter.XPATH);
	public static final ElementFormatter sortOptionInDropdownPanel = new ElementFormatter("Sort-Option-In-Dropdown-Panel", "//cx-sorting//ng-dropdown-panel//div[@role='option'][{0}]", ElementFormatter.XPATH);
	public static final ElementFormatter productPriceArrayListInPLP = new ElementFormatter("List-Of-Product-Prices-of-Products-Displayed-In-PLP", "//cx-product-grid-item//div[contains(@aria-label,'Product price')]", ElementFormatter.XPATH);
	public static ElementFormatter seeOptionsButtonInPLP = new ElementFormatter
			("See-Options-Button-In-PLP", "//app-product-grid-item[{0}]//button[contains(@aria-label,'Add to bag')]", ElementFormatter.XPATH) ;   
	public static ElementFormatter seeOptionsButtonForFirstItemInBodyPLP = new ElementFormatter
			("See-Options-Button-for-First-Item-In-Body-PLPs", 
					"//app-product-grid-item[1]//button//span[contains(@aria-label,'Add to bag')]",
					ElementFormatter.XPATH); 
	public static ElementFormatter productNameByPositionInPLP = new ElementFormatter("Product-Name-In-PLP", "//cx-product-grid-item//button[contains(@class,'product-tile__name')]", ElementFormatter.XPATH) ;
	public static ElementFormatter productPriceByPositionInPLP = new ElementFormatter("Product-Price-In-PLP", "//cx-product-grid-item[{0}]//div[contains(@aria-label,'Product price')]", ElementFormatter.XPATH);
	public static ElementFormatter lnk_TBSProduct = new ElementFormatter("Product-Link-In-PLP", "(//div[contains(@class,'product-tile')]//a[contains(@class,'product-tile__name')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter addToBagInPLP = new ElementFormatter("Add-to-Bag-Button-In-PLP", "(//cx-add-to-cart)['{0}']/button/span[@aria-label='Add to bag']", ElementFormatter.XPATH);
	public static ElementFormatter addToBagInSeeOptionsModal = 
			new ElementFormatter("See_options-Add-To-Cart-Button", 
					"//h2[contains(text(),'{0}')]/ancestor::div[contains(@class,'modal__body')]//cx-add-to-cart//button[contains(@aria-label,'Add to cart')]", ElementFormatter.XPATH);
	public static ElementFormatter elm_TBSCloseSeeOptions = new ElementFormatter("See-Options-Close-button", "//cx-icon[contains(@ng-reflect-type,'CLOSE')]", ElementFormatter.XPATH);
	public static ElementFormatter productSpecificSeeOptionsBtn = new ElementFormatter("Product-Specific-See-Options-Button", "//a[contains(@class,'product-tile__name') and contains(@href,'{0}')]/./following-sibling::div[contains(@class,'product-tile__cta-container')]//button[contains(text(),'Add to bag')]", ElementFormatter.XPATH);
	public static ElementFormatter productSpecificLink= new ElementFormatter("Product-Specific-Link","//a[contains(@class,'product-tile__name') and contains(@href,'{0}')]", ElementFormatter.XPATH); 
	public static ElementFormatter emailMeWhenInStockBtn = new ElementFormatter("Email-Me-When-In-Stock-Button", "(//button[contains(@class,'btn btn--primary btn-block') and contains(text(),'Email me when in stock')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter notifyMeUserEmailInput = new ElementFormatter("Notify-Me_User-Email-Input", "//input[contains(@formcontrolname,'email')]", ElementFormatter.XPATH);
	public static ElementFormatter emailMeWhenInStockConfirmationBtn = new ElementFormatter("Email-Me-When-In-Stock-Button-Email-Me-Overlay", "//button[contains(@class,'btn btn--primary btn-block') and contains(text(),' Email me when in stock ')]", ElementFormatter.XPATH);
	public static ElementFormatter outOfStockMsg = new ElementFormatter("Out-Of-Stock-Message-See-Options-Overlay","//span[contains(@class,'price__stock') and contains(text(),'Out of stock')]", ElementFormatter.XPATH);
	public static ElementFormatter emailMeWhenInStockSuccessMsg = new ElementFormatter("Email-Me-When-In-Stock-Success-Message","//p[contains(text(),'Thank you, we will email you when this product is back in stock.')]", ElementFormatter.XPATH);
	public static ElementFormatter notifyMeUserEmailCloseIcon = new ElementFormatter("Notify-Me-User-Email-Close-Icon", "//h2[contains(text(),' Email me when in stock ')]//ancestor::div[contains(@class,'modal-content')]//cx-icon[contains(@class,'icon icon--close')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistButtonPLP= new ElementFormatter("PLP-wishlist-button", "(//button[contains(@class,'add-to-bag-set__fav btn btn--icon btn--none ng-star-inserted')])[2]", ElementFormatter.XPATH);
	public static ElementFormatter productName= new ElementFormatter("Product-Name", "(//a[contains(@class,'product-tile__name')])[1]", ElementFormatter.XPATH);
}
