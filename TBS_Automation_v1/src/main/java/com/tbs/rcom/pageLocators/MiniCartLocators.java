
package com.tbs.rcom.pageLocators;

import java.util.List;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import tbs_master.ElementFormatter;

public class MiniCartLocators {

    public static ElementFormatter viewBasketBtn = new ElementFormatter("View Basket-Button-InOverlay", "//button[contains(text(),'View basket')]", ElementFormatter.XPATH);
    public static ElementFormatter continueShoppingBtn =  new ElementFormatter("Continue-Shopping-Button-InOverlay", 
    		"//div[contains(@class,'mini-cart')]/a[contains(text(),'Continue')]", ElementFormatter.XPATH);
    public static ElementFormatter miniCartBtnInHeader = new ElementFormatter("Mini-Cart-Button-In-Header","//button[contains(@id,'mini-cart-desktop-trigger')]"
    		, ElementFormatter.XPATH);  
    public static ElementFormatter MiniCartOverLay = new ElementFormatter("Minicart-overlay",
    		"(//div[contains(@class,'mini-cart')]/app-custom-mini-cart-entry-item)[1]", ElementFormatter.XPATH);
    
    public static ElementFormatter miniCartCloseBtn = new ElementFormatter("Minicart-overlay-close-button", "//cx-custom-mini-cart-modal//cx-icon[contains(@class,'close')]", ElementFormatter.XPATH);
    public static ElementFormatter productDetailInOverlayTxt = new ElementFormatter("Products-in-overlay-text", "//div[contains(@class,'modal-header')]//cx-icon[contains(@ng-reflect-type,'CLOSE')]", ElementFormatter.XPATH);
    public static ElementFormatter minicartCountTxt = new ElementFormatter("ItemCount-Text-in-Minicart-overlay", "//button[contains(@id,'mini-cart-desktop-trigger')]//span[contains(@class,'count')]", ElementFormatter.XPATH);
    public static ElementFormatter itemsRowInMiniCartContainer = new ElementFormatter("Items-rows-in-MiniCart-Container","//div[contains(@class, 'mini-cart__col')]//app-custom-mini-cart-entry-item[contains(@class,'mini-cart__entry')]" , ElementFormatter.XPATH);
    public static ElementFormatter emptyCartText = new ElementFormatter("Empty-Cart-message", "//h2[contains(text(),'Your shopping bag is empty')]", ElementFormatter.XPATH); 
    public static ElementFormatter productNameInMinicartOverlay = new ElementFormatter("Product-Name-In-MiniCart-Overlay", "//cx-custom-mini-cart-entry-item//a[contains(@class,'cx-product-name')]", ElementFormatter.XPATH);
    public static ElementFormatter productQtyInMiniCartOverlay = new ElementFormatter("Product-quantity-In-Overlay", "//cx-custom-mini-cart-entry-item//div[contains(@class,'quantity')]", ElementFormatter.XPATH);
    public static ElementFormatter productPriceInMinicartOverlay = new ElementFormatter("Product-Price-In-Minicart-Overlay", "//div[contains(@class,'subtotal')]//span[contains(@class,'price')]", ElementFormatter.XPATH);
    public static ElementFormatter productNameWithPositionInMinicartOverlay = new ElementFormatter("Product-Name-In-MiniCart-Overlay", "(//app-custom-mini-cart-entry-item//a[contains(@class,'cx-product-name')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter productQtyWithPositionInMiniCartOverlay = new ElementFormatter("Product-quantity-In-Overlay", "(//app-custom-mini-cart-entry-item//div[contains(@class,'quantity')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter productPriceWithPositionInMinicartOverlay = new ElementFormatter("Product-Price-In-Minicart-Overlay", "(//app-custom-mini-cart-entry-item//div[contains(@aria-label,'price')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter removeItemFromCartBtn = new ElementFormatter("Remove-Item-From-Cart-Button", "//div[contains(@class,'cart-details')]//button[contains(@title,'Remove')]", ElementFormatter.XPATH);
    public static ElementFormatter miniCartSubtotal = new ElementFormatter("Subtotal-In-MiiCart-Overlay", "//div[contains(@class,'mini-cart-subtotal')]//span[contains(@class,'price')]", ElementFormatter.XPATH);
}
