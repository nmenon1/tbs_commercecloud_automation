package com.tbs.rcom.pageLocators;

import tbs_master.ElementFormatter;

public class StorePageLocators {

	public static ElementFormatter inputTextFieldInCISSection = new ElementFormatter("Input-text-field-in-CIS-Section",
			"//cx-checkout-collect-in-store//input[@formcontrolname='location']", ElementFormatter.XPATH);
	public static ElementFormatter errorMsgForInvalidStoreSearch = new ElementFormatter
				("Error-Message-For-Invalid-Store-Search", "//cx-checkout-collect-in-store//form[contains(@class,'address-finder')]/following-sibling::p[contains(@class,'alert-danger')]", ElementFormatter.XPATH);
	public static ElementFormatter searchIconInCISSection = new ElementFormatter("Search-Icon-In-CISSection",
			"//cx-checkout-collect-in-store//button[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter inputTxtFieldInStoreModal = new ElementFormatter("Input-Text-Field-In-Store-Modal", "//cx-checkout-collect-in-store//input[contains(@formcontrolname,'location')]", ElementFormatter.XPATH);
	public static ElementFormatter searchIconInStoreModal =
			new ElementFormatter("Search-Button-in-Store-Modal", "//cx-checkout-collect-in-store//button[@type='submit' and contains(@class,'address-finder')]", ElementFormatter.XPATH);
	public static ElementFormatter storeModalHeading = new ElementFormatter("Store-Results-Modal-Popup", "//ngb-modal-window[contains(@class,'cis-modal')]//h2", ElementFormatter.XPATH);
	public static ElementFormatter storeHeadingInResults = new ElementFormatter("List-of-Stores-In-Store-ModalResults", 
			"(//app-collection-point-item//h3[contains(@class,'collection-point__name')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter listedStoresStockAvailabilityBtn = 
    		new ElementFormatter("List-of-stores-with-Select-Button-Enabled", 
    				"//app-collection-point-item//button[contains(@class,'collection-point__btn') and contains(@class,'primary')]",
    				ElementFormatter.XPATH);
    public static ElementFormatter selectButtonForEachListedStore = new ElementFormatter("Select-Button-For-Each-Listed-Store", "(//app-collection-point-item//button[contains(@class,'collection-point__btn') and contains(@class,'primary')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter selectedStoreInCISChangeBtn =
    		new ElementFormatter("Change-Button-of-SelectedStore-In-CIS", 
    				"//app-collection-point-item//button[2]", ElementFormatter.XPATH); 


}
