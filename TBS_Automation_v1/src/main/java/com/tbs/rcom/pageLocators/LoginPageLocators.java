package com.tbs.rcom.pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import tbs_master.ElementFormatter;

public class LoginPageLocators {

	public static ElementFormatter breadCrumbForLoginModal = 
			new ElementFormatter("Login-Modal-breadcrumb", "//cx-breadcrumb//span[contains(@aria-label,'Login')]", ElementFormatter.XPATH);
	public static ElementFormatter emailAddressTxtFieldInLoginModal
	= new ElementFormatter("Email-Address-textfield-Login-Modal", "//form[@id='gigya-login-form']//input[@name='username']", 
			ElementFormatter.XPATH);
	public static ElementFormatter passwordTextFieldInLoginModal 
	= new ElementFormatter("Password-text-field", "//form[@id='gigya-login-form']//input[@name='password']", ElementFormatter.XPATH);
	public static ElementFormatter singInBtnInLoginModal = new ElementFormatter("Signin-button", "//form[@id='gigya-login-form']//input[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter loginPageCreateAccountQnLink = 
			new ElementFormatter("Register-button-in-Login-Page", "//div[contains(@id,'login-container')]//a[contains(@data-switch-screen,'register-screen')]/parent::div", ElementFormatter.XPATH);
	public static ElementFormatter registerLandingPageHeading = 
			new ElementFormatter("Register-Landing-Page-Heading", "//div[contains(@id,'register-site-login')]//h2", ElementFormatter.XPATH);
	public static ElementFormatter personalNumberField =new ElementFormatter("Personal-Nmber-Field", "//form[contains(@id,'gigya-register-form')]//input[contains(@name,'data.nationalId')]", ElementFormatter.XPATH);
	public static ElementFormatter registerBtn = 
			new ElementFormatter("Register-button",
					"//form[contains(@id,'gigya-register-form')]//input[@data-gigya-type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter emailAddressTxtFieldInRegistrationModal = 
			new ElementFormatter("Email-Address-Text-Field-In-Registration-Page",
					"//form[contains(@id,'gigya-register-form')]//input[@name='email']", ElementFormatter.XPATH);
	public static ElementFormatter registrationTitleSelectorDropdown = new ElementFormatter("Title-Dropdown-for-User-Registration", "//select[contains(@id,'gigya-dropdown')]", ElementFormatter.XPATH);
	public static ElementFormatter firstNameTxtField= 
			new ElementFormatter("First-Name-text-field", "//form[contains(@id,'gigya-register-form')]//input[contains(@name,'profile.firstName')]",
					ElementFormatter.XPATH);
	public static ElementFormatter lastNameTxtField = 
			new ElementFormatter("Last-Name-Text-Field", 
					"//form[contains(@id,'gigya-register-form')]//input[contains(@name,'profile.lastName')]", ElementFormatter.XPATH);
	public static ElementFormatter selectPreferredLanguage =
			new ElementFormatter("selectPreferredLanguage",
					"(//select[@name='data.languageSelector'])[3]",
					ElementFormatter.XPATH);
	public static ElementFormatter enterNewPasswordTextField =
			new ElementFormatter("Enter-New-Password-Text-Field-In-Register-Page",
					"//form[contains(@id,'gigya-register-form')]//input[@name='password']", 
					ElementFormatter.XPATH);
	public static ElementFormatter confirmPasswordTxtField = 
			new ElementFormatter("Confirm-Password-Text-Field", "//form[contains(@id,'gigya-register-form')]//input[@name='passwordRetype']", ElementFormatter.XPATH);
	public static ElementFormatter agreeTermsAndConditionsCheckbox = new ElementFormatter("Agree-Terms & conditons-Checkbox", 
			"//form[@id='gigya-register-form']//div[contains(@class,'age-verify')]//input[@type='checkbox']", ElementFormatter.XPATH);
	public static ElementFormatter subscribeToNewsLetterCheckbox = 
			new ElementFormatter("Subscribe-To-Email-NewLetter-checkbox", 
					"//form[contains(@id,'gigya-register-form')]//input[@name='data.subscribe' and @type='checkbox']",
					ElementFormatter.XPATH);

	public static ElementFormatter confirmEmailAddressField = 
			new ElementFormatter("Email-Address-Confirm-Text-Field-In-Registration-Page",
					"//form[contains(@id,'gigya-register-form')]//input[@type='text' and @name='profile.email']", ElementFormatter.XPATH);
	public static ElementFormatter loginModalHeading = 
			new ElementFormatter("Login-Modal-Heading-Text", "//div[@id='login-container']//h1[contains(@id,'login-container')]", ElementFormatter.XPATH);
	public static final ElementFormatter lybcScreensetForSignup = new ElementFormatter("LYBC-Screenset-Screen-", "//div[contains(translate(@id,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'lybc')]/form[contains(@id,'gigya-profile-form')]", ElementFormatter.XPATH);
	public static final ElementFormatter joinLYBCSignupBtn = new ElementFormatter("Join-LYBC-Button-For-Signup", "//form[@id='gigya-profile-form']//input[@type='submit']", ElementFormatter.XPATH);
	public static final ElementFormatter skipLYBCSignupBtn = new ElementFormatter("Skip-LYBC-Signup-Button", "//form[@id='gigya-profile-form']//input[@type='button']", ElementFormatter.XPATH);
	public static final ElementFormatter lybcConsentCheckbox = new ElementFormatter("Checkbox-for-LYBC-consent", "//div[contains(@id,'login-lybc')]//input[contains(@name,'LYBC') and @type='checkbox']", ElementFormatter.XPATH);
	public static final ElementFormatter birthDateDropdownForLYBC = new ElementFormatter("Date-of-Birth-dropdown-For-LYBC", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthDay')]", ElementFormatter.XPATH);
	public static final ElementFormatter birthMonthDropdownForLYBC = new ElementFormatter("Birth-Month-Dropdown-For-LYBC", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthMonth')]", ElementFormatter.XPATH);
    public static final ElementFormatter birthYearDropdownForLYBC = new ElementFormatter("Birth-Year-dropdown", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthYear')]", ElementFormatter.XPATH);
	
	public static final ElementFormatter guestCheckoutHeading = new ElementFormatter("Guest-Checkout-Screenset-heading", "//cx-checkout-login//h1", ElementFormatter.XPATH);
	public static final ElementFormatter guestUserEmailIdTxtField = new ElementFormatter("Guest-EMail-Id-Text-field", "//cx-checkout-login//input[@formcontrolname='email']", ElementFormatter.XPATH);
	public static final ElementFormatter guestScreensetNextBtn = new ElementFormatter("Guest-screenset-Next-button", "//cx-checkout-login//button[@type='submit']", ElementFormatter.XPATH);
	public static final ElementFormatter signInScreenSetInCheckout = new ElementFormatter("Sign-In-User-Screenset-In-checkout", "//div[contains(@id,'login-checkout')]", ElementFormatter.XPATH);
	public static final ElementFormatter registeredUserEmailIdTxtInSignInScreenset = new ElementFormatter("Registered-Email-Id-Displayed-In-Signin-screenset", "//cx-checkout-login//input[@name='username']/preceding-sibling::span[contains(@class,'display-email')]", ElementFormatter.XPATH);
	public static final ElementFormatter passwordTxtFieldInCheckoutSignIn = new ElementFormatter("Password-Field-In-SignIn-checkout-Screenset", "//cx-checkout-login//input[@name='password']", ElementFormatter.XPATH);
	public static final ElementFormatter signInBtnInCheckoutScreenset = new ElementFormatter("Sign-In-Button-Checkout-Screenset", "//cx-checkout-login//input[@type='submit']", ElementFormatter.XPATH);
    public static final ElementFormatter checkoutAsGuestBtnInCheckoutScreenset = new ElementFormatter("Checkout-AS-Guest-Screenset", "//cx-checkout-login//button[@type='button']", ElementFormatter.XPATH);
    public static final ElementFormatter loginScreenset = new ElementFormatter("Gigya-Login-Screen-set", "//div[@id='login-container_content']", ElementFormatter.XPATH);
}
