package com.tbs.rcom.pageLocators;


import gherkin.lexer.El;
import tbs_master.ElementFormatter;

public class MyAccountPageLocators {

	public static final ElementFormatter myAccountPageHeadingTxt = new ElementFormatter("MyAccount-Page-Heading-text",
			"//app-my-account-landing//div[contains(@id,'accountMainContent')]//h1", ElementFormatter.XPATH);
	public static final ElementFormatter myAccountLinkInNavMenu = new ElementFormatter("My-Account-Link-In-Navigation-menu", 
			"(//app-side-navigation//a[contains(@href,'/my-account')])[1]", ElementFormatter.XPATH);
	public static final ElementFormatter profileLinkInMyAccount = new ElementFormatter("Profile-Link-In-MyAccount-menu", "//cx-custom-link//button[contains(@title,'Login') or contains(@title,'My Account')]", ElementFormatter.XPATH);
	public static final ElementFormatter signedInUserNameTxt = new ElementFormatter("Signed-In-Username-In-MyAccount", 
			"//app-my-account-landing//span[@user-name='{0}']", ElementFormatter.XPATH);
	public static final ElementFormatter signedInEmailIdTxt = new ElementFormatter("Signed-In-UserEmail-Id-Text-In-MyAccount", "//app-my-account-landing//span[@user-email='{0}']", ElementFormatter.XPATH);
	public static final ElementFormatter logoutLink = new ElementFormatter("Logout-Link", "//a[contains(@href,'logout')]", ElementFormatter.XPATH);
	public static final ElementFormatter sectionInMyAccount = new ElementFormatter("The Section in My Account", "//app-side-navigation//a[contains(@href,'{0}')]", ElementFormatter.XPATH);
	public static final ElementFormatter savedPaymentsSectionHeading = new ElementFormatter("Saved Payment-section-in-My-account", "//div[contains(@class,'cx-payment container')]//h1", ElementFormatter.XPATH);
	public static final ElementFormatter lybcScreensetForSignup = new ElementFormatter("LYBC-Screenset-Screen", "//div[contains(@id,'profile-LYBC-screen')]", ElementFormatter.XPATH);
	public static final ElementFormatter joinLYBCSignupBtn = new ElementFormatter("Join-LYBC-Button-For-Signup", "//input[@type='submit' and @value='JOIN']", ElementFormatter.XPATH);
	public static final ElementFormatter skipLYBCSignupBtn = new ElementFormatter("Skip-LYBC-Signup-Button", "//input[@value='SKIP THIS']", ElementFormatter.XPATH);
	public static final ElementFormatter lybcConsentCheckbox = new ElementFormatter("Checkbox-for-LYBC-consent", "//div[contains(@id,'login-lybc')]//input[contains(@name,'LYBC') and @type='checkbox']", ElementFormatter.XPATH);
	public static final ElementFormatter birthDateDropdownForLYBC = new ElementFormatter("Date-of-Birth-dropdown-For-LYBC", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthDay')]", ElementFormatter.XPATH);
	public static final ElementFormatter birthMonthDropdownForLYBC = new ElementFormatter("Birth-Month-Dropdown-For-LYBC", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthMonth')]", ElementFormatter.XPATH);
    public static final ElementFormatter birthYearDropdownForLYBC = new ElementFormatter("Birth-Year-dropdown", "//div[contains(@id,'login-lybc')]//select[contains(@name,'birthYear')]", ElementFormatter.XPATH);
    public static final ElementFormatter nonLYBCUserPtsAndRewardsSectionHeading = new ElementFormatter("Non-LYBC-user-Pts-and-rewards-section-heading", "//div[@id='lybc-container']//h1", ElementFormatter.XPATH);
    public static final ElementFormatter lybcUserPtsAndRewardsSectionHeading = new ElementFormatter("Lybc-User-Pts-and-rewards-section-heading", "//app-loyalty-points-and-rewards//h1", ElementFormatter.XPATH);
    
    public static final ElementFormatter pointsForLYBCUser = new ElementFormatter("Points-for-LYBC-user", "//app-loyalty-points-and-rewards//app-progress-circle//span[2]", ElementFormatter.XPATH);
    public static final ElementFormatter orderHistoryHeadingTxt = new ElementFormatter("Order-History-heading", "//app-order-history//h1", ElementFormatter.XPATH);
    public static final ElementFormatter orderNumInOrderHistory = new ElementFormatter("Order-number-in-Order-history", "(//app-order-history//table//tr)[{0}]//td[1]", ElementFormatter.XPATH);
    public static final ElementFormatter orderDetailsBtnInOrderHistory = new ElementFormatter("Order-details-button-Order-history", "(//app-order-history//table//tr)[2]//a[contains(@href,'order')]", ElementFormatter.XPATH);
    public static final ElementFormatter orderNumInOrderDetails = new ElementFormatter("Order-number-In-Order-DEtails-Section", "//app-order-details-headline//div[contains(@aria-label,'{0}')]", ElementFormatter.XPATH);
    public static final ElementFormatter orderDetailsPageHeadingTxt = new ElementFormatter("Order-details-page-heading", "//app-order-history//div[contains(@class,'cx-order-history-body')][1]", ElementFormatter.XPATH);
    public static final ElementFormatter earnRewardsModalInRewardsSection = new ElementFormatter("Earn-rewards-modal-in-rewards-section", "//div[@id='gigya-update-profile-LYBC-myAccount-screen']", ElementFormatter.XPATH);
    public static final ElementFormatter orderTotalInOrderDetailsSection = new ElementFormatter("Order-total-in-Order-details-section", "(//td[contains(@tabindex,'0')])[{0}]", ElementFormatter.XPATH);
    public static final ElementFormatter cardHolderNameInSavedPayment = new ElementFormatter("Card-Holder-name-in-saved-payment-section", "//app-payment-card//div[contains(@class,'card__holder-name')]", ElementFormatter.XPATH);
    public static final ElementFormatter cardImageInSavedPayments = new ElementFormatter("Card-Image-Saved-Payment-section", "//app-payment-card//div[contains(@class,'card-img')]", ElementFormatter.XPATH);
    public static final ElementFormatter savedPaymentCardNumber = new ElementFormatter("Card-Number-in-Saved-Payment-section", "//app-payment-card//div[contains(@class,'card-number')]", ElementFormatter.XPATH);
    public static final ElementFormatter savedPaymentExpiryDate = new ElementFormatter("Saved-Payment-Expiry date", "//app-payment-card//div[starts-with(@aria-label,'Card number')]/following-sibling::div[contains(@aria-label,'Exp')]", ElementFormatter.XPATH);
    public static final ElementFormatter listOfSavedCards = new ElementFormatter("List-of-payment-cards", "//app-payment-card", ElementFormatter.XPATH);
    
    public static final ElementFormatter addNewAddressBtnInMyaccount = new ElementFormatter("Add-New-Address-Button-In-MyAccount", "(//cx-account-address-book//cx-address-book-list//button)[1]", ElementFormatter.XPATH);
    public static final ElementFormatter listOfAddressesInMyAccount = new ElementFormatter("List-Of-Addresses-In-MyAccount", "//cx-account-address-book//mat-radio-group//mat-radio-button", ElementFormatter.XPATH);
    public static final ElementFormatter removeSavedAddressBtn = new ElementFormatter("Remove-Saved-Address-Button-In-MyAccount", "(//cx-address-book-list//button[@aria-label='Remove'])[1]", ElementFormatter.XPATH); 
    public static final ElementFormatter removeAddressModal = new ElementFormatter("Remove-Address-Modal-MyAccount", "//cx-address-remove-modal", ElementFormatter.XPATH);
    public static final ElementFormatter savedAddressRadioBtn = new ElementFormatter("Saved-Address-Selector-Radio-button", "(//cx-address-book-list//mat-radio-group//mat-radio-button)[1]", ElementFormatter.XPATH);
    public static final ElementFormatter addressRemoveBtnInOverlay = new ElementFormatter("Address-Remove-Button-In-removeAddress-Overlay", "//cx-address-remove-modal//button[@aria-label='Remove']", ElementFormatter.XPATH);
    public static final ElementFormatter referrerIframeAtOrderConfirmation= new ElementFormatter("Referrer-iframe-At-Order-Confirmation-Page", "(//iframe[contains(translate(@id,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'referrerstage1')])", ElementFormatter.XPATH);
    public static final ElementFormatter referrerIframeCloseBtn = new ElementFormatter("Referrer-IFrame-Close-Button", "//div[contains(@id,'root')]//button[contains(translate(@aria-label,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'close')]", ElementFormatter.XPATH);
    public static final ElementFormatter postPurchaseOvderLayCloseBtn = new ElementFormatter("Post-Purchase-Overlay-Referer-Close-Button", "//div[contains(@id,'postpurchase')]//span[contains(@class,'frame-overlay-close-vector')]", ElementFormatter.XPATH);
}
