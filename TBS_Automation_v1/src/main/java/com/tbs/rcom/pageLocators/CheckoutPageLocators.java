package com.tbs.rcom.pageLocators;



import java.util.List;

import org.openqa.selenium.WebElement;

import gherkin.lexer.El;
import tbs_master.ElementFormatter;import tbs_master.SeleniumUtils;

public class CheckoutPageLocators {

	public static ElementFormatter continueToDeliveryOptionsBtn = new ElementFormatter("Continue-To-Delivery-Options-Button", "//button[contains(text(),'Continue')]", ElementFormatter.XPATH);
	public static ElementFormatter backToCartBtn=new ElementFormatter("Back-To-Cart-Button", "//button[contains(text(),'Back to cart')]", ElementFormatter.XPATH);
	public static ElementFormatter addNewAddressBtn =
			new ElementFormatter("Add-New-Address-Button", "//h3[contains(text(),'Deliver to')]/following-sibling::cx-checkout-address-cards//button[contains(@class,'address-btn')]", ElementFormatter.XPATH);
	public static ElementFormatter shipToThisAddressBtn = new ElementFormatter("Ship-To-This-Address-Button", "//a[text()='Ship to this address']", ElementFormatter.XPATH);

	public static ElementFormatter selectTheShippingAddressBtn = new ElementFormatter("Select-Shipping-Address-Button", "(//a[text()='Ship to this address'])[1]", ElementFormatter.XPATH);

	public static ElementFormatter enterAddressManualBtn = new ElementFormatter("Enter-Address-Manually", "//cx-address-loqate//button[contains(@class,'switch-manual')]", ElementFormatter.XPATH);
	public static ElementFormatter continueToPaymentDetailsBtn = 
			new ElementFormatter("Continue-To-Payment-Details-Btn", "//button[contains(text(),'Continue')]", ElementFormatter.XPATH);
	public static ElementFormatter deliveryModeRadioBtn = 
			new ElementFormatter("Select-DeliveryMode-Radio_Btn", 
					"//cx-checkout-delivery-modes//mat-radio-button//input[contains(@id,'{0}')]", ElementFormatter.XPATH) ;
	/*  //cx-checkout-delivery-modes//mat-radio-button//input[contains(@id,'{0}')]/ancestor::div[contains(@class,'mat-radio-container')]  */
	public static ElementFormatter deliveryModeRadioBtnNew =
			new ElementFormatter("deliveryModeRadioBtnNew",
					"//cx-checkout-delivery-modes//mat-radio-button[contains(@id,'{0}')]", ElementFormatter.XPATH) ;

	public static ElementFormatter selectDeliveryModeDropdownBtn= new ElementFormatter("DeliveryOptions_Dropdown_Btn",
			"//h3[contains(text(),'Delivery options')]/ancestor::div[@class='row']/following-sibling::cx-checkout-delivery-modes-cards//button[1]", 
			ElementFormatter.XPATH);

	/*******New Locators for Checkout***********/
    public static ElementFormatter deliveryMethodHeading =
    		new ElementFormatter("Delivery-Method-heading", "//cx-checkout-delivery-address//h3", ElementFormatter.XPATH);
	public static ElementFormatter checkoutPagebreadCrumb = new ElementFormatter("Checkout-Page-Breadcrumb", "//cx-breadcrumb//span[contains(@aria-label,'Checkout')]", ElementFormatter.XPATH);
	public static ElementFormatter fulfillmentMethodRadioBtn = new ElementFormatter(
			"Fulfillment-Type--Radio-button", "//ul[contains(@class,'nav-tabs-delivery')]//li[{0}]//mat-radio-button", ElementFormatter.XPATH) ;

	public static ElementFormatter addNewAddressModal = new ElementFormatter("Enter-Address-Modal", "//cx-custom-address-form", ElementFormatter.XPATH);
	public static ElementFormatter firstNameTxtField = new ElementFormatter("First-Name-Text-Field", "//input[contains(@formcontrolname,'firstName')]", ElementFormatter.XPATH);
	public static ElementFormatter lastNameTxtField = new ElementFormatter("Last-Name-Text-Field", "//input[contains(@formcontrolname,'lastName')]", ElementFormatter.XPATH);
	public static ElementFormatter addressLine1TxtField = new ElementFormatter("Address Line 1 Text Field", "//input[contains(@formcontrolname,'line1')]", ElementFormatter.XPATH);
	public static ElementFormatter addressLine2TxtField = new ElementFormatter("Address Line 2 Text Field", "//input[contains(@formcontrolname,'line2')]", ElementFormatter.XPATH);
    public static ElementFormatter phnNumberTxtField = new ElementFormatter("Phone-Number-Text-field-In-Add-ADdress-modal", "//input[@formcontrolname='phone']", ElementFormatter.XPATH);
	public static ElementFormatter cityTxtField = new ElementFormatter("City-Text-Field", "//input[contains(@formcontrolname,'town')]", ElementFormatter.XPATH);
	public static ElementFormatter zipcodeTxtField = new ElementFormatter("Zipcode-text-field", "//input[contains(@formcontrolname,'post')]", ElementFormatter.XPATH);
    public static ElementFormatter addressSavedForDelivery = new ElementFormatter("Address-Saved-for-Delivery", "//cx-checkout-address-cards//mat-select", ElementFormatter.XPATH);
    public static ElementFormatter CountryField = new ElementFormatter("Country-Field-In-Address-Form", "//div[contains(@formgroupname,'country')]//input", ElementFormatter.XPATH);
    
	public static ElementFormatter setAddressAsDefaultCheckbox = new ElementFormatter("Set-Address-as-Default-Checkbox", "//cx-custom-address-form//mat-checkbox", ElementFormatter.XPATH);
	public static ElementFormatter continueBtnInAddressModal = new ElementFormatter("Continue-Button-In-Address-Modal", "//cx-checkout-delivery-address//button[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter addressCardsFirstNameList= new ElementFormatter("Address-Cards-First-Name-List", "//div[contains(@class,'mat-form-field-address-overla')]//div[contains(@class,'card-body')]//div[contains(@class,'card-label-bold')]", ElementFormatter.XPATH);
	public static ElementFormatter addressContainerAddressLineList=new ElementFormatter("Address-Container-Address-Line-List", "//div[contains(@class,'mat-form-field-address-overla')]//div[contains(@class,'card-body')]//div[contains(@class,'card-label-bold')]/following-sibling::div[1]//div[contains(@class,'card-label')]", ElementFormatter.XPATH);
	public static ElementFormatter selectStateDropdown = 
			new ElementFormatter("Select-state-dropdown", "//div[@formgroupname='country']//mat-select[contains(@formcontrolname,'isocode')]", ElementFormatter.XPATH);
    
	public static ElementFormatter countyField = new ElementFormatter("County-drop-down", "//mat-select[contains(@formcontrolname,'isocode')]", ElementFormatter.XPATH);
	
	public static ElementFormatter dropDownValueByText = new ElementFormatter("Dropdown-Value-By-Text", "//div[contains(@class,'mat-select-panel')]//span[contains(@class,'mat-option-text') and contains(text(),'{0}')]", ElementFormatter.XPATH);
	public static ElementFormatter dropDownValue = new ElementFormatter
			("Dropdown-value", "//div[contains(@class,'mat-select-panel')]//span[contains(text(),'{0}')]", ElementFormatter.XPATH); 
	public static ElementFormatter addNewDeliveryAddress =
			new ElementFormatter("Add-New-Delivery-Address-Text", "//cx-checkout-delivery-address//h2", ElementFormatter.XPATH);
	public static final ElementFormatter addNewAddressFromDropdown = new ElementFormatter("Add-New-Address-From-Address-dropdown", "//mat-option[contains(@class,'address')]//button", ElementFormatter.XPATH);
	public static ElementFormatter selectDeliveryAddress =
			new ElementFormatter("Select-Delivery-Address-Button", "//mat-select-trigger[contains(@class,'card-address-option')]", ElementFormatter.XPATH);
	public static ElementFormatter deliveryAddressList =
			new ElementFormatter("Delivery-Address-List", "//mat-option[contains(@class,'card-address-option')]", ElementFormatter.XPATH);
	public static final ElementFormatter savedAddressSelectedInDropdown = new ElementFormatter("Saved-Address-selected-In-dropdown", "//mat-select-trigger//div[@class='cx-card-container']", ElementFormatter.XPATH);
	public static ElementFormatter selectDeliveryAddress2 =
			new ElementFormatter("Select-Delivery-Address-2", "(//mat-option[contains(@class,'card-address-option')])[2]", ElementFormatter.XPATH);

	public static ElementFormatter savedDeliveryAddress = new ElementFormatter("Saved-delivery-Address-at-Checkout", "//cx-checkout-address-cards/mat-select", ElementFormatter.XPATH);
	public static ElementFormatter editDeliveryAddress =
			new ElementFormatter("Edit-Delivery-Address-Button", "//button[contains(@aria-label,'Edit')]", ElementFormatter.XPATH);

	public static ElementFormatter chooseDeliveryOption =
			new ElementFormatter("Choose-Delivery-Option-Button", "//cx-checkout-delivery-modes-cards//button[contains(@class,'btn mat-form-field-address-btn')]", ElementFormatter.XPATH);

	public static ElementFormatter selectDeliveryOption =
			new ElementFormatter("Select-Delivery-Option-Button", "(//div[contains(@class,'form-check cx-delivery-form-check')]//input[@type='radio'])[1]", ElementFormatter.XPATH);

	public static ElementFormatter addressFirstName= new ElementFormatter("Address-Cards-First-Name-List", "//div[contains(@class,'card-body')]//div[contains(@class,'card-label-bold')]", ElementFormatter.XPATH);


	public static ElementFormatter collectionPointTab = 
			new ElementFormatter("Collection-Point-tab","//div[contains(text(),'Collection point')]/preceding-sibling::div[contains(@class,'mat-radio-container')]" ,ElementFormatter.XPATH);
	public static ElementFormatter collectionPtSearchField =
			new ElementFormatter("Collection-Point-Address-Input", "//input[@formcontrolname='location']", ElementFormatter.XPATH);
	/*public static ElementFormatter collectionPtSearchField =
			new ElementFormatter("Collection-Point-Address-Input", "//cx-checkout-collection-point//input[@formcontrolname='location']", ElementFormatter.XPATH);
 	*/
	public static ElementFormatter collectionPtSearchBtn =
			new ElementFormatter("Collection-Point-Search_Button","//button[@type='submit' and contains(@class,'address-finder')]", ElementFormatter.XPATH);
	public static final ElementFormatter collectionPtResultsModal = new ElementFormatter("Modal-with-Collection-point-results", "//ngb-modal-window[contains(@class,'collection-point')]//div[contains(@class,'collection-point__search-results')]", ElementFormatter.XPATH);
	public static final ElementFormatter listOfCollectionPoints = new ElementFormatter("List-Of-Collection-Points", 
			"//ngb-modal-window[contains(@aria-labelledby,'collection-point')]//app-collection-point-item", ElementFormatter.XPATH);
	public static final ElementFormatter listOfCollectionPtsWithSelectBtnEnabled = 
			new ElementFormatter("List-of-Collection-Points-with-Select-Button", "//app-collection-point-item//button[2]", ElementFormatter.XPATH);
	public static final ElementFormatter unidentifiedLocationErrMsgInStoreModal = 
			new ElementFormatter("Collection-Point--Unidentified-Location-Error-Message", "//*[contains(@class,'alert-danger')]" , ElementFormatter.XPATH);
//	public static final ElementFormatter unidentifiedLocationErrMsgInStoreModal =
//			new ElementFormatter("Collection-Point--Unidentified-Location-Error-Message", "//cx-checkout-collection-point//p[contains(@class,'alert-danger')]" , ElementFormatter.XPATH);

	public static final ElementFormatter collectionPtAddressOfStoreWithStock = new ElementFormatter("Collection-Point-Address-of-Store-with-stock", 
			"(//app-collection-point-item//div[contains(@class,'address')])[{0}]", ElementFormatter.XPATH);
	public static final ElementFormatter selectedCollectionPtAddress = new ElementFormatter("Selected-Collection-Point-Address", "//cx-checkout-collection-point//app-collection-point-item//div[contains(@class,'address')]", ElementFormatter.XPATH);
	
	
	public static ElementFormatter nonStoreCollectionPointChangeButton = 
			new ElementFormatter("Non-Store-Collection-Point-Change-Button", "//div[contains(@class,'collection-point')]//button[contains(text(),'Change')]",ElementFormatter.XPATH);
	public static ElementFormatter nonStoreCollectionPointInfoButton = 
			new ElementFormatter("Non-Store-Collection-Point-Info-Button", "//div[contains(@class,'collection-point')]//button[contains(text(),'Info')]",ElementFormatter.XPATH);
	public static ElementFormatter nonStoreCollectionPointMap = 
			new ElementFormatter("Non-Store-Collection-Point-Map-view", "//div[contains(@class,'collection-point__list-view-map')]", ElementFormatter.XPATH); 
	public static ElementFormatter nonStoreCollectionPointAddressDetails = 
			new ElementFormatter("Non-Store-Collection-Point-Address-Details", "//div[contains(@class,'collection-point-info__address ng-star-inserted')]", ElementFormatter.XPATH);
	public static ElementFormatter nonStoreCollectionPointOpeningHourDetails = 
			new ElementFormatter("Non-Store-Collection-Point-Opening-Hour-Details", "//div[contains(@class,'collection-point-info__opening-hrs')]", ElementFormatter.XPATH);
	public static ElementFormatter nonStoreCollectionPointInfoModalClose = 
			new ElementFormatter("Non-Store-Collection-Point-Info-Close-Button", "//div[contains(@class,'modal-content')]//button[contains(@class,'close')]", ElementFormatter.XPATH);

	public static final ElementFormatter addCollectorDetailModal = new ElementFormatter("Modal-for-adding-Collector-details", "//div[contains(@class,'who-will-collect-section')]", ElementFormatter.XPATH);
	public static ElementFormatter addCollectorDetailsBtn = 
			new ElementFormatter("Add-Who-Will-Collect-details-Button","//cx-who-will-collect//button[1]",ElementFormatter.XPATH);
	public static ElementFormatter collectorFirstNameTxtField = 
			new ElementFormatter("Who-Will-Collect-First-Name","//div[contains(@class,'who-will-collect-section')]//input[@id='firstName']",ElementFormatter.XPATH);
	public static ElementFormatter collectorLastNameTxtField = 
			new ElementFormatter("Who-Will-Collect-Last-Name","//div[contains(@class,'who-will-collect-section')]//input[@id='lastName']",ElementFormatter.XPATH);
	public static ElementFormatter collectorPhnNumberTxtField = 
			new ElementFormatter("Who-Will-Collect-Mobile-Phone","//div[contains(@class,'who-will-collect-section')]//input[contains(translate(@id,'CELLPHONE','cellphone'),'cellphone')]",ElementFormatter.XPATH);
	public static ElementFormatter submitCollectorDetailsBtn = 
			new ElementFormatter("Who-Will-Collect-Details-Submit-Button","//div[contains(@class,'who-will-collect-section')]//button[@type='submit']",ElementFormatter.XPATH);
    public static final ElementFormatter savedCollectorDetails = new ElementFormatter("Saved-collector-details", "//cx-who-will-collect//div[contains(@class,'cx-card-body')]//div[{0}]", ElementFormatter.XPATH);

	public static ElementFormatter state= new ElementFormatter("State-dropdown-elements" , "//mat-Option/span", ElementFormatter.XPATH);

	public static ElementFormatter poRestrictionErrorMessage= new ElementFormatter("PoRestriction-error-message" , "//input[@formcontrolname='line1']/ancestor::div[contains(@class,'field-flex')]/following-sibling::div[@ng-reflect-ng-switch='error']//mat-error", ElementFormatter.XPATH);
	public static ElementFormatter checkoutPageHeading = new ElementFormatter("Checkout-Page-heading", "//cx-checkout-orchestrator//h1", ElementFormatter.XPATH);
	public static ElementFormatter taxAmount = new ElementFormatter("Tax-amount-added", "(//div[contains(@class,'cx-summary-amount')])[4]", ElementFormatter.XPATH);
}

