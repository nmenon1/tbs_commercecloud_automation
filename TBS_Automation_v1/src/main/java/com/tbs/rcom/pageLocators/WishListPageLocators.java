package com.tbs.rcom.pageLocators;

import tbs_master.ElementFormatter;

public class WishListPageLocators {

	public static ElementFormatter userProfileIcon = new ElementFormatter("User-profile-icon", "//cx-custom-link//button[contains(@title,'Login') or contains(@title,'My Account') or contains(@title,'Inicia sesión o Regístrate')]", ElementFormatter.XPATH);
	public static ElementFormatter wishListTitleTxt= new ElementFormatter("Title-heading-for-Wishlist-section", "//h1[contains(@class,'wishlists__title')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistStartShoppingBtn = new ElementFormatter("Start-shopping-CTA-Button-in-Wishlist", "//h1[contains(@class,'wishlists__title')]/parent::app-wishlist-intro/following-sibling::p[1]//button[contains(@class,'wishlists__cta')]", ElementFormatter.XPATH);

	public static ElementFormatter createNewWishlistModal = new ElementFormatter("Create-New-Wishlist-Modal", "//div[contains(@class,'modal-content')]//div[contains(@class,'wishlist')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistIntro = new ElementFormatter("Wishlist-Intro-text","//app-wishlist-intro//p[contains(@class,'intro')]" , ElementFormatter.XPATH);
    public static ElementFormatter createWishlistBtn = new ElementFormatter("Create-Wishlist-Button", "//app-wishlist-intro/following-sibling::p//button[contains(@class,'wishlists__cta')]", ElementFormatter.XPATH);
    public static ElementFormatter wishListNumber = new ElementFormatter("Wishlist-number", "//app-wishlist-lists//h3[contains(@class,'wishlists')]", ElementFormatter.XPATH);	
    public static ElementFormatter wishListNameInput = new ElementFormatter("WishlisT-Name-Input-Field", "(//input[contains(@formcontrolname,'wishlist')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter wishListName= new ElementFormatter("Wishlist-Name-Inside-Wishlist", "//div[contains(@class,'wishlist')]/h1[contains(text(),'{0}')]", ElementFormatter.XPATH);
    public static ElementFormatter emptywishlistMsg = new ElementFormatter("No-products-In-Wishlist-Message", "//div[contains(@class,'wishlist')]//following-sibling::p", ElementFormatter.XPATH);
    public static ElementFormatter wishListLink = new ElementFormatter("Wishlist-Link", "(//a[contains(@class,'wishlists__link')])[{0}]", ElementFormatter.XPATH);
    public static ElementFormatter numberOfWishlistsForUser = new ElementFormatter("Total-Number-Of-Wishlist-For-User", "//a[contains(@class,'wishlists__link')]", ElementFormatter.XPATH);
    public static ElementFormatter wishListItemCount=new ElementFormatter("Wishlist-Count", "//a[@class='wishlists__link']//span[contains(text(),'{0}')]/following-sibling::span[1]/span[contains(@class,'counter-value')]", ElementFormatter.XPATH) ;
    public static ElementFormatter cancelWishList = new ElementFormatter("Cancel-Wishlist-Button", "(//app-wishlist-form//button[contains(@class,'cancel')])[1]", ElementFormatter.XPATH);
    public static ElementFormatter saveWishList = new ElementFormatter("Save-Wishlist-button", "(//app-wishlist-form//button[contains(@type,'submit')])[1]", ElementFormatter.XPATH); 
    public static ElementFormatter backFromWishlistBtn = new ElementFormatter("Back-button-from-wishlist", "//div[contains(@class,'wishlists')]//a[contains(@class,'back')]", ElementFormatter.XPATH);

	
	public static ElementFormatter createWishlistButton= new ElementFormatter("Create-Wishlist-Button", "//button[contains(@aria-label,'Create a wishlist')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistNameField= new ElementFormatter("Wishlist-Name-Field", "//div[contains(@class,'mat-form-field-infix')]/input[contains(@ng-reflect-name,'wishlistName')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistSaveButton= new ElementFormatter("Wishlist-Save-Button", "//button[contains(@class,'btn btn--primary wishlists__cta-save')]", ElementFormatter.XPATH);
	public static ElementFormatter deleteWishlistButton= new ElementFormatter("Delete-Wishlist-Button", "//button[@title='Delete list']", ElementFormatter.XPATH);
	public static ElementFormatter addToExistingWishlistButton= new ElementFormatter("Add-To-Existing-Wishlist-Button", "//span[contains(@class,'h4 wishlists__name')]/following-sibling::span[contains(@class,'add')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistButtonHeader= new ElementFormatter("Wishlist-Button-in-Header", "//cx-icon[contains(@aria-label,'Wishlist link')]", ElementFormatter.XPATH);
	public static ElementFormatter removeWishlistButton= new ElementFormatter("Remove-Wishlist-Button", "//button[text()=' Remove ']", ElementFormatter.XPATH);
	public static ElementFormatter productsInWishlist= new ElementFormatter("Products-in-Wishlist", "//a[contains(@class,'product-tile__name')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistPageLink= new ElementFormatter("Wishlist-Page-Link", "//span[contains(text(),'{0}')]", ElementFormatter.XPATH);
	

}
