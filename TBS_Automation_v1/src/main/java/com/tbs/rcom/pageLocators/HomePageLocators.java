/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import gherkin.lexer.El;
import tbs_master.ElementFormatter;

public class HomePageLocators {
	
    public static ElementFormatter cookiesAcceptButton= new ElementFormatter("Cookies-Accpet-Button", "//div[contains(@id,'onetrust')]/button[contains(@id,'accept')]", ElementFormatter.XPATH);
	public static ElementFormatter homePageLogo = new ElementFormatter("TBSHomePage_Logo", "//img[contains(@src,'tbs-logo')]", ElementFormatter.XPATH);
	public static ElementFormatter marketSelectorDropdown = new ElementFormatter("MarketSelector_dropdown", "//cx-market-selector//mat-select",ElementFormatter.XPATH);
	public static ElementFormatter marketSelectDropdownValue = new ElementFormatter("MarketName-In-Market-Selector-DropDown", "//div[contains(@class,'mat-select')]//mat-option/span[contains(text(),'{0}')]", ElementFormatter.XPATH);
	public final String xpath_TBSMasterCategory = "//nav[contains(@class,'large-down')]//h5[contains(@aria-label,'$')]";
	public final String xpath_SubCategory = "(//a[contains(@ng-reflect-router-link,'/$/c')])[1]";
	public static ElementFormatter searchBtn = new ElementFormatter("Search_Btn", "//cx-page-slot[contains(@class,'SiteLinks')]/cx-searchbox//cx-custom-icon[@role='search']", ElementFormatter.XPATH);
	public static ElementFormatter searchTextField = new ElementFormatter("Search-TextField", "//input[contains(@id,'searchInput')]", ElementFormatter.XPATH);
	public static ElementFormatter searchTextEnteredInTxtField = new ElementFormatter("Search_Results", "//h1[contains(@class,'search-listing-refine__heading')]", ElementFormatter.XPATH);
	public static ElementFormatter searchSuggestionsLinkText = new ElementFormatter("SearchSuggestions_link", "(//a[contains(@class,'search-suggestion__link')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter searchSuggestionHeader = new ElementFormatter("search-suggestion-header", "//div[text()=' Suggestions ']", ElementFormatter.XPATH);
	public static ElementFormatter invalidSearchResultErrMsg = new ElementFormatter("invalid-search-result-error-message", "//div[text()='We could not find any results']", ElementFormatter.XPATH);
	public static ElementFormatter siteRatingImage = new ElementFormatter("Site-rating-image", "//div[contains(@id,'ratingbadge')]", ElementFormatter.XPATH);
	public static ElementFormatter wishlistBtnInHeader = new ElementFormatter("Wishlist-Button-In-Header", "//a[contains(@ng-reflect-router-link,'/wishlist')]//cx-icon[@aria-label='Wishlist link']", ElementFormatter.XPATH); 
	//__old__public static ElementFormatter plpCategoryHeading = new ElementFormatter("PLP-Category-Heading", "(//cx-navigation-ui//nav/a[contains(@href,'{0}')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter plpCategoryHeading = new ElementFormatter("PLP-Category-Heading", "(//cx-navigation-ui//nav/div/button[contains(@title,'{0}')])[1]", ElementFormatter.XPATH);

//__old__public static ElementFormatter plpCategoryOverlay = new ElementFormatter("PLP-Category-Overlay", "((//cx-navigation-ui//nav/a[contains(@href,'{0}')])[1]/parent::nav//div[contains(@class,'childs')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter plpCategoryOverlay =
		new ElementFormatter("PLP-Category-Overlay",
				"(//cx-navigation-ui//nav/div/button[contains(@title,'{0}')])[1]//parent::div[1]//parent::nav//div[contains(@class,'wrapper')][1]", ElementFormatter.XPATH);

//	__old__public static ElementFormatter subCategoryLinkInOverlay =
//			new ElementFormatter("PLP-Subcategory-Link-In-Overlay",
//					"(//cx-navigation-ui//nav/a[contains(@href,'{0}')])[1]/parent::nav//a[contains(@href,'{1}')]"
//					//"(//cx-generic-link//a[contains(@ng-reflect-router-link,'{0}')])[1]"
//					,ElementFormatter.XPATH) ;
	public static ElementFormatter subCategoryLinkInOverlay =
			new ElementFormatter("PLP-Subcategory-Link-In-Overlay",
					"(//cx-navigation-ui//nav/div/button[contains(@title,'{0}')])[1]//parent::div[1]//parent::nav//button[contains(@title,'{1}')][1]"
					//"(//cx-generic-link//a[contains(@ng-reflect-router-link,'{0}')])[1]"
					,ElementFormatter.XPATH) ;
//__old__public static ElementFormatter subCategoryBreadCrumbLink =
//			new ElementFormatter("SubCategory-BreadCrumb-link",
//					"//cx-breadcrumb//span[contains(text(),'{0}')]",
//					ElementFormatter.XPATH);
	public static ElementFormatter subCategoryBreadCrumbLink =
			new ElementFormatter("SubCategory-BreadCrumb-link",
					"//cx-breadcrumb[1]",
					ElementFormatter.XPATH);
	public static ElementFormatter footerLinkWaysToShop = new ElementFormatter("Footer-Link-WaysToShop", "(//app-footer-navigation//h4[contains(@aria-label,'Ways To Shop')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter myAccountBtnInHeader = new ElementFormatter("My-Account-Button-In-Header", "//cx-icon[contains(@class,'my-account icon')]", ElementFormatter.XPATH);
}
