package com.tbs.rcom.pageLocators;



import gherkin.lexer.El;

import tbs_master.ElementFormatter;

public class PaymentPageLocators {
	public static ElementFormatter billingAddressSection = new ElementFormatter("Billing-address-section", "//cx-checkout-payment-address", ElementFormatter.XPATH);
	public static ElementFormatter elm_selectTheBillingAddressBtn = 
			new ElementFormatter("Billing-Address-Select_Btn", "(//a[text()='bill to this address'])[1]", ElementFormatter.XPATH);
	public static ElementFormatter sameAsDeliveryAddressCheckbox = new ElementFormatter("Same-as-Delivery-Address-Checkbox", "//mat-checkbox[contains(@aria-label,'Save to my account')]//input[@type='checkbox']",ElementFormatter.XPATH);
	public static ElementFormatter sameAsDeliveryAddressCheckboxNew = new ElementFormatter("Sane-As-delivery-address-checkbox-temp-RCOM-8827", "//cx-checkout-payment-address//mat-checkbox//input[@type='checkbox']", ElementFormatter.XPATH);
	public static ElementFormatter sameAsDeliveryAddressCheckboxToClick = new ElementFormatter("Same-as-Delivery-Address-Checkbox-To-Uncheck", "//mat-checkbox[contains(@aria-label,'Save to my account')]",ElementFormatter.XPATH);
	public static ElementFormatter cardNumberField 
	= new ElementFormatter("Card-Number-Text-Field", "//input[contains(@id,'encryptedCardNumber')]", ElementFormatter.XPATH);
	public static ElementFormatter billingAddressSavedInCheckout = new ElementFormatter("Billing-Address-Saved-In-Checkout", "//cx-checkout-payment-address//div[contains(@class,'cx-card-label-container')]", ElementFormatter.XPATH);
	public static ElementFormatter expiryDate = 
			new ElementFormatter("Expiry-Date-Field", "//input[contains(@id,'encryptedExpiryDate')]", ElementFormatter.XPATH);
	public static ElementFormatter cardHolderNameField = new ElementFormatter("Card-Holder-Name-Text-Field", "//input[contains(@class,'checkout__card__holderName__input')]", ElementFormatter.XPATH);
	public static ElementFormatter btn_addNewAddress = new ElementFormatter("Add-New-Address-Button", "//button[contains(text(),'Add New Address')]", ElementFormatter.XPATH);

	public static ElementFormatter addNewBillingAddrsBtn = new ElementFormatter("Add-new-Billing-Address-Button", "//cx-checkout-payment-address//button[contains(@aria-label,'Add New Adress')]", ElementFormatter.XPATH); 
	public static ElementFormatter savedCardsBtn =
			new ElementFormatter("Saved-credit-card-Button", "(//div[contains(@id,'payment-method-credit-card')]/following::div[contains(@id,'credit')]//a)[1]", ElementFormatter.XPATH);

	public static ElementFormatter disabledPayNowBtn = new ElementFormatter("Disabled-Pay-Now-Button", "//button[@class='w-100 btn btn-primary mh-100' and @disabled='']", ElementFormatter.XPATH);

	public static ElementFormatter payNowBtn =
			new ElementFormatter("Pay-Now-Button", "//cx-order-summary/following-sibling::div//mat-checkbox[contains(@formcontrolname,'acceptedTandCs')]/parent::div/following-sibling::button", ElementFormatter.XPATH);

	public static ElementFormatter savedCreditCardDropdown =
			new ElementFormatter("Saved-credit-card-dropdown", "//div[@id='payment-method-credit-card']//mat-select", ElementFormatter.XPATH);

	public static ElementFormatter selectCardFromDropDown = new ElementFormatter("Select-Saved-Card-In-Dropdown", "//mat-option['{0}']", ElementFormatter.XPATH);
	public static ElementFormatter cvvtextField =
			new ElementFormatter("CVV text field", "//input[contains(@id,'SecurityCode')]", ElementFormatter.XPATH);

	public static ElementFormatter selectedCardValue = new ElementFormatter("Selected-Card-Value", "//cx-payment-method-accordion//mat-select//span[contains(text(),'*')]", ElementFormatter.XPATH);
	public static ElementFormatter saveCreditCardDetails =
			new ElementFormatter("Save-Creditcard-details-checkbox", "//div[contains(@class,'adyen-checkout')]//following::div//mat-checkbox[contains(@formcontrolname,'saveCardPayment')]", ElementFormatter.XPATH);


	public static ElementFormatter termsAndConditionsCheckbox =
			new ElementFormatter("Accept-Terms-and-Conditions-checkbox", "//mat-checkbox[contains(@formcontrolname,'acceptedTandCs')]//input", ElementFormatter.XPATH);

	public static ElementFormatter tandcCheckBox =
			new ElementFormatter("Accept-TandC-checkbox", "//mat-checkbox[contains(@formcontrolname,'TandC')]", ElementFormatter.XPATH);


	public static ElementFormatter savedCardNumber =
			new ElementFormatter("Saved-Card-Number", "//select[contains(@formcontrolname,'savedCard')]//option[contains(text(),'{0}')]", ElementFormatter.XPATH); 

	public static ElementFormatter selectPayPalMethod =
			new ElementFormatter("PayPAL-Select-RadioButton", "//div[contains(@id,'paypal')]//button", ElementFormatter.XPATH);


	public static ElementFormatter selectPaymentMethod = 
			new ElementFormatter("Select-Payment-Method", "//button[@aria-controls='{0}']", ElementFormatter.XPATH);

	public static ElementFormatter giftCardPaymentMethod = new ElementFormatter("Gift-Card-Payment-Method", "//button[contains(@aria-controls,'gift-card')]", ElementFormatter.XPATH);
	public static ElementFormatter deliveryOptionsHeading =
			new ElementFormatter("Delivery-Options-Heading-In-Checkout", "//cx-checkout-delivery-modes//h3", ElementFormatter.XPATH);
	public static ElementFormatter deliveryOptionsList = 
			new ElementFormatter("Delivery-options-List", 
					"//div[contains(@class,'checkout-delivery-modes')]//mat-radio-group//mat-radio-button", 
					ElementFormatter.XPATH);
	public static ElementFormatter DeliveryOptionInList = 
			new ElementFormatter("Delivery-Option-In-List",
					"(//mat-radio-button[contains(@id,'deliveryMode')])[{0}]", 
					ElementFormatter.XPATH);
	
	public static ElementFormatter listOFDeliveryModes = new ElementFormatter("List-Of-Delivery_modes_available", "//mat-radio-button[contains(@id,'deliveryMode')]", ElementFormatter.XPATH);
	
	public static ElementFormatter DeliveryOptionPrice =
			new ElementFormatter("First-Delivery-option-Price",
					"(//mat-radio-button[contains(@id,'deliveryMode')])[{0}]//div[contains(@class,'price')]",
					ElementFormatter.XPATH);
	public static ElementFormatter addDeliveryOption =
			new ElementFormatter("Add-Delivery-Option", "//cx-checkout-delivery-modes-cards//button/cx-icon", ElementFormatter.XPATH);

	public static ElementFormatter deliveryChargePrice = 
			new ElementFormatter("Delviery-Option-Charge", 
					"//mat-radio-button[contains(@id,'{0}')]//div[contains(@class,'price')]",
					ElementFormatter.XPATH); 

	public static ElementFormatter orderConfirmationMsg=
			new ElementFormatter("Order-confirmation-message","//div[contains(@class,'order-confirmation-message')]",ElementFormatter.XPATH);

	public static ElementFormatter saveCreditCardCheckboxDiv =
			new ElementFormatter("Save-Card-For-Next-Time","//mat-checkbox[contains(@formcontrolname,'saveCardPayment')]//input[@type='checkbox']/parent::div",ElementFormatter.XPATH);

	public static ElementFormatter saveCreditCardCheckbox =
			new ElementFormatter("Save-Card-For-Next-Time","//mat-checkbox[contains(@formcontrolname,'saveCardPayment')]//input[@type='checkbox']",ElementFormatter.XPATH);


	public static ElementFormatter payPalSiteButton =
			new ElementFormatter("PayPAL-Site-Button", "//img[contains(@class,'paypal-logo') and @alt='PayPal']", ElementFormatter.XPATH);


	public static final ElementFormatter paypalEmailTxtField =
			new ElementFormatter("EmailId-Or-Mobile-Number-Field", "//section[contains(@data-title,'Log in to your PayPal')]//input[@id='email']", ElementFormatter.XPATH);

	public static ElementFormatter payPalNextButton =
			new ElementFormatter("Next-Button", "//button[contains(@id,'btnNext')]", ElementFormatter.XPATH);

	public static final ElementFormatter payPalPasswordField =
			new ElementFormatter("Password-Field", "//input[contains(@id,'password')]", ElementFormatter.XPATH);

	public static final ElementFormatter paypalSiteLoginBtn =
			new ElementFormatter("PayPal-login-Button", "//button[contains(@id,'btnLogin')]", ElementFormatter.XPATH);

	
	public static final ElementFormatter paypalAgreeAndPayBtn =
			new ElementFormatter("Paypal-Agree-And-Pay-button", "//button[contains(@id,'submit')]", ElementFormatter.XPATH);
	public static final ElementFormatter paypalPayNowBtn = new ElementFormatter("Paypal-PayNow-Button", "//button[contains(@id,'submit')]", ElementFormatter.XPATH);
	public static final ElementFormatter paypalBankNameDiv = new ElementFormatter("Paypal-BankName-section", "//div[contains(@class,'FundingInstrument_item')]", ElementFormatter.XPATH);
	
	public static ElementFormatter payPalInvalidErrorMessageText =
			new ElementFormatter("PayPal-Invalid-Authorization-Error-Message", "//section[contains(@id,'login')]//p[contains(text(),'Please try again.')]", ElementFormatter.XPATH);
	public static ElementFormatter addNewBillingAddressBtn = new ElementFormatter("Add-New-Billing-Address-Button", "//cx-checkout-payment-address//button[contains(@class,'form-field-address-btn')]//cx-icon", ElementFormatter.XPATH);
	public static ElementFormatter addNewAddressBtnUnderDropdown = new ElementFormatter("Add-new-Address-Button-under-dropdown", "//mat-option[contains(@class,'cx-card-address-btn')]//button", ElementFormatter.XPATH);
	public static ElementFormatter firstNameTxtField = new ElementFormatter
			("First-Name-Text-Field-In-BillingAddress", "//input[contains(@formcontrolname,'firstName')]", ElementFormatter.XPATH);
	public static ElementFormatter lastNameTxtField = 
			new ElementFormatter("Last-Name-In-BillingAddress", "//input[contains(@formcontrolname,'lastName')]", ElementFormatter.XPATH);
	public static ElementFormatter billingAddressOverlay=
			new ElementFormatter("Billing-address-overlay", "//cx-checkout-payment-address//cx-custom-address-form", ElementFormatter.XPATH);
	public static ElementFormatter addressLine1TxtField = new ElementFormatter("Address-Line1-Text-Field-In-BillingAddress", "//input[contains(@formcontrolname,'line1')]", ElementFormatter.XPATH);
	public static ElementFormatter addressLine2TxtField = new ElementFormatter("Address-Line2-Text-Field-In-BillingAddress", "//input[contains(@formcontrolname,'line2')]", ElementFormatter.XPATH);
	public static ElementFormatter cityNameTxtField 
	= new ElementFormatter("City-Name-Txt-Field", "//input[contains(@formcontrolname,'town')]", ElementFormatter.XPATH);
	public static ElementFormatter countryDropdown	
	= new ElementFormatter("Country-Dropdown-For-Billing-Address", "(//div[@formgroupname='country']//mat-select[contains(@formcontrolname,'isocode')]//span)[1]", ElementFormatter.XPATH);
	public static ElementFormatter dropdownValue = new ElementFormatter("Dropdown-Value-For-Country/sate-dropdowns", "//mat-option//span[contains(text(),'{0}')]", ElementFormatter.XPATH) ;
	public static ElementFormatter stateDropdown
	= new ElementFormatter("State-Dropdown-In-BillingAddress", "//mat-select[contains(@formcontrolname,'isocode') and not(contains(@name,'country'))]", ElementFormatter.XPATH);
	public static ElementFormatter zipcodeTxtField = new ElementFormatter("Zipcode-Text-Field-In-BillingAddress", "//input[contains(@formcontrolname,'postalCode')]", ElementFormatter.XPATH);
	public static ElementFormatter setAsDefaultCheckboxForBillingAddress = new ElementFormatter("Set-AS-Default-checkbox", "//mat-checkbox[contains(@formcontrolname,'defaultAddress')]", ElementFormatter.XPATH);
	public static ElementFormatter continueBtnInBillingAddress = new ElementFormatter("Continue-Btn-In-BillingAddressFormModal", "//div[contains(@class,'billing-address')]//button[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter cancelBtnInBillingAddressModal = new ElementFormatter("Cancel-Button-For-BillingAddress-Form-Modal",
			"//mat-checkbox[contains(@formcontrolname,'defaultAddress')]/parent::div/following-sibling::div[contains(@class,'cx-checkout-btns')]//button[@type='submit']/parent::div/preceding-sibling::div//button[@type='button']", ElementFormatter.XPATH);
	public static ElementFormatter PaymentMethodAccordion =
			new ElementFormatter("Payment-Method-Accordion", "//cx-payment-method-accordion", ElementFormatter.XPATH);

	public static ElementFormatter addNewCardBtnInSavedCardsSection =
			new ElementFormatter("Add-New-Credit-card","//div[contains(@id,'credit-card')]//a[contains(@id,'ngb-tab-1')]", ElementFormatter.XPATH);

	public static ElementFormatter creditCardIframe =
			new ElementFormatter("Credit-card-Iframes", "(//iframe[contains(@title,'Iframe for secured card data input field')])[{0}]", ElementFormatter.XPATH);

	public static ElementFormatter klarnaPayLaterIframe = new ElementFormatter("Klarna-Pay-Later-Iframe", "//iframe[contains(@id,'klarna-hpp-instance-main')]", ElementFormatter.XPATH);
	
	public static ElementFormatter klarnaHeadline = new ElementFormatter("Klarna-Headline-in-Payment-Gateway", "h1[style*='Klarna Headline']", ElementFormatter.CSS);
	public static ElementFormatter spendGiftsRadioBtn = 
			new ElementFormatter("Spend-Gifts-radio-button", "//button[contains(@aria-controls,'payment-gift-card')]", ElementFormatter.XPATH);
	public static ElementFormatter giftCardNumberTextField = 
			new ElementFormatter("Gift-Card-Number-Text-Field", "//cx-gift-card-form//input[@formcontrolname='giftCardNumber']", ElementFormatter.XPATH);
	public static ElementFormatter giftCardPinTextField = 
			new ElementFormatter("Gift-Card-Pin-Text-Field", "//cx-gift-card-form//input[@formcontrolname='giftCardPin']", ElementFormatter.XPATH);
	public static ElementFormatter useGiftCardBtn = 
			new ElementFormatter("Use-Gift-Card-Button", "//cx-gift-card-form//button[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter addAnotherGiftCardLink = 
			new ElementFormatter("Add-Another-Gift-Card-Link", "//cx-gift-card-form/following-sibling::span[1]", ElementFormatter.XPATH);

	public static ElementFormatter orderSummaryExcludingDeliveryAmountTxt = 
			new ElementFormatter("Order-Summary-Excluding-Delivery-Amount-Text", "//cx-order-summary-totals//div[contains(@aria-label,'Total')]/following-sibling::div[contains(@class,'amount')]", ElementFormatter.XPATH);

	public static ElementFormatter excludingDeliveryTextInOrderSummary = 
			new ElementFormatter("Exlcuding-Delivery-Text-in-Order-Summary", "//cx-order-summary-totals//div[@aria-label='Total']", ElementFormatter.XPATH);
	public static ElementFormatter giftCardAppliedTxt = new ElementFormatter("Gift-Card-Applied-Text", "(//cx-gift-cards-added-list//td[contains(text(),'{0}')])[1]", ElementFormatter.XPATH);
	public static ElementFormatter giftCardRemoveBtn = new ElementFormatter("Gift-Card-Remove-Button", "(//cx-gift-cards-added-list//td[contains(text(),'{0}')])[1]/following-sibling::td//button[contains(@id,'remove')]", ElementFormatter.XPATH);
	public static ElementFormatter giftCardPaymentMethodHeadingTxt 
	= new ElementFormatter("Gift-Card-Payment-Heading" , "//h3[contains(@aria-label,'Spend gifts')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaPayLaterBuyBtn = new ElementFormatter("Klarna-Pay-Later-Buy-button", "//div[contains(@id,'payment') and contains(@class,'klarna')]/parent::div//button[contains(@id,'buy')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaLogoInExtGateway = new ElementFormatter("Klarna-Logo-In-External-gateway", "//div[@id='klarna-logo']", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaInstallmentSelector = new ElementFormatter("Klarna-Installment-Selector", "//label[contains(@id,'installments-invoice')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaSliceItIframe = new ElementFormatter("Klarna-Slice-it-Payment-details-Iframe", "//iframe[contains(@title,'payment-gateway-frame')]", ElementFormatter.XPATH);
    public static final ElementFormatter cardNumForKlarnaSliceItTxtField = new ElementFormatter("Card-number-text-field-Klarna-Slice-it", "//input[@id='cardNumber']", ElementFormatter.XPATH);
    public static final ElementFormatter cardExpiryFieldForKlarnaSliceIt = new ElementFormatter("Card-Expiry-field-for-Klarna-Slice-it", "//input[@id='expire']", ElementFormatter.XPATH);
    public static final ElementFormatter cvvFieldForKlarnaSliceIt = new ElementFormatter("Cvv-field-For-KlarnaSliceit", "//input[@id='securityCode']", ElementFormatter.XPATH);
    public static final ElementFormatter paypalSiteContinueBtn = new ElementFormatter("Paypal-site-contnue-button", "//button[contains(@ng-click,'continue')]", ElementFormatter.XPATH);
    public static final ElementFormatter payaplSiteLogo = new ElementFormatter("Paypal-site-logo", "//div[@id='paypalLogo']", ElementFormatter.XPATH);
    public static final ElementFormatter orderTotalInPaypalSite = new ElementFormatter("Order-tota-displayed-In-Paypal-site", "//button[contains(translate(@class,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'cart')]/span[@data-testid='header-cart-total']", ElementFormatter.XPATH);
    public static final ElementFormatter orderTotalInPaypalSiteForRegUser = new ElementFormatter("Order-Total-in-PaypalSite-For-Registered-user", "//a[contains(@href,'CartDetails')]//span[contains(@ng-bind-html,'amount')]", ElementFormatter.XPATH);
    public static final ElementFormatter paypalCookies = new ElementFormatter("Accept-Cookies-Button-In-PaypalSite", "//button[@id='acceptAllButton']", ElementFormatter.XPATH);
    public static final ElementFormatter paypalLoginSectionHeading = new ElementFormatter("Paypal-Login-Section-Heading", "//section[contains(@data-title,'Log in to your PayPal account')]//h1", ElementFormatter.XPATH);
    public static final ElementFormatter usernameInPaypalSite = new ElementFormatter("Username-in-Paypalsite", "//p[@id='reviewUserInfo']", ElementFormatter.XPATH);
    public static final ElementFormatter paypalSiteLoadingSpinner = new ElementFormatter("Paypal-site-loading-spinner", "//div[@id='preloaderSpinner']", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaAddressConfirmationFrame = new ElementFormatter("Klarna-Address-Confirmation-IFrame", "//iframe[@id='klarna-hpp-instance-fullscreen']", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaTitleField = new ElementFormatter("Title-Field-In-Klarna-Site", "//select[@name='title']", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaZipCodeField = new ElementFormatter("Zipcode-Field-In-Klarna", "//div[contains(@id,'postal_code')]/input[contains(@id,'postal_code')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaStreetAddressField = new ElementFormatter("Street-Address-In-Klarna", "//div[contains(@id,'street_address')]/input[contains(@id,'street_address')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaCityField = new ElementFormatter("City-Field-in-Klarna", "//div[contains(@id,'city')]/input[contains(@id,'city')]", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaTitleValue = new ElementFormatter("Klanra-Title-Dropdown-Value", "(//div[contains(@data-cid,'title')]//div/li)[1]", ElementFormatter.XPATH);
    public static final ElementFormatter phoneNumberFieldINKlarnaSite = new ElementFormatter("Phone-number-field-in-Klarna-Site", "//div[contains(@id,'phone')]//input[contains(@id,'phone')]", ElementFormatter.XPATH);
    public static final ElementFormatter DOBFieldKlarnaSite = new ElementFormatter("DOB-Field-Klarna-Site", "//input[contains(@id,'date-of-birth' )]", ElementFormatter.XPATH);
    public static final ElementFormatter continueBtnInKlarnaSite = new ElementFormatter("Continue-Button-in-Klarna-Site", "//div[contains(@id,'bottom')]//button", ElementFormatter.XPATH);
    public static final ElementFormatter warningMessageInKlarnaSite = new ElementFormatter("Warning-Message-Box-In-Klarna-Site", "//div[contains(@data-cid,'warning')]", ElementFormatter.XPATH);
    public static final ElementFormatter identityConfirmationModalinKlarna = new ElementFormatter("Identity-COnfirmation-Modal-in-Klarna-Site", "//div[contains(@id,'identification-dialog__container-wrapper')]", ElementFormatter.XPATH);
    public static final ElementFormatter identityConfirmBtnInKlarna = new ElementFormatter("Identity-Confirm-Button-In-Klarna-Site", "//div[@id='identification-dialog__footer-button-wrapper']//button", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaCheckYourDetailsModal=new ElementFormatter("Klarna-check-your-details-modal", "//div[@id='identification-dialog__container-wrapper']", ElementFormatter.XPATH);
    public static final ElementFormatter klarnaCheckYourdetailsIframe = new ElementFormatter("Klarna-Check your details-Iframe", "//iframe[@id='klarna-hpp-instance-fullscreen']", ElementFormatter.XPATH); 
    public static final ElementFormatter klarnaCheckYourDetailsModalConfirmBtn = new ElementFormatter("Klarna-check-your-details-modal-Confirm-Button", "//div[@id='identification-dialog__footer-button-wrapper']//button", ElementFormatter.XPATH);
}
