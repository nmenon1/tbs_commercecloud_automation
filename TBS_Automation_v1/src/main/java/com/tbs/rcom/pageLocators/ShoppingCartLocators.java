package com.tbs.rcom.pageLocators;

import gherkin.lexer.El;
import tbs_master.ElementFormatter;import tbs_master.SeleniumUtils;

public class ShoppingCartLocators {
	public static ElementFormatter proceedToCheckoutBtnInCart = new ElementFormatter("Proceed-To-Checkout-Button", "//cx-cart-totals//button[contains(@class,'checkout')]", ElementFormatter.XPATH);
	public static ElementFormatter removeItemBtn = 
			new ElementFormatter("Remove-Item-From-Cart-Button", 
					"//div[contains(@class,'cart-details')]//button[contains(@title,'Remove')]", ElementFormatter.XPATH);
	public static ElementFormatter cartTitleHeading=new ElementFormatter("Shopping-Cart-Title-Text", "//cx-cart-heading//h1", ElementFormatter.XPATH);
	public static ElementFormatter quantityAdjusterDrpdwn=new ElementFormatter("Quantity-Adjuster-Dropdown", "//select[contains(@class,'basket-select')][1]", ElementFormatter.XPATH);
	public static ElementFormatter basketContentsModal = new ElementFormatter("Basket-contents-Modal", "//cx-cart-item-list//cx-cart-item", ElementFormatter.XPATH);
	public static ElementFormatter listOfProductAddedInCart = 
			new ElementFormatter("List-Of-Products-Added-In-Car", "//cx-cart-item-list//cx-cart-item", ElementFormatter.XPATH);
   public static ElementFormatter emptyCartContinueShoppingBtn = new ElementFormatter("Empty-cart-continue-Shopping-Button", "//cx-cart-empty-paragraph//a[@aria-label='Continue shopping']", ElementFormatter.XPATH);
   public static ElementFormatter basketPageBreadCrumb = new ElementFormatter("Basket-Page-Breadcrumb", "//nav[@aria-label='Breadcrumbs']//span[contains(@aria-label,'Cart Page')]", ElementFormatter.XPATH);
	public static ElementFormatter addGiftMsgBtn =
			new ElementFormatter("Add-Personal-Message-Button", "//cx-cart-gift-wrap-message//button", ElementFormatter.XPATH);
	public static ElementFormatter giftMsgRecipientTxtField = 
			new ElementFormatter("Add-Personal-Message-To-Field","//cx-cart-personal-message-form//input[@formcontrolname='giftMessageName']", ElementFormatter.XPATH);
	public static ElementFormatter addPersonalMessageModal= new ElementFormatter("Add-Personal-Message-Modal", "//button[contains(@class,'close')]/preceding-sibling::h2/parent::div/parent::div", ElementFormatter.XPATH);
	public static ElementFormatter giftMsgSenderTxtField = 
			new ElementFormatter("Add-Personal-Message-From-Field","//cx-cart-personal-message-form//input[@formcontrolname='giftMessageSenderName']", ElementFormatter.XPATH);
	public static ElementFormatter giftMsgTxtField = 
			new ElementFormatter("Add-Personal-Message-Field","//cx-cart-personal-message-form//textarea[@formcontrolname='giftMessage']", ElementFormatter.XPATH);
	
	public static ElementFormatter giftMsgServiceAddBtnInOverlay = 
			new ElementFormatter("Add-Personal-Message-Overlay-Button","//cx-cart-personal-message-form//button[@type='submit']", ElementFormatter.XPATH);
	public static ElementFormatter giftMessageTxtAdded = 
			new ElementFormatter("Add-Personal-Message-Message-Entered","(//cx-cart-gift-wrap-message//button/preceding-sibling::div[contains(@class,'message')])[2]", ElementFormatter.XPATH);
	public static ElementFormatter editPersonalMessageModal = new ElementFormatter("Personal-Message-Editr-Modal", "//button[contains(@class,'close')]/preceding-sibling::h2/parent::div/parent::div", ElementFormatter.XPATH);
	public static ElementFormatter giftMsgServiceEditBtn = 
			new ElementFormatter("Add-Personal-Message-Edit-Button","(//cx-cart-gift-wrap-message//button)[1]", ElementFormatter.XPATH);
	public static ElementFormatter giftMsgServiceRemoveBtn = 
			new ElementFormatter("Add-Personal-Message-Remove-Button","//cx-cart-gift-wrap-message//span[contains(@class,'close')]/parent::button", ElementFormatter.XPATH);

	public static ElementFormatter removePersonalMessageModal = new ElementFormatter("Remove-Personal-Message-Modal", "//button[contains(@class,'close')]/preceding-sibling::h2/parent::div/parent::div", ElementFormatter.XPATH);
	public static ElementFormatter addGiftBoxOption=new ElementFormatter("Add-Gift Wrap-Button", "//cx-cart-gift-wrap-service//button", ElementFormatter.XPATH);

	public static ElementFormatter addGiftWrapModal = new ElementFormatter("Add-Gift-Wrap-Modal", "//button[contains(@class,'close')]/preceding-sibling::h2/parent::div/parent::div", ElementFormatter.XPATH);
	
	public static ElementFormatter removeGiftWrapModal = new ElementFormatter("Remove-Giftwrap-Modal", "//cx-gift-wrap-service-form/parent::div/parent::div", ElementFormatter.XPATH);
	
	public static ElementFormatter addCtaGiftWrapPopup=new ElementFormatter("Add-CTA-In-Gift-Wrap-Popup", "//cx-gift-wrap-service-form//button[(@type='submit')]", ElementFormatter.XPATH);

	public static ElementFormatter removeGiftWrapButton=new ElementFormatter("Remove-Gift Wrap-Button", "//cx-cart-gift-wrap-service//button[@type='button']", ElementFormatter.XPATH);

	public static ElementFormatter removeCtaGiftWrap=new ElementFormatter("Remove-CTA-Gift Wrap-Popup", "//cx-gift-wrap-service-form//button[@type='submit']", ElementFormatter.XPATH);

	public static ElementFormatter cancelCtaGiftWrapPopup=new ElementFormatter("Cancel-CTA-In-Gift-Wrap-Popup", "//cx-gift-wrap-service-form//button[@type='submit']/parent::div/preceding-sibling::div/button", ElementFormatter.XPATH);

	public static ElementFormatter orderSummaryExcludingDelvieryText = new ElementFormatter("Order-Summary-Excluding-delivery", "//cx-order-summary-totals/div[contains(@class,'cx-summary-amount')]", ElementFormatter.XPATH);

	public static ElementFormatter giftWrapPriceText = new ElementFormatter("Gift-Wrap-Price","//cx-cart-gift-wrap-service/p/span", ElementFormatter.XPATH);
	public static ElementFormatter listOfProductNamesInCart = new ElementFormatter("List-Of-Product-Names-In-Basket", "//cx-cart-item//a[contains(@class,'cx-link')]", ElementFormatter.XPATH);
	public static ElementFormatter listOfProductPricesInCart = new ElementFormatter("List-of-Product-Prices-In-Basket", "//cx-cart-item//div[contains(@aria-label,'Product price')]", ElementFormatter.XPATH);
	public static ElementFormatter listOfQtyAddedInBasket = new ElementFormatter("List-of-Quantity-added-For-Prpducts-In-Cart", "//cx-cart-item//cx-cart-quantity-selector//mat-select", ElementFormatter.XPATH);
	public static final ElementFormatter prodNameInBasket = new ElementFormatter("Product-Name-In-Basket", "//cx-cart-item//a[contains(@aria-label,'{0}') and @class='cx-link']", ElementFormatter.XPATH);
}

