/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.actions;
//import org.apache.poi.util.SystemOutLogger;
import org.junit.Assert;

import com.tbs.rcom.pages.PageFactory;
/*********
 *Function:TBSHomePageActions
 *Purpose: Actions in TBS home page
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class PLPPageAction {
	
	PageFactory factory=new PageFactory();
	public PLPPageAction CheckPLPtobeTested() throws Exception {
		
		return this;
		
	}
	
	
	public PLPPageAction validateFilterInPLP() throws Exception {
	
		return this;
	}
	
	public PLPPageAction GoToPDPofFirstProduct() {
		factory.plpPage().tbs_NavigateToPDP();
		return this;
	}
	
	/**
	 * Apply the Sort option and validate if the Sorting is applied
	 * @author Edwin_John
	 * @param sortType
	 * @return
	 */
	public PLPPageAction applyAndValidateIfSortingIsApplied(String sortType) {
		factory
		.plpPage()
		.selectOptionFromSortDropdown(sortType)
		.verifyIfSortingIsApplied();
		return this;
	}
	
	/**
	 * @author arjun.vijay
	 * Method to Add to Cart from PLP
	 *
	 */
	
	public PLPPageAction addItemToCartFromSeeOptions(int position)
	{
		factory
		.plpPage()
		.clickOnSeeOptionsButtonAndAddToCart(position);
		return this;
	}
	
	/**
	 * @author nithin.c03
	 * Method to validate See Options modal
	 *
	 */
	
	public PLPPageAction SeeOptionsModalValidations(int qty) throws Exception
	{
		//factory.plpPage().clickOnSeeOptionsButtonInPLP(1);
		factory.plpPage().updateQtyInSeeOptionsModal(qty);
		factory.plpPage().tbs_SeeOptionsModalClose();
		return this;
	}
	
	/**
	 * @author nithin.c03
	 * Method to add to cart with quantity adjust
	 *
	 */
	
	public PLPPageAction updateQtyInSeeOptionsModal(int index){
		factory
		.plpPage()
		.updateQtyInSeeOptionsModal(index);
		return this;
	}
	
	public PLPPageAction addToBagFromPLP(int position){
		factory
		.plpPage()
		.addToBagFromPLP(position);
		return this;
	}

	/**
	 * @author nithin.c03
	 * Method to click on See Options button for specific product using product ID
	 *
	 */
	
	public PLPPageAction productSpecificSeeOptionsClick(String productID) throws Exception{
		factory.plpPage().productSpecificSeeOptionsClick(productID);
		return this;
	}
	
	
	/**
	 * @author nithin.c03
	 * Method to opt in for notification email of Out of Stock variable
	 *
	 */
	
	public PLPPageAction emailMeWhenInStockOptIN() throws Exception{
		factory
		.plpPage()
		.emailMeWhenInStockValidation();
		return this;
	}
	
	/**
	 * @author nithin.c03
	 * Method to navigate to a specific PDP using product ID
	 */
	public PLPPageAction productSpecificPDPNavigation(String productID) throws Exception{
		factory.plpPage().productSpecificPDPNavigation(productID); 
		return this;
	}	
	
	
}
