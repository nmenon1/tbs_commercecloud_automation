package com.tbs.rcom.actions;

public class BaseAction {

	public HomePageAction homePageAction() {
		return new HomePageAction();
	}
	public WishlistAction wishlistAction(){
		return new WishlistAction();
	}
	
	public PLPPageAction plpAction() {
		return new PLPPageAction();
	}
	
	public MiniCartPageActions miniCartAction() {
		return new MiniCartPageActions();
	}
	
	public ShoppingCartPageActions shoppingCartAction() {
		return new ShoppingCartPageActions();
	}
	
	public PDPPageAction pdpAction() {
		return new PDPPageAction();
	}
	
	public CheckoutAction checkoutAction() {
		return new CheckoutAction();
	}
	
	public PaymentPageAction paymentAction() {
		return new PaymentPageAction();
	}
	
	public SearchAction  searchAction() {
		return new SearchAction();
	}
	
	public LoginAction loginAction() {
		return new LoginAction();
	}
	
	public MyAccountAction myAccountAction() {
		return new MyAccountAction();
	}
}
