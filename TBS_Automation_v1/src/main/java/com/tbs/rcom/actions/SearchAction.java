package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;

public class SearchAction {

	PageFactory factory=new PageFactory();
	
	public SearchAction NavToSRP() throws Exception
	{
		factory.searchResultsPage().NavToSRP();
		return this;
		
	}
	
	public SearchAction SearchResultValidation()
	{
		factory.searchResultsPage().validateZeroSearchResult();
		return this;
	}
	
	public SearchAction validBarcodeSearchResultValidation() throws Exception
	{
		factory.searchResultsPage().validBarcodeSearchResultValidation();
		return this;
	}
	
	/**
	 * Click on the product title by position displayed in SRP
	 * @author Edwin_John
	 * @param position
	 * @return
	 */
	public SearchAction clickOnTheProductByPositionInSRP(int index) {
		factory
		.searchResultsPage()
		.clickOnTheProductByPositionDisplayedInSRP(index);
		return this;
	}
	
	
}
