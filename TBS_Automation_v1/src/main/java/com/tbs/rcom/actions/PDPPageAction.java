/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.actions;
import org.junit.Assert;

import com.tbs.rcom.pages.PageFactory;
/*********
 *Function:TBSHomePageActions
 *Purpose: Actions in TBS home page
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class PDPPageAction {

	PageFactory factory=new PageFactory();
	public PDPPageAction validatePDPContents() throws Exception {
		factory.pdpPage().verifyPDPPageContents()
		.verifyCurrencySymbolInPDP()
		.verifyPriceMetricInPDP();
		return this;
	}



	public PDPPageAction TBSAddToCartActions(int NoOfQuantity  ) throws Exception {
		factory.pdpPage().selectColorVariant()
		.selectTheQtyInPDP(NoOfQuantity)
		.clickOnAddToCartFromPDP();
		return this;


	}
	/**
	 * @author alwin.ashley
	 * Add to cart actions	
	 * @throws Exception
	 */
	public PDPPageAction TBSPDPpageActions(int NoOfQuantity) throws Exception {
		factory.pdpPage().selectColorVariant();
		factory.pdpPage().selectTheQtyInPDP(NoOfQuantity);

		return this;

	}
	/**
	 * @author alwin.ashley
	 * Add item to cart from PDP	
	 * @throws Exception
	 */
	public PDPPageAction addItemToCartFromPDP(int qty) throws Exception {
		factory
		.pdpPage()
		.selectTheQtyInPDP(qty)
		.clickOnAddToCartFromPDP();
		return this;

	}
	/**
	 * @author alwin.ashley
	 * 	Verify Product added to basket
	 * @throws Exception
	 */
	public PDPPageAction verifyItemIsAddedToCartAndInterstitialDisplayed() throws Exception {
		factory
		.pdpPage()
		.verifyItemIsAddedToCartAndInterstitialDisplayed();
		return this;

	}
	/**
	 * @author alwin.ashley
	 * Navigate to Basket
	 * @throws Exception
	 */	
	public PDPPageAction navigateToCartByClickingViewBasketInAddToBagInterstitial(){
		factory
		.pdpPage()
		.clickOnViewBasketBtnInInterstitial();
		return this;				
	}

	/**
	 * @author nithin.c03
	 * Method to navigate to product category using breadcrumb link/**
	 */	
	public PDPPageAction categoryNavigationViaBreadcrumb() throws Exception{
		factory
		.pdpPage()
		.categoryNavigationViaBreadcrumb();
		return this;
	}

	public PDPPageAction updateStockForItem(String stock,String productId,String warehouseCode) {
		factory.pdpPage()
		.updateStockForItemInWarehouse(stock, productId, warehouseCode);
		return this;
	}
	
	/**
	 * Validate the Product details in PDP and verify whether the details are matching
	 * @author Edwin_John
	 * @return
	 */
	public PDPPageAction validateProductDetailsInPDP() {
		factory
		.pdpPage()
		.validateProductDetailsInPDP();
		return this;
	}
	public PDPPageAction closeAddToCartInterstitial()
	{
		factory
		.pdpPage()
		.closeAddToCartInterstitial();
		return this;
	}
	/**
	 * @author Arjun
	 * Temporary methods
	 * @return
	 */
	public PDPPageAction navigateToSRP(String productID, String market)
	{
		factory
		.pdpPage()
		.navigateToSRP(productID,market);
		return this;
	}
}
