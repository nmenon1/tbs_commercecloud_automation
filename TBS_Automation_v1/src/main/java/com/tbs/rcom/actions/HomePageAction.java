/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.actions;
import org.junit.Assert;

import com.tbs.rcom.pages.PageFactory;
/*********
 *Function:TBSHomePageActions
 *Purpose: Actions in TBS home page
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class HomePageAction {

	PageFactory factory=new PageFactory();
	public HomePageAction goToHomePageOfCustomMarket(String expMarket) {
		factory.homePage()
		.validateIfHomePageIsDisplayed()
		.selectMarketFromDropdown(expMarket);
		return this;
	}

	public HomePageAction hoverOverPLPAndValidateOverlay(String plpName) {
		factory
		.homePage()
		.hoverOverPLP(plpName);
		return this;
	}
	public HomePageAction clickOnSubcategoryLinkInOverlay(String subCategoryName) {
		factory.homePage()
		.clickOnTheSubcategoryLink(subCategoryName);
		return this;
	}
	
	public HomePageAction enterSearchKeywordInTextField(String Keyword) {
		factory
		.homePage()
		.enterSearchKeyword(Keyword);
		return this;
	}

	public HomePageAction enterKeywordInSearchTextField(String Keyword) throws Exception {
		factory.homePage().enterSearchKeywordInTxtField(Keyword);
		return this;
	}
	public HomePageAction TBS_Display_Of_Suggestions() throws Exception {
		factory.homePage().validateSearchSuggestionsAreDisplayed();
		return this;
	}
	public HomePageAction TBS_Click_On_Suggestion() throws Exception {
		factory.homePage().clickOnSearchSuggestionTxt();
		return this;
	}
	public HomePageAction TBS_AccessingTheSRP(String sychkwd) throws Exception {
		factory.homePage().validateUserIsInSearchResultsPage(sychkwd);
		return this;
	}

	public HomePageAction TBS_SearchingInvalidKeywords() throws Exception {
		factory.homePage().validateSuggestionErrMsgForInvalidKeywords();
		return this;
	}

	//Site Rating Badge actions
	public HomePageAction validatingSiteRatingBadgeDisplayed() throws Exception {
		factory.homePage().validateSiteRatingBadgeDisplayed();
		return this;
	}
	public HomePageAction validatingSiteRatingBadgeNotDisplayed() throws Exception {
		factory.homePage().validateSiteRatingBadgeNotDisplayed();
		return this;

	}
	
	public HomePageAction clickOnWishlistBtnInHeader() {
		factory.homePage()
		.clickOnWishlistBtnInHeader();
		return this;
	}
	
	public HomePageAction clickOnMyAccountLinkInHeader() {
		factory
		.homePage()
		.clickOnMyAccountBtnInHeader();
		return this;
	}
}
