package com.tbs.rcom.actions;

import com.tbs.rcom.pageLocators.PaymentPageLocators;
import com.tbs.rcom.pages.PageFactory;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.PaymentData;

import tbs_master.SeleniumUtils;

public class PaymentPageAction {
	
	PageFactory factory=new PageFactory();
	
	/**
	 * @author alwin.ashley
	 * Selecting billing address
	 * @throws Exception
	 */
	public PaymentPageAction selectingBillingAddress() throws Exception {
		factory.paymentPage().clickOnSameAsDeliveryCheckbox();
				return this;
	}
	
	
	
	/**
	 * @author alwin.ashley
	 * clicking Finish and Pay button	
	 * @throws Exception
	 */
	public PaymentPageAction clickFinishAndPay() throws Exception {
		factory.paymentPage().proceedToFinishAndPay();
				return this;
	}
	
	/**
	 * Enter payment details in form modal
	 * @author Edwin_John
	 * @throws Exception 
	 */
	public PaymentPageAction enterPaymentDetails(PaymentData paymentData) throws Exception {
		factory
		.paymentPage()
		.enterCreditCardDetails(paymentData);
		return this;
	}

	public PaymentPageAction selectPaymentMethod(String payOption) {
		factory
		.paymentPage()
		.paymentMethodSelector(payOption);
		return this;
		
	}
	

    public PaymentPageAction selectSavedCreditCard() throws Exception
    {
    	factory
    	.paymentPage()
		.selectSavedCreditCard();
    	factory
    	.paymentPage()
    	.validateSelectedSavedCardDetails();
    	return this;
    }

	public PaymentPageAction approvePaymentForPaypal() {
		factory
		.paymentPage()
		.approvePaymentForPaypal();
		return this;
	}
	
	public PaymentPageAction clickOnPayPalSite() {
		factory
		.paymentPage()
		.selectPaypalSitePayment();
		return this;
	}
	


	public PaymentPageAction clickOnPayNowButton() {
		factory.paymentPage()
		.clickOnPayNowButton();
		return this;
		
	}

	
	public PaymentPageAction loginAsValidPaypalUser(PaymentData testpaypal) throws Exception{
		factory.paymentPage().loginIntoPayPalAccount(testpaypal);
		return this;
	}
	
	public PaymentPageAction makingPaymentPayPal() throws Exception{
		factory.paymentPage().paymentViaPaypal();
		return this;
	}
	
	public PaymentPageAction validationOfInvalidPayPalUser() throws Exception{
		factory.paymentPage().paymentViaPaypalAsInvalidUser();
		return this;
	}

	public PaymentPageAction addNewCardFromSavedCardSection() {
		factory
		.paymentPage()
		.addNewCardInSavedCardsSection();
		return this;
	}
	
	public PaymentPageAction addBillingAddressInCheckout(String type,AddressDetails data,String country) {
 		factory
 		.paymentPage()
		.addNewBillignAddressInCheckout(type,data.addressData,country);
 		return this;
 	}

	public PaymentPageAction unCheckSameAsDeliveryAddressForBillingAddress() {
		factory.paymentPage().clickToUnselectCheckboxForSameAsDeliveryAddress();
		return this;
		
	}
	
	public PaymentPageAction calculateOutstandingAmountInOrderSummary() {
		factory
		.paymentPage()
		.calculateOutstandingAmountInOrderSummary();
		return this;
	}
	
	public PaymentPageAction enterGiftCardDetailsAndApply(String giftCardNumber,String giftCardPIN) {
		factory
		.paymentPage()
		.enterGiftCardDetails(giftCardNumber, giftCardPIN);
		return this;
	}
	
	public PaymentPageAction validateGiftCardIsApplied() {
		factory
		.paymentPage()
		.validateGiftCardIsApplied();
		return this;
	}
	
	public PaymentPageAction validateIfAddAnotherGiftCardLinkDisplayed() {
		factory
		.paymentPage()
		.addAnotherGiftCardIfPayableAmtIsRemaining();
		return this;
		
	}
	
	public PaymentPageAction validateIfSameAsDeliveryAddressCheckboxIsSelected() {
		factory
		.paymentPage()
		.validateIfSameAsShippingAddressCheckboxIsSelected();
		return this;
	}
}
