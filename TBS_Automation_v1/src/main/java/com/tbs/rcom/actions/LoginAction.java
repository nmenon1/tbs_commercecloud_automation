package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;
import com.tbs.rcom.yaml.LoginData;
import com.tbs.rcom.yaml.LoginDetails;

public class LoginAction {

	PageFactory factory=new PageFactory();

	/**
	 * @author alwin.ashley
	 * Login Actions
	 * @throws Exception
	 */
	public LoginAction enterCredentialsInLoginModal(String userName){
		factory.loginPage()
		.enterLoginDetailsInCheckout(userName)
		.clickOnSignInBtn();
		return this;
	}
	
	/**
	 * Register as a new User from Checkout Login page
	 * @author Edwin_John
	 */
	public LoginAction registerAsNewUser(LoginData data) {
		factory.loginPage()
		.clickOnRegisterBtn()
		.registerNewUser(data);
		return this;
	}
	
	public LoginAction registerNewUser(LoginData data) {	
		factory
		.loginPage()
        .clickOnRegisterBtn()
		.registerNewUser(data)
		.signupForLYBC();
		return this;
	}
	
	public LoginAction validateIfLoginModalIsDisplayed() {
		factory
		.loginPage()
		.validateLoginModalDisplayedInMyAccount();
		return this;
	}
	
	public LoginAction validateGuestCheckoutScreensetAndEnterEmailID(String userType) throws InterruptedException {
		factory
		.loginPage()
		.enterGuestEmailIdInCheckout(userType);
		return this;
	}
	
	public LoginAction verifyRegEmailTxtAndSignInCheckoutScreenset(String flowType) {
		factory
		.loginPage()
		.validateSignInCheckoutScreenset()
		.enterPasswordAndClickSignInCheckout(flowType);
		return this;
	}
	
}
