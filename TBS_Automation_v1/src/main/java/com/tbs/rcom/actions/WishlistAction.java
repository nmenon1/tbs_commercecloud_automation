package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;

public class WishlistAction extends BaseAction{

	PageFactory factory = new PageFactory();
	/**
	 * Validate the empty wishlist for a guest user
	 * @author Edwin_John
	 * @return
	 */
	public WishlistAction validateEmptyWishlistForGuestUser() {
		factory.wishlistPage()
		.validateWishlistForGuestUser();
		return this;
	}

	public WishlistAction validateEmptyWishlistPageForNewlyRegisteredUser()
	{
		factory
		.wishlistPage()
		.validateEmptyWishlistPageForNewlyRegisteredUser();
		return this;
	}
	public WishlistAction clickOnCreateNewWishlistAndEnterName()
	{
		factory
		.wishlistPage()
		.clickOnCreateNewWishlistAndEnterName();
		return this;
	}
	public WishlistAction clickOnCancelInCreateWishlistModal()
	{
		factory
		.wishlistPage()
		.clickOnCancelInCreateWishlistModal();
		return this;
	}
	public WishlistAction validateNewWishListIsCreated()
	{
		factory
		.wishlistPage()
		.validateNewWishListIsCreated();
		return this;
	}
	public WishlistAction validateNewWishListEmpty()
	{
		factory
		.wishlistPage()
		.validateNewWishListIsEmpty();
		return this;
	}
	public WishlistAction clickOnSaveInCreateWishlistModal()
	{
		factory
		.wishlistPage()
		.clickOnSaveInCreateWishlistModal();
		return this;
	}
	public WishlistAction addProductFromPLPToNewWishlist() {
		factory.wishlistPage()
		.clickAddToWishlistButton()
		.createWishlist();
		return this;
	}
	
	
	public WishlistAction addProductFromPLPToExistingWishlist() {
		factory.wishlistPage()
		.clickAddToWishlistButton()
		.clickAddtoExistingListButton();
		return this;
	}
	public WishlistAction deleteExistingWishlist() {
		
		factory.wishlistPage()
		.clickDeleteListButton();
		return this;
		
	}
	public WishlistAction navigateToWishlistPage() {
		factory.wishlistPage()
		.clickToNavigateToWishlistPage();
		return this;
		
	}
	public WishlistAction addProductFromPDPToExistingWishlist() {
		factory.wishlistPage()
		.clickAddToWishlistButtonFromPDP()
		.clickAddtoExistingListButton();
		return this;
		
	}
	public WishlistAction addProductFromPDPToNewWishlist() {
		factory.wishlistPage()
		.clickAddToWishlistButtonFromPDP()
		.createWishlist();
		return this;

		
	}
	public WishlistAction validateTheProductAddedToWishlist() {
		factory.wishlistPage()
		.validateWishlist();
		return this;
		

	}
}

