package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;
import com.tbs.rcom.yaml.AddressData;

public class MyAccountAction {

	PageFactory factory = new PageFactory();
	
	public MyAccountAction validateUserIsSignedIn() {
		factory
		.myAccountPage()
		.validateUserIsSignedIn();
		return this;
	}
	
	public MyAccountAction clickOnLogoutLink() {
		factory
		.myAccountPage()
		.clickOnLogutLink();
		return this;
	}
	
	public MyAccountAction verifyUserIsSignedUpForLYBC() {
		factory
		.myAccountPage()
		.verifyLoyaltyMembershipInMyAccount();
		return this;
	}
	
	public MyAccountAction clickOnReqSectionInMyAccount(String section) {
		factory
		.myAccountPage()
		.clickOnRequiredSectionInMyAccount(section);
		return this;
	}
	
	/**
	 * Validate Order number is displayed in Order history page and Order details are matching in Order details page
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountAction validatePlacedOrderIsDisplayedInORderHistory() {
		factory
		.myAccountPage()
		.validateOrderDisplayedInOrderHistory()
		.validateOrderDetailsPage();
		return this;
	}
	
	public MyAccountAction validateSavedCardInMyAccount() {
		factory
		.myAccountPage()
		.validateSavedCardDetails();
		return this;
	}
	
	public MyAccountAction validateNewAddressModalOpened()
	{
		factory
		.myAccountPage()
		.validateNewAddressModalOpened();
		return this;
	}
	
	public MyAccountAction validateRemoveSavedAddressInMyAccount()
	{
		factory
		.myAccountPage()
		.validateRemoveSavedAddressInMyAccount();
		return this;
	}
	public MyAccountAction enterNewAddressInMyAccount(AddressData addressData)
	{
		factory
		.myAccountPage()
		.enterNewAddressInMyAccount(addressData);
		return this;
	}
}
