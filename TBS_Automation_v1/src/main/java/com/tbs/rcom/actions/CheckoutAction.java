package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;
import com.tbs.rcom.yaml.AddressData;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.WhoWillCollectData;
import com.tbs.rcom.yaml.WhoWillCollectDetails;

public class CheckoutAction {

	PageFactory factory=new PageFactory();
	/**
	 * @author alwin.ashley
	 * Proceed to delivery mode page	
	 * @throws Exception
	 */
	public CheckoutAction proceedToDeliveryModePage() throws Exception {
		factory.shippingAddressPage().selectShippingAddress();
		return this;
	}

	//Delivery mode page----------
	/**
	 * @author alwin.ashley
	 * Select a delivery option	
	 * @throws Exception
	 */
	public CheckoutAction selectDeliveryOption(String deliveryMode) throws Exception {
		factory
		.shippingAddressPage()
		.selectDeliveryMode(deliveryMode);
		return this;
	}

	/**
	 * Enter new Address in form modal
	 * @author Edwin_John
	 * @param add
	 * @return
	 * @throws Exception 
	 */
	public CheckoutAction enterNewAddress(AddressData addressData) {
		factory.shippingAddressPage()
		.enterNewShippingAddress(addressData);
		return this;
	}

	public CheckoutAction clickOnEditAddressBtn() {
		factory.shippingAddressPage().clickOnEditAddressBtn();
		return this;
	}

	public CheckoutAction validateTheAddressIsAdded() {
		factory
		.shippingAddressPage()
		.validateAddressIsAdded();
		return this;
	}

	public CheckoutAction validateTheEditedAddress() {
		factory.shippingAddressPage().validateEditedAddress();
		return this;
	}

	public CheckoutAction editExistingDeliveryAddress() throws Exception {
		factory.shippingAddressPage().editDeliveryAddress();
		return this;
	}

	/**
	 * @author nithin.c03
	 * Non Store collection	
	 * @throws Exception
	 */	

	public CheckoutAction selectNonStoreCollectionPoint(String location){
		factory
		.checkoutPage()
		.enterLocationInCollectionPointModal("Invalid")
		.enterLocationInCollectionPointModal(location)
		.selectCollectionPointFromModal();
		return this;

	}

	public CheckoutAction enterCollectorDetails(){
		factory
		.checkoutPage()
		.enterCollectorDetailsInCheckout();
		return this;
	}

	/**
	 * @author harish.prasanna
	 * PO restriction 
	 * @throws Exception
	 */
	public CheckoutAction validationOfPoRestrictionErrorMessage(){
		factory
		.shippingAddressPage()
		.validationOfPoRestrictedErrorMessage();
		return this;
	}

	/**
	 * Select the Fulfillment type
	 * @author Edwin_John
	 * @param fulfillmentType
	 * @return
	 */
	public CheckoutAction selectFulfillmentType(String fulfillmentType) {
		factory
		.checkoutPage()
		.selectFulfillmentMethodInCheckout(fulfillmentType);
		return this;
	}

	public CheckoutAction enterSearchKeywordInCISModal(String keyword) {
		factory
		.storeDetailsPage()
		.enterSearchKeywordInCISSection(keyword);
		return this;
	}

	public CheckoutAction validateErrorMsgForInvalidStoreSearch() {
		factory
		.storeDetailsPage()
		.validateGlobalErrorMsgForInvalidStoreSearchKeyword();
		return this;
	}

	public CheckoutAction selectStoreWithStockFromStoreSearchModal() {
		factory
		.storeDetailsPage()
		.selectCISStoreWithStockFromList();
		return this;
	}

	public CheckoutAction validateSelectedStoreIsDisplayedInCISSection() {
		factory
		.storeDetailsPage()
		.validateSelectedStoreIsDisplayedInCIS();
		return this;
	}

	/**
	 * Validate if Order Confirmation page is displayed and Order number is fetched
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutAction validateOrderConfirmationAndIsDisplayedwithOrderNumber() {
		factory
		.orderConfPage()
		.validateOrderConfirmationPageIsDisplayed()
		.fetchOrderNumberAndOrderDate();
		return this;
	}

	public CheckoutAction verifyUserNavigatedToCheckoutAndDeliveryIsSelected() {
		factory
		.checkoutPage()
		.verifyUserNavigatedToCheckout()
		.validateDeliveryIsSelectedAsDefaultFulfillmentInCheckout();
		return this;
	}

	/**
	 * Register Guest user in Order confirmation page
	 * @author Edwin_John
	 * @param userType
	 * @return
	 */
	public CheckoutAction signupGuestToCommerceInOrderConfirmationPage(String userType) {
		factory
		.orderConfPage()
		.clickRegisterAndValidateFormIsPrepopulated()
		.enterPasswordInRegFormAndRegister(userType);
		
		factory
		.loginPage()
		.signupForLYBC();
		return this;

	}
	
	/**
	 * Signup for LYBC in order confirmation page
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutAction signupForLYBCInOrderConfirmationPage() {
		factory
		.orderConfPage()
		.clickSignupForLYBCFromOrderConfPage();
		return this;
	}
	
	public CheckoutAction payUsingKlarnaPayLater() {
		factory
		.paymentPage()
		.clickBuyBtnInKlarnaPayLater();
		return this;
	}
	
	public CheckoutAction verifyCISIsHiddenForGiftWrapOrder()
	{
		factory
		.checkoutPage()
		.verifyCISIsHiddenForGiftWrapOrder();
		return this;
	}
	public CheckoutAction navigateBackToBasketPagefromCheckoutPage()
	{
		factory
		.checkoutPage()
		.navigateBackToBasketPagefromCheckoutPage();
		return this;
	}
	
	/**
	 * @author Arjun
	 * Validate Klarna site loaded
	 */
	public CheckoutAction validateKlarnaSiteLoaded()
	{
		factory
		.checkoutPage()
		.validateKlarnaSiteLoaded();
		return this;
	}
}
