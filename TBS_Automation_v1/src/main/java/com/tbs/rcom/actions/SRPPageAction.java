package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;

public class SRPPageAction {

	PageFactory factory=new PageFactory();
	
	public SRPPageAction NavToSRP() throws Exception
	{
		factory.searchResultsPage().NavToSRP();
		return this;
		
	}
	
	public SRPPageAction SearchResultValidation()
	{
		factory.searchResultsPage().validateZeroSearchResult();
		return this;
	}
	
	public SRPPageAction validBarcodeSearchResultValidation() throws Exception
	{
		factory.searchResultsPage().validBarcodeSearchResultValidation();
		return this;
	}
}
