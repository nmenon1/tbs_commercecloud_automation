package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;

public class MiniCartPageActions {

	PageFactory factory=new PageFactory();

	public MiniCartPageActions validateProductAddedIsPresentInMiniCart() {
		factory
		.miniCartPage()
		.validateProductAddedIsPresentInMiniCartOverlay();
		return this;

	}
	public MiniCartPageActions hoverOverMiniCart()
	{
		factory
		.miniCartPage()
		.hoverOverMiniCart();
		return this;
	}
	public MiniCartPageActions verifyUserNotNavigatedToEmptyBasket()
	{
		factory
		.miniCartPage()
		.verifyUserNotNavigatedToEmptyBasket();
		return this;
	}
	public MiniCartPageActions EmptyMiniCart() {
		
			factory.miniCartPage().EmptyMiniCartValidation();
	
		return this;
	}

	public MiniCartPageActions ValidateBasket() throws Exception {
		factory.miniCartPage().viewBasketValidation();
		return this;

	}
	
	public MiniCartPageActions continueShopFromMiniCart()
	{
		factory.miniCartPage()
		.clickOnContinueShoppingInMiniCartOverlay();
		return this;
	}

	public MiniCartPageActions closeMiniCartModal()
	{
		factory.miniCartPage()
		.closeMiniCart();
		return this;
	}
	public MiniCartPageActions clickOnMinicart() {
		factory.miniCartPage()
		.clickOnMiniCartInHeader();
		return this;
		
	}
}
