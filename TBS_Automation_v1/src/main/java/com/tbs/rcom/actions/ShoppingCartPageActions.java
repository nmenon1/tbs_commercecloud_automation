package com.tbs.rcom.actions;

import com.tbs.rcom.pages.PageFactory;

public class ShoppingCartPageActions {

	PageFactory factory=new PageFactory();

	/**
	 * @author alwin.ashley
	 * Proceed to checkout	
	 * @throws Exception
	 */
	public ShoppingCartPageActions proccedToCheckoutFromCart() throws Exception {
		factory
		.shoppingCartPage()
		.clickCheckoutBtnInCart();
		return this;

	}

	public ShoppingCartPageActions ValidateEmptyCart()
	{

		factory.shoppingCartPage().validateEmptyBasket();

		return this;
	}

	/**
	 * @author Edwin_John
	 * Remove all items from Basket
	 */
	public ShoppingCartPageActions removeAllITemsFromBasket() {

		factory.shoppingCartPage()
		.removeAllItemsFromBasket();
		return this;	
	}

	public ShoppingCartPageActions quantityAdjustments(String qty) throws Exception {
		factory.shoppingCartPage().adjustmentOfQuantity(qty);
		return this;	
	}


	public ShoppingCartPageActions addPersonalMessage() {
		factory.shoppingCartPage()
		.addPersonalMessage();

		return this;
	}

	public ShoppingCartPageActions editPersonalMessage() {
		factory
		.shoppingCartPage()
		.editPersonalMessage();
		return this;
	}

	public ShoppingCartPageActions removePersonalMessage() {
		factory.shoppingCartPage()
		.removePersonalMessage();
		return this;
	}
	/**
	 * @author harish.prasanna
	 * Click on Gift box option in Cart
	 */

	public ShoppingCartPageActions clickOnAddGiftBoxOption() throws Exception {
		factory.shoppingCartPage().clickAddGiftBoxOption();
		return this;	
	}

	public ShoppingCartPageActions clickCancelBtnInAddGiftWrapModal() throws Exception {
		factory.shoppingCartPage().clickCancelBtnInAddGiftWrapModal();
		return this;	
	}

	public ShoppingCartPageActions validateGiftWrapISAddedToCart() throws Exception {
		factory
		.shoppingCartPage()
		.validateGiftWrapIsAddedToCart();
		return this ;

	}

	public ShoppingCartPageActions clickOnRemoveButton() throws Exception {
		factory.shoppingCartPage().clickingRemoveButtonInBasket();
		return this ;

	}

	public ShoppingCartPageActions clickOnCancelInRemoveModal() throws Exception {
		factory.shoppingCartPage().clickingCancelButtonInRemoveModal();
		return this ;

	}



	public ShoppingCartPageActions removalOfGiftWrap() throws Exception {
		factory.shoppingCartPage().removingGiftwrapFromOrder();
		return this ;
	}

	public ShoppingCartPageActions giftWrapIneligibleProduct() throws Exception {
		factory
		.shoppingCartPage()
		.validationOfGiftWrapForIneligibleProduct();
		return this ;
	}
	public ShoppingCartPageActions validateItemAddedIsDisplayedInCart() {
		factory
		.shoppingCartPage()
		.validateItemAddedInShoppingCart();
		return this;
	}
}
