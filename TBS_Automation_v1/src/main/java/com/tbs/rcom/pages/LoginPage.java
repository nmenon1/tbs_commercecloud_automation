package com.tbs.rcom.pages;

import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import com.tbs.rcom.core.utils.CommonUtils;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.LoginPageLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.pageLocators.WishListPageLocators;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.LoginData;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class LoginPage {

    /**
     * Verify if Login Modal is displayed for a guest user on navigating to My Account page
     *
     * @return
     * @author Edwin_John
     */
    public LoginPage validateLoginModalDisplayedInMyAccount() {
        //SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.breadCrumbForLoginModal);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        TestAssert.verifyTrue(SeleniumUtils.isElementPresent(LoginPageLocators.loginModalHeading), "The Login Modal is not displayed when guest user navigates to My account landing page");
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Guest User is Directed to Login/Registration page");
        return this;
    }

    /**
     * Enter Login details in Login modal
     *
     * @param userName
     * @param password
     * @return
     * @author Edwin_John
     */
    public LoginPage enterLoginDetailsInCheckout(String userName) {
        if (userName.toLowerCase().equalsIgnoreCase("valid")) {
            SeleniumUtils.storeKeyValue(KeyStoreConstants.userType, userName);
            userName = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
        } else
            userName = "invaliduser@invalid.com";
        SeleniumUtils.type(LoginPageLocators.emailAddressTxtFieldInLoginModal, userName);
        SeleniumUtils.type(LoginPageLocators.passwordTextFieldInLoginModal, "Tester@123");
        SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.singInBtnInLoginModal);
        return this;
    }

    /**
     * @throws Exception
     * @author alwin.ashley
     * Click on  Sign in button
     */
    public LoginPage clickOnSignInBtn() {
        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(LoginPageLocators.singInBtnInLoginModal), "The Sign in button isn't displayed in Login Modal",
                "The Sign in modal is displayed in Login modal");
        SeleniumUtils.click(LoginPageLocators.singInBtnInLoginModal);
        //SeleniumUtils.click(LoginPageLocators.singInBtnInLoginModal);
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //SeleniumUtils.waitForOverlayToDisappear(LoginPageLocators.loginScreenset);

        return this;
    }

    /**
     * Register a new user in Checkout Login page
     *
     * @return
     * @author Edwin_John
     */
    public LoginPage registerNewUser(LoginData data) {
        String emailId = "autouser";
        String loyaltyType = "";
        if (data.loyaltyUser)
            loyaltyType = "lybc";
        emailId += loyaltyType;
        Random random = new Random();

        int x = random.nextInt(26) + 65;    //0  to 25
        x = (char) x;
        //SeleniumUtils.WaitForElementToBeClickable(CheckoutLoginPageLocators.agreeTermsAndConditionsCheckbox);
        TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(LoginPageLocators.agreeTermsAndConditionsCheckbox), "User isn't navigated to Register page");
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User is on Registration page");
        //SeleniumUtils.Select(LoginPageLocators.registrationTitleSelectorDropdown, "Mr");
        SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.firstNameTxtField);
        WebDriver driver = DriverFactory.getCurrentDriver();
        SeleniumUtils.type(LoginPageLocators.firstNameTxtField, data.firstName);
        WebElement elem = SeleniumUtils.findElement(driver, LoginPageLocators.firstNameTxtField);
        elem.sendKeys(data.firstName);
        SeleniumUtils.type(LoginPageLocators.lastNameTxtField, data.lastName);
        emailId = emailId + CommonUtils.getCurrentTimeStamp() + x + "@gmail.com";
        SeleniumUtils.type(LoginPageLocators.emailAddressTxtFieldInRegistrationModal, emailId);
        SeleniumUtils.type(LoginPageLocators.confirmEmailAddressField, emailId);
        if (SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString().equalsIgnoreCase("canada")) {
            SeleniumUtils.ScrollIntoView(LoginPageLocators.selectPreferredLanguage, true);
            SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.selectPreferredLanguage, 0);
            SeleniumUtils.smoothScrollToElement(LoginPageLocators.enterNewPasswordTextField);
        }
        SeleniumUtils.type(LoginPageLocators.enterNewPasswordTextField, data.password);
        SeleniumUtils.type(LoginPageLocators.confirmPasswordTxtField, data.password);
        if (SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString().equals("Sweden")) {
            AddressDetails adData = AddressDetails.fetch("BillingAddressSE");
            String personalNum = adData.addressData.personalNumber;
            personalNum = personalNum.replace("-", "");
            SeleniumUtils.type(LoginPageLocators.personalNumberField, personalNum);
        }
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Registration form after Adding Values");
        SeleniumUtils.click(LoginPageLocators.agreeTermsAndConditionsCheckbox);
        DriverFactory.Testlogger.log(LogStatus.INFO, "Enter all required details in Registration section");
        SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.registerBtn);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.emailID, emailId);
        String userName = data.firstName + " " + data.lastName;
        SeleniumUtils.storeKeyValue(KeyStoreConstants.userName, userName);
        SeleniumUtils.click(LoginPageLocators.registerBtn);
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return this;
    }


    /**
     * Click on Register button
     *
     * @author Edwin_John
     */
    public LoginPage clickOnRegisterBtn() {
        DriverFactory.Testlogger.log(LogStatus.INFO, "Click on Register Button in Sign in page");
        SeleniumUtils.click(LoginPageLocators.loginPageCreateAccountQnLink);
		/*SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.loginPageCreateAccountQnLink);
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement elem = DriverFactory.getCurrentDriver().findElement(ElementFormatter.getByObject(LoginPageLocators.loginPageCreateAccountQnLink));
		Actions action = new Actions(driver);
		action.moveToElement(elem);
		action.click(elem);
		action.build().perform();*/
        DriverFactory.Testlogger.log(LogStatus.INFO, "Click on Register as new User link");
        return this;
    }

    /**
     * LYBC Screenset - Join Loyalty membership or skip LYBC signup according to user type
     *
     * @author Edwin_John
     */
    public LoginPage signupForLYBC() {
        String userType = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString().toLowerCase();
        String firstName = "";
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SeleniumUtils.WaitForElementToBeVisible(LoginPageLocators.lybcScreensetForSignup);
        boolean consent = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), LoginPageLocators.lybcConsentCheckbox).isSelected();
        System.out.println("LYBC Consent " + consent);
        SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "User is directed to LYBC  Signup screenset");
        if (userType.contains("lybc")) {
            SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthDateDropdownForLYBC, 1);
            SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthMonthDropdownForLYBC, 1);
            SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthYearDropdownForLYBC, 20);
            if (consent == false)
                SeleniumUtils.click(LoginPageLocators.lybcConsentCheckbox);
            SeleniumUtils.click(LoginPageLocators.joinLYBCSignupBtn);
            SeleniumUtils.storeKeyValue(KeyStoreConstants.signupForLYBC, "true");
        } else if (userType.contains("guest")) {
            System.out.println("in guest lybc loop");
            firstName = SeleniumUtils.getStoredValue(KeyStoreConstants.userName).toString().toLowerCase();
            if (firstName.contains("lybc")) {
                SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthDateDropdownForLYBC, 1);
                SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthMonthDropdownForLYBC, 1);
                SeleniumUtils.selectFromDropdownByIndex(LoginPageLocators.birthYearDropdownForLYBC, 20);
                if (consent == false)
                    SeleniumUtils.click(LoginPageLocators.lybcConsentCheckbox);
                SeleniumUtils.click(LoginPageLocators.joinLYBCSignupBtn);
                SeleniumUtils.storeKeyValue(KeyStoreConstants.signupForLYBC, "true");
            } else
                SeleniumUtils.click(LoginPageLocators.skipLYBCSignupBtn);
        } else {
            SeleniumUtils.click(LoginPageLocators.skipLYBCSignupBtn);
            SeleniumUtils.storeKeyValue(KeyStoreConstants.signupForLYBC, "false");
        }
        //SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.myAccountLinkInNavMenu);
        SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.profileLinkInMyAccount);
        SeleniumUtils.click(HomePageLocators.homePageLogo);
        return this;
    }

    /**
     * Enter the Guest email Id in Checkout
     *
     * @return
     * @author Edwin_John
     */
    public LoginPage enterGuestEmailIdInCheckout(String userType) throws InterruptedException {
        String emailId = "";
        if (userType.equalsIgnoreCase("guest")) {
            emailId = "guest" + CommonUtils.getCurrentTimeStamp() + "@gmail.com";
            SeleniumUtils.storeKeyValue(KeyStoreConstants.emailID, emailId);
        } else
            emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
        SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.guestCheckoutHeading);
        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(LoginPageLocators.guestUserEmailIdTxtField), "Guest Checkout screenset isn't displayed for Guest user",
                "The Guest checkout screenset is displayed for the guest user");
        SeleniumUtils.type(LoginPageLocators.guestUserEmailIdTxtField, emailId);
        //Thread.sleep(25000);
        SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Guest Email ID provided");
        SeleniumUtils.click(LoginPageLocators.guestScreensetNextBtn);
        while(!SeleniumUtils.IsElementVisible(LoginPageLocators.registeredUserEmailIdTxtInSignInScreenset)){
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            DriverFactory.getCurrentDriver().navigate().refresh();
            emailId = "";
            if (userType.equalsIgnoreCase("guest")) {
                emailId = "guest" + CommonUtils.getCurrentTimeStamp() + "@gmail.com";
                SeleniumUtils.storeKeyValue(KeyStoreConstants.emailID, emailId);
            } else
                emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
            SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.guestCheckoutHeading);
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(LoginPageLocators.guestUserEmailIdTxtField), "Guest Checkout screenset isn't displayed for Guest user",
                    "The Guest checkout screenset is displayed for the guest user");
            SeleniumUtils.type(LoginPageLocators.guestUserEmailIdTxtField, emailId);
            //Thread.sleep(25000);
            SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Guest Email ID provided");
            SeleniumUtils.click(LoginPageLocators.guestScreensetNextBtn);
        }
        return this;
    }

    /**
     * Validate Sign in checkout screenset and Sign in with valid password
     *
     * @author Edwin_John
     */
    public LoginPage validateSignInCheckoutScreenset() {
        SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.signInScreenSetInCheckout);
        String emailID = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
        String actualEmail = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), LoginPageLocators.registeredUserEmailIdTxtInSignInScreenset).getText();
        TestAssert.verifyTrueAndPrintMsg(actualEmail.contains(emailID), "The Registered user email id isn't displayed in Sign in Screenset",
                "The Registered user email id is displayed in Sign in Screenset");
        return this;
    }

    /**
     * Proceed as guest user or signed in user according to the parameter passed flow
     *
     * @return
     * @author Edwin_John
     */
    public LoginPage enterPasswordAndClickSignInCheckout(String flowType) {
        flowType = flowType.toLowerCase();
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (flowType.contains("registered")) {
            SeleniumUtils.waitForElementToBeClickable(LoginPageLocators.passwordTxtFieldInCheckoutSignIn);
            SeleniumUtils.storeKeyValue(KeyStoreConstants.guestFlowForRegUser, "false");
            SeleniumUtils.type(LoginPageLocators.passwordTxtFieldInCheckoutSignIn, "Tester@123");
            SeleniumUtils.click(LoginPageLocators.signInBtnInCheckoutScreenset);
        } else if (flowType.contains("guest")) {
            SeleniumUtils.click(LoginPageLocators.checkoutAsGuestBtnInCheckoutScreenset);
            SeleniumUtils.storeKeyValue(KeyStoreConstants.guestFlowForRegUser, "true");
        }
        return this;
    }

}
