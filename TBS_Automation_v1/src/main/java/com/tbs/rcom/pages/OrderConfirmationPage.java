package com.tbs.rcom.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.springframework.core.annotation.Order;

import com.itextpdf.awt.geom.misc.RenderingHints.Key;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;
import com.tbs.rcom.core.utils.CommonUtils;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.LoginPageLocators;
import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.pageLocators.OrderConfirmationPageLocators;
import com.tbs.rcom.yaml.AddressDetails;

import tbs_master.DriverFactory;
import tbs_master.SeleniumUtils;

public class OrderConfirmationPage {

	/**
	 * Validate whether the Order was placed successfully and ORder confirmation page is displayed
	 * @author Edwin_John
	 * @return
	 */
	public OrderConfirmationPage validateOrderConfirmationPageIsDisplayed() {
		int flag=0;
		String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		if(emailId.contains("guest")|| !emailId.contains("NonLYBC"))
			flag=1;
		if(SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString().equalsIgnoreCase("Australia"))
		{
			try {
				Thread.sleep(100);
				 SeleniumUtils.waitForFrameAndswitchToIt(OrderConfirmationPageLocators.googleFeedBackIframe);
				 SeleniumUtils.click(OrderConfirmationPageLocators.googleFeedBackCancelBtn);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		try {
			Thread.sleep(100);
		} catch (Exception e) {
			// TODO: handle exception
		}
		SeleniumUtils.waitForElementToBeClickable(OrderConfirmationPageLocators.orderConfirmationImage);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Order was successfully placed and user is directed to Order confirmation page");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(OrderConfirmationPageLocators.orderConfirmationPageHeading), 
				"The Order Confirmation page heading isn't displayed" );
		SeleniumUtils.scrollToElement(OrderConfirmationPageLocators.orderConfirmationPageHeading);
		String userName = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		String thankYouMsg = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.orderConfirmationMsgWithUserNameEmail).getText();
		TestAssert.verifyTrue(thankYouMsg.contains(userName), 
				"The User name email isn't present in the Order confirmation message");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(OrderConfirmationPageLocators.orderConfirmationImage), "The Order Confirmation page Image isn't  displayed");
		if(flag==0)
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(OrderConfirmationPageLocators.guestUserRegisterBtn), 
					"Register button is displayed for the registered emailid", "Register button isn't displayed for the registered emailid");
		
		return this;
	}

	/**
	 * Fetch the Order number and verify the Order date
	 * @author Edwin_John
	 * @return
	 */
	public OrderConfirmationPage fetchOrderNumberAndOrderDate() {
		int flag=0;
		
		try {
			SeleniumUtils.waitForFrameAndswitchToIt(OrderConfirmationPageLocators.feedbackFormAtOrderConfirmation);
			SeleniumUtils.click(OrderConfirmationPageLocators.feedbackCloseButton);
			SeleniumUtils.switchToDefaultContent();
		}catch(NoSuchElementException e)
		{
			System.out.println("Feed back form not displayed");
		}
		try {
			if(SeleniumUtils.getStoredValue(KeyStoreConstants.orderFlag).toString().equals("true"))	
			{
			    try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    SeleniumUtils.waitForFrameAndswitchToIt(MyAccountPageLocators.referrerIframeAtOrderConfirmation);
				//SeleniumUtils.click(MyAccountPageLocators.referrerIframeCloseBtn);
				WebDriver driver = DriverFactory.getCurrentDriver();
				JavascriptExecutor jse = (JavascriptExecutor)driver;
				jse.executeScript("arguments[0].click()", SeleniumUtils.findElement(driver, MyAccountPageLocators.referrerIframeCloseBtn));
				SeleniumUtils.switchToDefaultContent();
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SeleniumUtils.click(MyAccountPageLocators.postPurchaseOvderLayCloseBtn);
				SeleniumUtils.storeKeyValue(KeyStoreConstants.orderFlag, "False");
			}
		    }catch(NoSuchElementException e)
			{
			  SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "Referrer pop not displayed");
			}
			

		
		String orderNumber = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.orderNumberInOrderConfPage).getText().replaceAll(" ", "");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.orderID, orderNumber);
		SeleniumUtils.scrollToElement(OrderConfirmationPageLocators.orderNumberInOrderConfPage);
		String pattern = "dd/MM/yyyy";
		String currentDate = new SimpleDateFormat(pattern).format(new Date());	
		String orderDate = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.orderDateInOrderConfPage).getAttribute("aria-label");
		String[] currentDateArr = currentDate.split("/");
		String[] orderDateArr = orderDate.split("/");
		
		
		System.out.println("currentDateArr[0] "+currentDateArr[0]+" orderDateArr[0] "+orderDateArr[0]+"\n"
				+"currentDateArr[1] "+currentDateArr[1]+" orderDateArr[1]" +orderDateArr[1]+"\n"
				+"currentDateArr[2] "+currentDateArr[2]+ " orderDateArr[2] "+orderDateArr[2]);
		if(currentDateArr[0].replaceAll("^0+(?!$)", "").equalsIgnoreCase(orderDateArr[0])
				&&currentDateArr[1].replaceAll("^0+(?!$)", "").contains(orderDateArr[1])
				&&currentDateArr[2].equalsIgnoreCase(orderDateArr[2]))
			flag=1;
		TestAssert.verifyTrue(flag==1,
				"The Order date isn't the Current date");
		SeleniumUtils.storeKeyValue("OrderDate", orderDate);
		String orderTotalDisplayed = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.orderTotalValue).getAttribute("aria-label").toString();
		orderTotalDisplayed=SeleniumUtils.getPriceAsActualNumber(orderTotalDisplayed);
		Double actualOrderTotal = Double.parseDouble(orderTotalDisplayed);
		Double expectedOrderTotal = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
		actualOrderTotal = SeleniumUtils.round(actualOrderTotal, 2);
		expectedOrderTotal = SeleniumUtils.round(expectedOrderTotal, 2);
		System.out.println("Order number:"+orderNumber);
		System.out.println("actualOrderTotal "+actualOrderTotal +" expectedOrderTotal "+expectedOrderTotal);
		System.out.println("Order total diff "+(actualOrderTotal-expectedOrderTotal));
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Order date and Order Number - "+orderNumber+" are displayed");
		TestAssert.verifyTrueAndPrintMsg(actualOrderTotal-expectedOrderTotal==0.0||actualOrderTotal-expectedOrderTotal==0.00, "The Expected Order total is different from the actual order total",
				"The Expected Order total is same as the actual order total");

		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
				
		return this;
	
	 }			

	/**
	 * Register a guest customer in Order confirmation page
	 * @author Edwin_John
	 * @return
	 */
	public OrderConfirmationPage clickRegisterAndValidateFormIsPrepopulated() {
		SeleniumUtils.ScrollIntoView(OrderConfirmationPageLocators.guestUserRegisterBtn, false);
		String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Click on the + symbol to register to Website");
		SeleniumUtils.click(OrderConfirmationPageLocators.guestUserRegisterBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(OrderConfirmationPageLocators.registrationFormForGuestUser), 
				"The Registration form isn't displayed for the Guest customer", "The Registration form is displayed for the Guest customer");
		String firstName = SeleniumUtils.getStoredValue(KeyStoreConstants.firstName).toString();
		String lastName = SeleniumUtils.getStoredValue(KeyStoreConstants.lastName).toString();
		SeleniumUtils.scrollToElement(OrderConfirmationPageLocators.firstNameTxtFieldRegForm);
		String actualFirstName = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.firstNameTxtFieldRegForm).getAttribute("value").toString();
		String actualLastName = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.lastNameTxtFieldInRegForm).getAttribute("value").toString();
		System.out.println("Actual firstname: "+actualFirstName+" Lastname : "+actualLastName);
		String actualEmailId = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.emailIdTxtFieldInRegForm).getAttribute("value").toString();
		String confirmEmailId = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.confirmEmailIdInRegForm).getAttribute("value").toString();
		TestAssert.verifyTrueAndPrintMsg(firstName.equals(actualFirstName)&&lastName.equals(actualLastName), "The first & last names of billing addresssaved by guest customer isn't prepopulated in registration form", 
				"The first & last names of billing addresssaved by guest customer is prepopulated in registration form");
		TestAssert.verifyTrueAndPrintMsg(actualEmailId.equals(emailId)&&confirmEmailId.equals(emailId),	 "The Email id of guest customer isn't prepopulated in Registration form", 
				"The Email id of guest customer is prepopulated in registration form");
		String isEmailDisabled = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.emailIdTxtFieldInRegForm).getAttribute("disabled").toString();
		String isConfEmailDisabled = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.confirmEmailIdInRegForm).getAttribute("disabled").toString();
		System.out.println("disabled value: "+isEmailDisabled+" , conf: "+isConfEmailDisabled);
		TestAssert.verifyTrueAndPrintMsg(isEmailDisabled.equals("true")&&isConfEmailDisabled.equals("true"), "The Email fields aren't disabled", "The Email fields are disabled");
		return this;
	}

	/**
	 * Enter password and click Register in form
	 * @author Edwin_John
	 * @param userType
	 * @return
	 */
	public OrderConfirmationPage enterPasswordInRegFormAndRegister(String userType) {
		String pwd = "Tester@123";
		String firstName = "";
		String lastName = "";
		WebDriver driver = DriverFactory.getCurrentDriver();
		SeleniumUtils.scrollToElement(OrderConfirmationPageLocators.firstNameTxtFieldRegForm);
		SeleniumUtils.hoverOverElement(MiniCartLocators.miniCartBtnInHeader);
		SeleniumUtils.click(OrderConfirmationPageLocators.pwdTxtFieldInRegForm);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(userType.toLowerCase().contains("lybc")) {
			firstName = "lybcFirst";
			lastName = "lybcLast";
		}
		else {
			firstName = "GuestFirst";
			lastName = "GuestLast";
		}
		SeleniumUtils.ScrollIntoView(OrderConfirmationPageLocators.firstNameTxtFieldRegForm, false);
		SeleniumUtils.type(OrderConfirmationPageLocators.firstNameTxtFieldRegForm, firstName);
		SeleniumUtils.type(OrderConfirmationPageLocators.lastNameTxtFieldInRegForm, lastName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.userName, firstName+" "+lastName);
		SeleniumUtils.type(OrderConfirmationPageLocators.pwdTxtFieldInRegForm, pwd);
		SeleniumUtils.type(OrderConfirmationPageLocators.confirmPwdTxtFieldInRegForm, pwd);
		if(SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString().equals("Sweden"))
		{
			AddressDetails adData = AddressDetails.fetch("BillingAddressSweden");
			String personalNum = adData.addressData.personalNumber;
			personalNum=personalNum.replace("-", "");
			SeleniumUtils.type(LoginPageLocators.personalNumberField, personalNum);
		}
		boolean consentValue = SeleniumUtils.findElement(driver, OrderConfirmationPageLocators.termsAndConditionsCheckbox).isSelected();
		System.out.println("Consent "+consentValue);
		if(consentValue==false)
			SeleniumUtils.click(OrderConfirmationPageLocators.termsAndConditionsCheckbox);
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Values are entered by guest in Registration form");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		SeleniumUtils.click(OrderConfirmationPageLocators.regFormSubmitBtn);
		return this;
	}
	
	/**
	 * Click Signup for LYBC in order confirmation page
	 * @author Edwin_John
	 * @return
	 */
	public OrderConfirmationPage clickSignupForLYBCFromOrderConfPage() {
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(OrderConfirmationPageLocators.lybcRegistrationBtn), 
				"LYBC signup button isn't displayed in Order conf page", "LYBC signup button is displayed in order conf page");
		SeleniumUtils.click(OrderConfirmationPageLocators.lybcRegistrationBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(OrderConfirmationPageLocators.lybcScreensetInOrderConfPage), "LYBC screenset isn't displayed", "LYBC screenset is displayed");
		boolean consent = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), OrderConfirmationPageLocators.lybcConsentCheckboxInForm).isSelected();
		SeleniumUtils.selectFromDropdownByIndex(OrderConfirmationPageLocators.birthDateDropdownInLybcForm, 1);
		SeleniumUtils.selectFromDropdownByIndex(OrderConfirmationPageLocators.birthMonthDropdownInLybcForm, 1);
		SeleniumUtils.selectFromDropdownByIndex(OrderConfirmationPageLocators.birthYearDropdownInLybcForm, 20);
		if(consent==false)
			SeleniumUtils.click(OrderConfirmationPageLocators.lybcConsentCheckboxInForm);
		SeleniumUtils.click(LoginPageLocators.joinLYBCSignupBtn);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.signupForLYBC, "true");
		SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.myAccountLinkInNavMenu);
		SeleniumUtils.click(HomePageLocators.homePageLogo);
		
	    
		return this;
	}
}
