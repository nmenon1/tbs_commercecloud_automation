package com.tbs.rcom.pages;

import java.util.ArrayList;
import java.util.List;

import cucumber.api.java.eo.Se;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.PaymentPageLocators;
import com.tbs.rcom.pageLocators.ShoppingCartLocators;
import com.tbs.rcom.pageLocators.CheckoutPageLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.pageLocators.StorePageLocators;
import com.tbs.rcom.yaml.AddressData;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.WhoWillCollectData;
import com.tbs.rcom.yaml.WhoWillCollectDetails;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class ShippingAddressPage {

	CheckoutPageLocators addressPageLocators=null;
	SeleniumUtils help=new SeleniumUtils();

	public ShippingAddressPage() {
		this.addressPageLocators=new CheckoutPageLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), addressPageLocators);
	}

	/**
	 * @author Edwin_John
	 * Click on the Continue button in Shipping Address page
	 * @return
	 */
	public ShippingAddressPage clickOnContinueBtn() {
		WebDriver driver=DriverFactory.getCurrentDriver();
		WebDriverWait wait=new WebDriverWait(driver, 20);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(addressPageLocators.continueToDeliveryOptionsBtn.getelementValue())));
			SeleniumUtils.click(addressPageLocators.continueToDeliveryOptionsBtn);
		}
		catch(Exception e) {

		}
		return this;
	}

	/**
	 * @author Edwin_John
	 * Select any Address given user already has Shipping Addresses added
	 */
	public ShippingAddressPage selectShippingAddress() {
		WebDriver driver=DriverFactory.getCurrentDriver();
		try {
			Thread.sleep(2000);
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.selectDeliveryAddress);
			SeleniumUtils.click(CheckoutPageLocators.selectDeliveryAddress);
			Thread.sleep(2000);
			List<WebElement> numOfAddress=driver.findElements(By.xpath("//mat-option[contains(@class,'card-address-option')]"));
			System.out.println("List size :"+numOfAddress.size());
			if(numOfAddress.size()>1) {
				SeleniumUtils.click(CheckoutPageLocators.selectDeliveryAddress2);
				System.out.println("Address 2 selected");				
			}
			DriverFactory.Testlogger.log(LogStatus.PASS, "The Delivery Address is selected");


		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "The Delivery Address is not selected");

		}
		return this;
	}


	/**
	 * @author alwin.ashley
	 * Selecting delivery mode
	 * @param DeliveryMode - Express,Standard,Economy and Regular
	 */
	public ShippingAddressPage selectDeliveryMode(String deliveryMode)  {
		WebDriver driver=DriverFactory.getCurrentDriver();
		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString().toLowerCase();
		String deliveryModeType="";
		String deliveryMethodPrice="";
		SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.deliveryOptionsHeading);
		List<WebElement> numOfdeliveryModes=driver.findElements(By.xpath(PaymentPageLocators.deliveryOptionsList.getelementValue()));
		TestAssert.verifyTrue(numOfdeliveryModes.size()>0,"Delivery Options are not displayed in Checkout");
		if(market.contains("kingdom")) {
			switch(deliveryMode) {
			case "Regular" :
				deliveryModeType = "regular";
				break;
			case "Standard" :
				deliveryModeType = "standard";
				break;
			case "Economy" :
				deliveryModeType = "economy";
				break;
			case "Express" :
				deliveryModeType = "express";
				break;
			}
		}
		else {
			deliveryModeType = "not found";
			/*deliveryMode=deliveryMode.toUpperCase();
			for(WebElement e:numOfdeliveryModes) {
				if(e.getAttribute("id").contains(deliveryMode)) {
					e.click();
					deliveryModeType = "found";
					break;
				}
			}*/
		}
		if(!(deliveryModeType.equalsIgnoreCase("not found"))) {
			SeleniumUtils.smoothScrollToElement(CheckoutPageLocators.deliveryModeRadioBtn.format(deliveryModeType));
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.deliveryModeRadioBtn.format(deliveryModeType));
			SeleniumUtils.jsdoubleclick(CheckoutPageLocators.deliveryModeRadioBtn.format(deliveryModeType));


		/*	SeleniumUtils.smoothScrollToElement(CheckoutPageLocators.deliveryModeRadioBtn.format("express"));
			for (int i = 0; i < 5; i++)
				if (SeleniumUtils.getAttributeValue(CheckoutPageLocators.deliveryModeRadioBtnNew.format("express"), "value").equalsIgnoreCase("false"))
				{
					SeleniumUtils.hoverOverElement(CheckoutPageLocators.deliveryModeRadioBtn);
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					SeleniumUtils.click(CheckoutPageLocators.deliveryModeRadioBtn.format(deliveryModeType));
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
			deliveryMethodPrice = SeleniumUtils.findElement(driver, PaymentPageLocators.deliveryChargePrice.format(deliveryModeType)).getText().toString();
			System.out.println("DeliveryMethodPrice "+deliveryMethodPrice);
		}
		if(deliveryModeType.equalsIgnoreCase("not found")) {
			List<WebElement> list = driver.findElements(By.xpath(PaymentPageLocators.listOFDeliveryModes.getelementValue()));
			if(list.size()>1)
			{
				SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.DeliveryOptionInList.format("2"));
				SeleniumUtils.click(PaymentPageLocators.DeliveryOptionInList.format("2"));
				deliveryMethodPrice = SeleniumUtils.findElement(driver, PaymentPageLocators.DeliveryOptionPrice.format("2")).getText().toString();	
			}
			else
			{
			SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.DeliveryOptionInList.format("1"));
			SeleniumUtils.click(PaymentPageLocators.DeliveryOptionInList.format("1"));
			deliveryMethodPrice = SeleniumUtils.findElement(driver, PaymentPageLocators.DeliveryOptionPrice.format("1")).getText().toString();
			}
		}
		deliveryMethodPrice = SeleniumUtils.getPriceAsActualNumber(deliveryMethodPrice);
		if(deliveryMethodPrice.equals(""))
		{
			deliveryMethodPrice="0";
		}
		SeleniumUtils.storeKeyValue("Delivery_Charge", deliveryMethodPrice);
		System.out.println("Delivery methhod price "+deliveryMethodPrice);
		Double deliveryCharge = Double.parseDouble(deliveryMethodPrice);
		Double cartTotal = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
		cartTotal+=deliveryCharge;
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, cartTotal);
		System.out.println("Final Order "+cartTotal);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Delivery Mode "+deliveryMode+" is Selected");
		return this;
	}


	public ShippingAddressPage enterNewShippingAddress(AddressData add) {
		String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		int flagForGuest=0;
		if(SeleniumUtils.getStoredValue(KeyStoreConstants.guestFlowForRegUser).toString().equals("true")||emailId.contains("guest"))
			flagForGuest=1;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(SeleniumUtils.isElementNotPresent(CheckoutPageLocators.addNewDeliveryAddress) /*&& SeleniumUtils.isElementNotPresent(MyAccountPageLocators.addNewAddressBtnInMyaccount)*/) {
			
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(CheckoutPageLocators.savedAddressSelectedInDropdown), "Saved Address isn't present in Checkout", "Saved Address is present in checkout");
			SeleniumUtils.click(CheckoutPageLocators.savedAddressSelectedInDropdown);
			SeleniumUtils.click(CheckoutPageLocators.addNewAddressFromDropdown);
		}
		else
		      //SeleniumUtils.click(CheckoutPageLocators.addNewDeliveryAddress);
			
		SeleniumUtils.WaitForElementToBeVisible(CheckoutPageLocators.addNewAddressModal);
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.firstNameTxtField);
		SeleniumUtils.type(CheckoutPageLocators.firstNameTxtField, add.firstName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.firstName, add.firstName);
		SeleniumUtils.type(CheckoutPageLocators.lastNameTxtField, add.lastName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.lastName, add.lastName);
		//SeleniumUtils.type(CheckoutPageLocators.phnNumberTxtField, "07878123456");
		if(SeleniumUtils.IsElementVisible(CheckoutPageLocators.enterAddressManualBtn)) {
		SeleniumUtils.click(CheckoutPageLocators.enterAddressManualBtn);}
		SeleniumUtils.type(CheckoutPageLocators.addressLine1TxtField, add.addressLine1);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.addressLine, add.addressLine1);
		SeleniumUtils.type(CheckoutPageLocators.addressLine2TxtField, add.addressLine2);
		SeleniumUtils.type(CheckoutPageLocators.cityTxtField, add.city);
		if(SeleniumUtils.IsElementClickable(CheckoutPageLocators.countyField)) {
		SeleniumUtils.click(CheckoutPageLocators.countyField);
		SeleniumUtils.click(CheckoutPageLocators.dropDownValue.format(add.state));}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		SeleniumUtils.type(CheckoutPageLocators.zipcodeTxtField, add.zipcode);
		/*SeleniumUtils.click(ShippingAddressPageLocators.selectStateDropdown);
		SeleniumUtils.click(ShippingAddressPageLocators.dropDownValue.format(1));*/
		if(flagForGuest==0)
			SeleniumUtils.click(CheckoutPageLocators.setAddressAsDefaultCheckbox);
		else
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(CheckoutPageLocators.setAddressAsDefaultCheckbox), 
					"The set as default checkbox is enabled for guest user","The Set as Default checkbox isn't present for guest user");
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter Required Values in Delivery Address Modal");
		SeleniumUtils.click(CheckoutPageLocators.continueBtnInAddressModal);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * Validate new address entered is added in Address container
	 * @author Edwin_John
	 */
	public ShippingAddressPage validateAddressIsAdded() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		int flag1=0;
		int flag2=0;
		int count1=0;
		int count2=0;
		String firstName = SeleniumUtils.getStoredValue(KeyStoreConstants.firstName).toString();
		String lastName = SeleniumUtils.getStoredValue(KeyStoreConstants.lastName).toString();
		String addressLine1= SeleniumUtils.getStoredValue(KeyStoreConstants.addressLine).toString();
		//to be removed
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(CheckoutPageLocators.addNewAddressModal), "Address Modal isn't closed on Clicking Save", 
				"Address modal is closed on clicking save");
		TestAssert.verifyTrueAndPrintMsg((SeleniumUtils.isElementPresent(CheckoutPageLocators.addressSavedForDelivery))||((SeleniumUtils.IsElementVisible(MyAccountPageLocators.savedAddressRadioBtn))), "The Added address isn't Saved", "The Added address is Saved");
		List<WebElement> firstNameList = driver.findElements(By.xpath(CheckoutPageLocators.addressCardsFirstNameList.getelementValue()));
		for(WebElement element:firstNameList) {
			String actualFirstName = element.getText();
			System.out.println("actualfirst name is"+actualFirstName); 
			System.out.println(firstName); 
			++count1;
			if(actualFirstName.contains(firstName)&& actualFirstName.contains(lastName)) {
				flag1=1;
				break;
			}
		}
		List<WebElement> addressLineList = driver.findElements(By.xpath(CheckoutPageLocators.addressContainerAddressLineList.getelementValue()));
		for(WebElement element:addressLineList) {
			String actualAddressLine = element.getText();
			++count2;
			if(addressLine1.equalsIgnoreCase(actualAddressLine)) {
				flag2=1;
				break;
			}
		}
		/*		System.out.println("flag 1: "+flag1+" , flag2= "+flag2+" ,count1 : "+count1+" ,count 2="+count2);
		TestAssert.verifyTrueAndPrintMsg((flag1==0||flag2==0)&&(count1!=count2), "Address entered isn't  saved and not present in dropdown", "Address entered is successfully saved and present in the container");*/
		System.out.println("flag 1: "+flag1+" , flag2= "+flag2+" ,count1 : "+count1+" ,count 2="+count2);
		if((flag1==0||flag2==0)&&(count1!=count2)) {
			System.out.println("Validation Unsuccessfull");
			SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "The Address entered isn't saved in dropdown");

			Assert.fail("Address added isnt present in Address dropdown");
		}
		else {
			SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Address entered is successfully saved and present in the container");
		}
		return this;
	}

	/**
	 * @author alwin.ashley
	 * Validate Edited Address
	 * @return
	 * @throws Exception 
	 */
	public ShippingAddressPage validateEditedAddress() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		System.out.println("Entering validateEditedAddress function");
		try {
			Thread.sleep(2000);
			String firstName = SeleniumUtils.getStoredValue(KeyStoreConstants.firstName).toString();
			String lastName = SeleniumUtils.getStoredValue(KeyStoreConstants.lastName).toString();
			String actualFullName = firstName+" "+lastName;
			System.out.println(actualFullName); 
			WebElement actualFirstName = driver.findElement(By.xpath("//div[contains(@class,'card-body')]//div[contains(@class,'card-label-bold')]"));
			String Fullname =actualFirstName.getText();
			System.out.println(Fullname); 
			Assert.assertTrue("Editted address is not Found",Fullname.equalsIgnoreCase(actualFullName) );
			DriverFactory.Testlogger.log(LogStatus.PASS, "Address Editted is successfully saved ");
			System.out.println("Validation Successfull");



		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure is validating Editted address ");
		}
		return this;
	}


	/**
	 * Select Address from Dropdown
	 * @return
	 */
	public ShippingAddressPage se() {
		return this;
	}

	/**
	 * @author alwin.ashley
	 * Clicking Edit button
	 * @return
	 */

	public ShippingAddressPage clickOnEditAddressBtn() {

		try {
			//Assert.assertTrue("Continue button is not displayed",tbs_SeleniumHelper.isElementPresent(addressPageLocators.btn_continueToPaymentDetailsBtn ));
			System.out.println("Entering function 1");
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.editDeliveryAddress);
			Thread.sleep(2000);
			SeleniumUtils.click(CheckoutPageLocators.editDeliveryAddress);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Edit Delivery address button clicked successfully ");
			System.out.println("Edit button is selected");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Unable to click Edit Delivery address button ");
		}
		return this;
	}

	/**
	 * @author alwin.ashley
	 * Editing Delivery Address
	 * @return
	 * @throws Exception 
	 */

	public ShippingAddressPage editDeliveryAddress() throws Exception {

		try {

			//Assert.assertTrue("Continue button is not displayed",tbs_SeleniumHelper.isElementPresent(addressPageLocators.btn_continueToPaymentDetailsBtn ));
			System.out.println("Entering editDeliveryAddress function ");
			Thread.sleep(2000);
			SeleniumUtils.WaitForElementToBeVisible(CheckoutPageLocators.addNewAddressModal);
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.firstNameTxtField);
			SeleniumUtils.type(CheckoutPageLocators.firstNameTxtField, "New");
			SeleniumUtils.storeKeyValue(KeyStoreConstants.firstName, "New");
			SeleniumUtils.type(CheckoutPageLocators.lastNameTxtField, "Tester");
			SeleniumUtils.storeKeyValue(KeyStoreConstants.lastName, "Tester");
			Thread.sleep(2000);
			SeleniumUtils.click(CheckoutPageLocators.continueBtnInAddressModal);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Delivery address edited successfully");
			String screenshotPath = SeleniumUtils.getScreenhot("Delivery address edited successfully");
			DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
			System.out.println("Delivery Address Edited ");

		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Unable to Edit Delivery address ");
			String screenshotPath = SeleniumUtils.getScreenhot("Failed to Edit Delivery address");
			DriverFactory.Testlogger.log(LogStatus.FAIL, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
		}
		return this;
	}



	/**
	 * @author harish.prasanna
	 * Validation of PO restriction error message in Shipping Address Page
	 * @return
	 */
	public ShippingAddressPage validationOfPoRestrictedErrorMessage() {
		/**
		 * market if UK , then validate the message
		 * check if address modal is closed
		 * Add Test Assert for error message displayed and modal being closed
		 */
		String title = SeleniumUtils.GetText(CheckoutPageLocators.poRestrictionErrorMessage);
		System.out.println(title);
		String ExpectedTitle = "We cannot ship to P.O. Boxes";
		//Boolean value = title.contains(ExpectedTitle);
		TestAssert.verifyTrueAndPrintMsg(title.contains(ExpectedTitle), "PO box Restriction Error message doesn't gets displayed", "PO box Restriction Error message gets displayed");

		return this;
	}
}
