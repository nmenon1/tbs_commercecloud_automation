package com.tbs.rcom.pages;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.itextpdf.text.Image;
import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.PaymentPageLocators;
import com.tbs.rcom.pageLocators.ShoppingCartLocators;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.WhoWillCollectData;

import cucumber.runtime.io.Helpers;

import com.tbs.rcom.pageLocators.CheckoutPageLocators;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class CheckoutPage {

	public CheckoutPage selectDeliveryMode(String deliverMode) {
		DriverFactory.Testlogger.log(LogStatus.INFO,"Click on the Delivery mode dropdown to select the Delivery option" );
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.selectDeliveryModeDropdownBtn);
		SeleniumUtils.click(CheckoutPageLocators.selectDeliveryModeDropdownBtn);

		return this;
	}

	/**
	 * Validate whether the user is on Checkout page 
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage validateUserIsOnCheckoutPage() {
		Assert.assertTrue("User isn't currently in Checkout page", SeleniumUtils.IsElementVisible(CheckoutPageLocators.checkoutPagebreadCrumb));
		return this;
	}

	/**
	 * Click on the required Fulfillment method in Checkout
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage selectFulfillmentMethodInCheckout(String fulfillmentType) {
		fulfillmentType=fulfillmentType.toLowerCase();
		int index=0;
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Select the "+fulfillmentType+" fulfillment in Checkout");
		switch(fulfillmentType) {
		case "delivery" : 
			if(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), 
					CheckoutPageLocators.fulfillmentMethodRadioBtn.format(1)).getAttribute("class").contains("checked"))
				break;
			//TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(ShippingAddressPageLocators.deliveryMethodHeading), "The default Fulfillment selected isn't Delivery");
			else
				SeleniumUtils.click(CheckoutPageLocators.fulfillmentMethodRadioBtn.format(1));
			break;
		case "cis" :
			index = 2;
			break;
		case "collection point" : 
			index = 3;
			break;
		}
		if(index!=0) {
			String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
			if((market.contains("Sweden")|| market.contains("Denmark")|| market.contains("Australia")|| market.contains("Germany") || market.contains("USA")|| market.contains("Canada"))&&index==3)
			{
			  index=index-1;	// denmark and germany has to remove from line 83 canada has to add
			}
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.fulfillmentMethodRadioBtn.format(index));
			SeleniumUtils.click(CheckoutPageLocators.fulfillmentMethodRadioBtn.format(index));
		}
		SeleniumUtils.storeKeyValue("FulfillmentType", fulfillmentType);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Fullfilment type "+fulfillmentType+" is selected");
		return this;
	}

	/**
	 * Validate if the fulfillment type selected is Delivery or not
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage validateDeliveryIsSelectedAsDefaultFulfillmentInCheckout() {
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), 
				CheckoutPageLocators.fulfillmentMethodRadioBtn.format(1)).getAttribute("class").contains("checked"), 
				"The Fulfillment selected isn't Delivery by default", "The Fulfillment selected is Delivery by default");
		SeleniumUtils.storeKeyValue("FulfillmentType", "delivery");
		return this;
	}

	/**
	 * Verify if user is redirected to checkout
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage verifyUserNavigatedToCheckout() {
		try{
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.checkoutPageHeading);
			SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User is Directed to Checkout from Shopping Cart");	
		}
		catch(Exception e) {
			SeleniumUtils.attachScreenshotInReport(LogStatus.FAIL, "User isn't directed to Checkout page from Cart");
			TestAssert.fail("Error in navigating to Checkout");
		}
		return this;
	}

	/**
	 * Enter Search keyword in Collection point modal and verify results
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage enterLocationInCollectionPointModal(String location) {
		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
		MarketDetails data = MarketDetails.fetch(market);
		if(location.toLowerCase().contains("invalid")) {
			SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.collectionPtSearchField);
			SeleniumUtils.type(CheckoutPageLocators.collectionPtSearchField, "$%^&^");
			SeleniumUtils.click(CheckoutPageLocators.collectionPtSearchBtn);
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(CheckoutPageLocators.unidentifiedLocationErrMsgInStoreModal),
					"Unidentified location error message isn't displayed in collection point" ,
					"Unidentified location error message is displayed in collection point");

			SeleniumUtils.click(CheckoutPageLocators.unidentifiedLocationErrMsgInStoreModal);
		}
		else {
			SeleniumUtils.type(CheckoutPageLocators.collectionPtSearchField, data.marketData.cisSearchKeyword );
			SeleniumUtils.click(CheckoutPageLocators.collectionPtSearchBtn);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SeleniumUtils.WaitForElementToBeVisible(CheckoutPageLocators.collectionPtResultsModal);
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(CheckoutPageLocators.collectionPtResultsModal), "The Collection point results modal isn't displayed", "The Colection point results modal is displayed");
		}
		return this;
	}

	/**
	 * Select the Collection Point from modal 
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage selectCollectionPointFromModal() {
		int index=1;
		String collectionPtAddress="";
		WebDriver driver= DriverFactory.getCurrentDriver();
		List<WebElement> listOfStores = driver.findElements(ElementFormatter.getByObject(CheckoutPageLocators.listOfCollectionPoints));
		TestAssert.verifyTrueAndPrintMsg(listOfStores.size()>0, "The List of Collection points aren't displayed in modal", 
				"Total of  "+listOfStores.size()+" collection points are displayed in the modal");
		List<WebElement> listOfPointsWithStock = driver.findElements(ElementFormatter.getByObject(CheckoutPageLocators.listOfCollectionPtsWithSelectBtnEnabled));
		TestAssert.verifyTrueAndPrintMsg(listOfPointsWithStock.size()>0, "There are no collection points with stock for the added items", 
				"There are "+listOfPointsWithStock.size()+" points with stock for the items added");
		for(WebElement e:listOfPointsWithStock) {
			collectionPtAddress = SeleniumUtils.findElement(driver, CheckoutPageLocators.collectionPtAddressOfStoreWithStock.format(index)).getText();
			System.out.println("Address "+collectionPtAddress);
			SeleniumUtils.storeKeyValue("CollectionPoint", collectionPtAddress);
			e.click();
			break;
		}
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.selectedCollectionPtAddress);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(driver, CheckoutPageLocators.selectedCollectionPtAddress).getText().contains(collectionPtAddress), 
				"The Selected Collection point in modal isn't reflected", 
				"The selected collection point is displayed");
		SeleniumUtils.switchToDefaultContent();
		return this;

	}	

	/**
	 * @author nithin.c03
	 * Validation of selected non-store collection point details
	 * @return
	 */
	public CheckoutPage verifySelectedCollectionPtDetails() {
		SeleniumUtils.click(CheckoutPageLocators.nonStoreCollectionPointInfoButton);
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(CheckoutPageLocators.nonStoreCollectionPointMap), "Map view of selected non-store collection point is not dispalyed");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Non-Store collection point map is displayed");
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(CheckoutPageLocators.nonStoreCollectionPointAddressDetails), "Address details of selected non-store collection point is not dispalyed");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Non-Store collection point address details are displayed");
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(CheckoutPageLocators.nonStoreCollectionPointOpeningHourDetails), "Opening hour details of selected non-store collection point is not dispalyed");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Non-Store collection point opening hour details are displayed");
		SeleniumUtils.click(CheckoutPageLocators.nonStoreCollectionPointInfoModalClose);
		return this;

	}


	/**
	 * Enter Collector details and verify if they are saved 
	 * @author Edwin_John
	 * @return
	 */
	public CheckoutPage enterCollectorDetailsInCheckout()  {
		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
		int flag = 0;
		String collectorFirstName = "CollectFirst";
		String collectorLastName = "CollectLast";
		MarketDetails data = MarketDetails.fetch(market);
		String phnNumber = data.marketData.phnNumbFormat;
		//SeleniumUtils.click(CheckoutPageLocators.addCollectorDetailsBtn);
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.addCollectorDetailModal);
		SeleniumUtils.type(CheckoutPageLocators.collectorFirstNameTxtField, collectorFirstName);
		SeleniumUtils.type(CheckoutPageLocators.collectorLastNameTxtField, collectorLastName);
		SeleniumUtils.type(CheckoutPageLocators.collectorPhnNumberTxtField, phnNumber);
		SeleniumUtils.click(CheckoutPageLocators.submitCollectorDetailsBtn);
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.addCollectorDetailsBtn);
		if(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), CheckoutPageLocators.savedCollectorDetails.format(1)).getText().contains(collectorFirstName)
				&&SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), CheckoutPageLocators.savedCollectorDetails.format(1)).getText().contains(collectorLastName)
				&&SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), CheckoutPageLocators.savedCollectorDetails.format(2)).getText().contains(phnNumber))
			flag=1;
		TestAssert.verifyTrueAndPrintMsg(flag==1, "The Collector details entered aren't saved", "The Collector details entered are saved");
		return this;

	}
	
	public CheckoutPage verifyCISIsHiddenForGiftWrapOrder()
	{
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(CheckoutPageLocators.fulfillmentMethodRadioBtn.format(2)), "CIS fulfilment displayed for Gift wrap order", "CIS fulfilment not displayed");
		return this;
	}
	
	public CheckoutPage navigateBackToBasketPagefromCheckoutPage()
	{
		//sleep to be removed
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    DriverFactory.getCurrentDriver().navigate().back();
		return this;
	}
	
	/**
	 * @author Arjun
	 * Validate Klarna site loaded
	 */
	public CheckoutPage validateKlarnaSiteLoaded()
	{
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlContains("klarna"));
		TestAssert.verifyTrueAndPrintMsg(driver.getCurrentUrl().toString().contains("klarna"),"User not directed to Klarna payment gateway", "User directed to Klarna Payment gateway");
		return this;
	}
}
