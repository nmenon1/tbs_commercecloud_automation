/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pages;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.util.concurrent.FluentFuture;
import com.jayway.restassured.specification.Argument;
import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.pageLocators.WishListPageLocators;

import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;
import tbs_master.DriverFactory;
import tbs_master.ProjectProperties;
import tbs_master.SeleniumUtils;

public class HomePage {
	HomePageLocators tbs_HomePageLocators=null;
	SeleniumUtils help = new SeleniumUtils();
	public HomePage()
	{
		this.tbs_HomePageLocators= new HomePageLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), tbs_HomePageLocators);

	}
	public HomePage validateIfHomePageIsDisplayed(){
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.homePageLogo);
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.cookiesAcceptButton);
		SeleniumUtils.click(HomePageLocators.cookiesAcceptButton);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "HomePage is displayed");
		return this;
	}
	public HomePage selectMarketFromDropdown(String Market){
		System.out.println("Market Selected"+Market);
		//Market= Market+" ";
		WebDriver driver = DriverFactory.getCurrentDriver();
		String initURL = driver.getCurrentUrl();
		SeleniumUtils.storeKeyValue(KeyStoreConstants.orderFlag, "true");
		Actions actions = new Actions(driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		SeleniumUtils.scrollToElement(HomePageLocators.marketSelectorDropdown);
		if(!(Market.equalsIgnoreCase("United Kingdom"))) {
			SeleniumUtils.click(HomePageLocators.marketSelectorDropdown);
			SeleniumUtils.click(HomePageLocators.marketSelectDropdownValue.format(Market));
			//SeleniumUtils.selectFromDropdownByText(HomePageLocators.marketSelectorDropdown, Market);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.homePageLogo);
			WebDriverWait wait = new WebDriverWait(driver, 50);
			wait.until(ExpectedConditions.not(ExpectedConditions.urlToBe(initURL)));
		}
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User navigated to "+Market+" market");
		return this;
	}

	/**
	 * Enter Search Keyword in text field and validate user is directed to corresponding Search Results Page
	 * @author Edwin_John
	 * @param searchKey
	 */
	public HomePage enterSearchKeyword(String searchKey){
		WebDriver driver = DriverFactory.getCurrentDriver();
		//SeleniumUtils.click(HomePageLocators.searchBtn);
		WebElement elem= SeleniumUtils.findElement(driver, HomePageLocators.searchBtn);
		JavascriptExecutor js=(JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", elem);
		//SeleniumUtils.type(HomePageLocators.searchTextField, searchKey); 
		//new Actions(driver).sendKeys(searchKey).perform();
		elem=SeleniumUtils.findElement(driver, HomePageLocators.searchTextField);
		js.executeScript("arguments[0].value='"+searchKey+"'", elem);
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter SearchKeyword "+searchKey+" in Text Field and click Enter");
		help.Keypress(HomePageLocators.searchTextField, "ENTER");
		
		//new Actions(driver).sendKeys(Keys.ESCAPE).perform();
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.searchTextEnteredInTxtField);
		TestAssert.verifyTrue(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(),
				HomePageLocators.searchTextEnteredInTxtField).getText().contains(searchKey), "User isn't directed to the corresponding Search Results Page");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User is navigated to the corresponding Search Results Page");
		
		
		return this;
	}

	/**
	 * @author harish.prasanna
	 * Entering keywords in the search field      
	 * @throws Exception
	 */
	public HomePage enterSearchKeywordInTxtField(String suggestkywd) throws Exception{
		SeleniumUtils.storeKeyValue(KeyStoreConstants.searchTag, suggestkywd);
		SeleniumUtils.click(HomePageLocators.searchBtn);
		SeleniumUtils.type(HomePageLocators.searchTextField, suggestkywd);
		return this;       
	}

	/**
	 * @author harish.prasanna
	 * Display of suggestions 
	 * @throws Exception
	 */
	public HomePage validateSearchSuggestionsAreDisplayed() throws Exception{
		try {

			SeleniumUtils.WaitForElementToBeVisible(tbs_HomePageLocators.searchSuggestionHeader);
			Boolean suggestion = SeleniumUtils.isElementPresent(tbs_HomePageLocators.searchSuggestionHeader);
			Assert.assertEquals(suggestion.booleanValue(), true);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Suggestions displayed");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Suggestions doesn't gets displayed");
			throw new RuntimeException("Suggestions doesn't gets displayed",e);
		}
		return this;

	}

	/**
	 * @author harish.prasanna
	 * Display of suggestions for invalid keywords       
	 * @throws Exception
	 */

	public HomePage validateSuggestionErrMsgForInvalidKeywords() throws Exception{
		try {
			SeleniumUtils.WaitForElementToBeVisible(tbs_HomePageLocators.invalidSearchResultErrMsg);
			String title = SeleniumUtils.GetText(tbs_HomePageLocators.invalidSearchResultErrMsg);
			String ExpectedTitle = "We could not find any results";
			Boolean value = title.equals(ExpectedTitle);
			Assert.assertEquals(value.booleanValue(), true);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Suggestions doesn't get displayed");

		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Suggestions displayed");
			throw new RuntimeException("Suggestions gets displayed",e);
		}
		return this;
	}



	/**
	 * @author harish.prasanna
	 * Clicking on first suggestion  
	 * @throws Exception
	 */
	public HomePage clickOnSearchSuggestionTxt() {
		try {
			SeleniumUtils.WaitForElementToBeVisible(tbs_HomePageLocators.searchSuggestionsLinkText);
			SeleniumUtils.click(tbs_HomePageLocators.searchSuggestionsLinkText);
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return this;
	}

	/**
	 * @author harish.prasanna
	 * Navigation to SRP      
	 * @throws Exception
	 */

	public HomePage validateUserIsInSearchResultsPage(String sychkwd) throws Exception {
		try {
			SeleniumUtils.WaitForElementToBeVisible(tbs_HomePageLocators.searchTextEnteredInTxtField);
			String SRPTitle = SeleniumUtils.GetText(tbs_HomePageLocators.searchTextEnteredInTxtField);
			Boolean search = SRPTitle.contains(sychkwd);
			Assert.assertEquals(search.booleanValue(), true);
			DriverFactory.Testlogger.log(LogStatus.PASS, "SRP has been Loaded");
			System.out.println("SRP has been loaded with "+ sychkwd +" products");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "SRP has not been loaded");
			throw new RuntimeException("Failure in loading SRP",e);
		}
		return this;
	}

	//Site Rating Badge Page---------------
	/**
	 * @author alwin.ashley
	 * validate whether Site rating badge is displayed      
	 * @throws Exception
	 */
	public HomePage validateSiteRatingBadgeDisplayed() throws Exception{
		try {
			Assert.assertTrue("Site Rating badge is not displayed", SeleniumUtils.isElementPresent(tbs_HomePageLocators.siteRatingImage));
			DriverFactory.Testlogger.log(LogStatus.PASS, "The Site Rating Badge is displayed");
			String screenshotPath = SeleniumUtils.getScreenhot("The Site Rating Badge is displayed");
			DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
		}
		catch (Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Site Rating Badge is not displayed, hence scenario failed");
			String screenshotPath = SeleniumUtils.getScreenhot("Site Rating Badge is not displayed");
			DriverFactory.Testlogger.log(LogStatus.FAIL, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
			throw new RuntimeException("Site Rating Badge scenario failed",e);
		}	
		return this;
	}


	/**
	 * @author alwin.ashley
	 * validate whether Site rating badge is not displayed      
	 * @throws Exception
	 */
	public HomePage validateSiteRatingBadgeNotDisplayed() throws Exception{
		try {
			boolean status = SeleniumUtils.isElementPresent(tbs_HomePageLocators.siteRatingImage);
			Assert.assertFalse("Site Rating badge is displayed", status);
			DriverFactory.Testlogger.log(LogStatus.PASS, "The Site Rating Badge is not displayed");
			String screenshotPath = SeleniumUtils.getScreenhot("The Site Rating Badge is not displayed");
			DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
		}

		catch (Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Site Rating Badge is displayed, hence scenario failed");
			String screenshotPath = SeleniumUtils.getScreenhot("Site Rating Badge is displayed");
			DriverFactory.Testlogger.log(LogStatus.FAIL, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
			throw new RuntimeException("Site Rating Badge scenario failed",e);
		}	
		return this;
	}

	/**
	 * Click on the Wishlist icon in header
	 * @author Edwin_John
	 * @return
	 */
	public HomePage clickOnWishlistBtnInHeader() {
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on the WishList button in Header");
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.wishlistBtnInHeader);
		SeleniumUtils.click(HomePageLocators.wishlistBtnInHeader);
		return this;
	}

	/**
	 * Hover over the PLP to get the overlay
	 * @author Edwin_John
	 * @return
	 */
	public HomePage hoverOverPLP(String categoryName) {
		SeleniumUtils.storeKeyValue(KeyStoreConstants.plpName, categoryName);
		/*//sleep to be removed
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
//		__old__SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.plpCategoryHeading.format(categoryName.toLowerCase()));
//		__old__SeleniumUtils.ScrollIntoView(HomePageLocators.plpCategoryHeading.format(categoryName.toLowerCase()), true);
//		__old__SeleniumUtils.hoverOverElement(HomePageLocators.plpCategoryHeading.format(categoryName.toLowerCase()));
		SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.plpCategoryHeading.format(categoryName));
		SeleniumUtils.ScrollIntoView(HomePageLocators.plpCategoryHeading.format(categoryName), true);
		SeleniumUtils.hoverOverElement(HomePageLocators.plpCategoryHeading.format(categoryName));
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(HomePageLocators.plpCategoryOverlay.format(categoryName)),
				"The PLP Overlay isn't displayed when user hovers over "+categoryName+" PLP heading");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User Hovers over the "+categoryName+" category and PLP Overlay is displayed");
		return this;
	} 

	/**
	 * Click on the subcategory link
	 * @author Edwin_John
	 * @param subCategory
	 * @return
	 */
	public HomePage clickOnTheSubcategoryLink(String subCategory) {
		//__old__String subCat = subCategory.replace(" ", "-");
		String subCat = subCategory;
		/*//Need to be commented
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		
		String plpName = SeleniumUtils.getStoredValue(KeyStoreConstants.plpName).toString();
		//__old__SeleniumUtils.waitForElementToBeClickable(HomePageLocators.subCategoryLinkInOverlay.format(plpName.toLowerCase(),subCat.toLowerCase()));
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.subCategoryLinkInOverlay.format(plpName,subCat));
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on the "+subCategory+" subcategory in the Overlay displayed");
		//SeleniumUtils.click(HomePageLocators.subCategoryLinkInOverlay.format(plpName.toLowerCase(),subCat.toLowerCase()));
		WebDriver driver = DriverFactory.getCurrentDriver();
		//__old__WebElement elem= SeleniumUtils.findElement(driver, HomePageLocators.subCategoryLinkInOverlay.format(plpName.toLowerCase(),subCat.toLowerCase()));
		WebElement elem= SeleniumUtils.findElement(driver, HomePageLocators.subCategoryLinkInOverlay.format(plpName,subCat));

		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();",elem);
		//SeleniumUtils.waitForElementToBeClickable(HomePageLocators.subCategoryBreadCrumbLink.format(subCategory.toLowerCase()));
//		--old--TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(HomePageLocators.subCategoryBreadCrumbLink.format(subCategory)),
//				"The User isn't navigated to the "+subCategory, "The User is navigated to the "+subCategory+" and breadcrumb is displayed");
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(HomePageLocators.subCategoryBreadCrumbLink),
				"The User isn't navigated to the "+subCategory, "The User is navigated to the "+subCategory+" and breadcrumb is displayed");

		return this;
	}

	/**
	 * Click on the Homepage Logo and navigate to Home page
	 * @author Edwin_John
	 * @return
	 */
	public HomePage clickOnHomePageLogo() {
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.homePageLogo);
		SeleniumUtils.click(HomePageLocators.homePageLogo);
		return this;
	}

	/**
	 * Click on My Account Button in Header
	 * @author Edwin_John
	 * @return
	 */
	public HomePage clickOnMyAccountBtnInHeader() {
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on My Account Button in HomePage header");
		SeleniumUtils.ScrollIntoView(WishListPageLocators.userProfileIcon, true);
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.userProfileIcon);
		//SeleniumUtils.click(WishListPageLocators.userProfileIcon);
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement element = SeleniumUtils.findElement(driver, WishListPageLocators.userProfileIcon);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
		return this;

	}

	/*public static void main(String[] args) {
		WebDriver driver;
		System.setProperty("http.proxyUser", "edwin.john");
		System.setProperty("http.proxyPassword", "Tower@123");
		System.getProperties().put("http.proxyHost", "192.168.0.57");
		System.getProperties().put("http.proxyPort", "8080");
		DesiredCapabilities capability = DesiredCapabilities.chrome();
		try {
			System.out.println(capability);
			driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),capability);
			driver.get("https://www.google.co.in");
			System.out.println("Google Home Page Opened");
			driver.quit();
			System.out.println("Driver Closed");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/
	
}
