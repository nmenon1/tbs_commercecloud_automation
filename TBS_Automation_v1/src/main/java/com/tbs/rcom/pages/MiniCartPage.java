package com.tbs.rcom.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.ShoppingCartLocators;
import tbs_master.DriverFactory;
import tbs_master.SeleniumUtils;

public class MiniCartPage {



	/**
	 * @author arjun.vijay
	 * View Basket Validation
	 * @throws Exception 
	 * getNoOfProducts() for fetching the number of prdcts added to cart 
	 */
	public MiniCartPage viewBasketValidation(){  
		SeleniumUtils.ScrollIntoView(MiniCartLocators.miniCartBtnInHeader, true);
		SeleniumUtils.elementHighlight(MiniCartLocators.miniCartBtnInHeader);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(MiniCartLocators.viewBasketBtn), "The View Basket Button isn't displayed in MiniCart overlay",
				"The MiniCart Button is displayed in Mini Cart overlay");
		SeleniumUtils.click(MiniCartLocators.viewBasketBtn);
		/*//to be removed
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.cartTitleHeading), "The user isn't navigated to Cart Page on clicking View Basket button in MiniCart overlay", 
				"User is navigated to cart page on clicking View Basket button in Mini cart overlay");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(ShoppingCartLocators.basketPageBreadCrumb), "User is navigated to Basket page");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(ShoppingCartLocators.basketContentsModal), "Products added to cart are not visible in Basket page");
		return this;
	}

	/**
	 * @author arjun.vijay
	 * Hover over mini cart icon
	 */

	public MiniCartPage hoverOverMiniCart()
	{
		    try {
				Thread.sleep(1000);
				} catch (Exception e) {
				// TODO: handle exception
			}
		    
		    SeleniumUtils.ScrollIntoView(MiniCartLocators.miniCartBtnInHeader, true);
			SeleniumUtils.hoverOverElement(MiniCartLocators.miniCartBtnInHeader);

			SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Hovered over the mini cart");
			
		return this;
	}

	/**@author arjun.vijay
	 * Empty mini cart validation
	 * @throws Exception 
	 */
	public MiniCartPage EmptyMiniCartValidation() {

		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Mini cart overlay not displayed as Mini cart is empty");
		TestAssert.verifyTrue(SeleniumUtils.isElementNotPresent(MiniCartLocators.MiniCartOverLay), "Mini cart overlay displayed as 	Cart is not empty");
		return this;

	}

	/**
	 * Validate whether the Product added to cart is displayed in MiniCart overlay
	 * @author Edwin_John
	 */
	public MiniCartPage validateProductAddedIsPresentInMiniCartOverlay(){
		double expectedSubTotalPrice = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
		System.out.println("Subtotal Store "+expectedSubTotalPrice);
		WebDriver driver=DriverFactory.getCurrentDriver();
		List<WebElement> miniCartProductRows=driver.findElements(By.xpath(MiniCartLocators.itemsRowInMiniCartContainer.getelementValue()));
		TestAssert.verifyTrue(miniCartProductRows.size()<=3, "The Number of Product rows in Mini cart overlay is more than 3");
		String prodName = SeleniumUtils.getStoredValue(KeyStoreConstants.productTitleName).toString();
		String prodQty = SeleniumUtils.getStoredValue(KeyStoreConstants.quantityAddedToCart).toString();
		String price = SeleniumUtils.getStoredValue(KeyStoreConstants.productPrice).toString();
		double lineItemPrice = Double.parseDouble(price)*Double.parseDouble(prodQty);
		String lineItemPriceAsString = ""+lineItemPrice;
		System.out.println("Minicart Price "+lineItemPrice);
		if(miniCartProductRows.size()>1) {
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.GetText(MiniCartLocators.productNameWithPositionInMinicartOverlay.format(1)).contains(prodName), 
					"The  recently added item isn't displayed first in Minicart overlay", 
					"The  recently added item is displayed first in Minicart overlay");
			TestAssert.verifyTrue(SeleniumUtils.GetText(MiniCartLocators.productPriceWithPositionInMinicartOverlay.format(1)).contains(lineItemPriceAsString), 
					"The Product Price of the recently added item isn't displayed first in MiniCart overlay");
			TestAssert.verifyTrue(SeleniumUtils.GetText(MiniCartLocators.productQtyWithPositionInMiniCartOverlay.format(1)).contains(prodQty), "The Quantity of the recently added item doesn't match with quantity displayed in Minicart overlay");
		}
		else {
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.GetText(MiniCartLocators.productNameInMinicartOverlay).contains(prodName), 
					"The  recently added item isn't displayed  in Minicart overlay", 
					"The  recently added item is displayed  in Minicart overlay");
			TestAssert.verifyTrue(SeleniumUtils.GetText(MiniCartLocators.productPriceInMinicartOverlay).contains(lineItemPriceAsString), 
					"The Line item Price of the recently added item isn't displayed  in MiniCart overlay");
			TestAssert.verifyTrue(SeleniumUtils.GetText(MiniCartLocators.productQtyInMiniCartOverlay).contains(prodQty), 
					"The Quantity of the recently added item doesn't match with quantity displayed in Minicart overlay");
		}
        SeleniumUtils.ScrollIntoView(MiniCartLocators.miniCartSubtotal, false);
		String actualSubtotal = SeleniumUtils.findElement(driver, MiniCartLocators.miniCartSubtotal).getText();
		actualSubtotal=SeleniumUtils.getPriceAsActualNumber(actualSubtotal);
		double actualSubtotalInValue = Double.parseDouble(actualSubtotal);
		System.out.println("Actual Price "+actualSubtotal);
		TestAssert.verifyTrueAndPrintMsg(actualSubtotalInValue-expectedSubTotalPrice==0, "The Subtotal displayed in Mini cart overlay doesn't match with Actual subtotal"
				,"The Subtotal displayed in Mini cart overlay doesn't match with Actual subtotal");
		return this;	
	}

	/**
	 * Click on Continue Shopping Button in Mini cart overlay
	 * @author arjun.vijay
	 * @return
	 */
	public MiniCartPage clickOnContinueShoppingInMiniCartOverlay()
	{
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		WebDriver driver = DriverFactory.getCurrentDriver();
	    String currentURL= driver.getCurrentUrl();
		SeleniumUtils.ScrollIntoView(MiniCartLocators.continueShoppingBtn, true);
			SeleniumUtils.click(MiniCartLocators.continueShoppingBtn);
			TestAssert.verifyTrue(SeleniumUtils.isElementNotPresent(MiniCartLocators.MiniCartOverLay), "Mini cart overaly not closed when clicked on Continue Shpping");
			String urlAfterContinueShopping = driver.getCurrentUrl();
			Boolean urlValue;
			if(urlAfterContinueShopping.equals(currentURL))
			{
				urlValue=true;
			}
			else
			{
				urlValue=false;
			}
			TestAssert.verifyTrue(urlValue, "User not stayed on the same page he was before");
			SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "User stayed on the same page he was in before on clicking Continue shopping button");
		
		return this;
	}

	/**
	 * Close the minicart overlay by clicking on 'x' button
	 * @author arjun.vijay
	 * @return
	 */
	public MiniCartPage closeMiniCart()
	{   
		SeleniumUtils.click(MiniCartLocators.miniCartCloseBtn);
		TestAssert.verifyTrue(SeleniumUtils.isElementNotPresent(MiniCartLocators.MiniCartOverLay), "Minicart overlay not closed when clicked on Close button");
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Minicart overlay closed on clicking Mini cart Close button");
		return this;

	}

	/**
	 * @author Arjun
	 * Verify user not navigated to empty minicart
	 */
	public MiniCartPage verifyUserNotNavigatedToEmptyBasket()
	{
		WebDriver driver=DriverFactory.getCurrentDriver();
		String currentURL= driver.getCurrentUrl().trim();
		SeleniumUtils.click(HomePageLocators.homePageLogo);
		/*//to be removed:
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
		SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.plpCategoryHeading.format("Body"));
		String afterLoadUrl=driver.getCurrentUrl().trim();
		Boolean urlValue;
		if(currentURL.equals(afterLoadUrl))
			urlValue=true;
		else
			urlValue=false;
		TestAssert.verifyTrue(urlValue, "User navigated away from Home page when clicked on Minicart icon when basket is empty");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User not navigated to Empty basket page on clicking mini cart icon");
		return this;
	}

	/**
	 * Click on Mini Cart icon in Header
	 * @author Edwin_John
	 * @return
	 */
	public MiniCartPage clickOnMiniCartInHeader() {
		SeleniumUtils.waitForElementToBeClickable(MiniCartLocators.miniCartBtnInHeader);
		SeleniumUtils.click(MiniCartLocators.miniCartBtnInHeader);
		return this;
	}


}
