package com.tbs.rcom.pages;

import java.sql.Driver;
import java.util.List;

import javax.mail.Store;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.StorePageLocators;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class StoreDetailsPage {

	public StoreDetailsPage validateUserIsOnStoreDetailsPage() {
		return this;
	}

	/**
	 * Search using any keyword in CIS section in checkout
	 * @author Edwin_John
	 * @param keyword
	 * @return
	 */
	public StoreDetailsPage enterSearchKeywordInCISSection(String keyword) {
		SeleniumUtils.waitForElementToBeClickable(StorePageLocators.inputTextFieldInCISSection);
		SeleniumUtils.type(StorePageLocators.inputTxtFieldInStoreModal, keyword);
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter the Search Keyword "+keyword+" in CIS text field and click on Search icon");
		SeleniumUtils.waitForElementToBeClickable(StorePageLocators.searchIconInCISSection);
		SeleniumUtils.click(StorePageLocators.searchIconInCISSection);
		return this;
	}

	/**
	 * Validate whether the Global error message is displayed for Invalid Store search
	 * @author Edwin_John
	 * @return
	 */
	public StoreDetailsPage validateGlobalErrorMsgForInvalidStoreSearchKeyword() {
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(StorePageLocators.errorMsgForInvalidStoreSearch), 
				"The Global Error Message for Invalid Store search isn't displayed");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Global Error message for Unidentifed location is displayed");
		return this;
	}

	/**
	 * Check if any Stores are having stock for the order and select a Store from the List
	 * @author Edwin_John
	 */
	public StoreDetailsPage selectCISStoreWithStockFromList() {
		int count=1,flag = 0;
		String storeSelected = "";
		WebDriver driver = DriverFactory.getCurrentDriver();
		//List<WebElement> listOfStoresDisplayedInResults = driver.findElements(ElementFormatter.getByObject(StorePageLocators))
		List<WebElement> storesWithStock = driver.findElements(ElementFormatter.getByObject(StorePageLocators.listedStoresStockAvailabilityBtn));
		TestAssert.verifyTrue(storesWithStock.size()>0, "There are no CIS Stores with stock for the Product");

		if(storesWithStock.size()>0) {
			for(WebElement e:storesWithStock) {
				if(SeleniumUtils.IsElementClickable(StorePageLocators.selectButtonForEachListedStore.format(count)))
				//if(e.getAttribute("aria-label").toString().contains("Select"))
				{
					SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Click on the Store with Stock available");
					storeSelected = SeleniumUtils.findElement(driver, StorePageLocators.storeHeadingInResults.format(count)).getAttribute("textContent").toString();
					e.click();
					flag = 1;
					System.out.println("Store Selected "+storeSelected);
					break;
				}
				count++;
			}
		}
		TestAssert.verifyTrue(flag==1, "There are no Stores displayed in List of Stores having Stock for the order");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SeleniumUtils.storeKeyValue("CIS Store", storeSelected);
		return this;
	}

	/**
	 * Verify if the Selected Store from Modal is displayed in CIS Section
	 * @author Edwin_John
	 * @return
	 */
	public StoreDetailsPage validateSelectedStoreIsDisplayedInCIS() {
		String selectedStore = SeleniumUtils.getStoredValue("CIS Store").toString();
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(StorePageLocators.selectedStoreInCISChangeBtn.format(selectedStore)), "The Selected store is not displayed in CIS Section");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Selected Store "+selectedStore+" is displayed in CIS Section");
		return this;
	}

	/**
	 * Click on the Change Link for the Selected Store
	 * @author Edwin_John
	 */
	public StoreDetailsPage clickOnChangeLinkToSelectAnotherStore() {
		String selectedStore = SeleniumUtils.getStoredValue("CIS Store").toString();
		SeleniumUtils.click(StorePageLocators.selectedStoreInCISChangeBtn.format(selectedStore));
		SeleniumUtils.waitForElementToBeClickable(StorePageLocators.storeModalHeading);
		String searchKeyword = SeleniumUtils.getStoredValue("CISSearchKeyword").toString();
		String actualKeywordInStoreModal = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), 
				StorePageLocators.inputTxtFieldInStoreModal).getAttribute("ng-reflect-value");
		TestAssert.verifyTrue(actualKeywordInStoreModal.equalsIgnoreCase(searchKeyword), "The Search Keyword isn't "
				+ "retained in Store modal text field when user clicked on Change link in CIS section");
		return this;
	}

}
