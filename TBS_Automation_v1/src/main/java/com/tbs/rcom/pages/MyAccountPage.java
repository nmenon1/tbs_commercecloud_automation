package com.tbs.rcom.pages;

import java.sql.Driver;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.CheckoutPageLocators;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.LoginPageLocators;
import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.pageLocators.WishListPageLocators;
import com.tbs.rcom.yaml.AddressData;

import gherkin.lexer.El;
import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class MyAccountPage {

	/**
	 * Validate User is signed in by navigating to My Account page - Checking the user name and email id is present
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage validateUserIsSignedIn() {
		SeleniumUtils.WaitForElementToBeVisible(MyAccountPageLocators.myAccountPageHeadingTxt);
		String userName = SeleniumUtils.getStoredValue(KeyStoreConstants.userName).toString();
		String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(MyAccountPageLocators.signedInUserNameTxt.format(userName))
				&&SeleniumUtils.isElementPresent(MyAccountPageLocators.signedInEmailIdTxt.format(emailId)), 
				"The User isn't signed in successfully", "The user  "+emailId.replace(".", ",")+" is signed in successfully");
		return this;
	}

	/**
	 * Click on the Logout Link in My Account 
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage clickOnLogutLink() {
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(MyAccountPageLocators.logoutLink), "The Logout link isn't visible in My Account page", "The logout link is visible in My Account page");
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Click on Logout from My Account");
		SeleniumUtils.click(MyAccountPageLocators.logoutLink);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SeleniumUtils.click(HomePageLocators.homePageLogo);
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.userProfileIcon);
		//sleep to be removed
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement elem= SeleniumUtils.findElement(driver, WishListPageLocators.userProfileIcon);
		//SeleniumUtils.click(WishListPageLocators.userProfileIcon);
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", elem);
		SeleniumUtils.WaitForElementToBeVisible(LoginPageLocators.loginModalHeading);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(LoginPageLocators.emailAddressTxtFieldInLoginModal),
				"User isn't logged out successfully",
				"User has logged out successfully");
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(MiniCartLocators.minicartCountTxt), "Items in cart aren't cleared after Logout", "Items in cart are cleared after logout");
		return this;
	}

	/**
	 * Click on the respective section in My Account - Parametrized
	 * possible values of section are :
	 * Payment - Saved payment methods
	 * Addresses - Address
	 * Orders - Order history
	 * Wishlists - Wislist section
	 * Profile - PRofile 
	 * Preferences - preferences
	 * Rewards - Points and Rewards
	 * @author Edwin_John
	 */
	public MyAccountPage clickOnRequiredSectionInMyAccount(String section) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SeleniumUtils.WaitForElementToBeVisible(MyAccountPageLocators.profileLinkInMyAccount);
		SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.profileLinkInMyAccount);
		String link = "";
		switch(section.toLowerCase()) {
		case "payment":
			link = "payment";
			break;
		case "rewards":
			link = "rewards";
			break;
		case "wishlists":
			link = "wishlists";
			break;
		case "preferences":
			link = "preferences";
			break;
		case "order history":
			link ="order";
			break;
		case "addresses":
			link = "address";
			break;	
		}
		
		SeleniumUtils.click(MyAccountPageLocators.sectionInMyAccount.format(link));
		return this;
	}

	/**
	 * Verify loyalty membership for a newly registered user in Points and Rewards section
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage verifyLoyaltyMembershipInMyAccount(){
		
		String ptsBalance = "";
		String user = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
		String username = SeleniumUtils.getStoredValue(KeyStoreConstants.userName).toString();
		boolean lybcType=false;
		if(SeleniumUtils.getStoredValue(KeyStoreConstants.signupForLYBC).toString().equals("true"))
			lybcType=true;
		if(user.contains("lybc")||lybcType) {
			SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.lybcUserPtsAndRewardsSectionHeading);
			ptsBalance = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), MyAccountPageLocators.pointsForLYBCUser).getText();
			TestAssert.verifyTrueAndPrintMsg(ptsBalance.equalsIgnoreCase("0"), "The Points balance isn't displayed as 0 for newly registered lybc user", 
					"The points balance is displayed as 0 for a newly registered lybc user");
		}
		else if(username.contains("lybc")&&user.contains("guest")) {
			SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.lybcUserPtsAndRewardsSectionHeading);
			ptsBalance = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), MyAccountPageLocators.pointsForLYBCUser).getAttribute("ng-reflect-balance").toString();
			TestAssert.verifyTrueAndPrintMsg(ptsBalance.equalsIgnoreCase("0"), "The Points balance isn't displayed as 0 for newly registered lybc user", 
					"The points balance is displayed as 0 for a newly registered lybc user");
		}
		else {
			/*//sleep to be removed
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO: handle exception
			}*/
			SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.earnRewardsModalInRewardsSection);
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(LoginPageLocators.joinLYBCSignupBtn),
					"The Join button isn't displayed for a non-LYBC user in points and rewards section ",
					"The Join button is displayed for a non-LYBC user in points and rewards section");
		}
		return this;	
	}

	/**
	 * Validate the placed order is displayed first in Order history section
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage validateOrderDisplayedInOrderHistory() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.myAccountLinkInNavMenu);
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(MyAccountPageLocators.orderHistoryHeadingTxt), "User isn't directed to Order history section");
		String orderNumber= SeleniumUtils.getStoredValue(KeyStoreConstants.orderID).toString();
		System.out.println("Expected Order number "+orderNumber);
		System.out.println("Actual Oder num "+SeleniumUtils.findElement(DriverFactory.getCurrentDriver(),MyAccountPageLocators.orderNumInOrderHistory.format(2)).getText());
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(),MyAccountPageLocators.orderNumInOrderHistory.format(2)).getText().contains(orderNumber), 
				"The Order number of recently placed order isn't displayed first in Order history section", 
				"The Order number of recently placed order is displayed first in Order history section");
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on Order details button in Order history section");
		
		SeleniumUtils.ScrollIntoView(MyAccountPageLocators.orderDetailsBtnInOrderHistory, false);
		SeleniumUtils.click(MyAccountPageLocators.orderDetailsBtnInOrderHistory);
		return this;
	}

	/**
	 * Validate Order information in Order details section
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage validateOrderDetailsPage() {
		String orderId = SeleniumUtils.getStoredValue(KeyStoreConstants.orderID).toString();
		String cartTotal = SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString();
		double orderTotal = SeleniumUtils.round(Double.parseDouble(cartTotal), 2);
		//cartTotal = Double.toString(orderTotal);
		SeleniumUtils.ScrollIntoView(MyAccountPageLocators.orderDetailsPageHeadingTxt,false);
		SeleniumUtils.WaitForElementToBeVisible(MyAccountPageLocators.orderDetailsPageHeadingTxt);
		//TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(MyAccountPageLocators.orderNumInOrderDetails.format(orderId)),
		//		"User isn't directed to order details of clicked Order - "+orderId, "User is directed to Order details of Clicked order"+orderId);
		//String orderTotalAtOrderDetailsPage = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), MyAccountPageLocators.orderTotalInOrderDetailsSection.format(orderId)).getText().replaceAll("\\W", ".");
		String orderTotalInDetailSection = SeleniumUtils.getPriceAsActualNumber(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), MyAccountPageLocators.orderTotalInOrderDetailsSection.format(6)).getText());
		Double actualTotal=Double.parseDouble(orderTotalInDetailSection);
		System.out.println("Cart total "+ cartTotal +" orderTotalInOrderDetailsSection "+ SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), MyAccountPageLocators.orderTotalInOrderDetailsSection.format(6)).getText());
		TestAssert.verifyTrue(actualTotal-orderTotal==0.0||actualTotal-orderTotal==0.00, 
				"The Total value displayed for order doesn't match the Place order total value");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, "0.00");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartItemCount, "0");
		return this;
	}
	
	/**
	 * Validate Saved Card details in Saved payments section
	 * @author Edwin_John
	 * @return
	 */
	public MyAccountPage validateSavedCardDetails() {
		String img="";
		String actCardNum="";
		String actName="";
		boolean imgValue=false;
		boolean holderValue = false;
		boolean cardNumberValue = false;
		String cardType = SeleniumUtils.getStoredValue(KeyStoreConstants.savedCardType).toString();
		String cardNum = SeleniumUtils.getStoredValue(KeyStoreConstants.savedCreditCardNumber).toString();
		String cardlastDigit = cardNum.substring(cardNum.length()-4).trim();
		String cardHolderName = SeleniumUtils.getStoredValue(KeyStoreConstants.cardHolderName).toString();
		SeleniumUtils.waitForElementToBeClickable(MyAccountPageLocators.savedPaymentsSectionHeading);
		WebDriver driver = DriverFactory.getCurrentDriver();
		List<WebElement> list = driver.findElements(ElementFormatter.getByObject(MyAccountPageLocators.listOfSavedCards));
		if(list.size()>1) {
			
		}
		else {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			img=SeleniumUtils.findElement(driver, MyAccountPageLocators.cardImageInSavedPayments).getAttribute("aria-label");
			imgValue = img.toLowerCase().contains(cardType.toLowerCase());
			System.out.println("Img value "+imgValue);
			TestAssert.verifyTrueAndPrintMsg(imgValue, "Image for "+cardType.toUpperCase()+" isn't displayed for saved card",
					"Image for "+cardType.toUpperCase()+" is displayed for saved card");
			actName = SeleniumUtils.findElement(driver, MyAccountPageLocators.cardHolderNameInSavedPayment).getAttribute("aria-label");
			holderValue = actName.contains(cardHolderName);
			TestAssert.verifyTrueAndPrintMsg(holderValue, "Card holder name isn't correct in Saved Payments", 
					"Card holder name is correct in Saved Payments");
			actCardNum = SeleniumUtils.findElement(driver, MyAccountPageLocators.savedPaymentCardNumber).getAttribute("aria-label");
			cardNumberValue = actCardNum.contains(cardlastDigit);
			TestAssert.verifyTrueAndPrintMsg(cardNumberValue, "Card number displayed isn't ending with "+cardlastDigit, "Card number displayed is ending with "+cardlastDigit);
		}
		return this;
		
	}
	
	public MyAccountPage validateNewAddressModalOpened()
	{
		SeleniumUtils.click(MyAccountPageLocators.addNewAddressBtnInMyaccount);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(CheckoutPageLocators.addNewAddressModal), "Add new address modal isn't displayed", "Add new address modal displayed");
		return this;
	}
	
	public MyAccountPage validateRemoveSavedAddressInMyAccount()
	{
		WebDriver driver= DriverFactory.getCurrentDriver();
		List <WebElement> listOfAddresses = driver.findElements(ElementFormatter.getByObject(MyAccountPageLocators.listOfAddressesInMyAccount));
		System.out.println("ListofaddressSize "+ listOfAddresses.size());
		for(int i=0;i<listOfAddresses.size();i++)
		{
			SeleniumUtils.click(MyAccountPageLocators.removeSavedAddressBtn);
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(MyAccountPageLocators.removeAddressModal), "Remove address modal isn't displayed", "Remove address modal displayed");
		    SeleniumUtils.click(MyAccountPageLocators.addressRemoveBtnInOverlay);
		    /*//sleep to be removed
		    try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		    SeleniumUtils.waitForOverlayToDisappear(MyAccountPageLocators.removeAddressModal);
		    //TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(MyAccountPageLocators.removeAddressModal), "Remove address modal isn't closed on clicking remove button", "Remove address modal closed on clicking remove button");
		}
		
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(MyAccountPageLocators.listOfAddressesInMyAccount), "Address is not removed", "Saved Address removed successfully");
		return this;
	}
	public MyAccountPage enterNewAddressInMyAccount(AddressData add)
	{
		SeleniumUtils.WaitForElementToBeVisible(CheckoutPageLocators.addNewAddressModal);
		SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.firstNameTxtField);
		SeleniumUtils.type(CheckoutPageLocators.firstNameTxtField, add.firstName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.firstName, add.firstName);
		SeleniumUtils.type(CheckoutPageLocators.lastNameTxtField, add.lastName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.lastName, add.lastName);
		SeleniumUtils.type(CheckoutPageLocators.phnNumberTxtField, add.phoneNumber);
		//SeleniumUtils.click(CheckoutPageLocators.enterAddressManualBtn);
		SeleniumUtils.type(CheckoutPageLocators.addressLine1TxtField, add.addressLine1);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.addressLine, add.addressLine1);
		SeleniumUtils.type(CheckoutPageLocators.addressLine2TxtField, add.addressLine2);
		SeleniumUtils.type(CheckoutPageLocators.cityTxtField, add.city);
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		SeleniumUtils.type(CheckoutPageLocators.zipcodeTxtField, add.zipcode);
		SeleniumUtils.click(CheckoutPageLocators.selectStateDropdown);
		SeleniumUtils.click(CheckoutPageLocators.dropDownValue.format(1));
		SeleniumUtils.click(CheckoutPageLocators.setAddressAsDefaultCheckbox);
			
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter Required Values in Delivery Address Modal");
		SeleniumUtils.click(CheckoutPageLocators.continueBtnInAddressModal);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
}
