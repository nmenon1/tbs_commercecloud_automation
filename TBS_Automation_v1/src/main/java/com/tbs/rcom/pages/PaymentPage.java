package com.tbs.rcom.pages;


import java.sql.Driver;
import java.util.Iterator;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.service.DriverService;
import org.openqa.selenium.support.PageFactory;
import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.actions.PaymentPageAction;
import com.tbs.rcom.core.utils.CommonUtils;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.PaymentPageLocators;
import com.tbs.rcom.pageLocators.CheckoutPageLocators;
import com.tbs.rcom.pageLocators.MyAccountPageLocators;
import com.tbs.rcom.yaml.AddressData;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.PaymentData;
import com.tbs.rcom.yaml.PaymentDetails;

import aj.org.objectweb.asm.Label;
import cucumber.api.java.eo.Se;
import io.appium.java_client.events.api.general.JavaScriptEventListener;
import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class PaymentPage {


    PaymentPageLocators tbs_PaymentPageLocators = null;

    SeleniumUtils help = new SeleniumUtils();

    public PaymentPage() {

        this.tbs_PaymentPageLocators = new PaymentPageLocators();
        PageFactory.initElements(DriverFactory.getCurrentDriver(), tbs_PaymentPageLocators);
    }

    /**
     * @return
     * @author alwin.ashley
     * click On Same As Delivery Checkbox
     */
    public PaymentPage clickOnSameAsDeliveryCheckbox() {

        WebDriver driver = DriverFactory.getCurrentDriver();
        try {
            SeleniumUtils.waitForElementToBeClickable(tbs_PaymentPageLocators.sameAsDeliveryAddressCheckbox);
            Boolean status = driver.findElement(By.xpath("//mat-checkbox[contains(@ng-reflect-aria-label,'Save to my account')]")).isSelected();

            if (status == false) {
                SeleniumUtils.click(tbs_PaymentPageLocators.sameAsDeliveryAddressCheckbox);

            }
            System.out.println("Same as del status" + status);
        } catch (Exception e) {

        }
        return this;
    }

    /**
     * @return
     * @author alwin.ashley
     * Proceed To Finish And Pay
     */
    public PaymentPage proceedToFinishAndPay() {

        try {


            SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.payNowBtn);
            SeleniumUtils.click(PaymentPageLocators.payNowBtn);

            DriverFactory.Testlogger.log(LogStatus.PASS, "Proceed to Finish and Pay");
            System.out.println("Finish and Pay button successfully clicked ");

        } catch (Exception e) {
            DriverFactory.Testlogger.log(LogStatus.FAIL, "Couldnot proceed to Finish and Pay");
            throw new RuntimeException("Couldnot proceed to Finish and Pay", e);

        }
        return this;
    }


    public PaymentPage selectPaymentMethod(String payOption) {
        SeleniumUtils.click(PaymentPageLocators.selectPaymentMethod.format(payOption));
        return this;
    }

    /**
     * Enter Credit card details
     *
     * @param paymentData
     * @return
     * @author Edwin_John
     */
    public PaymentPage enterCreditCardDetails(PaymentData paymentData) {
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.creditCardIframe.format("1"));
        String cardHolderName = paymentData.cardHolderName + CommonUtils.getCurrentTimeStamp();
        int flagForGuest = 0;
        if (SeleniumUtils.getStoredValue(KeyStoreConstants.guestFlowForRegUser).toString().equals("true"))
            flagForGuest = 1;
        String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
        int flag = 0;
        if (emailId.contains("guest") || flagForGuest == 1)
            flag = 1;
        //SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.creditCardIframe.format("1"));
        //SeleniumUtils.ScrollIntoView(PaymentPageLocators.cardNumberField, true);
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.cardNumberField);
        SeleniumUtils.click(PaymentPageLocators.cardNumberField);
        JavascriptExecutor js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
        js.executeScript("arguments[0].click();", SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.cardNumberField));
        SeleniumUtils.type(PaymentPageLocators.cardNumberField, paymentData.cardNumber);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.savedCreditCardNumber, paymentData.cardNumber);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.cardHolderName, cardHolderName);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.savedCardCvv, paymentData.cvvNumber);
        SeleniumUtils.switchToDefaultContent();
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.creditCardIframe.format("2"));
        js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
        js.executeScript("arguments[0].click();", SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.expiryDate));
        SeleniumUtils.type(PaymentPageLocators.expiryDate, paymentData.expiryDate);
        SeleniumUtils.switchToDefaultContent();
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.creditCardIframe.format("3"));
        String cvv = paymentData.cvvNumber;
        String cvvElem = PaymentPageLocators.cvvtextField.getelementValue();
        js = (JavascriptExecutor) DriverFactory.getCurrentDriver();
        js.executeScript("arguments[0].click();", SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.cvvtextField));
        SeleniumUtils.type(PaymentPageLocators.cvvtextField, paymentData.cvvNumber);
        SeleniumUtils.switchToDefaultContent();
        SeleniumUtils.type(PaymentPageLocators.cardHolderNameField, cardHolderName);
        if (flag == 0) {
            SeleniumUtils.smoothScrollToElement(PaymentPageLocators.cardHolderNameField);
//            SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.saveCreditCardCheckbox);
//            SeleniumUtils.elementHighlight(PaymentPageLocators.saveCreditCardCheckbox);
//            SeleniumUtils.hoverOverElement(PaymentPageLocators.saveCreditCardCheckbox);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            for (int i = 0; i < 10; i++)
                if (!SeleniumUtils.getAttributeValue(PaymentPageLocators.saveCreditCardCheckbox, "aria-checked").equalsIgnoreCase("true")) {
                    SeleniumUtils.smoothScrollToElement(PaymentPageLocators.saveCreditCardCheckbox);
                    SeleniumUtils.xpathClick(PaymentPageLocators.saveCreditCardCheckboxDiv);
                    System.out.println("Clicked save card");
                }
            System.out.println("Wating after save card");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Card details entered and saved the card for next time");
        } else
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(PaymentPageLocators.saveCreditCardCheckbox),
                    "The Save card checkbox is enabled for guest user", "The Save card checkbox isn't present for guest user");
        return this;
    }

    /**
     * @return
     * @author arjun.vijay
     * Select payment method
     */
    public PaymentPage paymentMethodSelector(String paymentMethod) {
        paymentMethod = paymentMethod.toLowerCase();
        switch (paymentMethod) {
            case "credit card":
                paymentMethod = "payment-method-credit-card";
                break;
            case "gift card":
                SeleniumUtils.click(PaymentPageLocators.giftCardPaymentMethod);
                break;
            case "klarna pay now":
                paymentMethod = "payment-method-klarna_paynow";
                break;
            case "paypal":
                paymentMethod = "payment-method-paypal";
                break;
            case "klarna slice it":
                paymentMethod = "payment-method-klarna_account";
                break;
            case "klarna pay later":
                paymentMethod = "payment-method-klarna";
                break;
        }
        SeleniumUtils.waitForOverlayToDisappear(PaymentPageLocators.billingAddressOverlay);
        if (!(paymentMethod.contains("gift")))
            SeleniumUtils.click(PaymentPageLocators.selectPaymentMethod.format(paymentMethod));

        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Select the " + paymentMethod + " payment method");
        return this;
    }

    /**
     * @return
     * @throws Exception
     * @author arjun.vijay
     * Select saved credit card
     */
    public PaymentPage selectSavedCreditCard() {
        String firstName = SeleniumUtils.getStoredValue(KeyStoreConstants.cardHolderName).toString();
        String cardNumber = SeleniumUtils.getStoredValue(KeyStoreConstants.savedCreditCardNumber).toString();
        SeleniumUtils.click(PaymentPageLocators.savedCardsBtn);
        SeleniumUtils.click(PaymentPageLocators.savedCreditCardDropdown);
        SeleniumUtils.click(PaymentPageLocators.selectCardFromDropDown.format(firstName));
        String savedCardDetails = SeleniumUtils.GetText(PaymentPageLocators.selectedCardValue);
        //SeleniumUtils.storeKeyValue(KeyStoreConstants.savedCardDetails, savedCardDetails);
        String cvv = SeleniumUtils.getStoredValue(KeyStoreConstants.savedCardCvv).toString();
        System.out.println("Card details: " + savedCardDetails);
        String cardlastDigit = cardNumber.substring(cardNumber.length() - 4).trim();
        SeleniumUtils.storeKeyValue(KeyStoreConstants.cardLast4digit, cardlastDigit);
        System.out.println("CardLastDigit " + cardlastDigit);
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.creditCardIframe.format("1"));
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.cvvtextField);
        SeleniumUtils.type(PaymentPageLocators.cvvtextField, cvv);
        SeleniumUtils.switchToDefaultContent();
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Selected saved credit card");
        return this;
    }

    /**
     * @throws Exception
     * @author arjun.vijay
     * Validating saved card is available in the dropdown
     */
    public PaymentPage validateSelectedSavedCardDetails() {
		/*String cardDetails= SeleniumUtils.getStoredValue(KeyStoreConstants.savedCardDetails).toString().trim();
		String sFirstName=cardDetails.split("\\*")[0].trim();
		String unmaskedSavedCard=cardDetails.replaceAll("\\*", "");
		String unmaskedCardDetails=unmaskedSavedCard.replaceAll("(\\s)+","+");
		String sCardLastDigit=unmaskedCardDetails.split("\\+")[1].trim();
		String storedCardHolderName=SeleniumUtils.getStoredValue(KeyStoreConstants.cardHolderName).toString();
		String storedCardLastDigits=SeleniumUtils.getStoredValue(KeyStoreConstants.cardLast4digit).toString();
		TestAssert.verifyTrue(sFirstName.equals(storedCardHolderName) && sCardLastDigit.equals(storedCardLastDigits), "Wrong card selected from drop down");*/
        return this;
    }

    /**
     * @return
     * @author arjun.vijay
     * Click on PayNow button
     */

    public PaymentPage clickOnPayNowButton() {
        String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
        WebDriver driver = DriverFactory.getCurrentDriver();
        if (market.toLowerCase().contains("usa") || market.toLowerCase().contains("canada")) {
            SeleniumUtils.ScrollIntoView(CheckoutPageLocators.taxAmount, true);
            String taxAmount = SeleniumUtils.findElement(driver, CheckoutPageLocators.taxAmount).getAttribute("aria-label").toString();

            taxAmount = SeleniumUtils.getPriceAsActualNumber(taxAmount);
            Double taxAmnt = Double.parseDouble(taxAmount);
            Double cartTotal = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
            cartTotal += taxAmnt;
            SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, cartTotal);
        }
        SeleniumUtils.smoothScrollToElement(PaymentPageLocators.termsAndConditionsCheckbox);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SeleniumUtils.click(PaymentPageLocators.termsAndConditionsCheckbox);
        TestAssert.verifyTrueAndPrintMsg(!SeleniumUtils.IsElementVisible(PaymentPageLocators.disabledPayNowBtn),
                "The Pay now button is still disabled after clicking the T&C checkbox",
                "The Pay now Button is enabled after clicking the T&C checkbox");
        SeleniumUtils.click(PaymentPageLocators.payNowBtn);
        return this;
    }


    /**
     * Approve payment for Paypal
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage approvePaymentForPaypal() {
		/*if(SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString().contains("guest"))
		{
			
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(PaymentPageLocators.paypalPayNowBtn), "Pay Now Button isn't displayed in Paypal site", 
				"Pay Now button displayed in Paypal site");
		}
		else
		{
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(PaymentPageLocators.paypalAgreeAndPayBtn), "Agree and Pay button isn't displayed in Paypal site", 
					"Agree and Pay button displayed in Paypal site");
		}*/
        //SeleniumUtils.click(PaymentPageLocators.paypalSiteContinueBtn);
        Double orderTotal = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
        try {
            SeleniumUtils.click(PaymentPageLocators.paypalCookies);
        } catch (Exception e) {
        }
        //SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.orderTotalInPaypalSite);
        //System.out.println("Paypal "+ SeleniumUtils.GetText(PaymentPageLocators.orderTotalInPaypalSite));
        WebDriver driver = DriverFactory.getCurrentDriver();
        if (SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString().contains("guest")) {

            WebElement elem = SeleniumUtils.findElement(driver, PaymentPageLocators.orderTotalInPaypalSite);
            String value = elem.getText();
            System.out.println("Paypal Guest" + value);
            SeleniumUtils.scrollToElement(PaymentPageLocators.orderTotalInPaypalSite);
            value = SeleniumUtils.getPriceAsActualNumber(value);
            Double actualTotal = Double.parseDouble(value);
            TestAssert.verifyTrueAndPrintMsg(orderTotal - actualTotal == 0.0 || orderTotal - actualTotal == 0.00,
                    "Order total in Paypal is incorrect ", "Order total in Paypal is matching");
            SeleniumUtils.ScrollIntoView(PaymentPageLocators.paypalPayNowBtn, true);
            SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.paypalBankNameDiv);
            SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.paypalPayNowBtn);
            SeleniumUtils.click(PaymentPageLocators.paypalPayNowBtn);
        } else {
            WebElement elem = SeleniumUtils.findElement(driver, PaymentPageLocators.orderTotalInPaypalSite);
            String value = elem.getText();
            System.out.println("Paypal reg" + value);
            SeleniumUtils.scrollToElement(PaymentPageLocators.orderTotalInPaypalSite);
            value = SeleniumUtils.getPriceAsActualNumber(value);
            Double actualTotal = Double.parseDouble(value);
            TestAssert.verifyTrueAndPrintMsg(orderTotal - actualTotal == 0.0 || orderTotal - actualTotal == 0.00,
                    "Order total in Paypal is incorrect ", "Order total in Paypal is matching");
            SeleniumUtils.ScrollIntoView(PaymentPageLocators.paypalAgreeAndPayBtn, true);
            SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.paypalBankNameDiv);
            SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.paypalAgreeAndPayBtn);
            SeleniumUtils.click(PaymentPageLocators.paypalAgreeAndPayBtn);

        }

        return this;
    }

    /**
     * @return
     * @author harish.prasanna
     * clicking the paypal site redirection button
     */

    public PaymentPage selectPaypalSitePayment() {
		/*WebDriver driver=DriverFactory.getCurrentDriver();
		WebElement frame= driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(frame); 
		SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.payPalSiteButton);
		SeleniumUtils.click(PaymentPageLocators.payPalSiteButton);
		Set<String> handles =  driver.getWindowHandles();
		Iterator<String> it = handles.iterator();
		while (it.hasNext())
			driver.switchTo().window(it.next()).manage().window().maximize();
		System.out.println(driver.getTitle());
		String siteTitle=driver.getTitle();
		TestAssert.verifyTrue(siteTitle.toLowerCase().contains("paypal"), "Unable to select Paypal site payment");
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Selected Paypal site payment");
		//DriverFactory.Testlogger.log(LogStatus.PASS, "Selected Paypal site payment");
		System.out.println("PayPAl site Button clicked Successfully");*/

        return this;
    }

    /**
     * @return
     * @author harish.prasanna
     * Login into paypal account
     */
    public PaymentPage loginIntoPayPalAccount(PaymentData data) {
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.paypalEmailTxtField);
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User is navigated to Paypal site");
        boolean flag = SeleniumUtils.isElementPresent(PaymentPageLocators.paypalLoginSectionHeading);
        if (flag) {
            SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter valid credentials in Paypal site");
            SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.paypalEmailTxtField);
            SeleniumUtils.type(PaymentPageLocators.paypalEmailTxtField, data.PayPalEmailId);
            SeleniumUtils.type(PaymentPageLocators.payPalPasswordField, data.PayPalpassword);
            SeleniumUtils.click(PaymentPageLocators.paypalSiteLoginBtn);
        } else {
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(PaymentPageLocators.usernameInPaypalSite), "User isn't already signed in",
                    "User is signed in Paypal site");
        }
        return this;
    }

    /**
     * @return
     * @author harish.prasanna
     * Making payment via Paypal
     */

    public PaymentPage paymentViaPaypal() throws Exception {
        WebDriver driver = DriverFactory.getCurrentDriver();
		/*SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.payPalContinueButton);
		SeleniumUtils.click(PaymentPageLocators.payPalContinueButton);*/
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.paypalAgreeAndPayBtn);
        Thread.sleep(2000);
        Set<String> handles = driver.getWindowHandles();
        String windowName = null;
        SeleniumUtils.click(PaymentPageLocators.paypalAgreeAndPayBtn);
        for (String window : handles) {
            if (!window.equals(driver.getWindowHandle()))
                windowName = window;
            else
                continue;
        }
        driver.switchTo().window(windowName);
        DriverFactory.Testlogger.log(LogStatus.PASS, "User makes the payment via PayPal successfully");
        System.out.println("User makes the payment via PayPal successfully");

        DriverFactory.Testlogger.log(LogStatus.FAIL, "Unable to make payment through Paypal");

        return this;

    }

    /**
     * @return
     * @author harish.prasanna
     * validation of error message for invalid paypal credentials
     */

    public PaymentPage paymentViaPaypalAsInvalidUser() throws Exception {
        try {

            SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.payPalInvalidErrorMessageText);
            String title = SeleniumUtils.GetText(PaymentPageLocators.payPalInvalidErrorMessageText);
            String ExpectedTitle = "Some of your info isn't correct. Please try again.";
            Boolean value = title.contains(ExpectedTitle);
            Assert.assertEquals(value.booleanValue(), true);
            DriverFactory.Testlogger.log(LogStatus.PASS, "Unable to login into PayPal with invalid credentails");
            System.out.println("Unable to login into PayPal with invalid credentails");
        } catch (Exception e) {
            DriverFactory.Testlogger.log(LogStatus.FAIL, "Able to login into PayPal with invalid credentails");
            throw new RuntimeException("Able to login into PayPal with invalid credentails", e);
        }
        return this;
    }

    /**
     * Click on Add new card option for a user with Saved cards
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage addNewCardInSavedCardsSection() {
        if (SeleniumUtils.IsElementVisible(PaymentPageLocators.addNewCardBtnInSavedCardsSection) == true) {
            SeleniumUtils.click(PaymentPageLocators.addNewCardBtnInSavedCardsSection);
            SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Click on Add New Card when Saved cards are available");
        }
        return this;
    }


    /**
     * Validate if the same as shipping address Cehckbox is selected by default
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage validateIfSameAsShippingAddressCheckboxIsSelected() {
        String fulfillmentType = SeleniumUtils.getStoredValue("FulfillmentType").toString().toLowerCase();
        //change sameAsDeliveryAddressCheckboxNew back to sameAsDeliveryAddressCheckbox once RCOM-8821 is resolved
        SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.billingAddressSection);
        SeleniumUtils.scrollToElement(PaymentPageLocators.sameAsDeliveryAddressCheckboxNew);
        if (fulfillmentType.equalsIgnoreCase("delivery")) {
            //remove this line and uncomment next TestAssert line once RCOM-8821 is resolved
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.sameAsDeliveryAddressCheckboxNew).getAttribute("checked").equalsIgnoreCase("true"),
                    "The Checkbox Same as Delivery address isn't selected by default", "The Checkbox Same as Delivery address is selected by default");
			/*TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.sameAsDeliveryAddressCheckbox).getAttribute("checked").equalsIgnoreCase("true"),
					"The Checkbox Same as Delivery address isn't selected by default", "The Checkbox Same as Delivery address is selected by default");*/
        } else if (fulfillmentType.equalsIgnoreCase("cis") || fulfillmentType.equalsIgnoreCase("collection point"))
            //change sameAsDeliveryAddressCheckboxNew back to sameAsDeliveryAddressCheckbox once RCOM-8821 is resolved
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(PaymentPageLocators.sameAsDeliveryAddressCheckboxNew),
                    "The Checkbox for Same as delivery Address is present for " + fulfillmentType + " fulfillment",
                    "The Checkbox for Same as delivery Address isn't present for " + fulfillmentType + " fulfillment");

        return this;
    }


    /**
     * Add a new billing address
     *
     * @author Edwin_John
     */
    public PaymentPage addNewBillignAddressInCheckout(String type, AddressData add, String country) {
        String emailId = SeleniumUtils.getStoredValue(KeyStoreConstants.emailID).toString();
        int flagForGuest = 0;
        if (SeleniumUtils.getStoredValue(KeyStoreConstants.guestFlowForRegUser).toString().equals("true"))
            flagForGuest = 1;
        int flag = 0;
        if (emailId.contains("guest") || flagForGuest == 1)
            flag = 1;
        String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
        if (SeleniumUtils.getStoredValue("FulfillmentType").toString().contains("delivery")) {
            //change it back to sameAsDeliveryAddressCheckboxToClick once RCOM-8313 is fixed
            SeleniumUtils.WaitForElementToBeVisible(PaymentPageLocators.sameAsDeliveryAddressCheckboxNew);
            SeleniumUtils.click(PaymentPageLocators.sameAsDeliveryAddressCheckboxNew);
        }
        if (SeleniumUtils.IsElementVisible(PaymentPageLocators.billingAddressSavedInCheckout)) {
            System.out.println("Saved is displayed");
            SeleniumUtils.click(PaymentPageLocators.billingAddressSavedInCheckout);
            SeleniumUtils.click(PaymentPageLocators.addNewAddressBtnUnderDropdown);
        } else {

            System.out.println("+ is displayed");
            //SeleniumUtils.click(PaymentPageLocators.addNewBillingAddressBtn);
        }

        SeleniumUtils.WaitForElementToBeVisible(CheckoutPageLocators.addNewAddressModal);
        SeleniumUtils.waitForElementToBeClickable(CheckoutPageLocators.firstNameTxtField);
        SeleniumUtils.type(CheckoutPageLocators.firstNameTxtField, add.firstName);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.firstName, add.firstName);
        SeleniumUtils.type(CheckoutPageLocators.lastNameTxtField, add.lastName);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.lastName, add.lastName);
        //Remove this code once the issue RCOM-8313 is fixed
        //SeleniumUtils.type(CheckoutPageLocators.phnNumberTxtField, "07878123456"); commented as RCOM-8313 is resolved
        if (SeleniumUtils.IsElementVisible(CheckoutPageLocators.enterAddressManualBtn)) {
            SeleniumUtils.click(CheckoutPageLocators.enterAddressManualBtn);
        }
        SeleniumUtils.type(CheckoutPageLocators.addressLine1TxtField, add.addressLine1);
        SeleniumUtils.storeKeyValue(KeyStoreConstants.addressLine, add.addressLine1);
        SeleniumUtils.type(CheckoutPageLocators.addressLine2TxtField, add.addressLine2);
        SeleniumUtils.type(CheckoutPageLocators.cityTxtField, add.city);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SeleniumUtils.type(CheckoutPageLocators.zipcodeTxtField, add.zipcode);
        SeleniumUtils.click(PaymentPageLocators.countryDropdown);
        if (country.toLowerCase().equalsIgnoreCase("same")) {
            SeleniumUtils.click(CheckoutPageLocators.dropDownValueByText.format(add.countryForBillingAddress));
            try {
                Thread.sleep(100);
                SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.stateDropdown);
                SeleniumUtils.click(PaymentPageLocators.stateDropdown);
                SeleniumUtils.click(CheckoutPageLocators.dropDownValueByText.format(add.state));
            } catch (Exception e) {
            }
        } else {
            SeleniumUtils.click(CheckoutPageLocators.dropDownValue.format('a'));
            try {
                Thread.sleep(100);
                SeleniumUtils.click(PaymentPageLocators.stateDropdown);
                SeleniumUtils.click(CheckoutPageLocators.dropDownValue.format('a'));
            } catch (Exception e) {
            }
        }

        if (type.toLowerCase().equalsIgnoreCase("default") && flag == 0)
            SeleniumUtils.click(CheckoutPageLocators.setAddressAsDefaultCheckbox);
        else if (flag == 1)
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(CheckoutPageLocators.setAddressAsDefaultCheckbox), "The Set as default option in billing address is enabled for guest user",
                    "The Set as default option in billing address is disabled for Guest user");
        SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Enter Required Values in Billing Address Modal");
        SeleniumUtils.click(PaymentPageLocators.continueBtnInBillingAddress);
        return this;
    }

    /**
     * Click on the Continue button in billing address form modal
     *
     * @return
     */
    public PaymentPage clickOnContinueBtnForBillingAddress() {
        SeleniumUtils.click(PaymentPageLocators.continueBtnInBillingAddress);
        SeleniumUtils.switchToDefaultContent();
        return this;
    }

    /**
     * Unselect the checkbox 'Same as delivery address'
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage clickToUnselectCheckboxForSameAsDeliveryAddress() {
        SeleniumUtils.ScrollIntoView(PaymentPageLocators.sameAsDeliveryAddressCheckbox, true);
        SeleniumUtils.click(PaymentPageLocators.sameAsDeliveryAddressCheckbox);
        return this;
    }

    /**
     * Enter Gift Card details in the text field
     *
     * @param giftCardNumber
     * @param giftCardPIN
     * @return
     * @author Edwin_John
     */
    public PaymentPage enterGiftCardDetails(String giftCardNumber, String giftCardPIN) {
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.giftCardNumberTextField);
        SeleniumUtils.type(PaymentPageLocators.giftCardNumberTextField, giftCardNumber);
        SeleniumUtils.storeKeyValue("giftCardNumber", giftCardNumber);
        SeleniumUtils.type(PaymentPageLocators.giftCardPinTextField, giftCardPIN);
        SeleniumUtils.click(PaymentPageLocators.useGiftCardBtn);
        return this;
    }

    /**
     * Obtain the outstanding amount payable displayed in Order Summary section
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage calculateOutstandingAmountInOrderSummary() {
        SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.excludingDeliveryTextInOrderSummary);
        SeleniumUtils.ScrollIntoView(PaymentPageLocators.orderSummaryExcludingDeliveryAmountTxt, false);
        TestAssert.verifyTrueAndPrintMsg(
                SeleniumUtils.IsElementVisible(PaymentPageLocators.orderSummaryExcludingDeliveryAmountTxt),
                "The Outstanding Amount payable isn't displayed in Order Summary",
                "The Outstanding Amount payable is displayed in Order Summary");
        String amount = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PaymentPageLocators.orderSummaryExcludingDeliveryAmountTxt).getText();
        amount = SeleniumUtils.getPriceAsActualNumber(amount);
        SeleniumUtils.storeKeyValue("pendingAmount", amount);
        return this;
    }

    /**
     * Click the Add another Gift card link is when amount payable is pending
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage addAnotherGiftCardIfPayableAmtIsRemaining() {
        SeleniumUtils.scrollToElement(PaymentPageLocators.giftCardPaymentMethodHeadingTxt);
        if (SeleniumUtils.getStoredValue("pendingAmount").toString().equals("0.00")) {
            TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(PaymentPageLocators.addAnotherGiftCardLink),
                    "Add Another Gift Card link is displayed when outstanding payable amount is 0",
                    "Add Another Gift Card Link isn't displayed since the Outstanding payable amount is 0");
        } else
            SeleniumUtils.click(PaymentPageLocators.addAnotherGiftCardLink);
        return this;
    }

    /**
     * Validate if Gift Card is successfully applied in checkout and Remove button is displayed
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage validateGiftCardIsApplied() {
        String giftCardNumber = SeleniumUtils.getStoredValue("giftCardNumber").toString();
        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(PaymentPageLocators.giftCardRemoveBtn.format(giftCardNumber)) &&
                        SeleniumUtils.isElementPresent(PaymentPageLocators.giftCardAppliedTxt.format(giftCardNumber)),
                "The Gift Card added wasn't applied successfully and Remove button isn't displayed",
                "The Gift Card was applied successfully and Remove button is displayed");
        return this;
    }

    /**
     * Place order from Klarna pay later external gateway
     *
     * @return
     * @author Edwin_John
     */
    public PaymentPage clickBuyBtnInKlarnaPayLater() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
        MarketDetails data = MarketDetails.fetch(market);
        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(PaymentPageLocators.klarnaHeadline), "User isn't in Klarna Pay later gateway", "User is in Klarna Pay later gateway");
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.klarnaPayLaterIframe);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
     //   SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.klarnaInstallmentSelector);
        SeleniumUtils.switchToDefaultContent();
        SeleniumUtils.scrollToElement(PaymentPageLocators.klarnaPayLaterBuyBtn);
        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(PaymentPageLocators.klarnaPayLaterBuyBtn), "Buy button isn't present in Klarna pay later", "Click on Buy button in Klarna Pay later");
        SeleniumUtils.click(PaymentPageLocators.klarnaPayLaterBuyBtn);
        SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.klarnaAddressConfirmationFrame);
        //TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(PaymentPageLocators.klarnaAddressConfirmationFrame), "Klarna Address confirmation modal not displayed", "Klarna Address confirmation modal displayed");
        WebDriver driver = DriverFactory.getCurrentDriver();
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS,"Klarna screen 1");
     //   SeleniumUtils.selectFromDropdownByIndex(PaymentPageLocators.klarnaTitleField, 1);
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        //SeleniumUtils.click(PaymentPageLocators.klarnaTitleValue);
        Actions act = new Actions(driver);
        SeleniumUtils.attachScreenshotInReport(LogStatus.PASS,"Klarna screen 2");
        WebElement dobElem = driver.findElement(By.xpath("//input[contains(@id,'date-of-birth' )]"));
   //     SeleniumUtils.waitForElementToBeClickable(PaymentPageLocators.DOBFieldKlarnaSite);
   //     SeleniumUtils.click(PaymentPageLocators.DOBFieldKlarnaSite);


        //act.sendKeys(SeleniumUtils.findElement(driver, PaymentPageLocators.DOBFieldKlarnaSite), "02121995");
        //act.build().perform();
        dobElem.click();
        dobElem.clear();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].value='1102121995'", dobElem);
        SeleniumUtils.type(PaymentPageLocators.DOBFieldKlarnaSite, "02121995");
        WebElement elem = SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaZipCodeField);
    //    elem.sendKeys(Keys.chord(Keys.CONTROL, "a"), data.marketData.zipCode.toString());
        //act.sendKeys(SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaZipCodeField), data.marketData.zipCode.toString());
        //act.build().perform();
    //    elem = SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaStreetAddressField);
    //    elem.sendKeys(Keys.chord(Keys.CONTROL, "a"), data.marketData.addressOne.toString());
        //act.sendKeys(SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaStreetAddressField), data.marketData.addressOne.toString());
        //act.build().perform();
    //    elem = SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaCityField);
    //    elem.sendKeys(Keys.chord(Keys.CONTROL, "a"), data.marketData.city.toString());
        //act.sendKeys(SeleniumUtils.findElement(driver, PaymentPageLocators.klarnaCityField), data.marketData.city.toString());
        //act.build().perform();
        elem = SeleniumUtils.findElement(driver, PaymentPageLocators.phoneNumberFieldINKlarnaSite);
        elem.sendKeys(Keys.chord(Keys.CONTROL, "a"), data.marketData.phnNumbFormat.toString());
        SeleniumUtils.click(PaymentPageLocators.continueBtnInKlarnaSite);
        try {
            if (SeleniumUtils.IsElementVisible(PaymentPageLocators.warningMessageInKlarnaSite)) {
                SeleniumUtils.click(PaymentPageLocators.continueBtnInKlarnaSite);
            }
        } catch (Exception e) {
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
/*        TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(PaymentPageLocators.identityConfirmationModalinKlarna), "Identity Confirmation Modal not displayed", "Identity Confirmation Modal displayed");
        System.out.println("IdentiftBtnKlarna " + SeleniumUtils.IsElementVisible(PaymentPageLocators.identityConfirmBtnInKlarna));
        SeleniumUtils.click(PaymentPageLocators.identityConfirmBtnInKlarna);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            SeleniumUtils.waitForFrameAndswitchToIt(PaymentPageLocators.klarnaCheckYourdetailsIframe);
            SeleniumUtils.click(PaymentPageLocators.klarnaCheckYourDetailsModalConfirmBtn);

        } catch (Exception e) {

            try {
                SeleniumUtils.click(PaymentPageLocators.identityConfirmBtnInKlarna);
            } catch (Exception s) {
            }
        }

*/
        return this;
    }
}

