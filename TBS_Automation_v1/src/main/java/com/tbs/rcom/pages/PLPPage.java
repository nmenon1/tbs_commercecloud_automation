/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.xerces.impl.io.UCSReader;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.PDPPageLocators;
import com.tbs.rcom.pageLocators.PLPPageLocators;
import com.tbs.rcom.pageLocators.CheckoutPageLocators;

import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;
import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.ProjectProperties;
import tbs_master.SeleniumUtils;


public class PLPPage {

	/**
	 * Click on See options and add item to cart from See options modal
	 * @author arjun.vijay
	 */
	public PLPPage clickOnSeeOptionsButtonAndAddToCart(int position)
	{
		double qtyAdded = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.quantityAddedToCart).toString());
		WebDriver driver = DriverFactory.getCurrentDriver();
		String count = SeleniumUtils.getStoredValue(KeyStoreConstants.cartItemCount).toString();
		int minicartCount = Integer.parseInt(count);
		String plpName = SeleniumUtils.getStoredValue(KeyStoreConstants.plpName).toString();
		String productName = SeleniumUtils.findElement(driver, PLPPageLocators.productNameByPositionInPLP.format(position)).getText().toString();
		String productPrice = SeleniumUtils.findElement(driver, PLPPageLocators.productPriceByPositionInPLP.format(position)).getText().toString();
		if(plpName.contains("Body")&&position==1) {
			SeleniumUtils.scrollToElement(PLPPageLocators.seeOptionsButtonForFirstItemInBodyPLP);
			SeleniumUtils.click(PLPPageLocators.seeOptionsButtonForFirstItemInBodyPLP);
		}
		else {
			SeleniumUtils.scrollToElement(PLPPageLocators.seeOptionsButtonInPLP.format(position));
			SeleniumUtils.click(PLPPageLocators.seeOptionsButtonInPLP.format(position));
		}
		productPrice = SeleniumUtils.getPriceAsActualNumber(productPrice);
		double itemPrice = Double.parseDouble(productPrice);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productTitleName, productName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productPrice, productPrice);
		double cartTotal = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString());
		cartTotal = cartTotal+(itemPrice*qtyAdded);
		SeleniumUtils.click(PLPPageLocators.addToBagInSeeOptionsModal.format(productName));
		int updatedMiniCartCount = Integer.parseInt(SeleniumUtils.findElement(driver, MiniCartLocators.minicartCountTxt).getText());
		if(updatedMiniCartCount-minicartCount==1)
			SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Item is Added to cart from See Options Modal and MiniCart count is updated to "+updatedMiniCartCount);
		else
			DriverFactory.Testlogger.log(LogStatus.FAIL, "The item wasn't added to Cart from See Options Modal PLP");
		return this;
	}

	/**
	 * @author arjun.vijay
	 * Add number of products to cart from PLP
	 * @throws Exceptions
	 */
	public PLPPage addToBagFromPLP(int position)
	{
		SeleniumUtils.waitForElementToBeClickable(PLPPageLocators.addToBagInSeeOptionsModal.format(position));
		SeleniumUtils.click(PLPPageLocators.addToBagInSeeOptionsModal.format(position));
		DriverFactory.Testlogger.log(LogStatus.PASS, "User is able to add Item to cart from See options Modal in PLP");
		String screenshotPath = SeleniumUtils.getScreenhot("Added to Cart from See Options modal");
		DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
		return this;
	}

	/**
	 * Select Filter Option in PLP/SRP
	 * @author Edwin_John
	 * @return
	 */
	public PLPPage selectFilterOptionInPLP(String filterType) {
		SeleniumUtils.click(PLPPageLocators.filterByBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(PLPPageLocators.filterModalPopup), "The Filter By Modal isn't displayed on clicking the Filter by Button", "The filter By modal is displayed when the user clicks on Filter By button");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PLPPageLocators.filterOptionTypeTxt.format(filterType)), "The Filter by Label isn't present for "+filterType+"");
		SeleniumUtils.click(PLPPageLocators.filterByOptionCheckbox.format(filterType));
		String filterOption = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PLPPageLocators.filterOptionLabelTxt).getText().toString();
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Click on the First Filter Option: "+filterOption+" under Filter - "+filterType);
		return this;
	}
	/**
	 * Click on Apply button in FilterBy modal
	 * @author Edwin_John
	 * @return
	 */
	public PLPPage clickOnApplyFilterBtn() {
		SeleniumUtils.click(PLPPageLocators.filterModalApplyBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(PLPPageLocators.filterModalPopup), 
				"The Filter modal was not closed on Applying the Filter", "The Filter Modal was closed on Applying the Filter");
		return this;
	}

	public void tbs_NavigateToPDP(){
		SeleniumUtils.click(PLPPageLocators.lnk_TBSProduct);
	}

	public void tbs_FilterByRating() throws Exception{

	}

	/**
	 * Select the sorting option from Sort dropdown
	 * @author Edwin_John
	 * @param sortType
	 * @return
	 */
	public PLPPage selectOptionFromSortDropdown(String sortType){
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(PLPPageLocators.sortDropdown), "The Sort dropdown isn't displayed");
		SeleniumUtils.scrollToElement(PLPPageLocators.sortDropdown);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Sort dropdown is present in the page");
		SeleniumUtils.click(PLPPageLocators.sortDropdown);
		int index=0;
		switch(sortType.toLowerCase()) {
		case "price low to high":
			index=3;
			break;
		case "price high to low":
			index=4;
			break;
		case "top rated":
			index=2;
			break;
		}
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Select the "+sortType+" Sort type from the dropdown");
		SeleniumUtils.click(PLPPageLocators.sortOptionInDropdownPanel.format(index));
		SeleniumUtils.storeKeyValue("SortType", sortType);
		return this;
	}

	/**
	 * Validate if Sorting was applied to the page
	 * @author Edwin_John
	 * @return
	 */
	public PLPPage verifyIfSortingIsApplied(){ 
		WebDriver driver = DriverFactory.getCurrentDriver();
		int priceIndex=0,ratingIndex=0;
		String elementTxt="";
		String ratingTxt="";
		boolean isSortedFlag = false;
		String sortType = SeleniumUtils.getStoredValue("SortType").toString();
		ArrayList<String> arrayListForPriceSort = new ArrayList<String>();
		SeleniumUtils.scrollToElement(PLPPageLocators.sortDropdown);
		List<WebElement> priceList = driver.findElements(ElementFormatter.getByObject(PLPPageLocators.productPriceArrayListInPLP));
		SeleniumUtils.scrollToElement(PLPPageLocators.productNameByPositionInPLP.format(1));
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "The Sorting "+sortType+" was Applied to the PLP page");
		for(WebElement e:priceList) {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
			} 
			elementTxt = e.getText();
			elementTxt = SeleniumUtils.getPriceAsActualNumber(elementTxt);
			arrayListForPriceSort.add(elementTxt);
		}
		Double sortArrayOfPrice[]= new Double[arrayListForPriceSort.size()];
		while(priceIndex<arrayListForPriceSort.size()) {
			sortArrayOfPrice[priceIndex] = Double.parseDouble(arrayListForPriceSort.get(priceIndex)); 
			priceIndex++;
		}
		switch(sortType) {
		case "price high to low":
			DriverFactory.Testlogger.log(LogStatus.INFO, "The Prices are displayed in the Order : "+Arrays.toString(sortArrayOfPrice));
			Collections.reverse(Arrays.asList(sortArrayOfPrice));
			isSortedFlag = ArrayUtils.isSorted(sortArrayOfPrice);
			break;
		case "price low to high":
			DriverFactory.Testlogger.log(LogStatus.INFO, "The Prices are displayed in the Order : "+Arrays.toString(sortArrayOfPrice));
			isSortedFlag = ArrayUtils.isSorted(sortArrayOfPrice);
			break;
		case "top rated":
			List<WebElement> ratingList = driver.findElements(ElementFormatter.getByObject(PLPPageLocators.listOfStarRatingOfProductsInPLP));
			ArrayList<String> arrayListForRatingSort = new ArrayList<String>();
			for(WebElement e1:ratingList) {
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", e1);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e2) {
				} 
				ratingTxt = e1.getAttribute("title");
				arrayListForRatingSort.add(ratingTxt);
			}
			Double ratingArrayForSort[] = new Double[arrayListForRatingSort.size()];
			while(ratingIndex<arrayListForRatingSort.size()) {
				ratingArrayForSort[ratingIndex] = Double.parseDouble(arrayListForRatingSort.get(ratingIndex)); 
				ratingIndex++;
			}
			DriverFactory.Testlogger.log(LogStatus.INFO, "The Ratings are displayed in the Order : "+Arrays.toString(ratingArrayForSort));
			Collections.reverse(Arrays.asList(ratingArrayForSort));
			isSortedFlag = ArrayUtils.isSorted(ratingArrayForSort);
			break;
		}
		TestAssert.verifyTrueAndPrintMsg(isSortedFlag==true, "The Products aren't sorted in the order of "+sortType, "The Products are Sorted in the Order of "+sortType);
		SeleniumUtils.click(HomePageLocators.homePageLogo);
		return this;
	}

	/**
	 * Validate whether user is able to increment quantity in See Options modal using count increment
	 * @author Edwin_John
	 */
	public PLPPage  updateQtyInSeeOptionsModal(int qty){
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		String currentQty = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getText();
		int flag1=0,flag2=0;
		int qtyInNum = Integer.parseInt(currentQty);
		SeleniumUtils.click(PDPPageLocators.qtyAdjusterPlusBtn);
		currentQty = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getText();
		int updatedQty = Integer.parseInt(currentQty);
		if(updatedQty == qtyInNum+1)
			flag1=1;
		SeleniumUtils.click(PDPPageLocators.qtyAdjusterMinusBtn);
		currentQty=SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getText();
		updatedQty=Integer.parseInt(currentQty);
		if(updatedQty==qtyInNum)
			flag2=1;
		TestAssert.verifyTrueAndPrintMsg(flag1==1&&flag2==1, "The Quantity Adjuster Functionality isn't working fine in See Options Modal", 
				"The Quantity Adjuster is working fine in See Options Modal in PLP");
		if(qty!=1)
			for(int i=1;i<qty;i++) {
				SeleniumUtils.click(PDPPageLocators.qtyAdjusterPlusBtn);	 
			}
		int actualQtyAdded = Integer.parseInt(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getAttribute("value").toString());
		TestAssert.verifyTrue(actualQtyAdded==qty, "The Item quantity wasn't updated correctly to "+qty+" ,was added to "+actualQtyAdded+"." );
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Quantity is Updated to the required input");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.quantityAddedToCart, qty);
		return this;
	}

	/**
	 * @author nithin.c03
	 * Method to validate See Options modal close functionality
	 *
	 */
	public void tbs_SeeOptionsModalClose() throws Exception{
		SeleniumUtils.click(PLPPageLocators.elm_TBSCloseSeeOptions); 
		DriverFactory.pageRefresh();
	}

	/**
	 * @author nithin.c03
	 * Method to click on See Options button for specific product using product ID
	 */
	public void productSpecificSeeOptionsClick(String productID){
		SeleniumUtils.click(PLPPageLocators.productSpecificSeeOptionsBtn.format(productID));
	}

	/**
	 * @author nithin.c03
	 * Method to validate Out Of Stock Email Functionality
	 */

	public void emailMeWhenInStockValidation(){	
		SeleniumUtils.click(PLPPageLocators.emailMeWhenInStockBtn);
		SeleniumUtils.type(PLPPageLocators.notifyMeUserEmailInput, "test@gmail.com");
		SeleniumUtils.click(PLPPageLocators.emailMeWhenInStockConfirmationBtn);
		SeleniumUtils.WaitForElementToBeVisible(PLPPageLocators.emailMeWhenInStockSuccessMsg);
		SeleniumUtils.isElementPresent(PLPPageLocators.emailMeWhenInStockSuccessMsg);
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(PLPPageLocators.emailMeWhenInStockSuccessMsg), "Email notification success message is not displayed");		
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Email Me When In Stock Fucntionality works as expected");	
		SeleniumUtils.click(PLPPageLocators.notifyMeUserEmailCloseIcon);		
	}


	/**
	 * @author nithin.c03
	 * Method to navigate to a specific PDP using product ID
	 */
	public void productSpecificPDPNavigation(String productID) throws Exception{
		SeleniumUtils.WaitForElementToBeVisible(PLPPageLocators.productSpecificLink.format(productID));
		SeleniumUtils.click(PLPPageLocators.productSpecificLink.format(productID));

	}	


	public void Test() {
		ArrayList<String> arrayListForSort = new ArrayList<String>();
		arrayListForSort.add("7.00");
		arrayListForSort.add("6.50");
		arrayListForSort.add("5.20");
		Double sortArrayOfPrice[]= new Double[arrayListForSort.size()];
		int i=0;
		while(i<arrayListForSort.size()) {
			sortArrayOfPrice[i] = Double.parseDouble(arrayListForSort.get(i)); 
			i++;
		}
		System.out.println("Array: ");
		for(int j=0;j<sortArrayOfPrice.length;j++)
			System.out.println(sortArrayOfPrice[j]);
		Collections.reverse(Arrays.asList(sortArrayOfPrice));
		System.out.println("Modified Array: ");
		for(int j=0;j<sortArrayOfPrice.length;j++)
			System.out.println(sortArrayOfPrice[j]);
		System.out.println("Sorted: "+ArrayUtils.isSorted(sortArrayOfPrice));
	}
	public static void main(String[] args) {
		/*double[] a = {7.00,6.00,5.00,1.50};
		Collections.reverse(Arrays.asList(a));
		System.out.println("Reversed Array "+Arrays.toString(a));
		System.out.println("Sorted: "+ArrayUtils.isSorted(a));*/

		/*Double arr[] = {50.00, 40.00, 30.00, 20.00, 10.00}; 

		System.out.println("Original Array : " + 
				Arrays.toString(arr)); 
		Collections.reverse(Arrays.asList(arr)); 
		String arr1 = Arrays.toString(arr);
		System.out.println("New : "+arr1);
		System.out.println("Modified Array : " + 
				Arrays.toString(arr));
		System.out.println("Sorted: "+ArrayUtils.isSorted(arr));*/
		PLPPage p = new PLPPage();
		p.Test();

	}
}
