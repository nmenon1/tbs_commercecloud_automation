package com.tbs.rcom.pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.relevantcodes.extentreports.LogStatus;

import com.tbs.rcom.core.utils.CommonUtils;

import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;

import com.tbs.rcom.pageLocators.MiniCartLocators;
import com.tbs.rcom.pageLocators.PDPPageLocators;
import com.tbs.rcom.pageLocators.PLPPageLocators;
import com.tbs.rcom.pageLocators.PaymentPageLocators;
import com.tbs.rcom.pageLocators.CheckoutPageLocators;
import com.tbs.rcom.pageLocators.ShoppingCartLocators;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class ShoppingCartPage {

	ShoppingCartLocators tbs_ShoppingCartLocators=null;
	SeleniumUtils help = new SeleniumUtils();
	MiniCartLocators tbs_MiniCartLocators=null;

	public ShoppingCartPage() {

		this.tbs_ShoppingCartLocators= new ShoppingCartLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), tbs_ShoppingCartLocators);
		this.tbs_MiniCartLocators=new MiniCartLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), tbs_MiniCartLocators);
	}

	/**
	 * @author alwin.ashley
	 * Proceed to checkout page by clicking on Checkout button in Cart page
	 */
	public ShoppingCartPage clickCheckoutBtnInCart(){
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(ShoppingCartLocators.proceedToCheckoutBtnInCart), "The Checkout button isn't"
				+ "displayed in shopping Cart");
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Click on Proceed To Checkout Button");
		SeleniumUtils.click(ShoppingCartLocators.proceedToCheckoutBtnInCart);
		return this;
	}

	/**
	 * @author Edwin_John
	 * Remove all items present in basket page
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage removeAllItemsFromBasket() {

		WebDriver driver=DriverFactory.getCurrentDriver();
		SeleniumUtils.waitForElementToBeClickable(tbs_ShoppingCartLocators.cartTitleHeading);
		List<WebElement> removeBtnList=driver.findElements(By.xpath("//div[contains(@class,'cart-details')]//button[contains(@title,'Remove')]"));
		for(int i=0;i<removeBtnList.size();i++) {  
			TestAssert.verifyTrue(SeleniumUtils.IsElementClickable(ShoppingCartLocators.removeItemBtn), "User not able to remove product from basket page");
			SeleniumUtils.click(ShoppingCartLocators.removeItemBtn);

			if(i==removeBtnList.size()-1) {break;}
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "All products are removed from the basket");
		return this;
	}


	public ShoppingCartPage validateEmptyBasket()
	{
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(ShoppingCartLocators.emptyCartContinueShoppingBtn), "Cart is not empty and Continue shopping button not displayed");
		SeleniumUtils.ScrollIntoView(ShoppingCartLocators.emptyCartContinueShoppingBtn, true);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "All products are removed from basket and Basket empty message is display1wed");
		return this;
	}

	public ShoppingCartPage adjustmentOfQuantity(String quantitytobeadjusted) throws Exception {
		WebDriver driver = DriverFactory.getCurrentDriver();
		WebElement elm = SeleniumUtils.findElement(driver, ShoppingCartLocators.quantityAdjusterDrpdwn);
		SeleniumUtils.WaitForElementToBeVisible(ShoppingCartLocators.quantityAdjusterDrpdwn);
		help.elementHighlight(ShoppingCartLocators.quantityAdjusterDrpdwn);
		Select Selector = new Select(elm);
		Selector.selectByValue(quantitytobeadjusted);
		WebElement tile=Selector.getFirstSelectedOption();
		String Selectedoption = tile.getText().trim();
		Boolean Val = quantitytobeadjusted.equals(Selectedoption);
		Assert.assertTrue("Quantity has been adjusted", Val.booleanValue());
		DriverFactory.Testlogger.log(LogStatus.PASS, "Quantity has been adjusted");			
		return this;
	}


	/**
	 * Add Personal message service in Cart
	 * @author Edwin_John
	 * @return
	 */
	public ShoppingCartPage addPersonalMessage(){
		SeleniumUtils.ScrollIntoView(ShoppingCartLocators.addGiftMsgBtn, false);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(ShoppingCartLocators.giftMsgServiceRemoveBtn), "Personal message is already added for user", "Personal msg isn't present initially for user");
		DriverFactory.Testlogger.log(LogStatus.INFO,"Click on Add Message button");
		SeleniumUtils.click(ShoppingCartLocators.addGiftMsgBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.addPersonalMessageModal), "Add personal message modal isn't displayed", "Personal msg modal is displayed on Clicking Add");
		SeleniumUtils.type(ShoppingCartLocators.giftMsgRecipientTxtField, "To"+CommonUtils.getCurrentTimeStamp());
		SeleniumUtils.type(ShoppingCartLocators.giftMsgSenderTxtField, "From"+CommonUtils.getCurrentTimeStamp());
		String message= "Message"+CommonUtils.getCurrentTimeStamp();
		SeleniumUtils.type(ShoppingCartLocators.giftMsgTxtField,message);
		SeleniumUtils.click(ShoppingCartLocators.giftMsgServiceAddBtnInOverlay);
		SeleniumUtils.ScrollIntoView(ShoppingCartLocators.giftMessageTxtAdded, true);
		SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.giftMessageTxtAdded);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.GetText(ShoppingCartLocators.giftMessageTxtAdded).equals(message),"Added personal message is not Saved","Personal Message "+message +" is saved in Basket");
		return this;
	}


	/** @author harish.prasanna
	 * Click the Add Gift box option in Cart
	 */

	public ShoppingCartPage clickAddGiftBoxOption() {
		String giftWrapPrice = SeleniumUtils.getPriceAsActualNumber(SeleniumUtils.GetText(ShoppingCartLocators.giftWrapPriceText));
		double giftWrap = Double.parseDouble(giftWrapPrice);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.giftWrapPrice, giftWrap);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.addGiftBoxOption), "Add Gift Wrap option not available", "Add button for Gift wrap displayed");
		SeleniumUtils.click(ShoppingCartLocators.addGiftBoxOption);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.addGiftWrapModal), "Add Gift wrap modal not displayed", "Gift wrapping modal is displayed");
		return this;
	}

	/**
	 * @author harish.prasanna
	 * Click on cancel button in the Add gift wrap modal
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage clickCancelBtnInAddGiftWrapModal() throws Exception {
		try {
			SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.cancelCtaGiftWrapPopup);
			SeleniumUtils.click(ShoppingCartLocators.cancelCtaGiftWrapPopup);
			Boolean gift = SeleniumUtils.isElementPresent(ShoppingCartLocators.addGiftBoxOption);
			Assert.assertTrue("Cancel Button clicked successfully", gift.booleanValue());
			DriverFactory.Testlogger.log(LogStatus.PASS, "Cancel button clicked successfully and Gift wrap has not added to the order");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Cancel button not clicked");
			throw new RuntimeException("Cancel button not clicked",e);
		}
		return this;
	}

	/**
	 * @author harish.prasanna
	 * Validating whether order summary gets updated on adding gift wrap
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage validateGiftWrapIsAddedToCart()  {
		WebDriver driver = DriverFactory.getCurrentDriver();
		String orderTotal = SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString();
		orderTotal = SeleniumUtils.getPriceAsActualNumber(orderTotal);
		double initialOrdrTtl = Double.parseDouble(orderTotal);	
			SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.addCtaGiftWrapPopup);
			SeleniumUtils.click(ShoppingCartLocators.addCtaGiftWrapPopup);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String updatedOrdrTtl =(SeleniumUtils.findElement(driver, ShoppingCartLocators.orderSummaryExcludingDelvieryText)).getText();
			System.out.println("String value "+updatedOrdrTtl);
			updatedOrdrTtl = SeleniumUtils.getPriceAsActualNumber(updatedOrdrTtl);
			double newOrdrTtl = Double.parseDouble(updatedOrdrTtl);
			System.out.println("new ORder total"+newOrdrTtl);
			double giftWrapPrice = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.giftWrapPrice).toString());
			System.out.println("NewTotal "+ newOrdrTtl);
			TestAssert.verifyTrueAndPrintMsg((initialOrdrTtl + giftWrapPrice)-newOrdrTtl==0, "Gift wrap is not added to cart", "Gift wrap added successfully");
 
		return this;
	}

	/**
	 * @author harish.prasanna
	 * Clicking on remove gift wrap button
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage clickingRemoveButtonInBasket() {
		
			SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.removeGiftWrapButton);
			SeleniumUtils.click(ShoppingCartLocators.removeGiftWrapButton);
			TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.removeGiftWrapModal), "Remove Gift wrap modal not displayed", "Remove Gift wrap modal displayed");

		return this;
	}

	/**
	 * @author nithin.c03
	 * Method to edit personal message
	 *
	 */

	public ShoppingCartPage editPersonalMessage(){
		SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.giftMsgServiceEditBtn);
		DriverFactory.Testlogger.log(LogStatus.INFO,"Click on Edit Message button");
		SeleniumUtils.click(ShoppingCartLocators.giftMsgServiceEditBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.editPersonalMessageModal), "Edit Personal msg modal not displayed", "Edit Personal msg modal is displayed");
		SeleniumUtils.type(ShoppingCartLocators.giftMsgRecipientTxtField, "ToEdited"+CommonUtils.getCurrentTimeStamp());
		SeleniumUtils.type(ShoppingCartLocators.giftMsgSenderTxtField, "FromEdited"+CommonUtils.getCurrentTimeStamp());
		String message = "MessageEdited"+CommonUtils.getCurrentTimeStamp();
		SeleniumUtils.type(ShoppingCartLocators.giftMsgTxtField,message);
		SeleniumUtils.click(ShoppingCartLocators.giftMsgServiceAddBtnInOverlay);
		SeleniumUtils.waitForOverlayToDisappear(ShoppingCartLocators.editPersonalMessageModal);
		SeleniumUtils.ScrollIntoView(ShoppingCartLocators.giftMsgServiceRemoveBtn,false);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.GetText(ShoppingCartLocators.giftMessageTxtAdded).equals(message), "Edited personal message is not updated", "Message is edited to "+message);
		return this;
	}

	/**
	 * @author nithin.c03
	 * Method to remove personal message
	 */
	public ShoppingCartPage removePersonalMessage(){
		DriverFactory.Testlogger.log(LogStatus.INFO,"Click on Remove Message button");
		SeleniumUtils.ScrollIntoView(ShoppingCartLocators.giftMsgServiceRemoveBtn, false);
		SeleniumUtils.click(ShoppingCartLocators.giftMsgServiceRemoveBtn);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.removePersonalMessageModal), "Remove personal message modal not displayed", "Remove personal message is displayed");
		SeleniumUtils.click(ShoppingCartLocators.giftMsgServiceAddBtnInOverlay);
		TestAssert.verifyTrueAndPrintMsg((SeleniumUtils.IsElementVisible(ShoppingCartLocators.addGiftMsgBtn) && SeleniumUtils.isElementNotPresent(ShoppingCartLocators.giftMsgServiceEditBtn)), "User not able to remove personal message", "Personal message removed successfully");
		return this;
	}


	/** @author harish.prasanna
	 * Clicking on cancel button in the Remove gift wrap modal
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage clickingCancelButtonInRemoveModal() throws Exception {
		SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.cancelCtaGiftWrapPopup);
		SeleniumUtils.click(ShoppingCartLocators.cancelCtaGiftWrapPopup);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(ShoppingCartLocators.removeGiftWrapModal), "Remove giftwrap modal not closed when clicked on Cancel ", "Remove giftwrap modal closed on clicking Cancel");
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.IsElementVisible(ShoppingCartLocators.removeGiftWrapButton), "Giftwrap stil removed when clicked on Cancel button", "Giftwrap removal cancelled");
		return this;
	}


	/**
	 * @author harish.prasanna
	 * Validating whether order summary gets updated on removing gift wrap from the order
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage removingGiftwrapFromOrder() {

		SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.removeCtaGiftWrap);
		SeleniumUtils.click(ShoppingCartLocators.removeCtaGiftWrap);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SeleniumUtils.WaitForElementToBeVisible(ShoppingCartLocators.orderSummaryExcludingDelvieryText);
		String removedOrdrTtl = (SeleniumUtils.GetText(ShoppingCartLocators.orderSummaryExcludingDelvieryText)).replaceAll("[^.,0-9]", "");
		removedOrdrTtl=removedOrdrTtl.replace(",", ".");
		if(removedOrdrTtl.indexOf('.')==0) {
			removedOrdrTtl=removedOrdrTtl.substring(1, removedOrdrTtl.length());
		}
		double removedGiftwrap = Float.parseFloat(removedOrdrTtl);
		System.out.println("RemovedorderTotal "+removedOrdrTtl);
		System.out.println("CartOrdertotal "+ SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal));
		Double basketTotal= Double.parseDouble((String) SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal));
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//TestAssert.verifyTrue((double) SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal)==removedGiftwrap, "Gift wrap has been removed from the order successfully");
		TestAssert.verifyTrueAndPrintMsg(basketTotal==removedGiftwrap, "Giftwrap not removed", "Gift wrap removed successfully");
		return this;
	}


	/**
	 * @author harish.prasanna
	 * Validation of gift wrap option when cart contains ineligible item
	 * @throws InterruptedException 
	 */

	public ShoppingCartPage validationOfGiftWrapForIneligibleProduct() throws Exception {
		Boolean val = SeleniumUtils.isElementPresent(ShoppingCartLocators.addGiftBoxOption);
		Assert.assertFalse("Gift wrap option not applicable for the ineligible product", val.booleanValue());
		DriverFactory.Testlogger.log(LogStatus.PASS, "Gift wrap option not applicable for the ineligible product");	
		return this;
	}

	/**
	 * Validate details of Item added in Shopping Cart page
	 * @author Edwin_John
	 * @return
	 */
	public ShoppingCartPage validateItemAddedInShoppingCart() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		String prodName = SeleniumUtils.getStoredValue(KeyStoreConstants.productTitleName).toString();
		System.out.println("Prod name"+prodName);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementPresent(ShoppingCartLocators.prodNameInBasket.format(prodName)),
				"The Product added to cart isn't displayed in basket", "The Product Added to Cart is displayed in Basket");
		return this;
	}


	public static void main(String[] args) {
		String a = "  "
				+ ""
				+ "  "
				+ "adsa "
				+ ""
				+ ""
				+ "test"
				+ ""
				+ "atda";
		a=a.replace(" ", "");
		System.out.println("New "+a);
	}
}

