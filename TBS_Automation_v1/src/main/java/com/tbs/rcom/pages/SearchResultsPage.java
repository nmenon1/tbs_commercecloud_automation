/**
 * @author arjun.vijay
 * Search Results page
 */

package com.tbs.rcom.pages;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.PLPPageLocators;
import com.tbs.rcom.pageLocators.SRPPageLocators;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class SearchResultsPage {
	
	HomePageLocators homePageLocators= null;
	SRPPageLocators srpPageLocators=new SRPPageLocators();
	SeleniumUtils help=new SeleniumUtils();
	
	public SearchResultsPage()
	{
		this.homePageLocators=new HomePageLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), homePageLocators);
		//this.srpPageLocators=new SRPPageLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), srpPageLocators);
	}
	/**
	 * @author arjun.vijay
	 * Validating redirection to SRP by verifying product tagged 
	 * 
	 */
	
	public SearchResultsPage NavToSRP() throws Exception
	{
		try {
			
			help.Keypress(homePageLocators.searchTextField, "ENTER");
			SeleniumUtils.WaitForElementToBeVisible(srpPageLocators.SRPProductsTaggedTxt);
		    String productsTagged= SeleniumUtils.GetText(srpPageLocators.SRPProductsTaggedTxt).split("'")[1].trim();
			System.out.println("Product tagged "+productsTagged);
			Assert.assertEquals(KeyStoreConstants.searchTag, productsTagged);
			String screenshotPath = SeleniumUtils.getScreenhot("Search results showed for correct search term");
			DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
			
		} catch (Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Search result of wrong search term displayed");
			String screenshotPath = SeleniumUtils.getScreenhot("Search result issue- wrong search term ");
			DriverFactory.Testlogger.log(LogStatus.FAIL, DriverFactory.Testlogger.addScreenCapture(screenshotPath));
			throw new RuntimeException("Wrong product tag",e);
		}
		return this;
	}
	
	/**
	 * @author arjun.vijay
	 * Validate Invalid barcode search by verifying SRP breadcrumb and result count message 
	 * @return
	 */
	
	public SearchResultsPage validateZeroSearchResult()
	{
		String elem= SeleniumUtils.GetText(srpPageLocators.SRPBreadCrumb.format(KeyStoreConstants.searchTag));
		String breadCrumbResultCount = elem.split(" ")[0].trim();
		try {
		Assert.assertEquals("0", breadCrumbResultCount);
		DriverFactory.Testlogger.log(LogStatus.PASS, "Showing 0 results in breadcrumb for invlid barcode search");
		String srpResultCountMsg = SeleniumUtils.GetText(srpPageLocators.SRPResultCountMsg);
		String[] resultCount=srpResultCountMsg.split(("[^\\d]+"));
		Assert.assertEquals("0", resultCount[1]);
		Assert.assertEquals("0", resultCount[2]);
		DriverFactory.Testlogger.log(LogStatus.PASS, "Showing 0 results for invlid barcode");
		DriverFactory.Testlogger.log(LogStatus.PASS, "Showing 0 results for invlid barcode");
		
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Showing wrong results for invlid barcode");
		}
		return this;
	}
	
	/**
	 * @author arjun.vijay
	 * Validating valid barcode search result
	 * validates product id of product displayed in SRP
	 * 
	 */
	
	public SearchResultsPage validBarcodeSearchResultValidation() throws Exception
	{
		try {
		Assert.assertTrue(SeleniumUtils.isElementPresent(srpPageLocators.SRPProdId.format(KeyStoreConstants.srpProductId)));
		DriverFactory.Testlogger.log(LogStatus.PASS, "Product having the searched barcode is displayed");
		return this;
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.PASS, "Incorrect search result displayed for valid barcode");
		}
		return this;
	}
	
	/**
	 * Click on the product title by position displayed in SRP
	 * @author Edwin_John
	 * @param position
	 * @return
	 */
	public SearchResultsPage clickOnTheProductByPositionDisplayedInSRP(int position) {
		//to be removed sleep
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO: handle exception
		}
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(SRPPageLocators.searchBoxModal), "Search Box modal isn't closed when searched", "Search box modal closed when navigated to SRP");
		SeleniumUtils.ScrollIntoView(SRPPageLocators.productLinkByPositionInSRP.format(position), true);
		SeleniumUtils.waitForElementToBeClickable(SRPPageLocators.productLinkByPositionInSRP.format(position));
		String productName = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), SRPPageLocators.productLinkByPositionInSRP.format(position)).getText().toString();	
		String productPrice = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), SRPPageLocators.productPriceInSRP.format(position)).getText().toString();
		productPrice = SeleniumUtils.getPriceAsActualNumber(productPrice);
		//System.out.println("ProductName "+productName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productTitleName, productName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productPrice, productPrice);
		//SeleniumUtils.click(SRPPageLocators.productImageByPositionInSRP.format(position));
		WebDriver driver= DriverFactory.getCurrentDriver();
		WebElement elem= SeleniumUtils.findElement(driver, SRPPageLocators.productImageByPositionInSRP.format(position));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", elem);
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on the Product displayed in "+position+" position");
		return this;
	}

	
	public static void main(String[] args) {
		String s = "12.50skr";
		s = s.replaceAll("[^.,0-9]", "");
		s=s.replace(",", ".");
		if(s.indexOf('.')==0) {
			s=s.substring(1, s.length());
		}
		System.out.println(s);
	}
}
