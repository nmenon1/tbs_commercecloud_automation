/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pages;

public class PageFactory {
	public HomePage homePage() {
		return new HomePage();
	}
	public PLPPage plpPage() {
		return new PLPPage();
	}
	public PDPPage pdpPage() {
		return new PDPPage();
	}
	public ShoppingCartPage shoppingCartPage() {
		return new ShoppingCartPage();
		
	}
	public LoginPage loginPage() {
		return new LoginPage();
		
	}
	public MiniCartPage miniCartPage() {
		// TODO Auto-generated method stub
		return new MiniCartPage();
	}
	public ShippingAddressPage shippingAddressPage() {
		return new ShippingAddressPage();
	}
	public PaymentPage paymentPage() {
		return new PaymentPage();
	}
	
	public SearchResultsPage searchResultsPage()
	{
		
		return new SearchResultsPage();
	}
	
	public  WishlistPage wishlistPage() {
		return new WishlistPage();
	}
	
	public CheckoutPage checkoutPage() {
		return new CheckoutPage();
	}
	
	public StoreDetailsPage storeDetailsPage() {
		return new StoreDetailsPage();
	}
	
	public OrderConfirmationPage orderConfPage() {
		return new OrderConfirmationPage();
	}
	
	public MyAccountPage myAccountPage() {
		return new MyAccountPage();
	}
}
