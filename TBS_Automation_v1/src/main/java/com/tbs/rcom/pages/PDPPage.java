/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package com.tbs.rcom.pages;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.remote.service.DriverService;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.LogStatus;
import com.tbs.rcom.core.utils.APIUtils;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.PDPPageLocators;
import com.tbs.rcom.pageLocators.ShoppingCartLocators;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.MarketTestData;

import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;
import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class PDPPage {
	PDPPageLocators tbs_PdpPageLocators=null;
	SeleniumUtils help = new SeleniumUtils();

	public PDPPage()
	{
		this.tbs_PdpPageLocators= new PDPPageLocators();
		PageFactory.initElements(DriverFactory.getCurrentDriver(), tbs_PdpPageLocators);

	}
	public PDPPage verifyPDPPageContents(){
		try {
			help.IsElementVisible(PDPPageLocators.productImage);
			help.IsElementVisible(PDPPageLocators.productTitleHeading);
			help.IsElementVisible(PDPPageLocators.productPriceMetric_Text);
			help.IsElementVisible(PDPPageLocators.productPriceBlockInPDP);
			DriverFactory.Testlogger.log(LogStatus.PASS, "The product image, title, price or price metric are present");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "The product image, title, price or price metric are absent");
			throw new RuntimeException("Failure in verifying PDP components",e);
		}
		return this;
	}
	public PDPPage verifyPriceMetricInPDP() throws Exception{
		//System.out.println("--------price metric----------");
		String testMarket=tbs_ConfigFileUtils.readConfigFile("tbs.market", "test.properties");
		String currency = null;
		String currPos = null;
		for(List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get(testMarket)) {
			currency = TDataDetailsList.get(0).currency.trim();
			currPos = TDataDetailsList.get(0).currPosition.trim();
		}
		String priceMetric = SeleniumUtils.GetText(PDPPageLocators.productPriceMetric_Text);
		priceMetric = priceMetric.replace(" "+currency, "");
		priceMetric = priceMetric.replace(currency, "");
		String priceValue = null;
		priceValue = priceMetric.split(" ")[0];
		String unitPrice = priceValue.split("/")[0].replace(",", ".");
		String unitSize = priceValue.split("/")[1];
		String strUnit = SeleniumUtils.GetText(PDPPageLocators.elm_ProductUnit);
		StringBuilder priceNumbers = new StringBuilder();
		for (int i = 0; i < strUnit.length(); i++) {
			if (Character.isDigit(strUnit.charAt(i))) {
				priceNumbers.append(strUnit.charAt(i));
			} 
		}
		String strUnitValue = priceNumbers.toString();
		String productPrice = SeleniumUtils.GetText(PDPPageLocators.productPriceBlockInPDP).replace(currency, "");
		productPrice = productPrice.replace(",", ".").trim();
		Float calcPrice = (Float.valueOf(unitPrice)/Float.valueOf(unitSize))*Float.valueOf(strUnitValue);
		calcPrice = SeleniumUtils.round(calcPrice);
		Boolean priceCalculation = calcPrice.equals(Float.valueOf(productPrice));
		Assert.assertEquals(priceCalculation.booleanValue(), true);
		return this;
	}

	public PDPPage verifyCurrencySymbolInPDP() throws Exception{
		String price = SeleniumUtils.GetText(PDPPageLocators.productPriceBlockInPDP);
		String testMarket=tbs_ConfigFileUtils.readConfigFile("tbs.market", "test.properties");
		String currency = null;
		String currencyPos = null;
		for(List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get(testMarket)) {
			currency = TDataDetailsList.get(0).currency.trim();
			currencyPos = TDataDetailsList.get(0).currPosition.trim();
		}
		Boolean currencyCheck = false;
		if (currencyPos.equalsIgnoreCase("LEFT")) {
			currencyCheck = price.split(" ")[0].contains(currency);
		} else {
			currencyCheck = price.split(" ")[1].contains(currency);
		}

		DriverFactory.Testlogger.log(LogStatus.PASS, "The currency symbol"+currency+" is correctly displayed in PDP");
		String screenshotPath = SeleniumUtils.getScreenhot("Currency");
		DriverFactory.Testlogger.log(LogStatus.PASS, DriverFactory.Testlogger.addScreenCapture(screenshotPath));

		return this;
	}




	/**
	 * @author alwin.ashley
	 * Select color variant in PDP	
	 * @throws Exception
	 */
	public PDPPage selectColorVariant() throws Exception{
		try {
			SeleniumUtils.WaitForElementToBeVisible(PDPPageLocators.txt_initialColor);
			String initialColor = SeleniumUtils.GetText(PDPPageLocators.txt_initialColor);
			System.out.println(initialColor);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Select a color variant");
			//Thread.sleep(3000);
			SeleniumUtils.click(PDPPageLocators.elm_ColorSwatch);
			//tbs_SeleniumDriver.waitForPageToLoad();
			//Thread.sleep(10000);
			String newColor =SeleniumUtils.GetText(PDPPageLocators.txt_initialColor);
			System.out.println(newColor);

			Assert.assertFalse("The color variant is not selected",initialColor.equals(newColor));
			DriverFactory.Testlogger.log(LogStatus.PASS, "User is able to select the color variant");
			System.out.println("color variant selected");

		}
		catch (Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Not able to select the Color Variant");
			throw new RuntimeException("Failure in selecting a Color variant",e);
		}
		return this;
	}

	/**
	 * @author alwin.ashley
	 * Select no of quantity to be added	
	 * @throws Exception
	 */
	public PDPPage selectTheQtyInPDP(int qtyToBeAdded) throws Exception{	
		SeleniumUtils.ScrollIntoView(PDPPageLocators.productDetailsExpandBtn, true);
		Thread.sleep(500);
		if(qtyToBeAdded!=1)
			for(int i=1;i<qtyToBeAdded;i++) {
				SeleniumUtils.click(PDPPageLocators.qtyAdjusterPlusBtn);
				System.out.println("qnty incremented");
			}
		//SeleniumUtils.ScrollIntoView(PDPPageLocators.qtyAdjusterPlusBtn, false);
		//SeleniumUtils.scrollToElement(PDPPageLocators.qtyAdjusterPlusBtn);
		Thread.sleep(500);
		SeleniumUtils.ScrollIntoView(PDPPageLocators.productDetailsExpandBtn, true);
		SeleniumUtils.waitForElementToBeClickable(PDPPageLocators.qtyAdjusterPlusBtn);
		System.out.println("actual qnt added"+SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getAttribute("value").toString());
		int actualQtyAdded = Integer.parseInt(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), PDPPageLocators.quantityTextFieldInPDP).getAttribute("value").toString());
		TestAssert.verifyTrue(actualQtyAdded==qtyToBeAdded, "The Item quantity wasn't updated correctly to "+qtyToBeAdded+" ,was added to "+actualQtyAdded+"." );
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Quantity is Updated to the required input");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.quantityAddedToCart, qtyToBeAdded);
		return this;
	}


	/**
	 * @author alwin.ashley
	 * selecting add to cart button from PDP	
	 * @throws Exception
	 */
	public PDPPage clickOnAddToCartFromPDP(){
		if(SeleniumUtils.IsElementVisible(PDPPageLocators.addToCartBtn)) { 
			SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "Add the Item to Cart");
			SeleniumUtils.ScrollIntoView(PDPPageLocators.productDetailsExpandBtn,true);
			SeleniumUtils.click(PDPPageLocators.addToCartBtn);
		}
		else {
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.emailMeOutOfStockBtn), "Email me Out of stock button isn't displayed for an Out of stock item");
		}
		return this;
	}


	/**
	 * @author alwin.ashley
	 * Validate whether the product is added to the basket and Add to cart interstitial is displayed 	
	 * @throws Exception
	 */
	public PDPPage verifyItemIsAddedToCartAndInterstitialDisplayed(){		
		WebDriver driver = DriverFactory.getCurrentDriver();
		SeleniumUtils.WaitForElementToBeVisible(PDPPageLocators.addedToCartInterstitialPopup);
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.addedToCartMessageInInterstitial), "The Add to cart interstitial isn't displayed upon adding item to cart from PDP");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "The Add To Cart Interstitial is getting displayed");
		String prodName = SeleniumUtils.getStoredValue(KeyStoreConstants.productTitleName).toString();
		String qtyToBeAdded = SeleniumUtils.getStoredValue(KeyStoreConstants.quantityAddedToCart).toString();
		String actualQtyAdded = SeleniumUtils.findElement(driver, PDPPageLocators.itemQtyInAddToCartInterstitial).getText().replaceAll("[^.,0-9]", "");
		double prodPrice = Double.parseDouble(SeleniumUtils.getStoredValue(KeyStoreConstants.productPrice).toString());
		try {
			String actualProdNameInPopup = SeleniumUtils.findElement(driver, PDPPageLocators.itemNameInAddToCartInterstitial.format(prodName)).getText().toString();
			//TestAssert.verifyTrue(actualProdNameInPopup.contains(prodName), "The Product name displayed in Add to cart interstitial is different to that displayed in PDP");
			String actualPriceInPopup = SeleniumUtils.findElement
					(driver, PDPPageLocators.itemPriceInAddtoCartInterstitial).getText().toString();
			actualPriceInPopup = SeleniumUtils.getPriceAsActualNumber(actualPriceInPopup);
			double actualPriceInPopupValue = Double.parseDouble(actualPriceInPopup);
			TestAssert.verifyTrue(actualPriceInPopupValue==prodPrice	, "The price displayed in Interstitial isn't the same as product price");
			System.out.println("Actual qty: "+actualQtyAdded+"\n Expected: "+qtyToBeAdded);
			TestAssert.verifyTrue(actualQtyAdded.equalsIgnoreCase(qtyToBeAdded), "The Actual Quantity added from PDP isn't displayed in Interstitial");
			DriverFactory.Testlogger.log(LogStatus.PASS, "Added to your basket message is displayed");
			}
		catch (Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Added to your basket message is not displayed");
			throw new RuntimeException("Failure in validating product added to the pasket",e);
		}
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.viewBasketBtnInAddToCartInterstitial), 
				"The View Basket button isn't displayed in Add to Cart interstitial");
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.continueShoppingBtnInAddToCartInterstitial), 
				"The Continue Shopping button isn't displayed in Add to Cart interstitial");
		String cartCount = SeleniumUtils.getStoredValue(KeyStoreConstants.cartItemCount).toString();
		int count = 0;
		if(cartCount.equalsIgnoreCase("0")) 
			count = 0;
		else
			count = Integer.parseInt(cartCount);
		String cartValue = SeleniumUtils.getStoredValue(KeyStoreConstants.cartOrderTotal).toString();
		double cartTotal = Double.parseDouble(cartValue);
		//double qtyValue = Double.parseDouble(actualQtyAdded);
		double qtyValue = Double.parseDouble(qtyToBeAdded);
		cartTotal = prodPrice*qtyValue + cartTotal;
		System.out.println("ProdPrice: "+prodPrice +"qtyValue: "+qtyValue + "cartTotal: "+cartTotal);
		int qtyIncrement = Integer.parseInt(actualQtyAdded);
		count = count+qtyIncrement;
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartItemCount, ""+count);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, ""+cartTotal);
		return this;
	}	


	/**
	 * Update the Order subtotal value and item count after the product is successfully added
	 * @author Edwin_John
	 * @return
	 *//*
	public PDPPage updateCartTotalAndPrice() {
		int count = Integer.parseInt(SeleniumUtils.getStoredValue(KeyStoreConstants.cartItemCount).toString());
		++count;
		String itemCountInCart = ""+count;
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartItemCount, itemCountInCart);
		return this;
	}*/


	/**
	 * @author alwin.ashley
	 * Click View basket button in Add to Cart interstitial and verify user is directed to Cart page
	 * @throws Exception
	 */
	public PDPPage clickOnViewBasketBtnInInterstitial() {
		//SeleniumUtils.click(PDPPageLocators.viewBasketBtnInAddToCartInterstitial);
		WebDriver driver= DriverFactory.getCurrentDriver();
		WebElement elem = SeleniumUtils.findElement(driver, PDPPageLocators.viewBasketBtnInAddToCartInterstitial);
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", elem);
		DriverFactory.Testlogger.log(LogStatus.PASS, "View Basket button is clicked successfully");
		SeleniumUtils.waitForElementToBeClickable(ShoppingCartLocators.cartTitleHeading);
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(ShoppingCartLocators.cartTitleHeading), "User isn't  navigated to Cart Page on clicking View Basket button in Add to Cart Interstitial");
		return this;
	}


	/**
	 * @author nithin.c03
	 * Method to navigate to product category using breadcrumb link/**
	 */


	public void categoryNavigationViaBreadcrumb() throws Exception{
		help.waitForElementToBeClickable(PDPPageLocators.categoryBreadcrumbLnk);
		SeleniumUtils.click(PDPPageLocators.categoryBreadcrumbLnk);
	}

	/**
	 * Get all the product details and store it to keystore constants - These values are to be cross checked with
	 * the actual values in cart and checkout
	 * @author Edwin_John
	 * @return
	 */
	public PDPPage getAllProductDetails() {
		WebDriver driver = DriverFactory.getCurrentDriver();
		String productName = SeleniumUtils.findElement(driver, PDPPageLocators.productTitleName).getText().toString();
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productTitleName, productName);
		String productPrice = SeleniumUtils.findElement(driver, PDPPageLocators.productPriceBlockInPDP).getText().toString();
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productPrice, productPrice);
		String priceMetricText = SeleniumUtils.findElement(driver, PDPPageLocators.productPriceMetric_Text).getText().toString();
		String[] priceMetricArray = priceMetricText.split("#");
		String productPartNumber = priceMetricArray[1].replace(")", "");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productPartNumber, productPartNumber);
		return this;
	}

	/**
	 * Update stock for any item in corresponding warehouse , warehouse is updated automatically from MarketProperties.yaml
	 * @param stock
	 * @param productId
	 * @param warehouseCode
	 * @return
	 */
	public PDPPage updateStockForItemInWarehouse(String stock,String productId,String warehouseCode) {
		try {
			APIUtils.updateStockJSON(stock, productId, warehouseCode);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	/**
	 * Validate the Product details in PDP and verify whether the details are matching
	 * @author Edwin_John
	 * @return
	 */
	public PDPPage validateProductDetailsInPDP() {
		String actualProdName="";
		String priceToBeSelected="";
		WebDriver driver = DriverFactory.getCurrentDriver();
		String actualPrice="",partNumber="", loyaltyPointsText="";
		int count = 1;
		String prodName = SeleniumUtils.getStoredValue(KeyStoreConstants.productTitleName).toString();
		System.out.println("Prod name" +prodName);
		String prodPrice = SeleniumUtils.getStoredValue(KeyStoreConstants.productPrice).toString();
		System.out.println("Price "+prodPrice);
		//sleep to be removed
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		SeleniumUtils.WaitForElementToBeVisible(PDPPageLocators.pdpProductTitle);
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.productNameInBreadCrumb.format(prodName)),
				"User isn't navigated to the PDP of the product he clicked");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "User is Directed to the PDP of the product");
		List<WebElement> list = driver.findElements(ElementFormatter.getByObject(PDPPageLocators.productSelectionRadioBtnList));
		if(list.size()>1) {
			//SeleniumUtils.click(PDPPageLocators.productRadioBtnRelatedToPrice.format(prodPrice));
			List<WebElement> priceList = driver.findElements(ElementFormatter.getByObject(PDPPageLocators.priceListforMultiVariantPDP));
			for(WebElement e:priceList) {
				priceToBeSelected = SeleniumUtils.getPriceAsActualNumber(e.getText());
				System.out.println("radio button price: "+priceToBeSelected);
				if(priceToBeSelected.equals(prodPrice)) {
					e.click();
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					break;
				}
				++count;

			}
			SeleniumUtils.ScrollIntoView(PDPPageLocators.multiVariantProductPriceTxt.format(count), true);
			//SeleniumUtils.scrollToElement(PDPPageLocators.multiVariantProductPriceTxt.format(count));
			SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "The Price,Loyalty Points,Item ID are getting displayed for the Product");
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.multiVariantProductPriceTxt.format(count)), "The Price of variant isn't displayed in PDP");
			actualPrice = SeleniumUtils.findElement(driver, PDPPageLocators.multiVariantProductPriceTxt.format(count)).getText().toString();
			actualPrice = SeleniumUtils.getPriceAsActualNumber(actualPrice);
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.multiVariantProductID.format(count)), "The Product ID of variant isn't displayed in PDP");
			String[] partNumberArr = SeleniumUtils.findElement(driver, PDPPageLocators.multiVariantProductID.format(count)).getText().toString().split("#");
			partNumber = partNumberArr[1];
			TestAssert.verifyTrue(SeleniumUtils.findElement(driver, PDPPageLocators.productTitleHeadingText).getText().contains(prodName), "The PDP title heading isn't the same as Productname");
			/*TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.multiVariantLoyaltyPointsTxt.format(count)), "The Loyalty Points Text of variant isn't displayed in PDP");
			loyaltyPointsText = SeleniumUtils.findElement(driver, PDPPageLocators.multiVariantLoyaltyPointsTxt.format(count)).getText().toString();
			loyaltyPointsText = loyaltyPointsText.replaceAll("[^.,0-9]", "");*/
			partNumber = partNumber.replaceAll("[^.,0-9]", "");
			actualProdName=SeleniumUtils.findElement(driver, PDPPageLocators.productTitleHeadingText).getText();
		}
		else {
			SeleniumUtils.ScrollIntoView(PDPPageLocators.singleVariantProductPriceTxt, true);
			SeleniumUtils.attachScreenshotInReport(LogStatus.INFO, "The Price,Loyalty Points,Item ID are getting displayed for the Product");
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.singleVariantProductPriceTxt), "The Price of variant isn't displayed in PDP");
			actualPrice = SeleniumUtils.findElement(driver, PDPPageLocators.singleVariantProductPriceTxt).getText().toString();
			actualPrice = SeleniumUtils.getPriceAsActualNumber(actualPrice);
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.singleVariantProductID), "The Product ID of variant isn't displayed in PDP");
			String[] partNumberArr = SeleniumUtils.findElement(driver, PDPPageLocators.singleVariantProductID).getText().toString().split("#");
			partNumber = partNumberArr[1];

			TestAssert.verifyTrue(SeleniumUtils.findElement(driver, PDPPageLocators.productTitleHeadingText).getText().contains(prodName), "The PDP title heading isn't the same as Productname");
			/*TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.singleVariantLoyaltyPtsTxt), "The Loyalty Points Text of variant isn't displayed in PDP");

			//sleep to be removed
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//TestAssert.verifyTrue(SeleniumUtils.findElement(driver, PDPPageLocators.productTitleHeadingText).getText().replaceAll(" ", "").contains(prodName), "The PDP title heading isn't the same as Productname");
			TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(PDPPageLocators.singleVariantLoyaltyPtsTxt), "The Loyalty Points Text of variant isn't displayed in PDP");

			loyaltyPointsText = SeleniumUtils.findElement(driver, PDPPageLocators.singleVariantLoyaltyPtsTxt).getText().toString();
			loyaltyPointsText = loyaltyPointsText.replaceAll("[^.,0-9]", "");*/
			partNumber = partNumber.replaceAll("[^.,A-Za-z0-9]", "");
			actualProdName=SeleniumUtils.findElement(driver, PDPPageLocators.productTitleHeadingText).getText();
		}
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productTitleName,actualProdName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productPartNumber, partNumber);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.loyaltyPoints, loyaltyPointsText);
		System.out.println("Actual Price "+actualPrice +" Product Price "+prodPrice);
		TestAssert.verifyTrue( actualPrice.contains(prodPrice),
				"The price displayed isn't consistent in PLP/SRP and PDP");
		return this;
	}

	/**
	 * Click on the Add To Cart Interstitial  in PDP page
	 * @author Edwin_John
	 * @return
	 */
	public PDPPage closeAddToCartInterstitial(){
		SeleniumUtils.click(PDPPageLocators.closeInterstitial);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.isElementNotPresent(PDPPageLocators.addedToCartInterstitialPopup), 
				"The Add To cart Interstitial isn't closed when user clicks on Close button", 
				"The Add to Cart interstitial is closed when user clicks on Close button");
		return this;
	}


	public void Test() {
		ArrayList<String> list = new ArrayList<>();
		HashMap<String,ArrayList<String>> productMatrix = new HashMap<>();
		list.add("9.00");
		list.add("9");
		list.add("TestName");
		productMatrix.put("abc",list);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productMatrix, productMatrix);
		HashMap<String,ArrayList<String>> test = (HashMap<String, ArrayList<String>>) SeleniumUtils.getStoredValue(KeyStoreConstants.productMatrix);
		ArrayList<String> s = test.get("abc");
		System.out.println("value: "+s);
		ArrayList<String> list1 = new ArrayList<>();
		list1.add("5.00");
		list1.add("5");
		list1.add("XYZ Name");
		test.put("xyz", list1);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productMatrix, test);
		HashMap<String,ArrayList<String>> test1 = (HashMap<String, ArrayList<String>>) SeleniumUtils.getStoredValue(KeyStoreConstants.productMatrix);
		System.out.println("value 22:"+test.get("xyz"));
	}

	public void pushProductValuesToHashmap(String prodId,String price,String qty,String prodName) {
		ArrayList<String> list = new ArrayList<>();	
		HashMap<String,ArrayList<String>> test = (HashMap<String, ArrayList<String>>) SeleniumUtils.getStoredValue(KeyStoreConstants.productMatrix);
		list.add(prodName);
		list.add(price);
		list.add(qty);
		test.put(prodId, list);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productMatrix, test);
	}

	public void fetchProductValuesFromHashmap(String productId) {
		HashMap<String,ArrayList<String>> test = (HashMap<String, ArrayList<String>>) SeleniumUtils.getStoredValue(KeyStoreConstants.productMatrix);
		System.out.println("Value of "+productId+" is "+test.get(productId));
	}

	public void setHashmap() {
		HashMap<String,ArrayList<String>> productMatrix = new HashMap<>();
		SeleniumUtils.storeKeyValue(KeyStoreConstants.productMatrix, productMatrix);
	}

	public static void main(String[] args) {
		PDPPage p =new PDPPage();
		/*p.setHashmap();
		p.pushProductValuesToHashmap("109168", "9.00", "3", "Shea body Butter 200ML");
		p.pushProductValuesToHashmap("109134", "2.00", "1", "Ginger Shampoo");
		p.pushProductValuesToHashmap("1084411", "7.50", "6", "Charcoal Face Mask");
		p.fetchProductValuesFromHashmap("109168");
		p.fetchProductValuesFromHashmap("109134");
		p.fetchProductValuesFromHashmap("1084411");*/


	}
	
	public PDPPage navigateToSRP(String productID,String market)
	{
		System.out.println("Market "+market);
		MarketDetails data = MarketDetails.fetch(market);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartItemCount, "0");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.marketToBeTested,market);
		String currency = data.marketData.currencySymbol;
		SeleniumUtils.storeKeyValue(KeyStoreConstants.currencySymbol, currency);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, "0.00");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.guestFlowForRegUser, "false");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.orderFlag, "true");
		String URL = data.marketData.marketURL.toString()+"search/"+productID;
		System.out.println("URL "+URL);
		DriverFactory.openPage(URL);
		SeleniumUtils.waitForElementToBeClickable(HomePageLocators.cookiesAcceptButton);
		SeleniumUtils.click(HomePageLocators.cookiesAcceptButton);
		SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.searchTextEnteredInTxtField);
		TestAssert.verifyTrueAndPrintMsg(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(),
				HomePageLocators.searchTextEnteredInTxtField).getText().contains(productID), "User isn't directed to the corresponding Search Results Page","User is navigated to the corresponding search result page");
		return this;
	}


}

