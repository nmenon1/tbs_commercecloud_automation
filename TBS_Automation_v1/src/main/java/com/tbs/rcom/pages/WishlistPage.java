package com.tbs.rcom.pages;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.checkerframework.checker.nullness.compatqual.KeyForType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.yaml.snakeyaml.error.MarkedYAMLException;

import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Test;
import com.tbs.rcom.core.utils.CommonUtils;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.TestAssert;
import com.tbs.rcom.pageLocators.LoginPageLocators;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.pageLocators.WishListPageLocators;
import com.tbs.rcom.pageLocators.PDPPageLocators;
import com.tbs.rcom.pageLocators.PLPPageLocators;

import tbs_master.DriverFactory;
import tbs_master.ElementFormatter;
import tbs_master.SeleniumUtils;

public class WishlistPage {

	/**
	 * Validate empty wishlist for a guest user
	 * @author Edwin_John
	 * @return
	 */
	public WishlistPage validateWishlistForGuestUser() {
		DriverFactory.Testlogger.log(LogStatus.PASS, "User is navigated to Wishlist page");
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.wishListTitleTxt);
		String screenshotPath = /*".\\"+*/SeleniumUtils.getScreenhot("WishList");
		System.out.println("screenshotpath: "+screenshotPath);
		DriverFactory.Testlogger.log(LogStatus.PASS,"Screenshot"+DriverFactory.Testlogger.addScreenCapture(screenshotPath));
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(WishListPageLocators.wishlistStartShoppingBtn),
				"The Start Shopping button isn't present in the Empty wishlist page for a guest user");
		return this;
	}


	/**
	 * Validate empty wishlist for a newly registered user
	 * @Arjun.Vijay
	 * @return
	 */
	public WishlistPage validateEmptyWishlistPageForNewlyRegisteredUser(){
		TestAssert.verifyTrue(SeleniumUtils.isElementPresent(WishListPageLocators.wishlistIntro), "Wishlist introductory message is not present in the registered user wishlist page");
		String wishListCount = SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), WishListPageLocators.wishListNumber).getText().toString();
		wishListCount = SeleniumUtils.getPriceAsActualNumber(wishListCount);
		TestAssert.verifyTrue(SeleniumUtils.isElementNotPresent(WishListPageLocators.numberOfWishlistsForUser), "There are Wishlists present for a newly registered user");
		TestAssert.verifyTrue(wishListCount.equalsIgnoreCase("0"), "The text displays  the newly registered user has existing wishlists");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Empty Wishlist Page displayed for Newly Registered User");
		return this;
	}


	public WishlistPage clickOnCreateNewWishlistAndEnterName()
	{
		DriverFactory.Testlogger.log(LogStatus.INFO, "Click on Create Wishlist Button");
		SeleniumUtils.click(WishListPageLocators.createWishlistBtn);
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.createNewWishlistModal);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Create WishList Modal is displayed");
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.wishListNameInput.format(1));
		String wishListName = "TestWL"+CommonUtils.getCurrentTimeStamp();
		SeleniumUtils.click(WishListPageLocators.wishListNameInput.format(1));
		SeleniumUtils.type(WishListPageLocators.wishListNameInput.format(1), wishListName);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.wishListName, wishListName);
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Enter Wishlist name");
		return this;
	}

	public WishlistPage clickOnSaveInCreateWishlistModal()
	{
		SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.saveWishList);
		SeleniumUtils.click(WishListPageLocators.saveWishList);
		DriverFactory.Testlogger.log(LogStatus.PASS, "Click on Save Button in Create Wishlist Modal to create the Wishlist");
		return this;
	}


	public WishlistPage clickOnCancelInCreateWishlistModal()
	{
		SeleniumUtils.click(WishListPageLocators.cancelWishList);
		DriverFactory.Testlogger.log(LogStatus.PASS, "Click on Cancel in Create Wishlist Modal to close the modal");
		return this;
	}

	/**
	 * Validate whether New wishlist is created or not
	 * @author Edwin_John
	 * @return
	 */
	public WishlistPage validateNewWishListIsCreated() {
		SeleniumUtils.WaitForElementToBeVisible(HomePageLocators.wishlistBtnInHeader);
		String sel=SeleniumUtils.getStoredValue(KeyStoreConstants.wishListName).toString();
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(WishListPageLocators.wishListName.format(sel)), "Wishlist not created with provided name");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "New wishlist is Created");
		return this;
	}



	/**
	 * Validate newly Created wishlist is Empty or not
	 * @author Edwin_John
	 * @return
	 */
	public WishlistPage validateNewWishListIsEmpty()
	{
		TestAssert.verifyTrue(SeleniumUtils.IsElementVisible(WishListPageLocators.emptywishlistMsg), "Empty wishlist message not displayed");
		SeleniumUtils.click(WishListPageLocators.backFromWishlistBtn);
		String name = SeleniumUtils.getStoredValue(KeyStoreConstants.wishListName).toString();
		TestAssert.verifyTrue(SeleniumUtils.findElement(DriverFactory.getCurrentDriver(), WishListPageLocators.wishListItemCount.format(name)).getText().equals("0"),
				"The Newly created wishlist isn't empty");
		SeleniumUtils.attachScreenshotInReport(LogStatus.PASS, "Newly Created Wishlist is Empty");
		return this;
	}


	/**
	 * @author alwin.ashley
	 * Clicking Add to wishlist button      
	 */
	public WishlistPage clickAddToWishlistButton() {
		try {

			String name=SeleniumUtils.GetText(PLPPageLocators.productName);
			
			SeleniumUtils.storeKeyValue(KeyStoreConstants.productTitleName, name);
			Thread.sleep(3000);
			//SeleniumUtils.WaitForElementToBeClickable(PLPPageLocators.wishlistButtonPLP);
			SeleniumUtils.click(PLPPageLocators.wishlistButtonPLP);
			System.out.println("Add to wishlist button clicked successfully");
			DriverFactory.Testlogger.log(LogStatus.PASS, "Add to wishlist button clicked successfully");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in clicking Add to wishlist button");
		}

		return this;
	}
	/**
	 * @author alwin.ashley
	 * Creating wishlist while adding a product      
	 */
	public WishlistPage createWishlist() {
		try{

			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.createWishlistButton);
			SeleniumUtils.click(WishListPageLocators.createWishlistButton);

			String WishlistName = "test" + CommonUtils.getCurrentTimeStamp();
			SeleniumUtils.storeKeyValue(KeyStoreConstants.wishListName, WishlistName);
			SeleniumUtils.type(WishListPageLocators.wishlistNameField, WishlistName);
			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.wishlistSaveButton);
			SeleniumUtils.click(WishListPageLocators.wishlistSaveButton);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Wishlist created successfully");
			System.out.println("Wishlist created successfully");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in creating new Wishlist");
		}
		return this;

	}

	/**
	 * @author alwin.ashley
	 * Adding product to an existing wishlist from PLP     
	 */
	public WishlistPage clickAddtoExistingListButton() {

		try {
			System.out.println("Entering clickAddtoExistingListButton function");
			Thread.sleep(3000);
			SeleniumUtils help = new SeleniumUtils();
			help.elementHighlight(WishListPageLocators.addToExistingWishlistButton);
			//DriverFactory.waitForPageToLoad();
			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.addToExistingWishlistButton);
			SeleniumUtils.click(WishListPageLocators.addToExistingWishlistButton);
			System.out.println("Button clicked Successfully");
			DriverFactory.Testlogger.log(LogStatus.PASS, "Add to existing wishlist button clicked successfully");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in clicking Add to existing wishlist button");
		}
		return this;
	}

	/**
	 * @author alwin.ashley
	 * Delete wishlist     
	 */
	public WishlistPage clickDeleteListButton() {

		try {
			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.deleteWishlistButton);
			SeleniumUtils.click(WishListPageLocators.deleteWishlistButton);
			Thread.sleep(3000);
			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.removeWishlistButton);
			SeleniumUtils.click(WishListPageLocators.removeWishlistButton);
			System.out.println("Successfully deleted the existing wishlist");
			DriverFactory.Testlogger.log(LogStatus.PASS, "Successfully deleted the existing wishlist");
		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in deleting wishlist");
		}
		return this;

	}

	/**
	 * @author alwin.ashley
	 * Navigate to wishlist page
	 */
	public WishlistPage clickToNavigateToWishlistPage() {
		try {
			WebDriver driver= DriverFactory.getCurrentDriver();
			Thread.sleep(3000);
			SeleniumUtils.waitForElementToBeClickable(WishListPageLocators.wishlistButtonHeader);
			SeleniumUtils.click(WishListPageLocators.wishlistButtonHeader);
			String wishlistName = SeleniumUtils.getStoredValue(KeyStoreConstants.wishListName).toString();
			SeleniumUtils.click(WishListPageLocators.wishlistPageLink.format(wishlistName));
			System.out.println("Navigated to wishlist page successfully");    
			DriverFactory.Testlogger.log(LogStatus.PASS, "Navigated to wishlist page successfully");

		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in navigating to wishlist page");
		}

		return this;

	}

	/**
	 * @author alwin.ashley
	 * Adding product to an existing wishlist from PDP
	 */
	public WishlistPage clickAddToWishlistButtonFromPDP() {

		try {
			Thread.sleep(3000);
			SeleniumUtils.waitForElementToBeClickable(PDPPageLocators.wishlistButtonPDP);
			SeleniumUtils.click(PDPPageLocators.wishlistButtonPDP);	
			System.out.println("Clicking wishlist button from PDP");

		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Failure in clicking add to Wishlist button from PDP");
		}
		return this;
	}

	/**
	 * @author alwin.ashley
	 * validate wishlist
	 */
	public WishlistPage validateWishlist() {

		try {
			WebDriver driver= DriverFactory.getCurrentDriver();
			System.out.println("Entering validation function ");
			String productName = SeleniumUtils.getStoredValue(KeyStoreConstants.productTitleName).toString();
			System.out.println(productName);
			List<WebElement> elementName = driver.findElements(By.xpath("//a[contains(@class,'product-tile__name')]"));
			int flag=0;
			for(WebElement i:elementName)	
			{
				if(i.getText().contains(productName))
				{
					flag=1;			    	 
				}
			}
			Assert.assertTrue("Product not added to Wishlist",flag==1);
			DriverFactory.Testlogger.log(LogStatus.PASS, "Product added to Wishlist Successfully");

		}
		catch(Exception e) {
			DriverFactory.Testlogger.log(LogStatus.FAIL, "Product wasn't added to wishlist");
		}
		return this;

	}




}
