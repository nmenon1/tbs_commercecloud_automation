package com.tbs.rcom.yaml;

public class MarketTestData {

	public String currencySymbol;
	public String currencyPosition;
	public String storeWareHouse;
	public String dcWareHouse;
	public String sortType;
	public String giftWrapValue;
	public String cisSearchKeyword;
	public String giftCardNumber;
	public String giftCardPIN;
	public String phnNumbFormat;
	public String marketURL;
	public String zipCode;
	public String addressOne;
	public String city;
}
