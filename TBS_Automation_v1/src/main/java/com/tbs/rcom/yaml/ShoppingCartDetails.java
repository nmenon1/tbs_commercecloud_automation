package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class ShoppingCartDetails {

	public ShoppingCartData shoppingCartData;
	public static ShoppingCartDetails fetch(String key) {
	    ShoppingCartDetails obj = (ShoppingCartDetails)YamlUtils.loadYaml(key, "tbs_TestData\\test-data\\ShoppingCart.yaml");
	    if(obj==null)
	    	throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}
