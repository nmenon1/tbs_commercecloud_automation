package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class AddressDetails {

	public AddressData addressData;
	public static AddressDetails fetch(String key) {
		AddressDetails obj = (AddressDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/Address.yaml");
	    if(obj==null)
	    	throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}
