package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class MarketDetails {
	public MarketTestData marketData;
	public static MarketDetails fetch(String key) {
		MarketDetails obj = (MarketDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/MarketProperties.yaml");
	    if(obj==null)
	    	throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}
