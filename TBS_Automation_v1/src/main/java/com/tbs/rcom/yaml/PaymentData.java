package com.tbs.rcom.yaml;

public class PaymentData {
	public String cardNumber;
	public String cvvNumber;
	public String cardHolderName;
	public String expiryDate;
	public String PayPalEmailId;
	public String PayPalpassword;
	public String giftCardNumber;
}
