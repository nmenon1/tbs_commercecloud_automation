package com.tbs.rcom.yaml;

public class AddressData {
	public String firstName;
	public String lastName;
	public String addressLine1;
	public String addressLine2;
	public String city;
	public String state;
	public String zipcode;
    public String phoneNumber;
    public String countryForBillingAddress;
    public String personalNumber;
}
