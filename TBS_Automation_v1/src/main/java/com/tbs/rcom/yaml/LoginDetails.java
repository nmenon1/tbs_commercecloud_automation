package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class LoginDetails {
	public LoginData loginData;
	public static LoginDetails fetch(String key) {
		LoginDetails obj = (LoginDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/LoginValues.yaml");
	    if(obj==null)
	    	throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}
