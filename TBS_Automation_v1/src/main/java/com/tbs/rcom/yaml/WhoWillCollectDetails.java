package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class WhoWillCollectDetails {
	public WhoWillCollectData whoWillCollectData;
	
	public static WhoWillCollectDetails fetch(String key) {
		WhoWillCollectDetails obj = (WhoWillCollectDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/WhoWillCollect.yaml");
	    if(obj==null)
	    	throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}

}
