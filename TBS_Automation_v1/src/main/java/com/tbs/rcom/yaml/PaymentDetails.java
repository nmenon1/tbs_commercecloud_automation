package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class PaymentDetails {

	public PaymentData paymentData;
	public static PaymentDetails fetch(String key) {
		PaymentDetails obj = (PaymentDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/Payment.yaml");
		if(obj==null)
			throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}

