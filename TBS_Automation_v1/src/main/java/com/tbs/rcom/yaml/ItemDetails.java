package com.tbs.rcom.yaml;

import com.tbs.rcom.core.utils.YamlUtils;

public class ItemDetails {

	public ItemData itemData;
	public ItemDetails fetch(String key) {
		ItemDetails obj = (ItemDetails)YamlUtils.loadYaml(key, "src/main/java/tbs_TestData/test-data/ItemDetails.yaml");
		if(obj==null) 
			throw new RuntimeException("Data not found for "+key +"in the yaml file");
		return obj;
	}
}
