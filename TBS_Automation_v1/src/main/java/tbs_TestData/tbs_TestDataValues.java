/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_TestData;

public class tbs_TestDataValues {
	public String testingFor;
	public String url;
	public String currency;
	public String language;
	public String plp;
	public String SubPlp;
	public String filters;
	public String sort;
	public String rating;
	public String currPosition;
}
