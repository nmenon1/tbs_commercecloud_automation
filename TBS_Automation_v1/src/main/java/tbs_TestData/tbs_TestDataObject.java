/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_TestData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.tbs.rcom.core.utils.tbs_ExcelUtils;

import tbs_TestData.tbs_TestDataExcelColumnNames.Sheet_Master;
/*********
*Function:tbs_SeleniumDriver
*Purpose: To handle testdata
*Author:mibin.b
*Copyright : INFOSYS LTD
*********/
public class tbs_TestDataObject {
	public static  MultiValuedMap<String, List<tbs_TestDataValues>> TestDataHashMap = new ArrayListValuedHashMap<>();
	public static void loadTestDetailsHashMap(String ExcelPath, String excelSheetName){
		tbs_ExcelUtils excelUtils=new tbs_ExcelUtils();
		Workbook workbook=excelUtils.openExcelWorkBook(ExcelPath);
		Sheet sheetName=excelUtils.openExcelWorkSheet(workbook,excelSheetName);
		fetchTestDataDetails(sheetName);
		excelUtils.closeExcelWorkBook(workbook);
	}
	public static void fetchTestDataDetails(Sheet sheetName) {
		int testingFor=0;
		int url=0;
		int currency=0;
		int language=0;
		int plp=0;
		int subplp=0;
		int filters=0;
		int sort=0;
		int rating=0;
		int currPos=0;
		boolean startFlag=true;
		try {
			Iterator<Row> rowIterator = sheetName.rowIterator();
	        DataFormatter dataFormatter = new DataFormatter();
	        while (rowIterator.hasNext()) {
	            Row row = rowIterator.next();
	          	Iterator<Cell> columnIterator=row.cellIterator();
	          	if(startFlag==true) {
	          		while(columnIterator.hasNext()) {
	          			Cell cell=columnIterator.next();
	          			if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.Market.name())==true) {
	          				testingFor=cell.getColumnIndex();
	          				//System.out.println(testingFor);
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.URL.name())==true) {
	          				url=cell.getColumnIndex();
	          				//System.out.println(url);
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.Currency.name())==true) {
	          				currency=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.Language.name())==true) {
	          				language=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.PLP_Category.name())==true) {
	          				plp=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.PLP_SubCategory.name())==true) {
	          				subplp=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.Filters.name())==true) {
	          				filters=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.Sort.name())==true) {
	          				sort=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.RatingFilter.name())==true) {
	          				rating=cell.getColumnIndex();
		          		}
	          			else if(cell.getStringCellValue().toString().equalsIgnoreCase(Sheet_Master.CurrencyPosition.name())==true) {
	          				currPos=cell.getColumnIndex();
		          		}
	          			startFlag=false;
	          		}
	            }
	          	else {
	          		tbs_TestDataValues TestDatadetails=new tbs_TestDataValues();
	          		TestDatadetails.url=dataFormatter.formatCellValue(row.getCell(url)).toString();
	          		TestDatadetails.currency=dataFormatter.formatCellValue(row.getCell(currency)).toString();
	          		TestDatadetails.language=dataFormatter.formatCellValue(row.getCell(language)).toString();
	          		TestDatadetails.plp=dataFormatter.formatCellValue(row.getCell(plp)).toString();
	          		TestDatadetails.SubPlp=dataFormatter.formatCellValue(row.getCell(subplp)).toString();
	          		TestDatadetails.filters=dataFormatter.formatCellValue(row.getCell(filters)).toString();
	          		TestDatadetails.sort=dataFormatter.formatCellValue(row.getCell(sort)).toString();
	          		TestDatadetails.rating=dataFormatter.formatCellValue(row.getCell(rating)).toString();
	          		TestDatadetails.currPosition=dataFormatter.formatCellValue(row.getCell(currPos)).toString();
	          		List<tbs_TestDataValues> DataDetailsList= new ArrayList<tbs_TestDataValues>();
	          		DataDetailsList.add(TestDatadetails);
	          		TestDataHashMap.put(dataFormatter.formatCellValue(row.getCell(0)).toString(), DataDetailsList);
	          		//System.out.println(dataFormatter.formatCellValue(row.getCell(0)).toString());
	          	} 
	        }
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
