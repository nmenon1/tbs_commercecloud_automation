/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_TestData;

public class tbs_TestDataExcelColumnNames {
	public enum Sheet_Master{
		Market,
		URL,
		Currency,
		Language,
		PLP_Category,
		PLP_SubCategory,
		Filters,
		Sort,
		RatingFilter,
		CurrencyPosition;
		;
	}
	public enum Sheet_TestCase{
		Test_Step,
		System,
		Step_Owner;
	}
}
