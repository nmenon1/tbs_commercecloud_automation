/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_TestData;

public class tbs_TestDataExcelSheetNames {
	public enum ExcelSheetName{
		Master,
		TestCase;
	}
}
