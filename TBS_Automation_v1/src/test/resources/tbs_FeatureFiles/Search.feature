
Feature: Search
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Testcases covered RCOM-819,RCOM-663,RCOM-658
@Regression
Scenario: Validate Filtering in SRP and Contents in a PDP.
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User is navigated to SRP "Hair" to be tested
Then User is able to filter the products
When User is navigated to PDP of product
Then User can validate Product details Price metric and Currency in PDP


#Testcases covered RCOM-618,RCOM-594
#AUTOMATION TC  - RCOM-4956
@Regression
@SortSRP
Scenario: Validate various types of Sorting is applied correctly in Search Results pages
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User is navigated to SRP "Body Butter" to be tested
Then User is able to sort the products using "price high to low" sort type
When User is navigated to SRP "Lips" to be tested
Then User is able to sort the products using "price low to high" sort type
When User is navigated to SRP "Hand" to be tested
Then User is able to sort the products using "top rated" sort type

#Testcases covered RCOM-946
@Regression
Scenario: Validating search suggestions based on invalid keywords.
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User enters "@##" in the search field 
Then User can validate the search suggestions for invalid keywords

#Testcases covered RCOM-829, RCOM-945 and RCOM-944
@Regression
Scenario: Validating search suggestions based on valid keywords.
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User enters "so" in the search field
Then User can validate the search suggestions
When User clicks on the suggestion
Then User should be navigated to the SRP "so" to be tested

#Testcases covered RCOM-644 and RCOM-646
@Regression
Scenario: Vaidate search using Bar code
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User enters "9999999999999" in the search field
Then User can validate the search suggestions for invalid keywords
And User is directed to Search Results page
And zero results message is displayed
When User enters valid barcode "4443332221110" of product "p001102" in the search field
Then User can validate the search suggestions
And User is directed to Search Results page
And products with respective bar code will be displayed