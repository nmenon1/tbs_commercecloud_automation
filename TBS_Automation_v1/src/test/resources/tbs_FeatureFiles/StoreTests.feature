#Scenarios related to Find In Store functionality in PDP and CIS fulfillment in checkout
@CISTest
Feature: StoreTests

Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Author - Edwin_John
@P1
@CISOrder @USP1 @UKP1
Scenario: Place a CIS order as registered LYBC user
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User logs in using "valid" credentials and Clicks on Sign in button
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User is navigated to SRP "p002347" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When Stock is updated to "100" for the item  in corresponding "cis" warehouse
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
When User proceeds to checkout by Clicking on The Checkout Button In cart
When User select the "cis" fulfillment type in Checkout
And User enters the keyword "invalid" in CIS Modal
Then The Global error message for Invalid Store Search is displayed
And User enters the keyword "valid" in CIS Modal
When CIS Store with Stock for the Product is selected from Store Modal
Then The Selected store is displayed in CIS
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page


@P1
@GuestCIS @USP1 @UKP1
Scenario: Place Guest CIS order with LYBC emailid 
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User is navigated to SRP "p002347" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When Stock is updated to "100" for the item  in corresponding "cis" warehouse
When User is able to add product to cart with quantity as 9
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Registered" Email id
And Sign in checkout screenset is displayed and user proceeds as "Guest" user
Then User is directed to Checkout and Delivery is default selected
When User select the "cis" fulfillment type in Checkout
And User enters the keyword "invalid" in CIS Modal
Then The Global error message for Invalid Store Search is displayed
And User enters the keyword "valid" in CIS Modal
When CIS Store with Stock for the Product is selected from Store Modal
Then The Selected store is displayed in CIS
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Klarna Pay Later" as payment method
When User clicks On Pay Now Button
And User approves the payment from Klarna Pay Later gateway
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed