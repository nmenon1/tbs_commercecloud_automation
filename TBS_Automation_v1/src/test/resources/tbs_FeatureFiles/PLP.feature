
Feature: PLP
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created


#Testcases covered RCOM-585, RCOM-597, RCOM-177 ,RCOM-661,RCOM-662, RCOM-660, and RCOM-657
@Regression
@P2
Scenario: Validate Filtering in PLP and Contents in a PDP.
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Conditioner" link in the PLP overlay
Then User is able to filter the products
When User is navigated to PDP of product
Then User can validate Product details Price metric and Currency in PDP

#Testcases covered  RCOM-179, RCOM-616, RCOM-617,RCOM-593
@Regression
@SortPLP
@P2
Scenario: Validate various types of Sorting is applied correctly in PLP page
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "Make-up" PLP and overlay is displayed
And User clicks on the "Eye Liners" link in the PLP overlay
Then User is able to sort the products using "price high to low" sort type
When User hovers over the "Face" PLP and overlay is displayed
And User clicks on the "Face Masks" link in the PLP overlay
Then User is able to sort the products using "price low to high" sort type
When User hovers over the "Make-up" PLP and overlay is displayed
And User clicks on the "Eye Liners" link in the PLP overlay
Then User is able to sort the products using "top rated" sort type

