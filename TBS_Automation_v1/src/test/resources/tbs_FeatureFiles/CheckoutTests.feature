@smoke1
Feature: Add to cart from PDP & checkout
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

Scenario: Validate Add to cart and checkout of Base product - Color swatch.
Given User is on the Home Page "https://www-qa-ccv2.thebodyshop.com" of TBS
And User is navigated to Market to be tested
When User is navigated to SRP "lips and cheek stain" to be tested
When User is navigated to PDP of product
Then User is able to update the color of product and the Quantity to 5
When User is able to add product to cart
Then item is added successfully to cart  
#When user navigates to cart
#Then User can validate the cart details
When user proceeds to checkout
And Login from checkout login page
When user can select delivery address
And user select a delivery option
And selects a valid billing address
And Enters a valid credit card 
And Clicks on Finish and Pay
Then Order is successfully placed

Scenario: Validate Add to cart and checkout of Base product -Size variants.
Given User is on the Home Page "https://www-qa-ccv2.thebodyshop.com" of TBS
And User is navigated to Market to be tested
When User is navigated to SRP "lips" to be tested
When User is navigated to PDP of product
Then User is able to update the color of product and the Quantity to 5
When User is able to add product to cart
Then item is added successfully to cart  
When user navigates to cart
Then User can validate the cart details
When user proceeds to checkout
And User registers from checkout login page 
When User enters a new address of type "NormalAddress" in form modal
And Clicks on Continue button
And Select a delivery option
And Proceeds to Payment
And selects a valid billing address
And Enters a valid credit card 
And Clicks on Finish and Pay
Then Order is successfully placed
#
#Scenario: Validate Add to cart and checkout of Variant product.
#Given User is on the Home Page "https://www-qa-ccv2.thebodyshop.com" of TBS
#And User is navigated to Market to be tested
#When User is navigated to SRP "lips" to be tested
#When User is navigated to PDP of product
#Then User is able to update the color of product and the Quantity to 5
#When User is able to add product to cart
#Then item is added successfully to cart  
#When user navigates to cart
#Then User can validate the cart details
#When user proceeds to checkout
#And Login from checkout login page
#When user can select delivery address
#And Select a delivery option
#And Proceeds to Payment
#And selects a valid billing address
#And Enters a valid credit card 
#And Clicks on Finish and Pay
#Then Order is successfully placed



