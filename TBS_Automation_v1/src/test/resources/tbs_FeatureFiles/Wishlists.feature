
Feature: Validation of Wishlist Functionalities

Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created
   
@Regression
Scenario: Validate the Wishlist page for a Guest User
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User clicks on the Wishlist button in Header
Then The Empty Guest Wishlist is displayed

@Regression
@WishTest
Scenario: Validate empty wishlist page for registered user and Create new Wishlist
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User registers as new user of type "User1" from homepage
When User clicks on the Wishlist button in Header
Then Wishlist Section is Empty for the Newly registered User
When User clicks on Create Wishlist Button and Enters a wishlist name
And User click on Cancel Button in Create Wishlist modal
Then Wishlist Section is Empty for the Newly registered User
When User clicks on Create Wishlist Button and Enters a wishlist name
And User clicks on Save Button in Create Wishlist Modal
Then New wishlist is created
And Newly created wishlist is empty 

@Regression
Scenario: Add product to Wishlist page
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User registers as new user of type "User1" from homepage
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Shampoo" link in the PLP overlay
Then User adds a product to new Wishlist from PLP
Then User validate whether the Product is added to wishlist
When User hovers over the "Body" PLP and overlay is displayed
And User clicks on the "Body Butters" link in the PLP overlay
Then User adds a product to existing Wishlist from PLP
Then User navigates to the wishlist
Then User validate whether the Product is added to wishlist
When User hovers over the "Body" PLP and overlay is displayed
And User clicks on the "Body Yogurts" link in the PLP overlay
When User is navigated to PDP of product
Then User adds a product to existing Wishlist from PDP
Then User navigates to the wishlist
Then User validate whether the Product is added to wishlist
Then User deletes the wishlist
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Styling" link in the PLP overlay
When User is navigated to PDP of product
Then User adds a product to new Wishlist from PDP
Then User validate whether the Product is added to wishlist

