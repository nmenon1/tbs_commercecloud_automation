Feature: Check out tests
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Testcases covered RCOM-1451
@POAddress
@P2
@UKP2 @DEP1 @NLP1 @USP1 @SEP1 @AUP1
Scenario: Validation of PO Restriction Error Message
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Shampoo" link in the PLP overlay
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 6
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "POBoxAddress" in form modal
Then User will be displayed with an error message




@Giftcard
@P2
Scenario: Place Order using Gift card only for a Registered User
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User is navigated to SRP "Body Butter" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
Then User selects "Gift Card" as payment method
When User enters the "valid" gift card details and clicks on Use Card
Then The Gift card added is applied and Remove button is displayed
When the Outstanding amount  in Order summary section is paid
Then Add Another Gift Card link isn't displayed
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page

@Regression
@VISACARD
@P1 @AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Order with VISA Card for LYBC user logged in My Account
#Billing Address same as Shipping Address
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User logs in using "valid" credentials and Clicks on Sign in button
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User is navigated to SRP "Hands" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
Then User selects "Credit Card" as payment method
When User enter payment details with "Visa" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page
When User clicks on the "Payment" section in My Account
Then The Saved card details are present Saved Payments section
When User is navigated to SRP "Face" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
Then User selects "Credit Card" as payment method
And User is able to select Saved Card for Payment
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page

@Regression
@Master
@P1
@AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Order with MasterCard for non-LYBC user Signed in From Checkout
#Billing Address different from Shipping Address,Register from MyAccount landing page
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "NonLYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User is navigated to SRP "Face" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Registered" Email id
And Sign in checkout screenset is displayed and user proceeds as "Registered" user
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
#When User enters a new "default" billing address of "BillingAddress" type and of "different" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When Non-LYBC user signs up for LYBC in Order confirmation page
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed

#Testcases covered RCOM-1659,RCOM-1437, RCOM-1654, RCOM-1655, RCOM-1852, RCOM-1853 and RCOM-1855
@Regression
@CollectionPoint
@P1
@AUP1 @CAP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Collection Pt Order as LYBC User signed in from Checkout
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User hovers over the "Hands" PLP and overlay is displayed
And User clicks on the "Moisturisers" link in the PLP overlay
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 5
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User is navigated to SRP "Face Mask" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Registered" Email id
And Sign in checkout screenset is displayed and user proceeds as "Registered" user
Then User is directed to Checkout and Delivery is default selected
When User select the "Collection point" fulfillment type in Checkout
And User select "valid" non store collection point for delivery
When User selects the "Express" delivery option
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page

@P1
@GuestDeliveryOrder
@AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Delivery order as Guest and signup in orderconfirmation page
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "makeup" PLP and overlay is displayed
And User clicks on the "Primers" link in the PLP overlay
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 5
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User is navigated to SRP "Face" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
#When User enters a new "default" billing address of "BillingAddress" type and of "different" country
Then User selects "Credit Card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When Guest user registers as "LYBC" user to Commerce in Order confirmation page
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page

@P1
@GuestFlowForRegUser
@AUP1 @CAP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Collection Pt Order as non-LYBC User opted checkout as guest
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "NonLYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User hovers over the "makeup" PLP and overlay is displayed
And User clicks on the "Bronzing" link in the PLP overlay
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 6
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User is navigated to SRP "Face" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Registered" Email id
And Sign in checkout screenset is displayed and user proceeds as "Guest" user
Then User is directed to Checkout and Delivery is default selected
When User select the "Collection point" fulfillment type in Checkout
And User select "valid" non store collection point for delivery
When User selects the "Express" delivery option
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit Card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed

#Testcases covered RCOM-1718 and RCOM-1860
@Paypal
@P2
@AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Delivery Order as Registered user using Paypal
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User is navigated to SRP "Face masks" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
When User selects "Paypal" as payment method
When User clicks On Pay Now Button
And user clicks on PayPal button and proceeds to Paypal payment page
And User directed to Paypal and login with "ValidPaypal" user
And User approves Payment for Paypal
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page

@DEP1 @ATP1 @SEP1
@Klarna
Scenario: Place Delivery Order as Registered user using Klarna PayLater
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User is navigated to SRP "lips" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
When User selects "Klarna Pay Later" as payment method
When User clicks On Pay Now Button
Then user is directed to Klarna site
And User approves the payment from Klarna Pay Later gateway
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page
