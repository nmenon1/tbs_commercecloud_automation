
Feature: Validate E-commerce website of TBS3
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Testcases covered RCOM-519 and RCOM-520
#@Regression
#Scenario: Validate Login Page of TBS
#Given User is on the Home Page "https://www.thebodyshop.in/" of TBS
#And Home page title is "The Body Shop"
#When User move to My Account menu of TBS
#Then user should see login page title as "Customer Login"
#And placeholder for customer login search should be "Mobile/Email"
#
#
#Testcases covered RCOM-521
#@Regression
#Scenario: Validate Customer Care Page of TBSPQRS
#Given User is on the Home Page "https://www.thebodyshop.in/" of TBS
#When User move to Customer Care menu of TBS
#Then user should see "Contact Us" section

#Testcases covered RCOM-2041
#@Regression
#Scenario: Validate the Wishlist page for a Guest User
#Given User is on the Home Page "HomePage" of TBS
#And User is navigated to "United Kingdom" Market
#When User clicks on the Wishlist button in Header
#Then The Empty Guest Wishlist is displayed
