Feature: Payment and Delivery Options Tests

@TempVISA
@PR1 @AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Validation of Guest order using VISA Card Payment
Given user is navigated to the SRP of the product "Hair" in the market to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
Then User selects "Credit Card" as payment method
When User enter payment details with "Visa" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed

@MasterCard
@PR1 @AUP1 @CAP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Place Collection Pt Order Using MASTER card payment checkout as guest
Given user is navigated to the SRP of the product "Face" in the market to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User select the "Collection point" fulfillment type in Checkout
And User select "valid" non store collection point for delivery
When User selects the "Express" delivery option
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed

@TempPaypal
@PR1 @AUP1 @ATP1 @CAP1 @DKP1 @FRP1 @DEP1 @NLP1 @ESP1 @SEP1 @UKP1 @USP1
Scenario: Validation of Guest order using Paypal Payment
Given user is navigated to the SRP of the product "Hair" in the market to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
When User selects "Paypal" as payment method
When User clicks On Pay Now Button
And user clicks on PayPal button and proceeds to Paypal payment page
And User directed to Paypal and login with "ValidPaypal" user
And User approves Payment for Paypal
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed

@TempKlarnaPayLater
@PR1 @DEP1 @ATP1 @SEP1
Scenario: Validation of Guest order using Klarna Payment
Given user is navigated to the SRP of the product "Hair" in the market to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User enters a new address of type "RegularAddress1" in form modal
When User selects the "Regular" delivery option
Then the same as delivery address checkbox appearance is according to the fulfilment selected
When User selects "Klarna Pay Later" as payment method
When User clicks On Pay Now Button
Then user is directed to Klarna site
And User approves the payment from Klarna Pay Later gateway
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed

@TempCISReg
@PR1 @USP1 @UKP1
Scenario: Place a CIS order as guest user and register at order confirmation
Given user is navigated to the SRP of the product "p002347" in the market to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When Stock is updated to "100" for the item  in corresponding "cis" warehouse
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
And Guest Checkout screenset is displayed and user enters the "Guest" Email id
Then User is directed to Checkout and Delivery is default selected
When User select the "cis" fulfillment type in Checkout
And User enters the keyword "invalid" in CIS Modal
Then The Global error message for Invalid Store Search is displayed
And User enters the keyword "valid" in CIS Modal
When CIS Store with Stock for the Product is selected from Store Modal
Then The Selected store is displayed in CIS
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When Guest user registers as "LYBC" user to Commerce in Order confirmation page
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account

@SP1Regression
Scenario: Place Collection Point Order for LYBC user logged in My Account
#Billing Address same as Shipping Address
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User clicks on Logout from My account section
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User logs in using "valid" credentials and Clicks on Sign in button
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User is navigated to SRP "Hands" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to SRP "Face Mask" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 3
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then Item added to cart is displayed in Basket
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
When User select the "Collection point" fulfillment type in Checkout
And User select "valid" non store collection point for delivery
When User selects the "Express" delivery option
When User Enters Collector details
When User enters a new "default" billing address of "BillingAddress" type and of "same" country
Then User selects "Credit card" as payment method
When User enter payment details with "Master" card type
When User clicks On Pay Now Button
Then Order is placed successfully,user directed to Order confirmation page and Order number is displayed
When User Clicks on My Account Icon in Header
When User clicks on the "Order history" section in My Account
Then Order number is displayed first in Order history section and order details are matching in Order details page
