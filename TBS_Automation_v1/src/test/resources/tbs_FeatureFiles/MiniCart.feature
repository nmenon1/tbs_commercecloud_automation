Feature: MiniCart
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created


#Testcases covered RCOM-1212, RCOM-1211 and RCOM-1215
@MiniCart
Scenario: MiniCart validations
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hover over the minicart icon
Then MiniCart overlay is not displayed
When User click the minicart icon
Then user is not navigated to empty basket page
When User is navigated to SRP "Hair" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
And user is able to close the interstitial
When User hover over the minicart icon
Then user is able to Close the mini cart modal
When User hover over the minicart icon
Then user is able to click on continue shopping button
When User hover over the minicart icon
Then user is able to navigate to Basket page by clicking on View Basket button
When user remove all the products from cart
Then Empty cart message is displayed

#Testcases covered RCOM-1214,RCOM-1330,RCOM-1376 and RCOM-1378
@Regression
@MiniCartVal
@P2
Scenario: Mini Cart content validations
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "Face" PLP and overlay is displayed
And User clicks on the "Face Masks" link in the PLP overlay
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
And user is able to close the interstitial
Then The Product recently added is present in Minicart overlay and Subtotal price is reflected when Minicart is hovered
When User is navigated to SRP "Hair" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 2
Then Item is added successfully to cart and Interstitial is displayed
And user is able to close the interstitial
Then The Product recently added is present in Minicart overlay and Subtotal price is reflected when Minicart is hovered
When User is navigated to SRP "Face" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 5
Then Item is added successfully to cart and Interstitial is displayed
And user is able to close the interstitial
Then The Product recently added is present in Minicart overlay and Subtotal price is reflected when Minicart is hovered
When User is navigated to SRP "Lips" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 1
Then Item is added successfully to cart and Interstitial is displayed
And user is able to close the interstitial
Then The Product recently added is present in Minicart overlay and Subtotal price is reflected when Minicart is hovered