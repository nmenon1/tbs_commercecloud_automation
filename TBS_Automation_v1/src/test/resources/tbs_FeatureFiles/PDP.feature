
Feature: PDP
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Testcases covered RCOM-1171, RCOM-1275 ,RCOM-1276 and RCOM-1170
@Regression
@EmailMe
Scenario: Validation of 'Email me when in Stock' button functionality in PLP,SRP and PDP

Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User is navigated to SRP "hair" to be tested
And User clicks on the Item in "1" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When Stock is updated to "0" for the item  in corresponding "delivery" warehouse
#Add step to validate whether the Email me when in stock button is displayed in PDP and PLP
Then User is able to navigate to the PLP page using category breadcrumb link
#No step to select the Product
And User is able to opt in for Email me when available notification for the product             