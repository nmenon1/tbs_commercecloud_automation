
Feature: Home
Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Testcases covered RCOM-999 and RCOM-1000
@Regression
Scenario: Validate whether Site rating badge is displayed.
Given User is on the Home Page "HomePage" of TBS
Then User is navigated to "Australia" Market
Then User can validate whether the Site Rating Badge is displayed
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Conditioner" link in the PLP overlay
Then User can validate whether the Site Rating Badge is displayed
When User is navigated to PDP of product
Then User can validate whether the Site Rating Badge is displayed
Then User is navigated to "United Kingdom" Market
Then User can validate whether the Site Rating Badge is not displayed
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Conditioner" link in the PLP overlay
Then User can validate whether the Site Rating Badge is not displayed
When User is navigated to PDP of product
Then User can validate whether the Site Rating Badge is not displayed

