
Feature: Shopping Cart

Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created
        
#Testcases covered RCOM-1166  
@Regression12
Scenario: Validation of quantity adjustment in the cart 
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Conditioner" link in the PLP overlay
Then User can add the item in 1 position to cart successfully with quantity = 1
When User click the minicart icon
Then user should be able to adjust the quantity of the product to "9"

#Testcases covered RCOM-2182, RCOM-2195 and RCOM-2178
#Author - Arjun.Vijay
@Message
@P2
Scenario: Validate Add Personal Message functionality for registered and guest user.
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Rewards" section in My Account
And User has points modal displayed in Points and Rewards and 0 points are displayed
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Shampoo" link in the PLP overlay
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 6
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then User is able to  Add Personal Message to the order
Then User can edit the personal message entered
Then User can remove the personal message from cart
When User Clicks on My Account Icon in Header
When User clicks on Logout from My account section
When User is navigated to SRP "Body Butters" to be tested
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 6
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then User is able to  Add Personal Message to the order
Then User can edit the personal message entered
Then User can remove the personal message from cart


#Testcases covered RCOM-2357, RCOM-2358 and RCOM-2352
#Author - Harish.Prasanna
@Gif
@P2
Scenario: Validation of Add and Remove functionality of Gift wrap in Basket page
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User hovers over the "Hair" PLP and overlay is displayed
And User clicks on the "Shampoo" link in the PLP overlay
And User clicks on the Item in "2" position displayed in SRP and navigates to it's PDP
Then User is navigated to the corresponding PDP and details are matching
When User is able to add product to cart with quantity as 6
Then Item is added successfully to cart and Interstitial is displayed
When User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial
Then user is able to click on add gift wrap option
And user is able to add the gift wrap to the order successfully
When User proceeds to checkout by Clicking on The Checkout Button In cart
Then User is directed to Checkout and Delivery is default selected
Then user is not able to see the CIS fulfilment method
When user navigates back to the basket page
When user clicks on remove gift wrap button in the basket
Then user can cancel Removal of Gift Wrap
When user clicks on remove gift wrap button in the basket
Then user is able to remove the Gift wrap from cart

#Testcases covered RCOM-2354
@Regression
Scenario: Validation of gift wrap option when cart contains ineligible item
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User hovers over the "Face" PLP and overlay is displayed
And User clicks on the "Face Masks" link in the PLP overlay
Then User can add the item in 1 position to cart successfully with quantity = 1
When User hovers over the "Hands" PLP and overlay is displayed
And User clicks on the "Moisturisers" link in the PLP overlay
Then User can add the item in 1 position to cart successfully with quantity = 2
When User hover over the minicart icon
Then user is able to View Basket
Then user should not be able to see the gift wrap option
