#Scenarios related to My Accoount
@MyAccount
Feature: MyAccountTests

Background: Test data sheet is loaded
Given test data excel sheet is loaded and hashmap is created

#Author - Arjun
@MyAddress
@P2
Scenario: Add , Edit and remove address from My account
Given User is on the Home Page "HomePage" of TBS
And User is navigated to Market to be tested
When User Clicks on My Account Icon in Header
Then The Login Modal will be displayed to the guest user
When User registers as new user of type "LYBC" from homepage
When User Clicks on My Account Icon in Header
Then User is successfully signed in and username/emailid is displayed as Signed in in My Account
When User clicks on the "Addresses" section in My Account
Then user is able to open new address modal
When User enters a new address of type "BillingAddress" in My Account
When User can edit delivery address
And User is able to remove all the saved addresses