/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_stepFiles;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import tbs_master.DriverFactory;
/*********
*Function:tbs_SeleniumDriver
*Author:mibin.b
*Copyright : INFOSYS LTD
*********/
public class TBSAfterActions{

    @After
    public static void tearDown(Scenario scenario) {	
    	WebDriver driver=DriverFactory.getCurrentDriver();
    	System.out.println(scenario.isFailed());
    	 if (scenario.isFailed()) {
             byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
             scenario.embed(screenshotBytes, "image/png");          
         }
    	
     DriverFactory.tearDown();
    
    }
}
