/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_stepFiles;

import java.util.List;

import cucumber.api.java.en.When;

import tbs_TestData.tbs_TestDataExcelSheetNames.ExcelSheetName;
import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;

public class TBSBackgroundActions {
	@When("^test data excel sheet is loaded and hashmap is created$")
	public static void test_data_excel_sheet_is_loaded_and_hashmap_is_created() {
		String ExcelPath = "./src/test/resources/tbs_TestData/TBS_TestData.xlsx";
		tbs_TestDataObject.loadTestDetailsHashMap(ExcelPath,ExcelSheetName.Master.name());
		for(List<tbs_TestDataValues> TDataDetailsList : tbs_TestDataObject.TestDataHashMap.get("United Kingdom")) {
			//System.out.println(TDataDetailsList.get(0).url.trim());
		}
	}
}
