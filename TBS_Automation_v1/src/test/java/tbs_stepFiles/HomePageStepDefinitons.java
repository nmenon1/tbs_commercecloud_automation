/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_stepFiles;

import java.util.List;

import com.relevantcodes.extentreports.model.SystemProperties;
import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.HomePageAction;
import com.tbs.rcom.actions.PDPPageAction;
import com.tbs.rcom.actions.PLPPageAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;
import com.tbs.rcom.yaml.MarketDetails;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_TestData.tbs_TestDataExcelSheetNames.ExcelSheetName;
import tbs_TestData.tbs_TestDataObject;
import tbs_TestData.tbs_TestDataValues;
import tbs_master.DriverFactory;
import tbs_master.ProjectProperties;
import tbs_master.SeleniumUtils;

public class HomePageStepDefinitons {
	HomePageAction tbsHomePageActions = new HomePageAction();
	PLPPageAction tbsPLPPageActions = new PLPPageAction();
	PDPPageAction tbsPDPPageActions = new PDPPageAction();
	BaseAction action = new BaseAction();

	@Given("^User is on the Home Page \"([^\"]*)\" of TBS$")
	public void goToHomePage(String url){
		String urlEnv;
		if(url.equalsIgnoreCase("HomePage"));{
			url = ProjectProperties.baseURL;
			urlEnv=System.getProperty("baseURLenv");
		}
		if(urlEnv.contains("uat")) {
			url=url.replace("qa", "uat");
		}
		DriverFactory.openPage(url);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.guestFlowForRegUser, "false");
	}
	@And("^User is navigated to Market to be tested$")
	public void navigateToCustomMarket() throws Throwable {

		String testMarket= System.getProperty("marketToBeExecuted");
		if(testMarket.contains("_"))
		{
			testMarket=testMarket.replace("_", " ");	
		}
		MarketDetails data = MarketDetails.fetch(testMarket);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.marketToBeTested,testMarket);
		String currency = data.marketData.currencySymbol;
		SeleniumUtils.storeKeyValue(KeyStoreConstants.currencySymbol, currency);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartOrderTotal, "0.00");
		SeleniumUtils.storeKeyValue(KeyStoreConstants.cartItemCount, "0");
		action
		.homePageAction()
		.goToHomePageOfCustomMarket(testMarket);
	}

	@When("^User clicks on the Wishlist button in Header$")
	public void clickOnWishListBtnInHeader() {
		action
		.homePageAction()
		.clickOnWishlistBtnInHeader();
	}

	@Then("^User can validate whether the Site Rating Badge is displayed$")
	public void validate_whether_the_site_rating_badge_is_displayed() throws Throwable {
		tbsHomePageActions.validatingSiteRatingBadgeDisplayed();
	}


	@Then("^User is navigated to \"([^\"]*)\" Market$")		
	public void user_is_navigated_to_given_Market(String market) throws Throwable {
		tbsHomePageActions.goToHomePageOfCustomMarket(market);
	}

	@Then("^User can validate whether the Site Rating Badge is not displayed$")
	public void validate_whether_the_site_rating_badge_is_not_displayed() throws Throwable {
		tbsHomePageActions.validatingSiteRatingBadgeNotDisplayed();
	}

	@When("^User Clicks on My Account Icon in Header$")
	public void clickOnMyAccountIconInHeader() {
		action
		.homePageAction()
		.clickOnMyAccountLinkInHeader();
	}
}
