package tbs_stepFiles;

import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.HomePageAction;
import com.tbs.rcom.actions.PDPPageAction;
import com.tbs.rcom.actions.PLPPageAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.SeleniumUtils;

public class PLPStepDefinitions {
	HomePageAction tbsHomePageActions = new HomePageAction();
	PLPPageAction tbsPLPPageActions = new PLPPageAction();
	PDPPageAction tbsPDPPageActions = new PDPPageAction();
	BaseAction action = new BaseAction();
	
	@When("^User hovers over the \"([^\"]*)\" PLP and overlay is displayed$")
	public void hoverOverPLP(String plpName) throws Throwable {
		SeleniumUtils.storeKeyValue(KeyStoreConstants.plpName, plpName);
		action
		.homePageAction()
		.hoverOverPLPAndValidateOverlay(plpName);
	}
	
	@And("^User clicks on the \"([^\"]*)\" link in the PLP overlay$")
	public void clickOnSubcategoryLinkInOverlay(String subCategoryName) {
		tbsHomePageActions
		.clickOnSubcategoryLinkInOverlay(subCategoryName);
	}
	
	@Then("^User is able to filter the products$")
	public void user_is_able_to_filter_the_products() throws Throwable {
		action
		.plpAction()
		.validateFilterInPLP();
	}
	
	@Then("^User is able to sort the products using \"([^\"]*)\" sort type$")
	public void verifySortingIsApplied(String sortType) throws Throwable {
		action
		.plpAction()
		.applyAndValidateIfSortingIsApplied(sortType);
	}
	
	@When("^User is navigated to PDP of product$")
	public void user_is_navigated_to_PDP_of_product() throws Throwable {
		tbsPLPPageActions.GoToPDPofFirstProduct();
	}
	
	
	@Then("^User can add the item in (\\d+) position to cart successfully with quantity = (\\d+)$")
	public void addItemToCartFromSeeOptionsModal(int position,int qty) {
		action
		.plpAction()
		.addToBagFromPLP(position)
		.updateQtyInSeeOptionsModal(qty)
		.addItemToCartFromSeeOptions(position);
	}
}
