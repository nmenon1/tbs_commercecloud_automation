/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * author : mibin.b
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 *******************************************************************************/
package tbs_stepFiles;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.commons.io.FileUtils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.tbs.rcom.core.utils.tbs_ConfigFileUtils;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.runtime.ScenarioImpl;
import tbs_master.DriverFactory;
/*********
 *Function:TBSBeforeActions
 *Author:mibin.b
 *Copyright : INFOSYS LTD
 *********/
public class TBSBeforeActions  {

	@Before
	
	public static void setUp(Scenario scenario) throws MalformedURLException {
		System.out.println("Before actions");
		
		//System.out.println("Before - TBSBeforeActions");
		DriverFactory.setUpDriver(scenario);
		File file = new File(".\\src\\test\\java\\tbs_testcases\\");
		if(file.exists()) {
			System.out.println("TestDelet");
		try
	     { 
	    	 
	    	 
	    	 FileUtils.cleanDirectory(file); 
	     } 
	     catch(IOException e) 
	     { 
	         System.out.println("No such file/directory exists"); 
	     } 
	     
	     catch(NullPointerException e)
	     {
	    	 
	     }}
	}
}
