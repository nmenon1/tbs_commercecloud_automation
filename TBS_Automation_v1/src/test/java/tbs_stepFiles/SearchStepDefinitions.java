package tbs_stepFiles;

import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.HomePageAction;
import com.tbs.rcom.actions.PDPPageAction;
import com.tbs.rcom.actions.PLPPageAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.SeleniumUtils;

public class SearchStepDefinitions {

	HomePageAction tbsHomePageActions = new HomePageAction();
	PLPPageAction tbsPLPPageActions = new PLPPageAction();
	PDPPageAction tbsPDPPageActions = new PDPPageAction();

	BaseAction action = new BaseAction();

	@When("^User enters \"([^\"]*)\" in the search field$")
	public void User_enters_characters_in_the_search_field(String searchkey) throws Throwable {
		action
		.homePageAction()
		.enterKeywordInSearchTextField(searchkey);
	}

	@When("^User is navigated to SRP \"([^\"]*)\" to be tested$")
	public void user_is_navigated_to_SRP_to_be_tested(String searchKey){
		action
		.homePageAction()
		.enterSearchKeywordInTextField(searchKey);
	}

	@Then("^User can validate the search suggestions for invalid keywords$")
	public void User_can_validate_the_search_suggestions_for_invalid_keywords() throws Throwable {
		action
		.homePageAction()
		.TBS_SearchingInvalidKeywords();
	}

	@Then("^User can validate the search suggestions$")
	public void User_can_validate_the_search_suggestions() throws Throwable {
		action
		.homePageAction()
		.TBS_Display_Of_Suggestions();
	}

	@When("^User clicks on the suggestion$")
	public void User_clicks_on_the_suggestion() throws Throwable {
		tbsHomePageActions.TBS_Click_On_Suggestion();
	}

	@Then("^User should be navigated to the SRP \"([^\"]*)\" to be tested$")
	public void user_should_be_navigated_to_SRP_to_be_tested(String srchkywd) throws Throwable {
		tbsHomePageActions.TBS_AccessingTheSRP(srchkywd);
	}

	@And("^zero results message is displayed$")
	public void invalidBarCodeSearchResultValidation()
	{
		action
		.searchAction()
		.SearchResultValidation();
	}

	@And("^User is directed to Search Results page$")
	public void userDirectedToSRPValidation() throws Exception
	{
		action
		.searchAction()
		.NavToSRP();
	}

	@When("^User enters valid barcode \"([^\\\"]*)\" of product \"([^\\\"]*)\" in the search field$")
	public void enterValidarcodeIntoSearchField(String validBarCode, String productCode) throws Exception 
	{
		SeleniumUtils.storeKeyValue(KeyStoreConstants.searchTag, validBarCode);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.srpProductId, productCode);
		tbsHomePageActions.enterKeywordInSearchTextField(validBarCode);
	}

	@And("^products with respective bar code will be displayed$")
	public void validateValidBarCodeResult() throws Exception
	{
		action
		.searchAction()
		.validBarcodeSearchResultValidation();
	}

	@And("^User clicks on the Item in \"([^\"]*)\" position displayed in SRP and navigates to it's PDP$")
	public void clickOnTheProductByPositionDisplayedInSRP(String index) {
		int pos = Integer.parseInt(index);
		action
		.searchAction()
		.clickOnTheProductByPositionInSRP(pos);
	}


}
