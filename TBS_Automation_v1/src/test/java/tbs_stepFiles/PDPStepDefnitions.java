package tbs_stepFiles;

import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;

import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.HomePageAction;
import com.tbs.rcom.actions.PDPPageAction;
import com.tbs.rcom.actions.PLPPageAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.core.utils.tbs_ExcelUtils;
import com.tbs.rcom.pageLocators.HomePageLocators;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.MarketTestData;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.SeleniumUtils;
import com.tbs.rcom.core.utils.tbs_ExcelUtils;

public class PDPStepDefnitions {

	HomePageAction tbsHomePageActions = new HomePageAction();
	PLPPageAction tbsPLPPageActions = new PLPPageAction();
	PDPPageAction tbsPDPPageActions = new PDPPageAction();

	BaseAction action = new BaseAction();

	@Then("^User can validate Product details Price metric and Currency in PDP$")
	public void user_can_validate_Product_details_Price_metric_and_Currency_in_PDP() throws Throwable {
		tbsPDPPageActions.validatePDPContents();
	}

	@Then("^User is able to update the color of product and the Quantity to (\\d+)$")
	public void user_is_able_to_update_the_color_of_product_and_the_Quantity_to(int NoOfQuantity) throws Throwable {
		tbsPDPPageActions.TBSPDPpageActions(NoOfQuantity);
	}
	@When("^User is able to add product to cart with quantity as (\\d+)$")
	public void addItemToCartFromPDP(int quantity) throws Throwable {
		action
		.pdpAction()
		.addItemToCartFromPDP(quantity);

	}	

	@Then("^Item is added successfully to cart and Interstitial is displayed$")
	public void validateItemIsAddedToBasketAndInterstitialIsDisplayed() throws Throwable {

		action
		.pdpAction()
		.verifyItemIsAddedToCartAndInterstitialDisplayed();
	}
	
	@And("^user is able to close the interstitial$")
	public void closeAddToBagInterstitial(){
		action
		.pdpAction()
		.closeAddToCartInterstitial();
	}
	
	@When("^User is navigated to Cart page on clicking View Basket button in Add To Bag Interstitial$")
	public void navigateToCartByClickingViewBasketInAddToBagInterstitial(){
		action
		.pdpAction()
		.navigateToCartByClickingViewBasketInAddToBagInterstitial();
	}

	@Then("^User is able to click on the See Options button of out of stock product$")
	public void clickSeeOptionsBtnOfOOSProduct() throws Throwable {
		String productId = SeleniumUtils.getStoredValue(KeyStoreConstants.productPartNumber).toString();
		tbsPLPPageActions.productSpecificSeeOptionsClick(productId);

	}

	@And("^User is able to opt in for Email me when available notification for the product$")
	public void notifyMeWhenInAvailableOptIn() throws Throwable {
		action
		.plpAction()
		.emailMeWhenInStockOptIN();
	}
	@Then("^User is navigated to PDP of the product \"([^\"]*)\"$")
	public void productSpecificPDPNavigation(String productID) throws Throwable {

		tbsPLPPageActions.productSpecificPDPNavigation(productID); 
	}

	@Then("^User is able to navigate to the PLP page using category breadcrumb link$")
	public void categoryNavigationBreadcrumb() throws Throwable {
		action
		.pdpAction()
		.categoryNavigationViaBreadcrumb();

	}

	@SuppressWarnings("deprecation")
	@When("^Stock is updated to \"([^\"]*)\" for the item  in corresponding \"([^\"]*)\" warehouse$")
	public void updateStockForItemInWarehouse(String stock,String fulfillmentType) {
		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
		String productId = SeleniumUtils.getStoredValue(KeyStoreConstants.productPartNumber).toString();
		String planningID ="";
		tbs_ExcelUtils ExUtils = new tbs_ExcelUtils();
		Workbook workbook=ExUtils.openExcelWorkBook(".\\src\\main\\java\\tbs_TestData\\test-data\\ProductPlanningIds.xlsx");
		Sheet sheet = ExUtils.openExcelWorkSheet(workbook, "PlanningId");
		
		int rowCount=sheet.getLastRowNum();
	    for(int i=1;i<=rowCount;i++)
	    {
	    	Cell cell= sheet.getRow(i).getCell(0);
	    	cell.setCellType(Cell.CELL_TYPE_STRING);
	    	String product= cell.getStringCellValue();
	    	//System.out.println("Product "+product +" Product ID "+productId);
	    	if(product.trim().equals(productId.trim()))
	    	{
	    		cell = sheet.getRow(i).getCell(1);
	    		cell.setCellType(Cell.CELL_TYPE_STRING);
	    		planningID=cell.getStringCellValue();
	    		System.out.println("Plannind ID in loop"+ planningID);
	    		break;
	    	}
	    	if(i>rowCount) {
	    		System.out.println("Product not found");
	    		break;
	    	}
	    }
		
		
		System.out.println("Part number for API: "+planningID);
		MarketDetails data = MarketDetails.fetch(market);
		String wareHouse = "";
		if(fulfillmentType.toLowerCase().equalsIgnoreCase("cis"))
			wareHouse = data.marketData.storeWareHouse;
		else if(fulfillmentType.toLowerCase().equalsIgnoreCase("delivery")||fulfillmentType.toLowerCase().contains("collection point"))
			wareHouse = data.marketData.dcWareHouse;
		
		action
		.pdpAction()
		.updateStockForItem(stock, planningID, wareHouse);
	}

	@Then("^User is navigated to the corresponding PDP and details are matching$")
	public void validateProductDetailsInPDP() {
		action
		.pdpAction()
		.validateProductDetailsInPDP();
	}
	
     //Temporary stepfiles
	@Given("^user is navigated to the SRP of the product \"([^\"]*)\" in the market to be tested$")
	public void navigateToSRP(String productID)
	{
		String market="";
		String marketToBeTested = System.getProperty("marketToBeExecuted").toString();
		if(marketToBeTested.contains("_"))
		{
			marketToBeTested=marketToBeTested.replace("_", " ");	
		}
		action
		.pdpAction()
		.navigateToSRP(productID,marketToBeTested);
		
	}
}