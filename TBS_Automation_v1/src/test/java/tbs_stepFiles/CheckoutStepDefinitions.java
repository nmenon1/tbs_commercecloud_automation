package tbs_stepFiles;

import com.tbs.rcom.actions.LoginAction;
import com.tbs.rcom.actions.PaymentPageAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.CheckoutAction;
import com.tbs.rcom.yaml.AddressDetails;
import com.tbs.rcom.yaml.LoginDetails;
import com.tbs.rcom.yaml.MarketDetails;
import com.tbs.rcom.yaml.PaymentDetails;
import com.tbs.rcom.yaml.WhoWillCollectDetails;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.SeleniumUtils;

public class CheckoutStepDefinitions {
	BaseAction action = new BaseAction();

	LoginAction loginPageActions =new LoginAction();
	PaymentPageAction tbsPaymentPageAction = new PaymentPageAction();


	@And("^User logs in using \"([^\"]*)\" credentials and Clicks on Sign in button$")
	public void enterCredentialsInLoginModalAndClickOnSignIn(String userType) {
		action
		.loginAction()
		.enterCredentialsInLoginModal(userType);
	}

	@When("^User registers as new user of type \"([^\"]*)\" from checkout login page$")
	public void registerAsNewUser(String typeOfUser) {
		LoginDetails data = LoginDetails.fetch(typeOfUser);
		loginPageActions.registerAsNewUser(data.loginData);
	}
	@When("^User registers as new user of type \"([^\\\"]*)\" from homepage$")
	public void registerNewUser(String typeOfUser)
	{
		LoginDetails data = LoginDetails.fetch(typeOfUser);
		action
		.loginAction()
		.registerNewUser(data.loginData);
	}
	@When("^user can select delivery address$")
	public void user_proceeds_to_delivery_mode_page() throws Throwable {
		action.checkoutAction().proceedToDeliveryModePage();
	}

	@And("^User selects the \"([^\"]*)\" delivery option$")
	public void selectDeliveryOption(String deliveryMode) throws Throwable {
		action
		.checkoutAction()
		.selectDeliveryOption(deliveryMode);
	}

	/**
	 * Enter new Address in form modal
	 * @author Edwin_John
	 * @param add
	 * @throws Exception
	 */
	@When("^User enters a new address of type \"([^\"]*)\" in form modal")
	public void enterNewAddressInFormModal(String addressType)  {
		String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
		switch(market.toLowerCase())
		{
		 case "usa" : addressType = "BillingAddressUS"; break;
		 case "canada" : addressType = "BillingAddressCA"; break;
		 case "spain"  : addressType = "BillingAddressES"; break;
		 case "australia"  : addressType = "BillingAddressAU"; break;
		 case "germany"  : addressType = "BillingAddressDE"; break;

		}
		AddressDetails testData = AddressDetails.fetch(addressType);
		action
		.checkoutAction()
		.enterNewAddress(testData.addressData);
		String value = SeleniumUtils.getStoredValue(KeyStoreConstants.addressLine).toString().toLowerCase();
		if(value.contains("pobox"))
			validationOfPoRestrictionErrorMessage();
		else
			action
			.checkoutAction()
			.validateTheAddressIsAdded();
	}


	@When("^User can edit delivery address")
	public void editDeliveryAddress() throws Exception {
		action.checkoutAction().clickOnEditAddressBtn();
		action.checkoutAction().editExistingDeliveryAddress();
		action.checkoutAction().validateTheEditedAddress();
	}

	/**
	 * Select non store collection point as delivery method
	 * @author nithin.c03
	 */
	@And("^User select \"([^\"]*)\" non store collection point for delivery$")
	public void selectNonStoreCollectionPoint(String location) throws Exception {
		MarketDetails data = MarketDetails.fetch(SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString());
		location = data.marketData.cisSearchKeyword;
		SeleniumUtils.storeKeyValue("CISSearchKeyword", location);
		action
		.checkoutAction()
		.selectNonStoreCollectionPoint(location);
	}

	/**
	 * Enter who will collect details in form modal
	 * @author nithin.c03
	 */

	@When("^User Enters Collector details$")
	public void enterCollectorDetails() {
		action
		.checkoutAction()
		.enterCollectorDetails();
	}



	/**
	 * Validation of PO restriction error message
	 * @author harish.prasanna
	 */
	@Then("^User will be displayed with an error message$")
	public void validationOfPoRestrictionErrorMessage() {
		action
		.checkoutAction()
		.validationOfPoRestrictionErrorMessage();
	}


	@When("^selects a valid billing address$")
	public void selects_a_valid_billing_address() throws Throwable {

		tbsPaymentPageAction.selectingBillingAddress();
	}

	/*@When("^Enters a valid credit card$")
	public void enters_a_valid_credit_card() throws Throwable {

	    tbsPaymentPageAction.enterTheCardDetails();
	}*/

	@When("^Clicks on Finish and Pay$")
	public void clicks_on_Finish_and_Pay() throws Throwable {
		tbsPaymentPageAction.clickFinishAndPay();
	}

	@When("^User selects \"([^\"]*)\" as payment method$")
	public void selectPaymentOption(String paymentMethod)
	{
		//String payOption = paymentMethod.split(" ")[0];
		action
		.paymentAction()
		.selectPaymentMethod(paymentMethod);
	}

	@And("^User is able to select Saved Card for Payment$")
	public void selectSavedCreditCard() throws Exception
	{
		action
		.paymentAction()
		.selectSavedCreditCard();

	}



	@When("^User enter payment details with \"([^\"]*)\" card type$")
	public void enterPaymentDetails(String cardDetailType) throws Exception {
		PaymentDetails data = PaymentDetails.fetch(cardDetailType);
		SeleniumUtils.storeKeyValue(KeyStoreConstants.savedCardType, cardDetailType.toLowerCase());
		action
		.paymentAction()
		.addNewCardFromSavedCardSection()
		.enterPaymentDetails(data.paymentData);

	}

	@When("^User clicks On Pay Now Button$")
	public void clickOnPayNowBtn()
	{
		action
		.paymentAction()
		.clickOnPayNowButton();
	}

	@And("^User approves Payment for Paypal$")
	public void approvePaymentForPaypal() throws Throwable {
		action
		.paymentAction()
		.approvePaymentForPaypal();
	}

	@And("^user clicks on PayPal button and proceeds to Paypal payment page$")
	public void clickOnPaypalBtn() throws Throwable {
		action
		.paymentAction()
		.clickOnPayPalSite();

	}

	@And("^User directed to Paypal and login with \"([^\"]*)\" user$")
	public void loginIntoPayPalAccount(String typeOfPayPalUser) throws Throwable {
		PaymentDetails testpaypal = PaymentDetails.fetch(typeOfPayPalUser);
		action
		.paymentAction()
		.loginAsValidPaypalUser(testpaypal.paymentData);

	}

	@Then("^user can make payment via paypal successfully$")
	public void paymentViaPaypal() throws Throwable {
		tbsPaymentPageAction.makingPaymentPayPal();
	}

	@Then("^user should be displayed with authorization failure error message$")
	public void validationOfInvalidPayPal() throws Throwable {
		tbsPaymentPageAction.validationOfInvalidPayPalUser();
	}

	@When("^User unselect Same as billing address option$")
	public void UncheckSameAsDeliveryAddress()
	{
		tbsPaymentPageAction.unCheckSameAsDeliveryAddressForBillingAddress();
	}

	@When("^User enters a new \"([^\"]*)\" billing address of \"([^\"]*)\" type and of \"([^\"]*)\" country$")
	public void addNewBillingAddress(String type,String addressType,String country) {
		if(country.equalsIgnoreCase("same"))
		{
			String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
			switch(market)
			{
			case "United Kingdom": addressType= "BillingAddressUK"; break;
			case "USA" : addressType = "BillingAddressUS"; break;
			case "Netherlands" : addressType = "BillingAddressNL"; break;
			case "Germany" : addressType = "BillingAddressDE"; break;
			case "Denmark" : addressType = "BillingAddressDK"; break;
			case "Canada" : addressType = "BillingAddressCA"; break;
			case "Austria" : addressType = "BillingAddressAT"; break;
			case "Australia" : addressType = "BillingAddressAU"; break;
			case "Spain" : addressType = "BillingAddressES"; break;
			case "Sweden" : addressType="BillingAddressSE"; break;
			case "France" : addressType= "BillingAddressFR"; break;
			}
		}
		AddressDetails data = AddressDetails.fetch(addressType);
		if(addressType.contains(addressType))
			action
			.paymentAction()
			.addBillingAddressInCheckout(type,data,country);

	}


	@When("^User select the \"([^\"]*)\" fulfillment type in Checkout$")
	public void selectFulfillmentType(String fulfilmentMethod) {
		action
		.checkoutAction()
		.selectFulfillmentType(fulfilmentMethod);
	}

	/**
	 * Enter Search keyword in  CIS Modal
	 * @param keyword - Enter "valid" for a valid search keyword from YAML and "invalid" for a invalid search keyword/an address outside the current Market
	 */
	@And("^User enters the keyword \"([^\"]*)\" in CIS Modal$")
	public void enterSearchKeywordInCISModal(String keyword) {
		if(keyword.toLowerCase().equalsIgnoreCase("valid")) {
			MarketDetails data = MarketDetails.fetch(SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString());
			keyword = data.marketData.cisSearchKeyword;
			SeleniumUtils.storeKeyValue("CISSearchKeyword", keyword);
		}
		else
			keyword="InvalidLocation";
		action
		.checkoutAction()
		.enterSearchKeywordInCISModal(keyword);
	}

	@Then("^The Global error message for Invalid Store Search is displayed$")
	public void validateErrorMsgForInvalidStoreSearch() {
		action
		.checkoutAction()
		.validateErrorMsgForInvalidStoreSearch();
	}

	@When("^CIS Store with Stock for the Product is selected from Store Modal$")
	public void selectStoreFromModal() {
		action
		.checkoutAction()
		.selectStoreWithStockFromStoreSearchModal();
	}

	@Then("^The Selected store is displayed in CIS$")
	public void validateStoreSelectedIsDisplayedInCISSection() {
		action
		.checkoutAction()
		.validateSelectedStoreIsDisplayedInCISSection();
	}

	@When("^the Outstanding amount  in Order summary section is paid$")
	public void verifyOutstandingAmountInOrderSummary() {
		action
		.paymentAction()
		.calculateOutstandingAmountInOrderSummary();

	}

	@When("User enters the \"([^\"]*)\" gift card details and clicks on Use Card")
	public void addGiftCardDetailsInCheckout(String giftCardType) {
		MarketDetails data = MarketDetails.fetch(SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString());
		if(giftCardType.equalsIgnoreCase("valid")) {
			action
			.paymentAction()
			.enterGiftCardDetailsAndApply(data.marketData.giftCardNumber,data.marketData.giftCardPIN);
		}
		else if(giftCardType.equalsIgnoreCase("invalid")){
			action
			.paymentAction()
			.enterGiftCardDetailsAndApply("5045075617499910421","1111");
		}
	}

	@Then("The Gift card added is applied and Remove button is displayed")
	public void validateGiftCardIsApplied() {
		action
		.paymentAction()
		.validateGiftCardIsApplied();
	}

	@Then("Add Another Gift Card link isn't displayed")
	public void verifyAddAnotherGiftCardLinkIsDisplayed() {
		action
		.paymentAction()
		.validateIfAddAnotherGiftCardLinkDisplayed();
	}

	@Then("^Order is placed successfully,user directed to Order confirmation page and Order number is displayed$")
	public void validateOrderConfirmationAndIsDisplayedwithOrderNumber() {
		action
		.checkoutAction()
		.validateOrderConfirmationAndIsDisplayedwithOrderNumber();
	}

	@Then("^User is directed to Checkout and Delivery is default selected$")
	public void validateDeliveryIsSelectedAsDefaultFulfillmentInCheckout() {
		action
		.checkoutAction()
		.verifyUserNavigatedToCheckoutAndDeliveryIsSelected();
	}

	@Then("^the same as delivery address checkbox appearance is according to the fulfilment selected$")
	public void validateSameAsDeliveryAddressSelectedForBillingAddress() {
		action
		.paymentAction()
		.validateIfSameAsDeliveryAddressCheckboxIsSelected();
	}

	@And("^Guest Checkout screenset is displayed and user enters the \"([^\"]*)\" Email id$")
	public void verifyGuestCheckoutAndEnterEmailId(String userType) throws InterruptedException {
		action
		.loginAction()
		.validateGuestCheckoutScreensetAndEnterEmailID(userType);
	}

	@And("^Sign in checkout screenset is displayed and user proceeds as \"([^\"]*)\" user$")
	public void verifyRegEmailTxtAndSignInCheckoutScreenset(String flowType) {
		action
		.loginAction()
		.verifyRegEmailTxtAndSignInCheckoutScreenset(flowType);
	}

	@When("^Guest user registers as \"([^\"]*)\" user to Commerce in Order confirmation page")
	public void signupGuestToCommerceInOrderConfirmationPage(String userType) {
		action
		.checkoutAction()
		.signupGuestToCommerceInOrderConfirmationPage(userType);
	}

	@When("^Non-LYBC user signs up for LYBC in Order confirmation page$")
	public void signupForLYBCInOrderConfirmationPage() {
		action
		.checkoutAction()
		.signupForLYBCInOrderConfirmationPage();
	}

	@And("^User approves the payment from Klarna Pay Later gateway$")
	public void clickBuyBtnForKlarnaPayLater() {
		action
		.checkoutAction()
		.payUsingKlarnaPayLater();
	}

	@Then("^user is not able to see the CIS fulfilment method$")
	public void cisMethodHidden()
	{
		action
		.checkoutAction()
		.verifyCISIsHiddenForGiftWrapOrder();
	}

	@When("^user navigates back to the basket page$")
	public void navigateBackToBasketPagefromCheckoutPage()
	{
		action
		.checkoutAction()
		.navigateBackToBasketPagefromCheckoutPage();
	}

	/**
	 * @author Arjun
	 * Validate Klarna site loaded
	 */
	@Then("^user is directed to Klarna site$")
	public void validateKlarnaSiteLoaded()
	{
		action
		.checkoutAction()
		.validateKlarnaSiteLoaded();
	}


}
