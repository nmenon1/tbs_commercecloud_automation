package tbs_stepFiles;

import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.core.utils.KeyStoreConstants;
import com.tbs.rcom.yaml.AddressDetails;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.SeleniumUtils;

public class MyAccountStepDefinitions {
	BaseAction action = new BaseAction();

	@Then("^The Login Modal will be displayed to the guest user$")
	public void validateLoginModalIsDisplayed() {
		action
		.loginAction()
		.validateIfLoginModalIsDisplayed();
	}
	
	@Then("^User is successfully signed in and username/emailid is displayed as Signed in in My Account$")
	public void validateUserIsSignedIn() {
		action
		.myAccountAction()
		.validateUserIsSignedIn();
	}
	
	@And("^User has points modal displayed in Points and Rewards and 0 points are displayed")
	public void validateUserHasSignedUpForLYBC() {
		action
		.myAccountAction()
		.verifyUserIsSignedUpForLYBC();
	}
	
	@When("^User clicks on Logout from My account section$")
	public void clickOnLogoutLink() {
		action
		.myAccountAction()
		.clickOnLogoutLink();
	}
	
	@When("^User clicks on the \"([^\"]*)\" section in My Account$")
	public void clickOnReqSectionInMyAccount(String section) {
		action
		.myAccountAction()
		.clickOnReqSectionInMyAccount(section);
	}
	
	@Then("^Order number is displayed first in Order history section and order details are matching in Order details page$")
	public void validatePlacedOrderIsDisplayedInORderHistory() {
		action
		.myAccountAction()
		.validatePlacedOrderIsDisplayedInORderHistory();
	}
	
	@Then("^The Saved card details are present Saved Payments section$")
	public void validateSavedCardDetailsInMyAccount() {
		action
		.myAccountAction()
		.validateSavedCardInMyAccount();
	}
	
	@Then("^user is able to open new address modal$")
	public void addNewAddressMyAccount()
	{
		action
		.myAccountAction()
		.validateNewAddressModalOpened();
	}
	
	@And("^User is able to remove all the saved addresses$")
	public void removeSavedAdddress()
	{
		action
		.myAccountAction()
		.validateRemoveSavedAddressInMyAccount();
	}
	
	@When("^User enters a new address of type \"([^\"]*)\" in My Account$")
	public void enterNewAddressInMyAccount(String addressType) 
	{
		
			String market = SeleniumUtils.getStoredValue(KeyStoreConstants.marketToBeTested).toString();
			switch(market)
			{
			case "United Kingdom": addressType= "BillingAddressUK"; break;
			case "USA" : addressType = "BillingAddressUS"; break;
			case "Netherlands" : addressType = "BillingAddressNL"; break;
			case "Germany" : addressType = "BillingAddressDE"; break;
			case "Denmark" : addressType = "BillingAddressDK"; break;
			case "Canada" : addressType = "BillingAddressCA"; break;
			case "Austria" : addressType = "BillingAddressAT"; break;
			case "Australia" : addressType = "BillingAddressAU"; break;
			case "Spain" : addressType = "BillingAddressES"; break;
			case "Sweden" : addressType="BillingAddressSweden"; break;
			}
		
		AddressDetails testData = AddressDetails.fetch(addressType);
		action
		.myAccountAction()
		.enterNewAddressInMyAccount(testData.addressData);
		action
		.checkoutAction()
		.validateTheAddressIsAdded();
	}
}
