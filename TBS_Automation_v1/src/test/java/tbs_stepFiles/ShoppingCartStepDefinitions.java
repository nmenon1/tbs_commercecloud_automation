package tbs_stepFiles;

import com.tbs.rcom.actions.BaseAction;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ShoppingCartStepDefinitions {
	
	BaseAction action = new BaseAction();
	
	@When("^User proceeds to checkout by Clicking on The Checkout Button In cart$")
	public void proceedToCheckout() throws Throwable {
		
		action
		.shoppingCartAction()
		.proccedToCheckoutFromCart();	
	    
	}
	
	@When("^User removes all items from basket$")
	public void removeAllItemsFromBasket() throws Throwable{
		action
		.shoppingCartAction()
		.removeAllITemsFromBasket();
	}
	
	@Then("^user should be able to adjust the quantity of the product to \"([^\"]*)\"$")
	public void quantityAdjustement(String qty) throws Throwable{
		action
		.shoppingCartAction()
		.quantityAdjustments(qty);
	}
	
	@Then("^user is able to click on add gift wrap option$")
	public void clickAddGiftBoxOption() throws Throwable{
		action
		.shoppingCartAction()
		.clickOnAddGiftBoxOption();
	}
	
	@Then("^user click on cancel button in the Add Gift Wrap Modal$")
	public void clickOnCancelButton() throws Throwable{
		action
		.shoppingCartAction()
		.clickCancelBtnInAddGiftWrapModal();
	}
	
	
	@And("^user is able to add the gift wrap to the order successfully$")
	public void validateGiftWrapIsAdded() throws Throwable{
		action
		.shoppingCartAction()
		.validateGiftWrapISAddedToCart();
		
	}
	
	@When("^user clicks on remove gift wrap button in the basket$")
	public void clickOnRemovegiftWrap() throws Throwable{
		action
		.shoppingCartAction()
		.clickOnRemoveButton();
	}
	
	
	@Then("^user can cancel Removal of Gift Wrap$")
	public void clickOnCancelInRemoveModal() throws Throwable{
		action
		.shoppingCartAction()
		.clickOnCancelInRemoveModal();
	}
	
	@Then("^user is able to remove the Gift wrap from cart$")
	public void removeGiftWrap() throws Throwable{
		action
		.shoppingCartAction()
		.removalOfGiftWrap();
	}
	
	
	@Then("^user should not be able to see the gift wrap option$")
	public void giftWrapForIneligibleProduct() throws Throwable {
		action
		.shoppingCartAction()
		.giftWrapIneligibleProduct();
	}

	@Then("^User is able to  Add Personal Message to the order$")
	public void clickOnAddPersonalMsgInCart() throws Throwable {
		action
		.shoppingCartAction()
		.addPersonalMessage();
	}

	
	@Then("^User can edit the personal message entered$")
	public void editPersonalMsgAddedInCart() throws Throwable {
		action
		.shoppingCartAction()
		.editPersonalMessage();
	}

	@Then("^User can remove the personal message from cart$")
	public void removePersonalMsgAddedInCart() throws Throwable {
		action
		.shoppingCartAction()
		.removePersonalMessage();
	}
	
	@Then("^Item added to cart is displayed in Basket$")
	public void validateItemAddedIsDisplayedInCart(){
		action
		.shoppingCartAction()
		.validateItemAddedIsDisplayedInCart();
	}

}
