/*******************************************************************************
 * Infosys BDD Automation Framework provides a powerful and versatile platform to QA automation
 * @author arjun.vijay
 * Automated Test Cases in Behavior Driven Approach
 * Copyright : INFOSYS LTD
 * MiniCart button validation
 *******************************************************************************/

package tbs_stepFiles;

import org.apache.commons.codec.binary.Base32;

import com.tbs.rcom.actions.BaseAction;
import com.tbs.rcom.actions.MiniCartPageActions;
import com.tbs.rcom.actions.PLPPageAction;
import com.tbs.rcom.actions.ShoppingCartPageActions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import tbs_master.DriverFactory;


public class MiniCartStepDefinitions {

	MiniCartPageActions tbsMiniCartPageActions=new MiniCartPageActions();
	ShoppingCartPageActions shoppingCartAction=new ShoppingCartPageActions();
	PLPPageAction tbsPLPPageActions = new PLPPageAction();

	BaseAction action = new BaseAction();
	
	@When("^User hover over the minicart icon$")
	public void hoverOverMiniCartIcon() {   	
		action
		.miniCartAction()
		.hoverOverMiniCart();
	}
	
	@When("^User navigates to basket page by clicking on the minicart icon$")
	public void clickMiniCart()
	{
		action
		.miniCartAction()
		.clickOnMinicart();
	}
	
	@Then("^user is not navigated to empty basket page$")
	public void verifyUserNotNavigatedToEmptyBasketPage() {
		action
		.miniCartAction()
		.verifyUserNotNavigatedToEmptyBasket();
	}

	@Then("^MiniCart overlay is not displayed$")
	public void verify_MiniCart_overlay_is_not_displayed() throws Throwable {
		tbsMiniCartPageActions.EmptyMiniCart();
	}

	@Then("^the user can validate the number of products in cart is highlighted on the mini cart$")
	public void the_user_can_validate_the_number_of_products_in_cart_is_highlighted_on_the_mini_cart() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		throw new PendingException();
	}


	@Then("^user is able to Close the mini cart modal$")
	public void user_is_able_to_Close_the_mini_cart_modal() throws Throwable {
		tbsMiniCartPageActions.closeMiniCartModal();	    
	}

	@Then("^user is able to navigate to Basket page by clicking on View Basket button$")
	public void user_is_able_to_View_Basket() throws Throwable {	
		tbsMiniCartPageActions.ValidateBasket();

	}

	@When("^user remove all the products from cart$")
	public void user_remove_all_the_products_from_cart()
	{
		shoppingCartAction.removeAllITemsFromBasket();
	}

	@Then("^Empty cart message is displayed$")
	public void Empty_cart_message_is_displayed()
	{
		shoppingCartAction.ValidateEmptyCart();
	}

	@Then("^user is able to click on continue shopping button$")
	public void user_is_able_to_continue_shopping() throws Throwable {
		
	    tbsMiniCartPageActions.continueShopFromMiniCart();

	}
	
	@Then("^User is able to validate the close functionality of See Options modal$")
	public void closeSeeOptionsModal(int qty) throws Throwable {
		tbsPLPPageActions.SeeOptionsModalValidations(qty);

	}

	@Then("^User is able to add (\\d+) products to cart by adjusting quantity$")
	public void user_is_able_to_add_products_to_cart_by_adjusting_quantity(int index) throws Throwable {
		System.out.println("quantity"+index);
		for(int i=1;i<=index;++i) {
			tbsPLPPageActions.updateQtyInSeeOptionsModal(i);
		}
		
	}


	@Then ("^The Product recently added is present in Minicart overlay and Subtotal price is reflected when Minicart is hovered$")
	public void validateProductAddedIsPresentInMiniCart(){
		action
		.miniCartAction()
		.hoverOverMiniCart()
		.validateProductAddedIsPresentInMiniCart()
		.closeMiniCartModal();

	}
 

}
