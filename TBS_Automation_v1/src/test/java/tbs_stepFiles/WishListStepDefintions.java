package tbs_stepFiles;

import com.tbs.rcom.actions.BaseAction;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class WishListStepDefintions{

	BaseAction action  = new BaseAction();
	
	@Then("^The Empty Guest Wishlist is displayed$")
	public void validateEmptyGuestWishlist() {
		action.wishlistAction()
		.validateEmptyWishlistForGuestUser();
	}

    @Then("Wishlist Section is Empty for the Newly registered User$")
    public void validateEmptyWishlistPageForNewlyRegisteredUser()
    {
    	action
    	.wishlistAction()
		.validateEmptyWishlistPageForNewlyRegisteredUser();
    }
    @When("User clicks on Create Wishlist Button and Enters a wishlist name$")
    public void clickOnCreateNewWishlistBtnAndEnterName()
    {
    	action
    	.wishlistAction()
    	.clickOnCreateNewWishlistAndEnterName();
    }
    
    @And("^User click on Cancel Button in Create Wishlist modal$")
    public void clickOnCancelInCreateWishlistModal()
    {
    	action
    	.wishlistAction()
    	.clickOnCancelInCreateWishlistModal();
    }
    @And("^User clicks on Save Button in Create Wishlist Modal$")
    public void clickOnSaveInCreateWishlistModal()
    {
    	action
    	.wishlistAction()
    	.clickOnSaveInCreateWishlistModal();
    }
    @Then("^New wishlist is created$")
    public void validateNewWishListIsCreated()
    {
    	action
    	.wishlistAction()
    	.validateNewWishListIsCreated();
    }
    @And("^Newly created wishlist is empty$")
    public void validateEmptyWishList()
    {
    	action
    	.wishlistAction()
    	.validateNewWishListEmpty();
    }

	
	@Then("^User adds a product to new Wishlist from PLP$")
	public void addProductToWishlistFromPLP () {
		action
		.wishlistAction()
		.addProductFromPLPToNewWishlist();
		
	}
	
	
	@Then("^User adds a product to existing Wishlist from PLP$")
	public void addProductToExistingWishlistFromPLP () {
		action.wishlistAction().addProductFromPLPToExistingWishlist();
		
	}
	
	@Then("^User navigates to the wishlist$")
	public void navigateToWishlist() {
		action.wishlistAction().navigateToWishlistPage();
		
	}
	@Then("^User validate whether the Product is added to wishlist$")
	public void validateProductAddedToWishlist() {
		action.wishlistAction().validateTheProductAddedToWishlist();
		
	}
	
	@Then("^User adds a product to existing Wishlist from PDP$")
	public void addProductToExistingWishlistFromPDP() {
		action.wishlistAction().addProductFromPDPToExistingWishlist();
		
	}
	
	@Then("^User deletes the wishlist$")
	public void deleteWishlist() {
		action.wishlistAction().deleteExistingWishlist();
		
	}
	
	@Then("^User adds a product to new Wishlist from PDP$")
	public void addProductToNewWishlistFromPDP() {
		action.wishlistAction().addProductFromPDPToNewWishlist();
		
	}

}
